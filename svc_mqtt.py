from datetime import timedelta
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from config.config import CONMQTT, ENVIRONMENT, REDATA
from fastapi.middleware.cors import CORSMiddleware
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from util.util_waktu import convertStrDateTimeToDateTime, dateTimeNow, dateTimeNowStr
import requests
from config import config

app = FastAPI(openapi_url="/svc_mqtt/openapi.json", docs_url="/svc_mqtt/swgr")
app.mount("/svc_mqtt/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-service-mqtt',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}

mqtt = CONMQTT 
mqtt.init_app(app)

@mqtt.on_connect()
def connect(client, flags, rc, properties):
    mqtt.client.subscribe("/broker/messages/katalis-server") #subscribing mqtt topic
    print("Connected: ", client, flags, rc, properties)

@mqtt.on_message()
async def message(client, topic, payload, qos, properties):
    print("Received message: ",topic, payload.decode(), qos, properties)
    #deviceId                #20210122130001#0#8347F873HF#ping #0#0#0
    #5f9aab0377f5e41862415211#20210122130001#0#8347F873HF#check#0#0#0
    pesan = payload.decode()
    arPesan = pesan.split('#')
    if arPesan[1] == "ping":
        mqtt.publish("/broker/messages/"+arPesan[0], "pong")
    else:
        # simpan json data nfcid terakhir yang dikirimkan
        jPesan = {"datetime": dateTimeNowStr(), "nfcid": arPesan[3]}
        REDATA.hmset("mqtt-"+arPesan[0], jPesan)

        url = "https://api.dev.katalis.info/katalis/access/device/mqtt"
        if ENVIRONMENT == 'production':
            url = "https://api.katalis.info/katalis/access/device/mqtt" 
            
        request = {"message":payload.decode()}
        
        res = requests.post(url, json=request ,verify=True)
        print(res.text)
        msg = res.text
        arMsg = msg.split('#')
        deviceId = arMsg[0]
        mqtt.publish("/broker/messages/"+deviceId, msg) #publishing mqtt topic /broker/messages/5fbb7c5df17f2d719dccca79

@app.post("/svc_mqtt/publish")
async def publish(deviceid:str,message:str):
    mqtt.publish("/broker/messages/"+deviceid, message) #publishing mqtt topic /broker/messages/5fbb7c5df17f2d719dccca79
    return {"result": True,"message":"Published" }

@app.post("/svc_mqtt/sendtopic")
async def sendtopic(topic:str,message:str):
    mqtt.publish(topic, message) #publishing mqtt topic /broker/messages/5fbb7c5df17f2d719dccca79
    return {"result": True,"message":"Published" }

@app.get("/svc_mqtt/get_nfcid_by_deviceid/{deviceid}")
async def get_nfcid_by_deviceid(deviceid:str):
    
    cData = REDATA.hgetall("mqtt-"+deviceid)
    jData = {}
    for k,v in cData.items():
        jData[k.decode('utf-8')] = v.decode('utf-8')
        
    print(jData)
    waktuRedis = convertStrDateTimeToDateTime(jData["datetime"]) + timedelta(seconds=40)
    if waktuRedis >= dateTimeNow():
        nfcid = jData["nfcid"]
    else:
        nfcid = ""
    return {"nfcid":nfcid}

@mqtt.on_disconnect()
def disconnect(client, packet, exc=None):
    print("Disconnected")

@mqtt.on_subscribe()
def subscribe(client, mid, qos, properties):
    print("subscribed", client, mid, qos, properties)