from io import StringIO
from io import BytesIO
from docx.shared import Cm
from docxtpl import DocxTemplate, InlineImage
import os

def save_template(template, context, filename):    
    template = DocxTemplate(template)
    template.render(context)
    template.save("./images-upload-erp/"+filename+".docx")
    print('proses ubah docx jadi pdf')
    os.chdir('./images-upload-erp')
    os.system("soffice --headless --convert-to pdf "+filename+".docx")
    os.chdir('..')
    return {"ok":"ok"}

def save_parameter(template, parameter, filename):    
    template = DocxTemplate(template)
    parameter['parameter']['number'] = parameter['number']
    template.render(parameter['parameter'])
    template.save("./images-upload-erp/"+filename+".docx")
    print('proses ubah docx jadi pdf')
    os.chdir('./images-upload-erp')
    os.system("soffice --headless --convert-to pdf "+filename+".docx")
    os.chdir('..')
    return {"ok":"ok"}
