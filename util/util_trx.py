from fastapi.exceptions import HTTPException
from function.inquiry import GetInquiryAndUpdateTrue
from sql.config import TDK, COABANK
from sql.model import TransactionBase, TransactionInput
from config.config import URL_SQL
import json
import requests

from sql.transaction import create_transaction


async def cek_limit_trx(userId: str):
    url = URL_SQL + "/svc_sql/user/ceks_limit/" + userId
    x = requests.get(url)
    if x.status_code != 200:
        raise HTTPException(status_code=x.status_code, detail=x.text)
    print(x)
    print(str(x.json()))
    return x.json()


async def transaksi_universal(
    code: str,
    companyId: str,
    debtUserId: str,
    creditUserId: str,
    trxName: str,
    amount: int,
    channel: str,
    noreff: str,
    bankName: str = "",
    kodeCoa: str = "",
):
    if code != "direct-nocode":
        cekInq = await GetInquiryAndUpdateTrue(companyId, code)
        # TODO dicocokan nominal dan pelaku transaksi

    if trxName in TDK:
        conf = TDK[trxName]
        reqTrx = TransactionInput()
        reqTrx.companyId = companyId
        reqTrx.name = conf["name"]
        reqTrx.channel = channel
        reqTrx.category = conf["category"]
        reqTrx.noreff = noreff
        reqTrx.amount = amount

        reqTrx.message = conf["message"]
        # DEBT
        reqTrx.debtUserId = debtUserId
        if conf["debt"] == "COABANK":
            reqTrx.debtCoa = COABANK[bankName]
            reqTrx.message = bankName
        elif conf["debt"] == "KODECOA":
            reqTrx.debtCoa = kodeCoa
        else:
            reqTrx.debtCoa = conf["debt"]

        # CREDIT
        reqTrx.creditUserId = creditUserId
        if conf["credit"] == "COABANK":
            reqTrx.creditCoa = COABANK[bankName]
            reqTrx.message = bankName
        elif conf["credit"] == "KODECOA":
            reqTrx.creditCoa = kodeCoa
        else:
            reqTrx.creditCoa = conf["credit"]

        # url = URL_SQL + "/svc_sql/trx/create_transaction"
        # jTrx = json.dumps(reqTrx.dict())
        # x = requests.post(url, jTrx)
        # if x.status_code != 200:
        #     resp = x.json()
        #     raise HTTPException(status_code=x.status_code, detail=resp["detail"])
        # print(x)
        # print(str(x.json()))
        # return x.json()
        # jTrx = json.dumps(reqTrx.dict())

        jTrx = TransactionBase(**reqTrx.dict())
        return create_transaction(jTrx)
    else:
        raise HTTPException(status_code=404, detail="Config Transaksi Belum Ada")
