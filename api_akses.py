from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config

from route.auth import router_auth
from route.company import router_company
from route.akses.pengguna import router_pengguna
from route.akses.device import router_device
from route.akses.kartu import router_card
from route.akses.ruang import router_ruang
from route.akses.shift import router_shift
from route.akses.akses import router_akses
from route.akses.absensi_h2h import absensi_h2h


app = FastAPI(openapi_url="/api_akses/openapi.json", docs_url="/api_akses/swgr")
app.mount("/api_akses/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-akses',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}

app.include_router(router_auth,prefix="/api_default/auth",tags=["auth"],responses={404: {"description": "Not found"}})

# app.include_router(router_company,prefix="/api_default/company",tags=["company"],responses={404: {"description": "Not found"}})
app.include_router(router_pengguna,prefix="/api_akses/pengguna",tags=["pengguna"],responses={404: {"description": "Not found"}})
app.include_router(router_device,prefix="/api_akses/device",tags=["device"],responses={404: {"description": "Not found"}})
app.include_router(router_card,prefix="/api_akses/kartu",tags=["kartu"],responses={404: {"description": "Not found"}})
app.include_router(router_ruang,prefix="/api_akses/ruang",tags=["ruang"],responses={404: {"description": "Not found"}})
app.include_router(router_akses,prefix="/api_akses/akses",tags=["akses"],responses={404: {"description": "Not found"}})
app.include_router(router_shift,prefix="/api_akses/shift",tags=["shift"],responses={404: {"description": "Not found"}})
app.include_router(absensi_h2h,prefix="/api_akses/absensi",tags=["absensi"],responses={404: {"description": "Not found"}})

@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()