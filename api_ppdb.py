from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from route.auth import router_auth
from route.ppdb.pendaftar import router_pendaftar
from route.ppdb.ppdbconfig import router_ppdb_config
from route.ppdb.sekolah import router_sekolah

tags_metadata = [
    {
        "name": "auth",
        "description": "Endpoint yang berkaitan dengan autentikasi. Termasuk di dalamnya reset data, login, logout, dan cek token.",
    },
    {
        "name": "pendaftar",
        "description": "Endpoint yang berkaitan dengan manajemen pendaftar PPDB.",
    },
    {
        "name": "sekolah",
        "description": "Endpoint yang berkaitan dengan manajemen sekolah PPDB.",
    },
    {
        "name": "config ppdb",
        "description": "Endpoints untuk manajemen data2 config ppdb, termasuk harga, jadwal, dan setting document. Semua yang ada 'my_account' di url-nya berarti diperuntukkan bagi current_user (userId/companyId diambil secara automatis melalui token jwt). Yang lainnya, digunakan oleh superadmin, dengan userId/companyId yang didefinisikan secara manual",
    },
]

app = FastAPI(
    openapi_url="/api_ppdb/openapi.json",
    docs_url="/api_ppdb/swgr",
    openapi_tags=tags_metadata,
)
app.mount("/api_ppdb/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == "production":
    # setup Elastic APM Agent
    apm = make_apm_client(
        {
            "SERVICE_NAME": "pysolusi-ppdb",
            "SERVER_URL": "http://apm-server.logging:8200",
            "ELASTIC_APM_TRANSACTION_IGNORE_URLS": ["/health"],
            "METRICS_SETS": "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
        }
    )
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*", "*"],
    allow_credentials=True,
    allow_methods=["*", "*"],
    allow_headers=["*", "*"],
)


@app.get("/health")
async def health():
    return {"status": "ok"}


app.include_router(
    router_auth,
    prefix="/api_default/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_sekolah,
    prefix="/api_ppdb/sekolah",
    tags=["sekolah"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_pendaftar,
    prefix="/api_ppdb/pendaftar",
    tags=["pendaftar"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_ppdb_config,
    prefix="/api_ppdb/config",
    tags=["config ppdb"],
    responses={404: {"description": "Not found"}},
)
