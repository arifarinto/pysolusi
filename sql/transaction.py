from sql.user import cek_or_create_balance
from util.util_waktu import dateTimeNow
from model.default import SolutionEnum
from fastapi import APIRouter, Path, HTTPException, Security
from pydantic.main import BaseModel
from sqlmodel import Field, Session, SQLModel, create_engine, select
from .model import (
    BalanceBase,
    StatusTrx,
    TrxCategory,
    TrxChannel,
    engine,
    TransactionBase,
)
from datetime import date, datetime

router_transaction = APIRouter()

# get_transaction_by_id get_transaction_by_ownerid cek_balance_or_create_account debt_credit payment_device trx_nfc
# topup_nfc settlement withdraw transfer_balance reverse_trx


def debt_credit(trx: TransactionBase):
    if trx.debtUserId is None or trx.creditUserId is None or trx.companyId is None:
        raise HTTPException(status_code=400, detail="Data Tidak Valid")
    if trx.amount < 0:
        raise HTTPException(
            status_code=400, detail="Nominal transaksi tidak boleh minus"
        )

    trx.createTime = datetime.utcnow()

    dBalDebt = BalanceBase()
    dBalDebt.userId = trx.debtUserId
    dBalDebt.coaCode = trx.debtCoa
    dBalDebt.companyId = trx.companyId
    cekDebt = cek_or_create_balance(dBalDebt)
    if cekDebt.tipe == "pasiva":
        trx.debtTipe = -1
    elif cekDebt.tipe == "aktiva":
        trx.debtTipe = 1

    else:
        raise HTTPException(
            status_code=400, detail="terjadi kesalahan setting data tabel coa"
        )

    debet = trx.debtTipe * trx.amount
    sisaSaldoDebt = cekDebt.BalanceBase.balance + cekDebt.BalanceBase.plafon + debet

    dBalCredt = BalanceBase()
    dBalCredt.userId = trx.creditUserId
    dBalCredt.coaCode = trx.creditCoa
    dBalCredt.companyId = trx.companyId
    cekCredt = cek_or_create_balance(dBalCredt)
    if cekCredt.tipe == "pasiva":
        trx.creditTipe = 1
    elif cekCredt.tipe == "aktiva":
        trx.creditTipe = -1
    else:
        raise HTTPException(
            status_code=400, detail="terjadi kesalahan setting data tabel coa"
        )

    kredit = trx.creditTipe * trx.amount
    sisaSaldoCredt = cekCredt.BalanceBase.balance + cekCredt.BalanceBase.plafon + kredit

    print(debet)
    print(sisaSaldoDebt)
    print(kredit)
    print(sisaSaldoCredt)

    if sisaSaldoDebt < 0 or sisaSaldoCredt < 0:
        raise HTTPException(status_code=400, detail="Saldo tidak boleh minus")

    with Session(engine) as session:
        session.add(trx)
        session.commit()
        session.refresh(trx)

    with Session(engine) as session:
        stmtDebet = select(BalanceBase).where(
            BalanceBase.userId == trx.debtUserId, BalanceBase.coaCode == trx.debtCoa
        )
        rDebt = session.exec(stmtDebet)
        debt = rDebt.one()
        debt.balance = debt.balance + debet
        debt.lastTrxId = trx.id
        debt.lastDebt = trx.amount
        debt.lastCredit = 0
        debt.lastTrxTime = dateTimeNow()
        debt.lastMessage = trx.name
        session.add(debt)

        stmtCredit = select(BalanceBase).where(
            BalanceBase.userId == trx.creditUserId, BalanceBase.coaCode == trx.creditCoa
        )
        rCredt = session.exec(stmtCredit)
        credt = rCredt.one()
        credt.balance = credt.balance + kredit
        credt.lastTrxId = trx.id
        credt.lastDebt = 0
        credt.lastCredit = trx.amount
        credt.lastTrxTime = dateTimeNow()
        credt.lastMessage = trx.name
        session.add(credt)

        session.commit()

    # lempar data ke mongodb untuk di append ke userData (async aja)
    return trx


@router_transaction.post("/create_transaction", response_model=TransactionBase)
def create_transaction(transaction: TransactionBase):
    print(transaction)
    # TODO cek duplicate transaction
    return debt_credit(transaction)


@router_transaction.post("/transaction_with_limit")
def transaction_with_limit(transaction: TransactionBase):
    # cek activeBalance di limitbase
    return transaction_with_limit(transaction)
