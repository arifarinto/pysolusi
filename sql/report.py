from fastapi import APIRouter, Path, HTTPException, Security
from sqlmodel import Field, Session, SQLModel, create_engine, select, or_
from .model import MutationBase, TransactionBase, engine, CoaBase
import math

router_report = APIRouter()

# get_transactions get_transaction_by_user_uid get_transaction_by_id
# get_mutations get_mutation_by_user_uid get_mutation_by_user_nfcid get_mutation_by_id


@router_report.post("/coa")
def create_coa(coa: CoaBase):
    with Session(engine) as session:
        session.add(coa)
        session.commit()
        session.refresh(coa)
        return coa


@router_report.get("/coa")
def read_coas():
    with Session(engine) as session:
        coas = session.exec(select(CoaBase)).all()
        return coas


@router_report.get("/mutation_by_userid/{userUid}", response_model=dict)
async def mutation_by_userid(
    userUid: str,
    size: int = 10,
    page: int = 0,
    sort: str = "createTime",
    sortType: int = 1,
):
    skip = page * size
    with Session(engine) as session:
        statement = (
            select(MutationBase)
            .where(MutationBase.userId == userUid)
            .offset(skip)
            .limit(size)
        )
        results = session.exec(statement)
        mutations = results.all()
        totalElements = len(
            session.exec(
                select(MutationBase).where(MutationBase.userId == userUid)
            ).all()
        )
        totalPages = math.ceil(totalElements / size)
    return {
        "status": 200,
        "content": mutations,
        "size": size,
        "page": page,
        "totalElements": totalElements,
        "totalPages": totalPages,
        "sort": sort,
        "sortDirection": sortType,
    }


@router_report.get("/trx_by_userid/{userUid}", response_model=dict)
async def trx_by_userid(
    userUid: str,
    size: int = 10,
    page: int = 0,
    sort: str = "createTime",
    sortType: int = 1,
):
    skip = page * size
    with Session(engine) as session:
        statement = (
            select(TransactionBase)
            .where(
                or_(
                    TransactionBase.debtUserId == userUid,
                    TransactionBase.creditUserId == userUid,
                )
            )
            .offset(skip)
            .limit(size)
        )
        results = session.exec(statement)
        mutations = results.all()
        totalElements = len(
            session.exec(
                select(TransactionBase).where(
                    or_(
                        TransactionBase.debtUserId == userUid,
                        TransactionBase.creditUserId == userUid,
                    )
                )
            ).all()
        )
        totalPages = math.ceil(totalElements / size)
    return {
        "status": 200,
        "content": mutations,
        "size": size,
        "page": page,
        "totalElements": totalElements,
        "totalPages": totalPages,
        "sort": sort,
        "sortDirection": sortType,
    }
