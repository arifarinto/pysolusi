import os
from definitions import PROJECT_ROOT_PATH
from fastapi import APIRouter, Path, HTTPException, Security
from sqlmodel import Field, Session, SQLModel, create_engine, select
from .model import CoaCategoryBase, engine, CoaBase
import pandas as pd

router_coa = APIRouter()


@router_coa.get("/init")
def init():
    with Session(engine) as session:
        createTrigger = "CREATE TRIGGER after_account_balance_update AFTER UPDATE ON balancebase FOR EACH ROW BEGIN IF OLD.balance <> NEW.balance THEN INSERT INTO mutationbase (companyId, createTime, userId, uid, trxId, debt, credit, balance, coaCode, message) VALUES (OLD.companyId,NEW.lastTrxTime, OLD.userId, OLD.uid, NEW.lastTrxId, NEW.lastDebt, NEW.lastCredit, NEW.balance,OLD.coaCode,NEW.lastMessage);END IF; END;"
        session.execute(statement=createTrigger)
        return {"ok": "ok"}


@router_coa.post("/coa")
def create_coa(coa: CoaBase):
    with Session(engine) as session:
        session.add(coa)
        session.commit()
        session.refresh(coa)
        return coa


@router_coa.get("/coa")
def read_coas():
    with Session(engine) as session:
        coas = session.exec(select(CoaBase)).all()
        return coas


@router_coa.put("/setup_coa_category_base_from_csv")
def setup_coa_category_base_from_coa_csv():

    # TODO:
    # 1. delete / truncate tabel setiap kali method ini dipanggil

    path_to_csv = os.path.join(PROJECT_ROOT_PATH, "sql", "category_coa.csv")
    df = pd.read_csv(path_to_csv)
    for index, row in df.iterrows():
        coa = CoaCategoryBase()
        coa.name = row["name"]
        with Session(engine) as session:
            session.add(coa)
            session.commit()
            session.refresh(coa)
    return "ok"


@router_coa.put("/setup_coa_base_from_csv")
def setup_coa_base_from_coa_csv():

    # TODO:
    # 1. delete / truncate tabel setiap kali method ini dipanggil
    # 2. Handle kondisi kalau misalnya tabel coa category base nggak ada
    # 3. handle kondisi kalau idCoaCategory, foreign key, nggak ada di tabel CoaCategoryBase

    path_to_csv = os.path.join(PROJECT_ROOT_PATH, "sql", "coa.csv")
    # PROJECT_ROOT_PATH+"/"
    df = pd.read_csv(path_to_csv)
    print(df)
    for index, row in df.iterrows():
        coa = CoaBase()
        coa.code = row["code"]
        coa.name = row["name"]
        coa.tipe = row["tipe"]
        coa.idCoaCategory = row["idCoaCategory"]
        with Session(engine) as session:
            session.add(coa)
            session.commit()
            session.refresh(coa)
    return "ok"
    # return coa
