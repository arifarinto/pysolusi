from datetime import datetime
from os import stat
from fastapi import APIRouter, Path, HTTPException, Security
from typing import List
from starlette.status import *
from sqlalchemy import select, and_, text, String, asc, desc

from model.trx.transaction import TransactionDB
from model.util import SearchRequest
from model.auth import TokenData
from route.auth import get_current_user
from config.dbmysql import database, tbl_transaction

router_data_trx = APIRouter()

async def get_transaction_by_id(id: int):
    query = tbl_transaction.select().where(tbl_transaction.c.id == id)
    transaction = await database.fetch_one(query=query)
    if transaction:
        return transaction
    else:
        raise HTTPException(status_code=404, detail="Transaction not found")

# =================================================================================

@router_data_trx.post("/get_transactions", response_model=dict)
async def get_transactions(size: int = 10, page: int = 0, sort: str = "createTime", sortType : int = 1, 
    search: SearchRequest = None, current_user: TokenData = Security(get_current_user)):
    skip = page * size
    criteria = []
    condition = 'WHERE 1=1'

    if current_user.isAdmin == True or current_user.isFinance == True:
        print('admin or finance')    
    else:
        condition = "WHERE ownerUid='"+current_user.uid+"'"
        criteria.append(text("tbl_transaction.ownerUid = '"+current_user.uid+"'"))

    if search != None:
        for ii in search.search:        
            if len(ii.field) > 2:
                criteria.append(text("tbl_transaction."+ii.field+" like '"+ii.key+"%'"))
                condition = condition + " AND tbl_transaction."+ii.field+" like '"+ii.key+"%' "

        for jj in search.fix:
            if len(jj.field) > 2:
                criteria.append(text("tbl_transaction."+jj.field+" = '"+jj.key+"'"))
                condition = condition + " AND tbl_transaction."+jj.field+" = "+jj.key+" "

    direction = desc if sortType == 1 else asc

    query = (
        tbl_transaction
        .select()
        .where(and_(*criteria))
        .limit(size)
        .offset(skip)
        .order_by(direction(sort))
    )
    data = await database.fetch_all(query=query)

    statement = text("select count(id) as total from tbl_transaction "+condition)
    print(statement)
    cdata = await database.fetch_val(query=statement)

    print(cdata)
    return {"status": 200, "content": data, "size": size, "page": page, "totalElements":cdata, "sort": sort, "sortDirection": sortType}

@router_data_trx.post("/get_transaction_by_user_uid/{userUid}", response_model=dict)
async def get_transaction_by_user_uid(userUid: str, size: int = 10, page: int = 0, sort: str = "createTime", sortType : int = 1, 
    search: SearchRequest = None):
    skip = page * size
    criteria = []
    condition = "WHERE ownerUid='"+userUid+"'"
    criteria.append(text("tbl_transaction.ownerUid = '"+userUid+"'"))

    if search != None:
        for ii in search.search:        
            if len(ii.field) > 2:
                criteria.append(text("tbl_transaction."+ii.field+" like '"+ii.key+"%'"))
                condition = condition + " AND tbl_transaction."+ii.field+" like '"+ii.key+"%' "

        for jj in search.fix:
            if len(jj.field) > 2:
                criteria.append(text("tbl_transaction."+jj.field+" = '"+jj.key+"'"))
                condition = condition + " AND tbl_transaction."+jj.field+" = "+jj.key+" "

    direction = desc if sortType == 1 else asc

    query = (
        tbl_transaction
        .select()
        .where(and_(*criteria))
        .limit(size)
        .offset(skip)
        .order_by(direction(sort))
    )
    data = await database.fetch_all(query=query)

    statement = text("select count(id) as total from tbl_transaction "+condition)
    print(statement)
    cdata = await database.fetch_val(query=statement)

    print(cdata)
    return {"status": 200, "content": data, "size": size, "page": page, "totalElements":cdata, "sort": sort, "sortDirection": sortType}

@router_data_trx.get("/get_transaction_by_id/{id}", response_model=TransactionDB)
async def get_transaction_id(id: int = Path(..., gt=0)):
    transaction = await get_transaction_by_id(id)
    return transaction
