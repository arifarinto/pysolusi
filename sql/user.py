from typing import Optional
from sqlalchemy.sql.expression import desc
from starlette import requests
from util.util_waktu import dateTimeNow
from fastapi import APIRouter, Path, HTTPException, Security
from sqlmodel import Field, Session, SQLModel, create_engine, select, or_
from .model import (
    CoaBase,
    MutationBase,
    TransactionBase,
    engine,
    BalanceBase,
    UserBase,
    LimitBase,
)
from util.util import CekNfcLen

router_user = APIRouter()


def cek_data_user(userId: str):
    print("cek data user userid: " + str(userId))
    with Session(engine) as session:
        statement = select(UserBase).where(UserBase.userId == userId)
        results = session.exec(statement)
        for user in results:
            print("ada data USER")
            return user


def cek_coa_balance(coa: str, userId: str):
    with Session(engine) as session:
        statement = (
            select(BalanceBase, CoaBase.tipe)
            .join(CoaBase)
            .where(BalanceBase.coaCode == coa, BalanceBase.userId == userId)
        )
        results = session.exec(statement)
        for balance in results:
            print("ada data BALANCE")
            return balance


def cek_data_limit(userId: str):
    with Session(engine) as session:
        statement = select(LimitBase).where(LimitBase.userId == userId)
        results = session.exec(statement)
        for limit in results:
            print("ada data LIMIT")
            return limit


@router_user.post("/cek_or_create_user")
def cek_or_create_user(user: UserBase):
    print("cek or create user")
    cData = cek_data_user(user.userId)
    if cData is not None:
        print("cData: " + str(cData))
        return cData
    else:
        print("tidak ada data USER, jadi kita buatkan yaa")
        with Session(engine) as session:
            session.add(user)
            session.commit()
            session.refresh(user)
            return user


@router_user.get("/get_users")
def get_users():
    with Session(engine) as session:
        users = session.exec(select(UserBase)).all()
        return users


@router_user.post("/cek_or_create_balance")
def cek_or_create_balance(ubalance: BalanceBase):
    cData = cek_coa_balance(ubalance.coaCode, ubalance.userId)
    if cData is not None:
        return cData
    else:
        print("tidak ada data BALANCE, jadi kita buatkan yaa")
        with Session(engine) as session:
            user = UserBase()
            user.userId = ubalance.userId
            qUser = cek_or_create_user(user)
            ubalance.balance = 0
            ubalance.createTime = dateTimeNow()
            ubalance.uid = qUser.uid
            session.add(ubalance)
            session.commit()
            session.refresh(ubalance)
        with Session(engine) as session:
            statement = (
                select(BalanceBase, CoaBase.tipe)
                .join(CoaBase)
                .where(
                    BalanceBase.coaCode == ubalance.coaCode,
                    BalanceBase.userId == ubalance.userId,
                )
            )
            results = session.exec(statement)
            for balance in results:
                print("ada data BALANCE")
                return balance


@router_user.get("/ceks_all_balance/{userId}")
def ceks_all_balance(
    userId: str,
):
    with Session(engine) as session:
        statement = (
            select(BalanceBase.balance, BalanceBase.coaCode, CoaBase.name)
            .join(CoaBase)
            .where(BalanceBase.userId == userId)
        )
        results = session.exec(statement)
        # for balance in results:
        #     print("ada data")
        return results.all()


@router_user.get("/cek_main_balance/{userId}")
def cek_main_balance(userId: str):
    with Session(engine) as session:
        statement = (
            select(BalanceBase.balance, BalanceBase.coaCode, CoaBase.name)
            .join(CoaBase)
            .where(BalanceBase.userId == userId, BalanceBase.coaCode == "21-200")
        )
        results = session.exec(statement)
        print("results " + str(results))
        for balance in results:
            print("ada data")
            print("balance: " + str(balance))
            return balance
        print("something")


@router_user.get("/ceks_all_mutasi/{userId}")
def ceks_all_mutasi(userId: str, limit: int = -1):
    print("limit: " + str(limit))
    if limit >= 0:
        with Session(engine) as session:
            statement = (
                select(
                    MutationBase.message,
                    MutationBase.coaCode,
                    CoaBase.name,
                    MutationBase.createTime,
                    TransactionBase.name,
                )
                .where(MutationBase.userId == userId)
                .where(MutationBase.coaCode == CoaBase.code)
                .where(MutationBase.trxId == TransactionBase.id)
                .order_by(desc(MutationBase.createTime))
                .limit(limit)
            )
            results = session.exec(statement)
            return results.all()
    else:
        with Session(engine) as session:
            statement = (
                select(
                    MutationBase.message,
                    MutationBase.coaCode,
                    CoaBase.name,
                    MutationBase.createTime,
                    TransactionBase.name,
                )
                .where(MutationBase.userId == userId)
                .where(MutationBase.coaCode == CoaBase.code)
                .where(MutationBase.trxId == TransactionBase.id)
                .order_by(desc(MutationBase.createTime))
            )
            results = session.exec(statement)
            return results.all()


@router_user.get("/ceks_all_transaksi/{userId}")
def ceks_all_transaksi(userId: str, limit: int = -1):
    if limit >= 0:
        with Session(engine) as session:
            statement = (
                select(TransactionBase)
                .where(
                    or_(
                        TransactionBase.debtUserId == userId,
                        TransactionBase.creditUserId == userId,
                    )
                )
                .limit(limit)
            )
        results = session.exec(statement)
        return results.all()
    else:
        with Session(engine) as session:
            statement = select(TransactionBase).where(
                or_(
                    TransactionBase.debtUserId == userId,
                    TransactionBase.creditUserId == userId,
                )
            )
        results = session.exec(statement)
        return results.all()


# @router_user.get("/ceks_all_transaksi/{userId}")
# def ceks_all_transaksi(userId: str, limit: int):
#     print("limit: " + limit)
#     with Session(engine) as session:
#         statement = (
#             select(TransactionBase.message, TransactionBase.coaCode, CoaBase.name)
#             .join(CoaBase)
#             .where(MutationBase.userId == userId)
#         )
#         results = session.exec(statement)
#         return results.all()


@router_user.post("/cek_or_create_limit")
def cek_or_create_limit(limit: LimitBase):
    cData = cek_data_limit(limit.userId)
    if cData is not None:
        return cData
    else:
        print("tidak ada data LIMIT, jadi kita buatkan yaa")
        with Session(engine) as session:
            session.add(limit)
            session.commit()
            session.refresh(limit)
            return limit


@router_user.get("/ceks_limit/{userId}")
def ceks_limit(userId: str):
    with Session(engine) as session:
        statement = select(LimitBase).where(LimitBase.userId == userId)
        results = session.exec(statement)
        for limit in results:
            print("ada data")
            return limit


@router_user.post("/cek_all_user_data")
def cek_all_user_data(user: UserBase):
    qUser = cek_or_create_user(user)
    print(qUser)
    balance = BalanceBase()
    balance.companyId = user.companyId
    balance.userId = user.userId
    balance.uid = qUser.uid
    balance.coaCode = "21-200"
    cek_or_create_balance(balance)
    balance1 = BalanceBase()
    balance1.companyId = user.companyId
    balance1.userId = user.userId
    balance1.uid = qUser.uid
    balance1.coaCode = "21-201"
    cek_or_create_balance(balance1)
    balance2 = BalanceBase()
    balance2.companyId = user.companyId
    balance2.userId = user.userId
    balance2.uid = qUser.uid
    balance2.coaCode = "21-202"
    cek_or_create_balance(balance2)
    limit = LimitBase()
    limit.companyId = user.companyId
    limit.userId = user.userId
    limit.uid = qUser.uid
    cek_or_create_limit(limit)
    return qUser
