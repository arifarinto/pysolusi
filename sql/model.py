from enum import Enum
from typing import Optional
from pydantic.main import BaseModel
from sqlmodel import Field, Session, SQLModel, create_engine, select
from datetime import date, datetime
from model.default import SolutionEnum

from config.config import DATABASE_URL


class TipeCoa(str, Enum):
    aktiva = "aktiva"
    pasiva = "pasiva"


class CoaCategoryBase(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str


class CoaBase(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    code: str = None
    name: str = None
    tipe: TipeCoa = "aktiva"
    idCoaCategory: Optional[int] = Field(default=None, foreign_key="coacategorybase.id")


class UserBase(SQLModel, table=True):
    uid: Optional[int] = Field(default=None, primary_key=True)
    companyId: str = None
    userId: str = None
    name: str = None
    tags: str = None


class BalanceBase(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    createTime: datetime = None
    companyId: str = None
    uid: Optional[int] = Field(default=None, foreign_key="userbase.uid")
    userId: str = None
    coaCode: Optional[str] = Field(default=None, foreign_key="coabase.code")
    balance: int = 0
    plafon: int = 0
    lastTrxId: int = 0
    lastDebt: int = 0
    lastCredit: int = 0
    lastTrxTime: datetime = None
    lastMessage: str = None


class LimitBase(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    companyId: str = None
    uid: Optional[int] = Field(default=None, foreign_key="userbase.uid")
    userId: str = None
    noId: str = None
    activeBalance: int = 0
    limitDaily: int = 0
    limitMax: int = 0
    lastReset: datetime = None


class MutationBase(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    companyId: str = None
    createTime: datetime = None
    userId: str = None
    uid: Optional[int] = Field(default=None, foreign_key="userbase.uid")
    trxId: int = Field(default=None, foreign_key="transactionbase.id")
    debt: int = 0
    credit: int = 0
    balance: int = 0
    coaCode: Optional[str] = Field(default=None, foreign_key="coabase.code")
    message: str = None
    isPost: bool = False


class TrxChannel(str, Enum):
    mobile = "MOBILE"
    edc = "EDC"
    kasir = "KASIR"
    device = "DEVICE"
    web = "WEB"
    h2h = "H2H"
    offline = "OFFLINE"


class TrxCategory(str, Enum):
    topupcash = "TOPUP-CASH"
    topupbank = "TOPUP-BANK"
    topupbkManual = "TOPUP-BANK-MANUAL"
    loanBalance = "LOAN-BALANCE"
    loanCash = "LOAN-CASH"
    topupmerchant = "TOPUP-MERCHANT"
    withdrawcash = "WITHDRAW-CASH"
    withdrawbank = "WITHDRAW-BANK"
    settlementcash = "SETTLEMENT-VIA-CASH"
    settlementbank = "SETTLEMENT-VIA-BANK"
    transferToBank = "TRANSFER-TO-BANK"
    tap = "TAP-DEVICE"
    payment = "PAYMENT"
    qrClosed = "QRCLOSED"
    qris = "QRIS"
    ppob = "PPOB"
    reverse = "REVERSE"
    transferInternal = "TRANSFER-INTERNAL"
    invoice = "INVOICE"
    donasi = "DONASI"
    cost = "COST"
    income = "INCOME"


class StatusTrx(str, Enum):
    success = "SUCCESS"
    fail = "FAIL"
    reverse = "REVERSE"


class TransactionInput(BaseModel):
    companyId: str = None
    debtUserId: str = None
    debtCoa: str = None
    creditUserId: str = None
    creditCoa: str = None
    name: str = None
    channel: TrxChannel = "MOBILE"
    amount: int = 0
    noreff: str = None
    category: TrxCategory = "PAYMENT"
    message: str = None


class TransactionBase(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    companyId: str = None
    createTime: datetime = None
    debtUserId: str = None
    debtUid: Optional[int] = Field(default=None, foreign_key="userbase.uid")
    debtCoa: Optional[str] = Field(default=None, foreign_key="coabase.code")
    debtTipe: int = 1
    creditUserId: str = None
    creditUid: Optional[int] = Field(default=None, foreign_key="userbase.uid")
    creditCoa: Optional[str] = Field(default=None, foreign_key="coabase.code")
    creditTipe: int = 1
    name: str = None
    channel: TrxChannel = "MOBILE"
    amount: int = 0
    status: StatusTrx = "SUCCESS"
    message: str = None
    noreff: str = None
    category: TrxCategory = "PAYMENT"
    solution: SolutionEnum = None


# sqlite_file_name = "database.db"
# sqlite_url = f"sqlite:///{sqlite_file_name}"
# connect_args = {"check_same_thread": False}

# DATABASE_URL = "mysql://root:admin@localhost:3306/db_solusi"
engine = create_engine(DATABASE_URL, echo=True)


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)
