from datetime import datetime
from os import stat
from fastapi import APIRouter, Path, HTTPException, Security
from typing import List
from starlette.responses import JSONResponse
from starlette.status import *
from bson.objectid import ObjectId
from sqlalchemy import select, and_, text, String, asc, desc

from model.trx.mutation import MutationBase, MutationDB
from model.util import SearchRequest
from model.auth import TokenData
from util.util import CekNfcLen
from route.auth import get_current_user
from config.dbmysql import database, tbl_mutation

router_mutation = APIRouter()

async def get_mutation_by_id(id: int):
    query = tbl_mutation.select().where(tbl_mutation.c.id == id)
    mutation = await database.fetch_one(query=query)
    if mutation:
        return mutation
    else:
        raise HTTPException(status_code=404, detail="Mutation not found")

async def get_mutation_by_trxid(id: int):
    query = tbl_mutation.select().where(tbl_mutation.c.trxId == id)
    mutation = await database.fetch_all(query=query)
    if mutation:
        return mutation
    else:
        raise HTTPException(status_code=404, detail="Mutation not found")

async def get_mutation_by_ownerid(uid: str):
    query = tbl_mutation.select().where(tbl_mutation.c.ownerId == uid)
    mutation = await database.fetch_one(query=query)
    if mutation:
        return mutation
    else:
        raise HTTPException(status_code=404, detail="Mutation not found")

# =================================================================================


@router_mutation.post("/get_mutations", response_model=dict)
async def get_mutations(size: int = 10, page: int = 0, sort: str = "createTime", sortType : int = 1, 
    search: SearchRequest = None, current_user: TokenData = Security(get_current_user)):
    skip = page * size
    criteria = []
    condition = 'WHERE 1=1'

    if current_user.isAdmin == True or current_user.isFinance == True:
        print('admin or finance')    
    else:
        condition = "WHERE ownerUid='"+current_user.uid+"'"
        criteria.append(text("tbl_mutation.ownerUid = '"+current_user.uid+"'"))
        
    if search != None:
        for ii in search.search:        
            if len(ii.field) > 2:
                criteria.append(text("tbl_mutation."+ii.field+" like '"+ii.key+"%'"))
                condition = condition + " AND tbl_mutation."+ii.field+" like '"+ii.key+"%' "

        for jj in search.fix:
            if len(jj.field) > 2:
                criteria.append(text("tbl_mutation."+jj.field+" = '"+jj.key+"'"))
                condition = condition + " AND tbl_mutation."+jj.field+" = "+jj.key+" "

    direction = desc if sortType == 1 else asc

    query = (
        tbl_mutation
        .select()
        .where(and_(*criteria))
        .limit(size)
        .offset(skip)
        .order_by(direction(sort))
    )
    data = await database.fetch_all(query=query)

    statement = text("select count(id) as total from tbl_mutation "+condition)
    print(statement)
    cdata = await database.fetch_val(query=statement)

    print(cdata)
    return {"status": 200, "content": data, "size": size, "page": page, "totalElements":cdata, "sort": sort, "sortDirection": sortType}

@router_mutation.post("/get_mutation_by_user_uid/{userUid}", response_model=dict)
async def get_mutations_by_user_uid(userUid: str, size: int = 10, page: int = 0, sort: str = "createTime", sortType : int = 1, 
    search: SearchRequest = None):
    skip = page * size
    criteria = []
    condition = "WHERE ownerUid='"+userUid+"'"
    criteria.append(text("tbl_mutation.ownerUid = '"+userUid+"'"))
    if search != None:
        for ii in search.search:        
            if len(ii.field) > 2:
                criteria.append(text("tbl_mutation."+ii.field+" like '"+ii.key+"%'"))
                condition = condition + " AND tbl_mutation."+ii.field+" like '"+ii.key+"%' "

        for jj in search.fix:
            if len(jj.field) > 2:
                criteria.append(text("tbl_mutation."+jj.field+" = '"+jj.key+"'"))
                condition = condition + " AND tbl_mutation."+jj.field+" = "+jj.key+" "

    direction = desc if sortType == 1 else asc

    query = (
        tbl_mutation
        .select()
        .where(and_(*criteria))
        .limit(size)
        .offset(skip)
        .order_by(direction(sort))
    )
    data = await database.fetch_all(query=query)

    statement = text("select count(id) as total from tbl_mutation "+condition)
    print(statement)
    cdata = await database.fetch_val(query=statement)

    print(cdata)
    return {"status": 200, "content": data, "size": size, "page": page, "totalElements":cdata, "sort": sort, "sortDirection": sortType}


@router_mutation.post("/get_mutation_by_user_nfcid/{nfcId}", response_model=dict)
async def get_mutation_by_user_nfcid(nfcId: str, size: int = 10, page: int = 0, sort: str = "createTime", sortType : int = 1, 
    search: SearchRequest = None):
    nfcId = CekNfcLen(nfcId)
    dUser = ''
    if not dUser['id']:
        raise HTTPException(status_code=404, detail="NFC ID not found")
    skip = page * size
    criteria = []
    condition = "WHERE ownerId='"+str(dUser['id'])+"'"
    criteria.append(text("tbl_mutation.ownerId = '"+str(dUser['id'])+"'"))
    if search != None:
        for ii in search.search:        
            if len(ii.field) > 2:
                criteria.append(text("tbl_mutation."+ii.field+" like '"+ii.key+"%'"))
                condition = condition + " AND tbl_mutation."+ii.field+" like '"+ii.key+"%' "

        for jj in search.fix:
            if len(jj.field) > 2:
                criteria.append(text("tbl_mutation."+jj.field+" = '"+jj.key+"'"))
                condition = condition + " AND tbl_mutation."+jj.field+" = "+jj.key+" "

    direction = desc if sortType == 1 else asc

    query = (
        tbl_mutation
        .select()
        .where(and_(*criteria))
        .limit(size)
        .offset(skip)
        .order_by(direction(sort))
    )
    data = await database.fetch_all(query=query)

    statement = text("select count(id) as total from tbl_mutation "+condition)
    print(statement)
    cdata = await database.fetch_val(query=statement)

    print(cdata)
    return {"status": 200, "content": data, "size": size, "page": page, "totalElements":cdata, "sort": sort, "sortDirection": sortType}

@router_mutation.get("/get_mutation_by_id/{id}", response_model=MutationDB)
async def get_mutation_id(id: int = Path(..., gt=0)):
    mutation = await get_mutation_by_id(id)
    return mutation