from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from route.auth import router_auth
from route.user import router_user
from route.company import router_company
from route.notif import router_notif
from route.info import router_info
from route.berita import router_berita
from route.selfregister import router_selfregister
from route.admin import router_admin
from route.invoice import router_invoice
from route.edc import router_edc
from route.kasir import router_kasir
from route.wilayah import router_wilayah
from route.transaksi import router_transaksi
from route.donasi import router_donasi

# from route.document import router_document

app = FastAPI(openapi_url="/api_default/openapi.json", docs_url="/api_default/swgr")
app.mount("/api_default/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == "production":
    # setup Elastic APM Agent
    apm = make_apm_client(
        {
            "SERVICE_NAME": "pysolusi-default",
            "SERVER_URL": "http://apm-server.logging:8200",
            "ELASTIC_APM_TRANSACTION_IGNORE_URLS": ["/health"],
            "METRICS_SETS": "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
        }
    )

    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*", "*"],
    allow_credentials=True,
    allow_methods=["*", "*"],
    allow_headers=["*", "*"],
)


@app.get("/health")
async def health():
    return {"status": "ok"}


app.include_router(
    router_auth,
    prefix="/api_default/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_user,
    prefix="/api_default/user",
    tags=["user"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_company,
    prefix="/api_default/company",
    tags=["company"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_admin,
    prefix="/api_default/admin",
    tags=["admin"],
    responses={404: {"description": "Not found"}},
)

# app.include_router(
#     router_edc,
#     prefix="/api_default/edc",
#     tags=["edc"],
#     responses={404: {"description": "Not found"}},
# )

# app.include_router(
#     router_kasir,
#     prefix="/api_default/kasir",
#     tags=["kasir"],
#     responses={404: {"description": "Not found"}},
# )

app.include_router(
    router_notif,
    prefix="/api_default/notif",
    tags=["notif"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_info,
    prefix="/api_default/info",
    tags=["info"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_berita,
    prefix="/api_default/berita",
    tags=["berita"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_selfregister,
    prefix="/api_default/selfregister",
    tags=["self register"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_invoice,
    prefix="/api_default/invoice",
    tags=["invoice"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_wilayah,
    prefix="/api_default/wilayah",
    tags=["wilayah"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_transaksi,
    prefix="/api_default/transaksi",
    tags=["transaksi"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_donasi,
    prefix="/api_donasi/donasi",
    tags=["donasi"],
    responses={404: {"description": "Not found"}},
)

# app.include_router(
#     router_document,
#     prefix="/api_default/document",
#     tags=["document"],
#     responses={404: {"description": "Not found"}},
# )


@app.on_event("startup")
async def app_startup():
    config.load_config()


@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()
