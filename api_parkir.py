from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from route.auth import router_auth

from route.parkir.parkiran import router_parkiran
from route.parkir.gateway_parkir import router_gateway_parkir
from route.parkir.user_parkir import router_user_temp
from route.parkir.karcis import router_karcis

app = FastAPI(openapi_url="/api_parkir/openapi.json", docs_url="/api_parkir/swgr")
app.mount("/api_parkir/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-parkir',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}

app.include_router(router_auth,prefix="/api_default/auth",tags=["auth"],responses={404: {"description": "Not found"}})

app.include_router(router_parkiran,prefix="/api_parkir/parkiran",tags=["parkiran"],responses={404:{"description": "Not found"}})
app.include_router(router_user_temp,prefix="/api_parkir/user_parkir",tags=["data & user parkir"],responses={404: {"description": "Not found"}})
app.include_router(router_karcis,prefix="/api_parkir/karcis",tags=["karcis"],responses={404: {"description": "Not found"}})
app.include_router(router_gateway_parkir,prefix="/api_parkir/gateway_parkir",tags=["gateway parkir"],responses={404: {"description": "Not found"}})

@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()