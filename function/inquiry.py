from bson.objectid import ObjectId
from config.config import MGDB, CONF
from model.default import InquiryBase, JwtToken
from util.util_waktu import dateTimeNow
from util.util import RandomString
from fastapi import HTTPException
from datetime import timedelta

dbase = MGDB.tbl_inquiry


async def GetInquiryAndUpdateTrue(companyId: str, code: str):
    inquiry = await dbase.find_one({"companyId": ObjectId(companyId), "code": code})
    if inquiry:
        if inquiry["isUsed"] == False:
            await dbase.update_one(
                {"code": code},
                {"$set": {"isUsed": True}},
            )
            return inquiry
        else:
            raise HTTPException(status_code=404, detail="Kode Transaksi tidak valid")
    else:
        raise HTTPException(status_code=404, detail="Kode Transaksi tidak ditemukan")


async def GetInquiry(code: str):
    inquiry = await dbase.find_one({"code": code})
    if inquiry:
        return inquiry
    else:
        raise HTTPException(status_code=404, detail="Kode Transaksi tidak ditemukan")


async def CreateInquiry(current_user: JwtToken, userIdTujuan: str, amount: int):
    inquiry = InquiryBase()
    inquiry.companyId = ObjectId(current_user.companyId)
    inquiry.userId = ObjectId(current_user.userId)
    inquiry.userDestination = ObjectId(userIdTujuan)
    inquiry.amount = amount

    inquiry.createTime = dateTimeNow()
    inquiry.expiredTime = dateTimeNow() + timedelta(minutes=1)
    inquiry.code = RandomString(8)
    await MGDB.tbl_inquiry.insert_one(inquiry.dict())
    return inquiry
