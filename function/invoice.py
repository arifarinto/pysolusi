from model.default import JwtToken
from function.crm_katalis.client import GetClientOr404
from logging import error
from util.util import IsiDefault, ValidateObjectId
from bson.objectid import ObjectId
from config.config import MGDB
from fastapi.exceptions import HTTPException
from model.support import InvoiceBase, InvoiceData, InvoiceInput, PaymentStatusEnum

# dbase = MGDB.tbl_invoice
dbase = MGDB.user_data


async def GetInvoiceBaseOnInvoiceId(id: str):
    _id = ValidateObjectId(id)
    invoiceBase = await dbase.find_one({"invoices.id": _id})
    if invoiceBase is None:
        raise HTTPException(status_code=404, detail="Invoice tidak ditemukan")
    return invoiceBase


async def GetInvoiceOr404(id: str):
    id = ValidateObjectId(id)
    pipeline = [
        {"$unwind": {"path": "$invoicesData"}},
        {"$match": {"invoicesData.id": id}},
        {"$replaceRoot": {"newRoot": "$invoicesData"}},
    ]
    results = await dbase.aggregate(pipeline).to_list(1000)
    if results:
        return results[0]
    raise HTTPException(status_code=404, detail="Invoice tidak ditemukan")


async def GetInvoiceBaseOnMonthAndYear(month: int, year: int):
    invoiceBase = await dbase.find_one({"month": month, "year": year})
    return invoiceBase


async def GetInvoiceBaseOnTitle(title: str):
    invoiceBase = await dbase.find_one({"title": title})
    return invoiceBase


async def GetInvoiceBaseOnMonthYearTitleAndCompanyId(
    month: int, year: int, title: str, companyId: str
):
    invoiceBase = await dbase.find_one(
        {"title": title, "month": month, "year": year, "companyId": ObjectId(companyId)}
    )
    return invoiceBase


async def CreateInvoice(data: InvoiceInput, current_user: JwtToken):
    data = InvoiceData(**data.dict())
    data = IsiDefault(data, current_user)
    userId = ValidateObjectId(data.userId)
    dbase = MGDB.user_data
    user = await dbase.find_one({"userId": userId})
    if user is None:
        raise HTTPException(status_code=404, detail="User Tidak Ditemukan")
    data.id = ObjectId()
    invoiceDate = data.invoiceDate
    year = invoiceDate.year
    data.invoiceYear = year
    data.userId = ObjectId(data.userId)
    invoiceId = data.id

    if "invoicesData" in user:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$addToSet": {"invoicesData": data.dict()}}
        )
    else:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$set": {"invoicesData": [data.dict()]}}
        )

    return await GetInvoiceOr404(invoiceId)


async def GetInvoicesBasedOnUserIdAndPaymentStatus(
    userId: str, paymentStatus: PaymentStatusEnum
):
    id = ValidateObjectId(userId)
    pipeline = [
        {"$unwind": {"path": "$invoicesData"}},
        {
            "$match": {
                "invoicesData.userId": id,
                "invoicesData.paymentStatus": paymentStatus,
            }
        },
        {"$replaceRoot": {"newRoot": "$invoicesData"}},
    ]
    cursor = await dbase.aggregate(pipeline).to_list(10000)
    return cursor


async def GetInvoicesUnpaidOnUserId(id: str):
    return await GetInvoicesBasedOnUserIdAndPaymentStatus(
        userId=id, paymentStatus="unpaid"
    )


async def GetInvoicesPaidOnUserId(id: str):
    return await GetInvoicesBasedOnUserIdAndPaymentStatus(
        userId=id, paymentStatus="paid"
    )


async def GetInvoicesOnUserId(id: str):
    id = ValidateObjectId(id)
    pipeline = [
        {"$unwind": {"path": "$invoicesData"}},
        {"$match": {"userId": ObjectId(id)}},
        {"$replaceRoot": {"newRoot": "$invoicesData"}},
    ]
    cursor = await dbase.aggregate(pipeline).to_list(10000)
    return cursor


async def UpdateInvoice(id: str, data: dict):
    user = await GetUserOnInvoiceId(id)
    for i in range(len(user["invoicesData"])):
        if str(user["invoicesData"][i]["id"]) == str(id):
            user["invoicesData"][i].update(data)
            break
    try:
        await dbase.update_one(
            {"_id": user["_id"]}, {"$set": {"invoicesData": user["invoicesData"]}}
        )
    except:
        raise HTTPException(status_code=500, detail="Gagal Update Invoice")
    return await GetInvoiceOr404(id)


async def GetInvoicesByCompanyId(companyId: str):
    ValidateObjectId(companyId)
    pipeline = [
        {"$unwind": {"path": "$invoicesData"}},
        {"$replaceRoot": {"newRoot": "$invoicesData"}},
        {"$match": {"companyId": ObjectId(companyId)}},
    ]
    cursor = await dbase.aggregate(pipeline).to_list(10000)
    return cursor


async def GetInvoicesAll():
    pipeline = [
        {"$unwind": {"path": "$invoicesData"}},
        {"$replaceRoot": {"newRoot": "$invoicesData"}},
        # {"$match": {"companyId": ObjectId(companyId)}},
    ]
    cursor = await dbase.aggregate(pipeline).to_list(10000)
    return cursor


async def GetUserOnInvoiceId(id: str):
    id = ValidateObjectId(id)
    user = await dbase.find_one({"invoicesData.id": id})
    if user:
        return user
    raise HTTPException(status_code=404, detail="Invoice tidak ditemukan")


async def DeleteInvoice(id: str):
    user = await GetUserOnInvoiceId(id)
    for invoice in user["invoicesData"]:
        if str(invoice["id"]) == str(id):
            user["invoicesData"].remove(invoice)
            break
    await dbase.update_one(
        {"invoicesData.id": ObjectId(id)},
        {"$set": {"invoicesData": user["invoicesData"]}},
    )
