# from route.ppdb.pendaftar import GetPendaftarOr404
import json

import requests
from definitions import OPEN_FILE_ROOT, PROJECT_ROOT_PATH, SVC_EMAIL_ROOT
import os
from model.crm_katalis.crmkatalis import ClientCredential
from function.document import GetDocumentSettingByCompanyIdAndTitle
from route.ppdb.ppdbconfig import GetPPDBConfigOnCompanyId
from util.util_waktu import convertStrDateToDate, dateTimeNow
from model.support import (
    DocumentData,
    DocumentInput,
    InvoiceData,
    InvoiceInput,
    ParameterData,
)
from function.user import CreateUser
from bson.objectid import ObjectId
from util.util import IsiDefault, RandomNumber, ValidateObjectId
from fastapi.exceptions import HTTPException
from function.company import GetCompanyOr404
from config.config import MGDB
from typing import List
from model.default import JwtToken, RoleEnum
from model.ppdb.pendaftar import (
    PendaftarCredential,
    PendaftarInput,
    StatusPendaftaran,
    TemplateProsesBase,
)
import re

dbase = MGDB.user_crmppdb_pendaftar


async def GetPendaftarOr404(id: str):
    _id = ValidateObjectId(id)
    pendaftar = await dbase.find_one({"userId": _id})
    if pendaftar:
        return pendaftar
    else:
        raise HTTPException(status_code=404, detail="Pendaftar not found")


async def GetPendaftarUnverifiedOr404(id: str):
    _id = ValidateObjectId(id)
    pendaftar = await dbase.find_one({"userId": _id, "isVerified": False})
    if pendaftar:
        return pendaftar
    else:
        raise HTTPException(
            status_code=404, detail="Pendaftar tidak ditemukan atau sudah terverifikasi"
        )


async def GetAllPendaftar():
    pendaftars = await dbase.find().to_list(1000)
    return pendaftars


async def GetPendaftarByCompanyId(companyId: str):
    companyId = ValidateObjectId(companyId)
    pendaftars = await dbase.find({"companyId": companyId}).to_list(1000)
    return pendaftars


async def SetPendaftarVerified(userId: str, status: StatusPendaftaran):
    update_op = await dbase.update_one(
        {"userId": ObjectId(userId)},
        {"$set": {"isVerified": True, "status": status}},
    )
    if update_op.modified_count:
        pass
    else:
        raise HTTPException(status_code=304)


async def GetInvoicePPDBOr404(id: str):
    id = ValidateObjectId(id)
    pipeline = [
        {"$unwind": {"path": "$invoicesData"}},
        {"$match": {"invoicesData.id": id}},
        {"$replaceRoot": {"newRoot": "$invoicesData"}},
    ]
    dbase = MGDB.user_crmppdb_pendaftar
    results = await dbase.aggregate(pipeline).to_list(1000)
    if results:
        return results[0]
    raise HTTPException(status_code=404, detail="Invoice tidak ditemukan")


async def GetInvoicesPPDBByUserId(userId: str):
    userId = ValidateObjectId(userId)
    pendaftar = await GetPendaftarOr404(userId)
    invoices = pendaftar["invoicesData"]
    return invoices


async def GetInvoicesPPDBByCompanyId(companyId: str):
    await GetCompanyOr404(companyId)
    companyId = ValidateObjectId(companyId)
    pipeline = [
        {"$unwind": {"path": "$invoicesData"}},
        {"$match": {"invoicesData.companyId": companyId}},
        {"$replaceRoot": {"newRoot": "$invoicesData"}},
    ]
    dbase = MGDB.user_crmppdb_pendaftar
    results = await dbase.aggregate(pipeline).to_list(1000)
    if results:
        return results
    else:
        return []


async def CreateInvoicePPDB(data: InvoiceInput, current_user: JwtToken):
    data = InvoiceData(**data.dict())
    data = IsiDefault(data, current_user)
    userId = ValidateObjectId(data.userId)
    dbase = MGDB.user_crmppdb_pendaftar  #
    user = await dbase.find_one({"userId": userId})
    if user is None:
        raise HTTPException(status_code=404, detail="User Tidak Ditemukan")
    data.id = ObjectId()
    invoiceDate = data.invoiceDate
    year = invoiceDate.year
    data.invoiceYear = year
    data.userId = ObjectId(data.userId)
    invoiceId = data.id

    if "invoicesData" in user:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$addToSet": {"invoicesData": data.dict()}}
        )
    else:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$set": {"invoicesData": [data.dict()]}}
        )

    # send to email
    linkInvoice = {
        "link_invoice": OPEN_FILE_ROOT + "/checkout_invoice_ppdb/" + str(invoiceId)
    }
    linkInvoice = json.dumps(linkInvoice, indent=4)
    # user = await GetUserOr404(invoice["userId"])
    user = await GetPendaftarOr404(str(data.userId))
    userEmail = user["email"]
    userEmail = "ristiriantoadi@gmail.com"
    params = {
        "emailTo": userEmail,
        "subject": "Invoice " + data.title,
        "templateName": "contoh_template_invoice.html",
        "environment": linkInvoice,
    }
    url = SVC_EMAIL_ROOT + "/kirim_email_standard"
    response = requests.post(url, headers={"Accept": "application/json"}, params=params)

    return await GetInvoicePPDBOr404(invoiceId)


async def GetUserPPDBOnInvoiceId(id: str):
    id = ValidateObjectId(id)
    user = await dbase.find_one({"invoicesData.id": id})
    if user:
        return user
    raise HTTPException(status_code=404, detail="Invoice tidak ditemukan")


async def UpdateInvoicePPDB(idInvoice: str, data: dict):
    user = await GetUserPPDBOnInvoiceId(idInvoice)
    for i in range(len(user["invoicesData"])):
        if str(user["invoicesData"][i]["id"]) == str(idInvoice):
            user["invoicesData"][i].update(data)
            break
    try:
        await dbase.update_one(
            {"_id": user["_id"]}, {"$set": {"invoicesData": user["invoicesData"]}}
        )
    except:
        raise HTTPException(status_code=500, detail="Gagal Update Invoice")
    return await GetInvoicePPDBOr404(idInvoice)


async def DeleteInvoicePPDB(id: str):
    user = await GetUserPPDBOnInvoiceId(id)
    for invoice in user["invoicesData"]:
        if str(invoice["id"]) == str(id):
            user["invoicesData"].remove(invoice)
            break
    await dbase.update_one(
        {"invoicesData.id": ObjectId(id)},
        {"$set": {"invoicesData": user["invoicesData"]}},
    )


async def CreatePendaftar(
    tblName: str,
    user: PendaftarCredential,
    isFirstLogin: bool,
    role: List[RoleEnum],
    current_user: JwtToken,
    password: str = "pass",
):
    dbase = MGDB[tblName]

    companyId = str(user.companyId)

    dCompany = await GetCompanyOr404(companyId)
    print("company: " + str(dCompany))

    # cek apakah company sudah memiliki solusi sesuai dengan tblName
    arTblName = tblName.split("_")
    if arTblName[1] not in dCompany["solution"]:
        raise HTTPException(
            status_code=400,
            detail="Company yang dipilih belum memiliki solusi " + arTblName[1],
        )

    # companyId = user.companyId

    # cek apakah company sudah memiliki config PPDB
    ppdbConfig = await GetPPDBConfigOnCompanyId(companyId)

    # cek apa company sudah memiliki document setting untuk ppdb
    await GetDocumentSettingByCompanyIdAndTitle(companyId, "PPDB Diterima")
    await GetDocumentSettingByCompanyIdAndTitle(companyId, "PPDB Menunggu")
    await GetDocumentSettingByCompanyIdAndTitle(companyId, "PPDB Ditolak")

    # setup username dan userId
    firstname_split = user.name.split(" ")
    firstname = firstname_split[0].upper()
    user.username = (firstname + RandomNumber(4)).lower()
    if user.noId:
        user.username = user.noId
    user.userId = ObjectId()

    user = await CreateUser(tblName, user, password, isFirstLogin, role)

    # setup invoice
    dataInvoice = InvoiceInput()
    userId = user.userId
    dataInvoice.userId = userId
    dataInvoice.title = "Pembayaran Pendaftaran PPDB"
    dataInvoice.invoiceDate = dateTimeNow()
    dataInvoice.total = ppdbConfig["harga"]
    await CreateInvoicePPDB(dataInvoice, current_user)

    user = PendaftarCredential(**user.dict())
    user.pipelines = []

    # Opening JSON file
    path_to_pipelines_json = os.path.join(PROJECT_ROOT_PATH, "route/ppdb/pipeline.json")
    with open(path_to_pipelines_json) as json_file:
        pipelines = json.load(json_file)
        i = 1
        for pipeline in pipelines["process"]:
            pipeline = TemplateProsesBase(**pipeline)
            pipeline.nomor = i
            user.pipelines.append(pipeline.dict())
            i += 1

    await dbase.update_one(
        {"userId": ObjectId(user.userId)},
        {"$set": {"pipelines": user.pipelines}},
    )

    return await GetPendaftarOr404(userId)


async def UpdatePendaftar(userId: str, pendaftar: PendaftarInput):
    pendaftar.name = pendaftar.name.upper()
    pendaftar.identity.dateOfBirth = convertStrDateToDate(
        pendaftar.identity.dateOfBirth
    )
    await dbase.update_one({"userId": ObjectId(userId)}, {"$set": pendaftar.dict()})
    pendaftar = await GetPendaftarOr404(userId)
    return pendaftar


async def DeletePendaftar(userId: str):
    await GetPendaftarOr404(userId)
    try:
        await dbase.delete_one({"userId": ObjectId(userId)})
    except:
        raise HTTPException(status_code=500, detail="Gagal hapus pendaftar")


async def GetDocumentPPDBOr404(id: str):
    id = ValidateObjectId(id)
    pipeline = [
        {"$unwind": {"path": "$documents"}},
        {"$match": {"documents.id": id}},
        {"$replaceRoot": {"newRoot": "$documents"}},
    ]
    dbase = MGDB.user_crmppdb_pendaftar
    results = await dbase.aggregate(pipeline).to_list(1000)
    if results:
        return results[0]
    raise HTTPException(status_code=404, detail="Document tidak ditemukan")


async def GetDocumentPPDBByUserId(userId: str):
    userId = ValidateObjectId(userId)
    pendaftar = await GetPendaftarOr404(userId)
    documents = pendaftar["documents"]
    return documents


async def GetDocumentPPDBByCompanyId(companyId: str):
    await GetCompanyOr404(companyId)
    companyId = ValidateObjectId(companyId)
    pipeline = [
        {"$unwind": {"path": "$documents"}},
        {"$match": {"documents.companyId": companyId}},
        {"$replaceRoot": {"newRoot": "$documents"}},
    ]
    dbase = MGDB.user_crmppdb_pendaftar
    results = await dbase.aggregate(pipeline).to_list(1000)
    if results:
        return results
    else:
        return []


async def CreateDocumentPPDB(doc: DocumentInput, current_user: JwtToken):
    data = DocumentData(**doc.dict())
    data = IsiDefault(data, current_user)
    userId = ValidateObjectId(data.userId)
    dbase = MGDB.user_crmppdb_pendaftar  #
    user = await dbase.find_one({"userId": userId})
    if user is None:
        raise HTTPException(status_code=404, detail="User Tidak Ditemukan")
    data.id = ObjectId()
    data.userId = ObjectId(data.userId)
    docId = str(data.id)

    if "documents" in user:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$addToSet": {"documents": data.dict()}}
        )
    else:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$set": {"documents": [data.dict()]}}
        )

    # send email
    linkDocument = {
        "linkDocument": OPEN_FILE_ROOT + "/get_document_ppdb/" + docId,
        "titleDocument": data.title,
    }
    linkDocument = json.dumps(linkDocument, indent=4)
    # user = await GetUserOr404(document["userId"])
    user = await GetPendaftarOr404(str(data.userId))
    userEmail = user["email"]
    userEmail = "ristiriantoadi@gmail.com"
    params = {
        "emailTo": userEmail,
        # "subject": "Dokumen Klien",#
        "subject": data.title,
        "templateName": "template_email_dokumen_baru.html",
        "environment": linkDocument,
    }
    url = SVC_EMAIL_ROOT + "/kirim_email_standard"
    response = requests.post(url, headers={"Accept": "application/json"}, params=params)
    # return document
    return await GetDocumentPPDBOr404(docId)


async def SetupDocumentPPDBHasilSeleksi(
    userId: str, current_user: JwtToken, status: StatusPendaftaran
):
    pendaftar = await GetPendaftarUnverifiedOr404(userId)
    nama = pendaftar["name"]
    nomor = pendaftar["nomor"]

    doc = DocumentInput()
    doc.title = "HASIL SELEKSI PPDB"
    doc.userId = userId
    if status.lower() == "terima":
        docSetting = await GetDocumentSettingByCompanyIdAndTitle(
            current_user.companyId, "ppdb diterima"
        )
    elif status.lower() == "tolak":
        docSetting = await GetDocumentSettingByCompanyIdAndTitle(
            current_user.companyId, "ppdb ditolak"
        )
    else:
        docSetting = await GetDocumentSettingByCompanyIdAndTitle(
            current_user.companyId, "ppdb menunggu"
        )
    doc.docSettingId = str(docSetting["_id"])
    paramNama = ParameterData(key="nama", value=nama)
    paramNomor = ParameterData(key="nomor", value=nomor)
    doc.parameter = [paramNama, paramNomor]

    await SetPendaftarVerified(userId, status)
    return doc


async def GetDocumentHasilSeleksi(userId: str):
    id = ValidateObjectId(userId)
    title = "HASIL seleksi ppdb *"
    title = re.compile(title, re.IGNORECASE)
    pipeline = [
        {"$match": {"userId": id}},
        {"$unwind": {"path": "$documents"}},
        {"$match": {"documents.title": title}},
        {"$replaceRoot": {"newRoot": "$documents"}},
    ]
    dbase = MGDB.user_crmppdb_pendaftar
    results = await dbase.aggregate(pipeline).to_list(1000)
    if results:
        return results[0]
    raise HTTPException(
        status_code=404, detail="Document Hasil Seleksi PPDB tidak ditemukan"
    )


async def GetUserPPDBOnDocumentId(documentId: str):
    id = ValidateObjectId(documentId)
    user = await dbase.find_one({"documents.id": id})
    if user:
        return user
    raise HTTPException(status_code=404, detail="Dokumen tidak ditemukan")


async def UpdateDocumentPPDB(documentId: str, data: dict):
    user = await GetUserPPDBOnDocumentId(documentId)
    for i in range(len(user["documents"])):
        if str(user["documents"][i]["id"]) == str(documentId):
            user["documents"][i].update(data)
            break
    try:
        await dbase.update_one(
            {"_id": user["_id"]}, {"$set": {"documents": user["documents"]}}
        )
    except:
        raise HTTPException(status_code=500, detail="Gagal Update Dokumen")
    return await GetDocumentPPDBOr404(documentId)


async def DeleteDocumentPPDB(id: str):
    user = await GetUserPPDBOnDocumentId(id)
    for document in user["documents"]:
        if str(document["id"]) == str(id):
            user["documents"].remove(document)
            break
    await dbase.update_one(
        {"documents.id": ObjectId(id)}, {"$set": {"documents": user["documents"]}}
    )
