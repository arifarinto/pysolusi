import re
from function.crm_katalis.partner import GetPartnerOr404
from model.default import JwtToken, UserTipeEnum
from util.util import IsiDefault, ValidateObjectId
from config.config import MGDB
from bson.objectid import ObjectId
from model.support import DocumentBase, DocumentData, DocumentInput, DocumentSettingBase
from function.crm_katalis.client import GetClientOr404
from fastapi.exceptions import HTTPException

dbase = MGDB.tbl_document


async def GetDocumentBaseOnMonthYearTitleAndCompanyId(
    month: int, year: int, title: str, companyId: str
):
    documentBase = await dbase.find_one(
        {"title": title, "month": month, "year": year, "companyId": ObjectId(companyId)}
    )
    return documentBase


async def GetDocumentBaseOnDocumentId(id: str):
    _id = ValidateObjectId(id)
    documentBase = await dbase.find_one({"documents.id": _id})
    if documentBase is None:
        raise HTTPException(status_code=404, detail="Document tidak ditemukan")
    return documentBase


async def GetAllDocuments():
    # documents = []  #
    # async for document in dbase.find():
    #     documents.append(document)
    pipeline = [
        {"$unwind": {"path": "$documents"}},
        {"$replaceRoot": {"newRoot": "$documents"}},
    ]
    # return documents
    cursor = await dbase.aggregate(pipeline).to_list(10000)
    return cursor


async def GetDocumentOr404(id: str):
    documentBase = await GetDocumentBaseOnDocumentId(id)
    if documentBase:
        for document in documentBase["documents"]:
            if str(document["id"]) == str(id):
                return document
    raise HTTPException(status_code=404, detail="Document tidak ditemukan")


async def CreateDocument(data: DocumentInput, current_user: JwtToken):
    data = DocumentData(**data.dict())

    # search user di semua tabel, satu per satu, sampai ketemu
    userId = ValidateObjectId(data.userId)
    user = None
    if user is None:
        user = await MGDB.user_crmkatalis_client.find_one({"userId": userId})
    if user is None:
        user = await MGDB.user_crmkatalis_partner.find_one({"userId": userId})

    # untuk user2 yang lain, tambahkan di sini

    if user is None:
        raise HTTPException(status_code=404, detail="User Tidak Ditemukan")

    # parse data date, ambil bulan dan tahun
    # ambil data title, assign jadi title DocumentBase
    documentDate = data.date
    month = documentDate.month
    year = documentDate.year
    titleDocumentBase = data.title
    companyId = current_user.companyId

    # setup documentData id, userId, year dan title
    data.id = ObjectId()
    # data.year = year
    data.title = titleDocumentBase + "-" + user["username"]
    data.userId = ObjectId(data.userId)

    # cek apakah entri DocumentBase dengan bulan dan tahun yang sedang dicari sudah ada atau belum
    # jika ada, insert DocumentData ke dalam array documents
    # jika tidak ada, buat entri DocumentBase baru
    # documentBase = await GetDocumentBaseOnMonthAndYear(month, year)
    documentBase = await GetDocumentBaseOnMonthYearTitleAndCompanyId(
        month, year, titleDocumentBase, companyId
    )
    if documentBase:
        try:
            documents = documentBase["documents"]
            documents.append(data.dict())
            await dbase.update_one(
                {"_id": documentBase["_id"]}, {"$addToSet": {"documents": data.dict()}}
            )
        except Exception as e:
            print("Error: " + str(e))
            raise HTTPException(status_code=500, detail="Gagal Tambah Dokumen")
        return await GetDocumentOr404(str(data.id))
    else:
        documentBase = DocumentBase(month=month, year=year)

        # setup documentBase companyId dan title
        # documentBase.companyId = ObjectId(companyId)
        documentBase.title = titleDocumentBase

        documentBase = IsiDefault(documentBase, current_user)

        documentBase = documentBase.dict()
        documentBase["documents"].append(data.dict())
        op = await dbase.insert_one(documentBase)
        if op:
            return await GetDocumentOr404(str(data.id))
    raise HTTPException(status_code=500, detail="Gagal Tambah Document")


async def GetDocumentsByCompanyId(companyId: str):
    ValidateObjectId(companyId)
    pipeline = [
        {"$match": {"companyId": ObjectId(companyId)}},
        {"$unwind": {"path": "$documents"}},
        {"$replaceRoot": {"newRoot": "$documents"}},
    ]
    cursor = await dbase.aggregate(pipeline).to_list(10000)
    return cursor


async def UpdateDocument(id: str, data: dict):
    documentBase = await GetDocumentBaseOnDocumentId(id)
    for i in range(len(documentBase["documents"])):
        if str(documentBase["documents"][i]["id"]) == str(id):
            documentBase["documents"][i].update(data)
            break
    try:
        await dbase.update_one(
            {"_id": documentBase["_id"]},
            {"$set": {"documents": documentBase["documents"]}},
        )
    except:
        raise HTTPException(status_code=500, detail="Gagal Update Document")
    return await GetDocumentOr404(id)


async def GetDocumentsOnUserId(id: str):
    id = ValidateObjectId(id)
    pipeline = [
        {"$unwind": {"path": "$documents"}},
        {"$match": {"documents.userId": id}},
        {"$replaceRoot": {"newRoot": "$documents"}},
    ]
    cursor = await dbase.aggregate(pipeline).to_list(10000)
    return cursor


async def DeleteDocument(id: str):
    documentBase = await GetDocumentBaseOnDocumentId(id)
    for document in documentBase["documents"]:
        if str(document["id"]) == str(id):
            documentBase["documents"].remove(document)
            break
    await dbase.update_one(
        {"documents.id": ObjectId(id)},
        {"$set": {"documents": documentBase["documents"]}},
    )


# === Document Setting ===


async def GetDocumentSettingOr404(id: str):
    _id = ValidateObjectId(id)
    dbase = MGDB.tbl_document_setting
    docSetting = await dbase.find_one({"_id": _id})
    if docSetting:
        return docSetting
    else:
        raise HTTPException(status_code=404, detail="Document Setting tidak ditemukan")


async def GetDocSettingCompanyIdTitle(companyId: str, title: str):
    companyId = ValidateObjectId(companyId)
    dbase = MGDB.tbl_document_setting
    title = re.compile(title, re.IGNORECASE)
    docSetting = await dbase.find_one(
        {"companyId": companyId, "documentSettingName": {"$regex": title}}
    )
    return docSetting


async def GetDocumentSettingByCompanyIdAndTitle(companyId: str, title: str):
    docSetting = await GetDocSettingCompanyIdTitle(companyId, title)
    if docSetting:
        return docSetting
    else:
        raise HTTPException(
            status_code=404, detail="Document Setting " + title + " tidak ditemukan"
        )


async def GetDocumentSettingByCompanyId(id: str):
    _id = ValidateObjectId(id)
    dbase = MGDB.tbl_document_setting
    docSettings = await dbase.find({"companyId": _id}).to_list(1000)
    if docSettings:
        return docSettings
    else:
        raise HTTPException(status_code=404, detail="Document Setting tidak ditemukan")


async def UpdateDocumentSetting(docSettingId: str, data: dict):
    docSettingId = ValidateObjectId(docSettingId)
    dbase = MGDB.tbl_document_setting
    await dbase.update_one(
        {"_id": docSettingId},
        {"$set": data},
    )
    return await GetDocumentSettingOr404(docSettingId)


async def AddDocumentSetting(docSetting: DocumentSettingBase):
    dbase = MGDB.tbl_document_setting
    op = await dbase.insert_one(docSetting.dict())
    if op.inserted_id:
        docSetting = await dbase.find_one({"_id": ObjectId(op.inserted_id)})
        return docSetting
    else:
        raise HTTPException(status_code=500, detail="Gagal Tambah Document Setting")
