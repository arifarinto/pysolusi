from function.user import CreateUser
from function.notif import CreateNotif
from model.util import EmailData
from util.util_waktu import convertStrDateToDate, dateTimeNow
from function.company import GetCompanyOr404
from typing import List
from model.default import CredentialData, JwtToken, NotifData, RoleEnum, UserDataBase
from bson.objectid import ObjectId
from function.crm_katalis.client import GetClientBasedOnCompanyId
from config.config import MGDB, CONF, MGDB_KATALIS, PROJECT_NAME
from util.util import (
    IsiDefault,
    ListToUp,
    RandomString,
    ValidPhoneNumber,
    ValidateEmail,
    ValidateObjectId,
    RandomNumber,
    PWDCONTEXT,
    CheckList,
    ConvertLevelToFlat,
    FlattenArray,
)
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from model.crm_katalis.crmkatalis import (
    ClientInput,
    ClientCredential,
    MemberInput,
)

dbase = MGDB.user_membership_member


async def GetClientBasedOnMemberId(id: str):
    id = ValidateObjectId(id)
    member = await GetMemberOr404(id)
    companyId = member["companyId"]
    client = await GetClientBasedOnCompanyId(companyId)
    if client is None:
        raise HTTPException(status_code=404, detail="Member tidak ditemukan")
    return client


async def GetMemberOr404(id: str):
    dbase = MGDB.user_membership_member
    member = await dbase.find_one({"userId": ObjectId(id)})
    if member:
        return member
    else:
        raise HTTPException(status_code=404, detail="Member tidak ditemukan")


async def UpdateMember(id: str, data: MemberInput):
    dbase = MGDB.user_membership_member
    id = ValidateObjectId(id)
    try:
        data = jsonable_encoder(data)
        await dbase.update_one({"userId": id}, {"$set": data})
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500, detail="Gagal Update Member")
    return await GetMemberOr404(id)


async def DeleteMember(memberId: str):
    dbase = MGDB.user_membership_member
    await GetMemberOr404((memberId))
    try:
        await dbase.delete_one({"userId": ObjectId(memberId)})
    except:
        raise HTTPException(status_code=500, detail="Gagal hapus data")


async def CreateMember(
    tblName: str,
    user: ClientCredential,
    isFirstLogin: bool,
    role: List[RoleEnum],
    password: str = "pass",
    tipe: str = "python",
):
    if tipe == "java":
        return "TODO: add member ke backend java"
        pass

    dCompany = await GetCompanyOr404(str(user.companyId))
    # cek apakah company sudah memiliki solusi sesuai dengan tblName
    arTblName = tblName.split("_")
    if arTblName[1] not in dCompany["solution"]:
        raise HTTPException(
            status_code=400,
            detail="Company yang dipilih belum memiliki solusi " + arTblName[1],
        )

    firstname_split = user.name.split(" ")
    firstname = firstname_split[0].upper()
    user.username = (firstname + RandomNumber(4)).lower()

    if user.noId:
        user.username = user.noId

    user.userId = ObjectId()

    user = await CreateUser(tblName, user, password, isFirstLogin, role)
    return user


async def GetMembersOnCompanyId(companyId: str):
    ValidateObjectId(companyId)
    # dbase = MGDB.user_membership_member
    dbase = MGDB_KATALIS.crm_katalis_member_temp
    members = []
    async for member in dbase.find({"companyId": ObjectId(companyId)}):
        members.append(member)
    return members
