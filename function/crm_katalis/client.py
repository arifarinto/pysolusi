from datetime import datetime
from model.crm_katalis.katalis_lama import CompanyBase, CompanyOnDB
from function.user import CreateUser
from model.support import ParameterData, PipelineData
from model.util import EmailData
from function.company import CreateCompany, GetCompanyOr404
from config.config import MGDB, CONF, MGDB_KATALIS, PROJECT_NAME
from model.default import (
    CompanyBasic,
    CompanyInput,
    IdentityData,
    JwtToken,
    NotifData,
    CredentialData,
    RoleEnum,
    UserCredential,
    UserDataBase,
    UserTipeEnum,
)
from model.crm_katalis.crmkatalis import (
    ClientInput,
    ClientCredential,
    # MemberCredential,
    # MemberInput,
    # # MemberComplete,
)
from typing import List
from fastapi.exceptions import HTTPException
from function.notif import CreateNotif
from motor.metaprogramming import Async
from util.util import (
    IsiDefault,
    ListToUp,
    RandomString,
    ValidPhoneNumber,
    ValidateEmail,
    ValidateObjectId,
    RandomNumber,
    PWDCONTEXT,
    CheckList,
    ConvertLevelToFlat,
    FlattenArray,
)
from util.util_waktu import (
    dateTimeNow,
    dateTimeNowStr,
    convertDateToStrDate,
    convertStrDateToDate,
)

from bson.objectid import ObjectId
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
import json
from definitions import PROJECT_ROOT_PATH
import os

dbase = MGDB.user_crmkatalis_client


async def GetClientOr404(id: str):
    _id = ValidateObjectId(id)
    user = await dbase.find_one({"userId": _id, "credential.role": "client"})
    if user:
        return user
    else:
        raise HTTPException(status_code=404, detail="Client tidak ditemukan")


async def GetAllClient():
    clients = []  #
    async for client in dbase.find({"credential.role": "client"}):
        clients.append(client)
    return clients

    # skip = page * size
    # search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    # criteria = CreateCriteria(search)
    # datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    # datas = await datas_cursor.to_list(length=size)
    # totalElements = await dbase.count_documents(criteria)
    # totalPages = math.ceil(totalElements / size)
    # reply = DestinasiPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)


async def CreateClient(
    tblName: str,
    user: ClientCredential,
    isFirstLogin: bool,
    role: List[RoleEnum],
    password: str = "pass",
):
    dbase = MGDB[tblName]
    dCompany = await GetCompanyOr404(str(user.companyId))
    # cek apakah company sudah memiliki solusi sesuai dengan tblName
    arTblName = tblName.split("_")
    if arTblName[1] not in dCompany["solution"]:
        raise HTTPException(
            status_code=400,
            detail="Company yang dipilih belum memiliki solusi " + arTblName[1],
        )

    firstname_split = user.name.split(" ")
    firstname = firstname_split[0].upper()
    user.username = (firstname + RandomNumber(4)).lower()

    if user.noId:
        user.username = user.noId

    user.userId = ObjectId()

    user = await CreateUser(
        "user_crmkatalis_client", user, password, isFirstLogin, role
    )

    # SETUP PIPELINE
    user = ClientCredential(**user.dict())
    user.pipelines = []
    # Opening JSON file
    path_to_pipelines_json = os.path.join(
        PROJECT_ROOT_PATH, "route/crm_katalis/pipelines.json"
    )
    with open(path_to_pipelines_json) as json_file:
        pipelines = json.load(json_file)
        i = 1
        for pipeline in pipelines:
            pipeline = PipelineData(**pipeline)
            pipeline.id = ObjectId()
            pipeline.createTime = datetime.now()
            pipeline.nomor = i
            user.pipelines.append(pipeline.dict())
            i += 1

    await dbase.update_one(
        {"userId": ObjectId(user.userId)},
        {"$set": {"pipelines": user.pipelines}},
    )

    # setup company

    return user


async def UpdateClient(userId: str, data: ClientInput):
    await GetClientOr404(userId)
    data = jsonable_encoder(data)
    try:
        await dbase.update_one(
            {"userId": ObjectId(userId), "credential.role": "client"}, {"$set": data}
        )
        return await GetClientOr404(userId)
    except:
        raise HTTPException(status_code=500, detail="Gagal Update Data")


async def DeleteClient(userId: str):
    await GetClientOr404((userId))
    try:
        await dbase.delete_one({"userId": ObjectId(userId)})
    except:
        raise HTTPException(status_code=500, detail="Gagal hapus data")


async def GetClientBasedOnCompanyId(id: str):
    _id = ValidateObjectId(id)
    user = await dbase.find_one({"companyId": _id, "credential.role": "client"})
    if user:
        return user
    else:
        raise HTTPException(status_code=404, detail="Client tidak ditemukan")


async def GetPipelineByNomorOr404(clientId: str, nomor: int):
    client = await GetClientOr404(clientId)
    for pipeline in client["pipelines"]:
        if pipeline["nomor"] == nomor:
            return pipeline
    raise HTTPException(status_code=404, detail="Pipeline Tidak Ditemukan")


async def GetPipelineByNameOr404(clientId: str, name: int):
    client = await GetClientOr404(clientId)
    for pipeline in client["pipelines"]:
        if pipeline["name"] == name:
            return pipeline
    raise HTTPException(status_code=404, detail="Pipeline Tidak Ditemukan")


async def GetClientBasedOnPipelineId(id: str):
    # pass
    id = ValidateObjectId(id)
    print("passed validation object id")
    client = await dbase.find_one({"pipelines.id": id})
    if client is None:
        raise HTTPException(status_code=404, detail="Pipeline tidak ditemukan")
    return client


async def GetPipelineByIdPipelineOr404(pipelineId: str):
    client = await GetClientBasedOnPipelineId(pipelineId)
    for pipeline in client["pipelines"]:
        if str(pipeline["id"]) == pipelineId:
            return pipeline
    raise HTTPException(status_code=404, detail="Pipeline Tidak Ditemukan")


async def UpdatePipeline(idPipeline: str, data: dict):
    client = await GetClientBasedOnPipelineId(idPipeline)
    for i in range(len(client["pipelines"])):
        if str(client["pipelines"][i]["id"]) == idPipeline:
            client["pipelines"][i].update(data)
            break
    try:
        await dbase.update_one(
            {"userId": client["userId"]}, {"$set": {"pipelines": client["pipelines"]}}
        )
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500, detail="Gagal Update Pipeline")
    return await GetPipelineByIdPipelineOr404(idPipeline)


async def SetPipelineIsPassedStatus(id: str, passed: bool):
    return await UpdatePipeline(idPipeline=id, data={"isPassed": passed})


async def SetPipelineIsValidatedStatus(id: str, validated: bool):
    return await UpdatePipeline(idPipeline=id, data={"isValidated": validated})


async def UpdatePipelineNote(id: str, note: str):
    return await UpdatePipeline(idPipeline=id, data={"note": note})


async def UpdatePipelineForms(id: str, formData: ParameterData):
    pipeline = await GetPipelineByIdPipelineOr404(id)
    forms = pipeline["form"]
    for i in range(len(forms)):
        if str(forms[i]["key"]) == formData.key:
            # forms[i]["value"] = formData.value
            forms[i].update(formData.dict())
            return await UpdatePipeline(idPipeline=id, data={"form": forms})
    # forms.append({"key": key, "value": value})
    forms.append(formData.dict())
    return await UpdatePipeline(idPipeline=id, data={"form": forms})


async def DeleteKeyValuePairPipelineForms(id: str, key: str):
    pipeline = await GetPipelineByIdPipelineOr404(id)
    forms = pipeline["form"]
    for i in range(len(forms)):
        if str(forms[i]["key"]) == key:
            forms.remove(forms[i])
            return await UpdatePipeline(idPipeline=id, data={"form": forms})
            break
    raise HTTPException(404, detail="Key Tidak Ditemukan")


async def CreateCompanyClient(comp: CompanyBase):
    comp.createTime = dateTimeNow()
    comp.updateTime = dateTimeNow()
    comp.companyId = ObjectId()
    comp.companyName = comp.companyName.upper()
    comp.initials = comp.initials.upper()
    company = await MGDB_KATALIS.tbl_company.insert_one(comp.dict())
    if company.inserted_id:
        comp = await MGDB_KATALIS.tbl_company.find_one({"_id": company.inserted_id})
        comp = CompanyOnDB(**comp)
        #TODO kirim api ke katalis java
        return comp
    else:
        raise HTTPException(status_code=500, detail="Gagal Tambah Company Client")


async def CreateCompanyForClient(
    data: CompanyInput, clientId: str, current_user: JwtToken
):
    data = data.dict()
    data["solution"] = ["CRM_KATALIS"]
    data["address"] = None
    data["companyName"] = data["name"]
    data["initials"] = data["initial"]
    data = CompanyBase(**data)
    company = await CreateCompanyClient(data)
    companyId = company.companyId
    await SetCompanyIdAndCompanyInitialForClient(clientId, companyId)
    return company


async def GetClientCompanyOr404(id: str):
    companyId = ValidateObjectId(id)
    dbase = MGDB_KATALIS.tbl_company
    company = await dbase.find_one({"companyId": companyId})
    if company:
        return company
    else:
        raise HTTPException(status_code=404, detail="Company not found")


async def SetCompanyIdAndCompanyInitialForClient(clientId: str, companyId: str):
    client = await GetClientOr404(clientId)
    username = client["username"]
    company = await GetClientCompanyOr404(companyId)
    companyInitial = company["initials"]
    username = companyInitial + "." + username.split(".")[1]
    try:
        updated_client = await dbase.update_one(
            {"userId": ObjectId(clientId)},
            {"$set": {"companyId": ObjectId(companyId), "username": username}},
        )
    except:
        raise HTTPException(500, detail="Gagal Set Company Id untuk client")
    return updated_client
