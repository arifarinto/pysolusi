from function.user import CreateUser
from model.util import EmailData
from function.company import GetCompanyOr404
from typing import List
from fastapi.exceptions import HTTPException
from function.notif import CreateNotif
from model.crm_katalis.crmkatalis import (
    ClientCredential,
    PartnerCredential,
    PartnerInput,
    LeadsInput,
    LeadsBasic,
    LeadsComplete,
    FollowUpInput,
    FollowUpComplete,
)
from model.default import NotifData, CredentialData, RoleEnum, UserDataBase

# from model.util import RoleEnum
from config.config import MGDB, CONF, PROJECT_NAME
from util.util import (
    RandomString,
    ValidPhoneNumber,
    ValidateEmail,
    ValidateObjectId,
    RandomNumber,
    PWDCONTEXT,
    CheckList,
    ConvertLevelToFlat,
    FlattenArray,
    cleanNullTerms,
)
from util.util_waktu import (
    dateTimeNow,
    dateTimeNowStr,
    convertDateToStrDate,
    convertStrDateToDate,
)
from bson.objectid import ObjectId
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder

dbase = MGDB.user_crmkatalis_partner


async def GetPartnerOr404(id: str = None):
    _id = ValidateObjectId(id)
    user = await dbase.find_one({"userId": _id, "credential.role": "partner"})
    if user:
        return user
    else:
        raise HTTPException(status_code=404, detail="Partner tidak ditemukan")


async def GetAllPartner():
    partners = []
    async for partner in dbase.find({"credential.role": "partner"}):
        partners.append(partner)
    return partners


async def CreatePartner(
    tblName: str,
    user: PartnerCredential,
    password: str,
    isFirstLogin: bool,
    role: List[RoleEnum],
):
    dbase = MGDB[tblName]
    dCompany = await GetCompanyOr404(str(user.companyId))
    # cek apakah company sudah memiliki solusi sesuai dengan tblName
    arTblName = tblName.split("_")
    if arTblName[1] not in dCompany["solution"]:
        raise HTTPException(
            status_code=400,
            detail="Company yang dipilih belum memiliki solusi " + arTblName[1],
        )

    firstname_split = user.name.split(" ")
    firstname = firstname_split[0].upper()
    user.username = (firstname + RandomNumber(4)).lower()

    if user.noId:
        user.username = user.noId

    user.userId = ObjectId()

    user = await CreateUser(
        "user_crmkatalis_partner", user, password, isFirstLogin, role
    )

    return user


async def UpdatePartner(userId: str, data: PartnerInput):
    await GetPartnerOr404(userId)
    # data = jsonable_encoder(data)
    try:
        updated_partner = await dbase.update_one(
            {"userId": ObjectId(userId), "credential.role": "partner"},
            {"$set": data.dict()},
        )
        return await GetPartnerOr404(userId)
    except:
        raise HTTPException(status_code=500, detail="Gagal Update Data")


async def DeletePartner(userId: str):
    await GetPartnerOr404((userId))
    try:
        await dbase.delete_one({"userId": ObjectId(userId)})
    except:
        raise HTTPException(status_code=500, detail="Gagal hapus data")


async def CreateLead(data: LeadsInput, id: str):
    try:
        data = LeadsComplete(**data.dict())
        data.id = ObjectId()
        await dbase.update_one(
            {"userId": ObjectId(id), "credential.role": "partner"},
            {"$addToSet": {"leads": data.dict()}},
        )
        return data
    except:
        raise HTTPException(status_code=500, detail="Gagal Tambah Lead")


async def GetPartnerBasedOnLeadId(id: str):
    id = ValidateObjectId(id)
    partner = await dbase.find_one({"leads.id": id})
    if partner is None:
        raise HTTPException(status_code=404, detail="Lead tidak ditemukan")
    return partner


def findLeadInsidePartner(id: str, partner: dict):
    id = ValidateObjectId(id)
    for lead in partner["leads"]:
        if lead["id"] == id:
            return lead


async def GetLeadOr404(id: str):
    pipeline = [
        {"$unwind": {"path": "$leads"}},
        {"$replaceRoot": {"newRoot": "$leads"}},
        {"$match": {"id": ObjectId(id)}},
    ]
    results = await dbase.aggregate(pipeline).to_list(1000)
    if results:
        return results[0]
    raise HTTPException(status_code=404, detail="Lead tidak ditemukan")


async def UpdateLead(leadId: str, data: LeadsInput):
    partner = await GetPartnerBasedOnLeadId(leadId)
    for i in range(len(partner["leads"])):
        if str(partner["leads"][i]["id"]) == leadId:
            data = data.dict()
            data = cleanNullTerms(data)
            partner["leads"][i].update(data)
            lead = partner["leads"][i]
            break
    await dbase.update_one(
        {"leads.id": ObjectId(leadId)}, {"$set": {"leads": partner["leads"]}}
    )
    if lead:
        return lead
    raise HTTPException(status_code=500, detail="Gagal Update Lead")


async def DeleteLead(leadId: str):
    partner = GetPartnerBasedOnLeadId(leadId)
    for lead in partner["leads"]:
        if str(lead["id"]) == leadId:
            partner["leads"].remove(lead)
            break
    await dbase.update_one(
        {"leads.id": ObjectId(leadId)}, {"$set": {"leads": partner["leads"]}}
    )


# Followup crud
async def CreateFollowUp(leadId: str, data: FollowUpInput):
    partner = await GetPartnerBasedOnLeadId(leadId)
    print("Partner Follow Up: " + str(partner))
    data = FollowUpComplete(**data.dict())
    data.id = ObjectId()
    for lead in partner["leads"]:
        if str(lead["id"]) == leadId:
            lead["followUp"].append(data.dict())
            break
    await dbase.update_one(
        {"leads.id": ObjectId(leadId)}, {"$set": {"leads": partner["leads"]}}
    )
    return data


async def GetAllFollowUp(leadId: str):
    lead = await GetLeadOr404(leadId)
    return lead["followUp"]


async def GetPartnerBasedOnFollowUpId(id):
    id = ValidateObjectId(id)
    partner = await dbase.find_one({"leads.followUp.id": id})
    if partner is None:
        raise HTTPException(status_code=404, detail="Follow Up tidak ditemukan")
    return partner


async def GetFollowUpOr404(id: str):
    # partner = await GetPartnerBasedOnFollowUpId(id)
    # for lead in partner["leads"]:
    #     for followUp in lead["followUp"]:
    #         if str(followUp["id"]) == id:
    #             return followUp
    pipeline = [
        {"$unwind": {"path": "$leads"}},
        {"$replaceRoot": {"newRoot": "$leads"}},
        {"$unwind": {"path": "$followUp"}},
        {"$replaceRoot": {"newRoot": "$followUp"}},
        {"$match": {"id": ObjectId(id)}},
    ]
    results = await dbase.aggregate(pipeline).to_list(10000)
    if results:
        return results[0]
    raise HTTPException(status_code=404, detail="Follow up tidak ditemukan")


async def UpdateFollowUp(id: str, data: FollowUpInput):
    partner = await GetPartnerBasedOnFollowUpId(id)
    followUp = None
    for i in range(len(partner["leads"])):
        for j in range(len(partner["leads"][i]["followUp"])):
            if str(partner["leads"][i]["followUp"][j]["id"]) == id:
                partner["leads"][i]["followUp"][j].update(data.dict())
                followUp = partner["leads"][i]["followUp"][j]
                break

    try:
        await dbase.update_one(
            {"leads.followUp.id": ObjectId(id)}, {"$set": {"leads": partner["leads"]}}
        )
    except:
        raise HTTPException(status_code=404, detail="Gagal Update Follow Up")

    if followUp:
        return followUp


async def DeleteFollowUp(id: str):
    partner = await GetPartnerBasedOnFollowUpId(id)
    for lead in partner["leads"]:
        for followUp in lead["followUp"]:
            if str(followUp["id"]) == id:
                lead["followUp"].remove(followUp)
    try:
        await dbase.update_one(
            {"leads.followUp.id": ObjectId(id)}, {"$set": {"leads": partner["leads"]}}
        )
    except:
        raise HTTPException(status_code=404, detail="Gagal Hapus Follow Up")
