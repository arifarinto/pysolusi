from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from route.auth import router_auth
from route.trxadmin import router_trxadmin
from route.trxsystem import router_trxsystem
from route.trxedc import router_trxedc
from route.trxkasir import router_trxkasir
from route.trxuser import router_trxuser

app = FastAPI(
    openapi_url="/api_transaction/openapi.json", docs_url="/api_transaction/swgr"
)
app.mount("/api_transaction/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == "production":
    # setup Elastic APM Agent
    apm = make_apm_client(
        {
            "SERVICE_NAME": "pysolusi-transaction",
            "SERVER_URL": "http://apm-server.logging:8200",
            "ELASTIC_APM_TRANSACTION_IGNORE_URLS": ["/health"],
            "METRICS_SETS": "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
        }
    )
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*", "*"],
    allow_credentials=True,
    allow_methods=["*", "*"],
    allow_headers=["*", "*"],
)


@app.get("/health")
async def health():
    return {"status": "ok"}


app.include_router(
    router_auth,
    prefix="/api_default/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_trxadmin,
    prefix="/api_transaction/trxadmin",
    tags=["transaksi admin"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_trxsystem,
    prefix="/api_transaction/trxsystem",
    tags=["transaksi system"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_trxedc,
    prefix="/api_transaction/trxedc",
    tags=["transaksi edc"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_trxkasir,
    prefix="/api_transaction/trxkasir",
    tags=["transaksi kasir"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_trxuser,
    prefix="/api_transaction/trxuser",
    tags=["transaksi user"],
    responses={404: {"description": "Not found"}},
)


@app.on_event("startup")
async def app_startup():
    config.load_config()


@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()
