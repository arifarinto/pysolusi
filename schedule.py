from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from fastapi_utils.tasks import repeat_every
from route.trxsystem import auto_delete_log_topup


app = FastAPI(openapi_url="/schedule/openapi.json", docs_url="/schedule/swgr")
app.mount("/schedule/static", StaticFiles(directory="static"), name="static")


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)


@app.get("/health")
async def health():
    return {"status": "ok"}

@app.on_event("startup")
def on_startup():
    print("start")

@repeat_every(seconds=60)  # 1 minute
def delete_log_topup() -> None:
    auto_delete_log_topup()


