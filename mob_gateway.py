from route.auth import login_for_access_token
from fastapi import FastAPI, Response, Depends
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm, SecurityScopes

from route.auth import router_auth

app = FastAPI(openapi_url="/mob_gateway/openapi.json", docs_url="/mob_gateway/swgr")
app.mount("/mob_gateway/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-mobile',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}

app.include_router(router_auth,prefix="/api_default/auth",tags=["auth"],responses={404: {"description": "Not found"}})

@app.post("/mob_gateway/login", response_model=dict, tags=["login"])
async def mobile_login(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await login_for_access_token(response, form_data)

@app.post("/mob_gateway/first_login", response_model=dict, tags=["login"])
async def first_login(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await login_for_access_token(response, form_data)

@app.post("/mob_gateway/change_password", response_model=dict, tags=["login"])
async def change_password(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await login_for_access_token(response, form_data)

# ============================================

@app.post("/mob_gateway/get_home", response_model=dict, tags=["umum"])
async def get_home(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await get_home(response, form_data)

@app.post("/mob_gateway/tap_potong_saldo_kartu", response_model=dict, tags=["umum"])
async def tap_potong_saldo_kartu(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await tap_potong_saldo_kartu(response, form_data)

@app.post("/mob_gateway/scan_tiket_qrcode", response_model=dict, tags=["umum"])
async def scan_tiket_qrcode(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await scan_tiket_qrcode(response, form_data)

@app.post("/mob_gateway/scan_tiket_qris", response_model=dict, tags=["umum"])
async def scan_tiket_qris(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await scan_tiket_qris(response, form_data)

@app.post("/mob_gateway/update_status_online", response_model=dict, tags=["umum"])
async def update_status_online(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await update_status_online(response, form_data)

@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()