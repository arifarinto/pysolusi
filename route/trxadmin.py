from bson.objectid import ObjectId
from function.inquiry import CreateInquiry, GetInquiry
from util.util_trx import transaksi_universal
from function.user import CheckMemberByUserId
import json
from util.util import RandomString
from config.config import URL_SQL
from sql.model import TransactionBase, TransactionInput
from model.default import InquiryBase, JwtToken
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response,
    Security,
    BackgroundTasks,
)
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
from route.auth import get_current_user, login_for_access_token
import requests

router_trxadmin = APIRouter()

CHANNEL = "WEB"


@router_trxadmin.post("/inquiry_userid", response_model=InquiryBase)
async def inquiry_userid(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    return await CreateInquiry(current_user, userIdTujuan, amount)


@router_trxadmin.post("/topup_saldo_via_cash_admin")
async def topup_saldo_via_cash_admin(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "topup_saldo_via_cash_admin"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/topup_va_bank_secara_manual")
async def topup_va_bank_secara_manual(
    code: str,
    userIdTujuan: str,
    amount: int,
    bank: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    trxName = "topup_va_bank_secara_manual"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
        bankName=bank,
    )
    return trx1


@router_trxadmin.post("/meminjamkan_saldo", response_model=dict)
async def meminjamkan_saldo(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "meminjamkan_saldo"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/meminjamkan_uang_cash", response_model=dict)
async def meminjamkan_uang_cash(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "meminjamkan_uang_cash"
    noreff = RandomString(8)
    # debtUserId = current_user.userId
    # creditUserId = userIdTujuan
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(debtUserId, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/reversal_transaksi", response_model=dict)
async def reversal_transaksi(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    # belum ada config
    trxName = "reversal_transaksi"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post(
    "/melimpahkan_dana_invoice_via_transfer_bank", response_model=dict
)
async def melimpahkan_dana_invoice_via_transfer_bank(
    code: str,
    kodeCoa: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):

    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "melimpahkan_dana_invoice_via_transfer_bank"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
        kodeCoa=kodeCoa,  # ini kodeCoa-nya mesti di-specify sendiri, kayaknya, soalnya di config nggak ada kode coa-nya
    )
    return trx1


@router_trxadmin.post("/melimpahkan_dana_donasi_via_transfer_bank", response_model=dict)
async def melimpahkan_dana_donasi_via_transfer_bank(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "melimpahkan_dana_donasi_via_transfer_bank"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/melimpahkan_dana_invoice_via_cash", response_model=dict)
async def melimpahkan_dana_invoice_via_cash(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "melimpahkan_dana_invoice_via_cash"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/melimpahkan_dana_donasi_via_cash", response_model=dict)
async def melimpahkan_dana_donasi_via_cash(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "melimpahkan_dana_donasi_via_cash"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/mencairkan_saldo_via_cash", response_model=dict)
async def mencairkan_saldo_via_cash(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "mencairkan_saldo_via_cash"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/melimpahkan_saldo_user_via_bank", response_model=dict)
async def melimpahkan_saldo_user_via_bank(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "melimpahkan_saldo_user_via_bank"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/memindahkan_dana_dari_bank_ke_cash", response_model=dict)
async def memindahkan_dana_dari_bank_ke_cash(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "memindahkan_dana_dari_bank_ke_cash"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/memindahkan_dana_antar_bank", response_model=dict)
async def memindahkan_dana_antar_bank(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):

    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "memindahkan_dana_antar_bank"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/menyetorkan_uang_cash_ke_bank", response_model=dict)
async def menyetorkan_uang_cash_ke_bank(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "menyetorkan_uang_cash_ke_bank"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/mencatat_biaya_pengeluaran_kas_dan_bank", response_model=dict)
async def mencatat_biaya_pengeluaran_kas_dan_bank(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "mencatat_biaya_pengeluaran_kas_dan_bank"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxadmin.post("/mencatat_pemasukan_lain_lain_kas_dan_bank", response_model=dict)
async def mencatat_pemasukan_lain_lain_kas_dan_bank(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["admin", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "mencatat_pemasukan_lain_lain_kas_dan_bank"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1
