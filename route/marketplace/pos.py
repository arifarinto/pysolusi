from bson import ObjectId
# from passlib.utils import _NULL
from config.config import MGDB, CONF, HOST_TAP_DEVICE
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime, timedelta
import logging
import math
import requests
import json
import urllib.parse

from model.order import MerchantPaymentType, PaymentType, PembeliBase, PembeliOnDB, ProductOrder, OrderHistory, OrderStatus, PaymentData

from model.auth import TokenData
from util.util import DollarDeliverAddr, IsiDefault, ValidateObjectId, CreateCriteria
from util.util_waktu import dateNow, dateTimeNow
from route.auth import get_current_user
from model.util import SearchRequest, FieldObjectIdRequest

router_pos = APIRouter()
dbase = MGDB.toko_temporary

async def GetPosOr404(id: str):
    _id = ValidateObjectId(id)
    pos = await dbase.find_one({"_id": _id})
    if pos:
        return pos
    else:
        raise HTTPException(status_code=404, detail="POS not found")

async def GetKasirByIdAkun(id: str):
    _id = ValidateObjectId(id)
    pembeli = await MGDB.toko_kasir.find_one({"idAkun": _id})
    if pembeli:
        return pembeli
    else:
        return False

# =================================================================================

async def SelfCekOrAddKasir(current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    cek_pembeli = await GetKasirByIdAkun(current_user.accountId)
    if cek_pembeli:
        return cek_pembeli
    else:
        #insert ke table pembeli
        pembeli = PembeliBase()
        pembeli.creatorName = current_user.userName
        pembeli.creatorId = ObjectId(current_user.accountId)
        pembeli.createTime = dateTimeNow()
        pembeli.updateTime = dateTimeNow()
        pembeli.idAkun = ObjectId(current_user.accountId)
        pembeli.name = current_user.userName
        pembeli_op = await MGDB.toko_kasir.insert_one(pembeli.dict())
        if pembeli_op.inserted_id:
            return pembeli

async def PosAddPembeli(current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    #insert ke table pembeli
    pembeli = PembeliBase()
    pembeli.creatorName = current_user.userName
    pembeli.creatorId = ObjectId(current_user.accountId)
    pembeli.createTime = dateTimeNow()
    pembeli.updateTime = dateTimeNow()
    pembeli.idAkun = ObjectId(current_user.accountId)
    pembeli.name = "temporary"
    pembeli.levelTipe.append('temporary')
    pembeli_op = await dbase.insert_one(pembeli.dict())
    if pembeli_op.inserted_id:
        return pembeli_op.inserted_id

@router_pos.get("/cart_create", response_model=dict)
async def cart_create(current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    pembeli_op = await PosAddPembeli(current_user)
    return {"posId": str(pembeli_op)}


@router_pos.get("/cart_add/{posId}/{productId}", response_model=ProductOrder)
async def cart_add(posId: str, productId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    cek_product = await MGDB.toko_product.find_one(
        {"_id": ObjectId(productId)},
        {"name": 1, "tags": 1, "stokistId": 1, "price": 1, "volume": 1, "volType": 1, "barcode": 1, "images": 1,"stock":1}
    )
    if cek_product:
        stok = cek_product['stock']
        nama = cek_product['name']
        # if stok < 1 : raise HTTPException(status_code=404, detail=f"Nama Produk: {nama}, Stok : {stok}")

        update_cart = await dbase.update_one(
            {"_id": ObjectId(posId),
             "cartData.productId": ObjectId(productId)},
            {"$set": {"cartData.$.product": cek_product, "cartData.$.updateTime": dateTimeNow()},
             "$inc": {"cartData.$.jumlahOrder": +1}}
        )
        print(update_cart.modified_count)
        if update_cart.modified_count:
            cek_cart = await dbase.find_one(
                {"_id": ObjectId(posId), "cartData.productId": ObjectId(productId)}, {
                    "cartData.$": 1}
            )
            return cek_cart["cartData"][0]
        else:
            order = ProductOrder()
            order.id = ObjectId()
            order.createTime = dateTimeNow()
            order.updateTime = dateTimeNow()
            order.productId = ObjectId(productId)
            order.jumlahOrder = 1
            order.product = cek_product
            add_cart_op = await dbase.update_one(
                {"_id": ObjectId(posId)},
                {"$addToSet": {"cartData": order.dict()}}
            )
            print(add_cart_op.modified_count)
            return order
    else:
        raise HTTPException(status_code=404, detail="Product ID not found")


@router_pos.get("/cart_del/{posId}/{productId}", response_model=ProductOrder)
async def cart_del(posId: str, productId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    cek_product = await MGDB.toko_product.find_one(
        {"_id": ObjectId(productId)},
        {"name": 1, "tags": 1, "stokistId": 1, "price": 1, "volume": 1, "volType": 1, "barcode": 1, "images": 1}
    )
    if cek_product:
        update_cart = await dbase.update_one(
            {"_id": ObjectId(posId),"cartData.productId": ObjectId(productId)},
            {"$set": {
                "cartData.$.product": cek_product, 
                "cartData.$.updateTime": dateTimeNow()},
            "$inc": {
                "cartData.$.jumlahOrder": -1}
            }
        )
        if update_cart.modified_count:
            cek_cart = await dbase.find_one(
                {"_id": ObjectId(posId), "cartData.productId": ObjectId(productId)}, {"cartData.$": 1}
            )
            if cek_cart["cartData"][0]["jumlahOrder"] == 0:
                dbase.update_one(
                    {"_id": ObjectId(posId)},
                    {"$pull": {"cartData": {"productId": ObjectId(productId)}}}
                )
            return cek_cart["cartData"][0]
        else:
            raise HTTPException(
                status_code=404, detail="POS ID or Product ID not found")
    else:
        raise HTTPException(status_code=404, detail="Product ID not found")


@router_pos.get("/cart_clear/{posId}", response_model=dict)
async def cart_clear(posId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    await dbase.update_one(
        {"_id": ObjectId(posId),"idAkun":ObjectId(current_user.accountId)},
        {"$set": {"cartData": []}}
    )
    return {"status": "ok"}


@router_pos.delete("/cart_remove/{posId}", response_model=dict)
async def cart_remove(posId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    await dbase.delete_one(
        {"_id": ObjectId(posId),"idAkun":ObjectId(current_user.accountId)}
    )
    return {"status": "ok"}


# cek data cart by pos
@router_pos.get("/pos_cart_data/{posId}", response_model=List[ProductOrder])
async def get_pos_cart_data(posId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    cek_cart = await dbase.find_one(
        {"_id": ObjectId(posId)}, 
        {"cartData.product.id": 0,"orderHistory": 0, "deliverBoyHistory": 0, "password": 0}
    )
    print(cek_cart)
    return list(cek_cart["cartData"])


@router_pos.get("/pos_cart_count/{posId}", response_model=dict)
async def get_pos_cart_count(posId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    pipeline = [
        {"$match": {"_id": ObjectId(posId)}},
        {"$project": {"totalCart": {"$sum": "$cartData.jumlahOrder"}}}
    ]
    cursor = dbase.aggregate(pipeline)
    cek = await cursor.to_list(1)
    return {"totalCart": cek[0]["totalCart"]}


@router_pos.get("/pos_cart_checkout/step1_start/{posId}", response_model=dict)
async def check_out_step1_start(posId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    cek_cart = await dbase.find_one(
        {"_id": ObjectId(posId), "isVerified": False}, 
        {"cartData.product.id": 0, "orderHistory": 0, "deliverBoyHistory": 0, "password": 0}
    )

    if not cek_cart : raise HTTPException(status_code=404, detail="Cart Data not found")

    totalItem = 0
    totalPrice = 0
    for x in cek_cart["cartData"]:
        totalItem += x["jumlahOrder"]
        totalPrice += x["jumlahOrder"] * x["product"]["price"]

    if totalItem > 0:
        orderStatus = OrderStatus()
        orderStatus.createTime = dateTimeNow()
        orderStatus.updateTime = dateTimeNow()
        orderStatus.status = "start"

        dataOrder = OrderHistory()
        dataOrder.id = ObjectId()
        dataOrder.productOrder = cek_cart["cartData"]
        dataOrder.createTime = dateTimeNow()
        dataOrder.updateTime = dateTimeNow()
        dataOrder.userWhoOrder = ObjectId(posId)
        dataOrder.totalItem = totalItem
        dataOrder.totalPrice = totalPrice
        dataOrder.orderStatus = [orderStatus]
        dataOrder.lastOrderStatus = "start"

        # dbase.update_one(
        #     {"_id": ObjectId(posId)},
        #     {"$set": {"cartData": []}, 
        #     "$addToSet": {"orderHistory": dataOrder.dict()}}
        # )

        dbase.update_one(
            {"_id": ObjectId(posId)},
            {"$set": {"orderHistory": dataOrder.dict()}}
        )

        return {"totalItem": totalItem, "totalPrice": totalPrice, "orderStatus": "start", "orderId": str(dataOrder.id)}
    else:
        raise HTTPException(status_code=404, detail="Cart Data is empty")


@router_pos.post("/pos_cart_checkout/step3_payment/{posId}/{orderId}", response_model=PaymentData)
async def check_out_step3_payment(paymentType: PaymentType, merchantPaymentType: MerchantPaymentType, posId: str, orderId: str,deviceId:str='',nfcId:str='', current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    #isi cara bayar : cash / katalis / internal
    cek_order = await dbase.find_one(
    {"_id": ObjectId(posId), "orderHistory.id": ObjectId(orderId),"isVerified": False}, 
    {"name": 1, "orderHistory": 1}
    )
    if not cek_order : raise HTTPException(status_code=404, detail="Order Not Found")
    payment = PaymentData()
    payment.paymentType = paymentType
    payment.merchant = merchantPaymentType
    payment.amount = cek_order["orderHistory"]["totalPrice"]
    payment.totalAmount = payment.amount + payment.ongkir
    payment.finalAmount = payment.totalAmount - payment.diskon
    payment.paid = 1
    payment.id = ObjectId()
    payment.createTime = dateTimeNow()
    payment.updateTime = dateTimeNow()
    payment.paymentTime = dateTimeNow()
    defMaxPayTime = 24
    payment.maxPaymentTime = dateTimeNow() + timedelta(hours=defMaxPayTime)
    payment.paymentRef = 'cash'
    payment.paymetStatus = True

    if paymentType == "cash":


        cek_order["orderHistory"]["paymentData"] = payment.dict()
        mv = await  MGDB.toko_kasir.update_one(
            { "idAkun": ObjectId(current_user.accountId)}, 
            { "$addToSet": { "orderHistory": cek_order["orderHistory"]}}
        )
        if not mv.modified_count:
            await SelfCekOrAddKasir(current_user)

            mv2 = await  MGDB.toko_kasir.update_one(
                { "idAkun": ObjectId(current_user.accountId)}, 
                { "$addToSet": { "orderHistory": cek_order["orderHistory"]}}
            )
            if not mv2.modified_count: raise HTTPException(status_code=404, detail="error update db to tbl_kasir")    
        upd = await dbase.update_one(
            {"_id": ObjectId(posId), "orderHistory.id": ObjectId(orderId),"isVerified":False}, 
            {"$set":  { "isVerified": True}}
        )
        if not upd.modified_count: raise HTTPException(status_code=404, detail="error update db status verified")
        # stok produk berkurang
        prod =cek_order["orderHistory"]['productOrder']
        for i in range(len(prod)) :
            prid = prod[i]['productId']
            jml_order = prod[i]['jumlahOrder']
            stok_before = prod[i]['product']['stock']
            stok_after = stok_before -jml_order
            MGDB.toko_product.update_one({"_id":ObjectId(prid)},{'$set':{'stock': stok_after}})

        return payment

    if paymentType == "katalis":
        if deviceId == '' : raise HTTPException(status_code=404, detail="deviceId required")

        if nfcId == '':
            payment.paymentRef = 'katalis'
            total_str = "{:0,.0f}".format(float(payment.totalAmount))
            diskon_str = "{:0,.0f}".format(float(payment.diskon))
            msg = f"{deviceId}#6489#0#3196354234#check#00#NAMA#0.0#0.0#0.0#TOTAL Rp{total_str}   #DISKON Rp{diskon_str}"
            msg = urllib.parse.quote_plus(msg)
            url = f"{HOST_TAP_DEVICE}/fastmqtt/publish?deviceid={deviceId}&message={msg}"
            headers = {'Content-type': 'application/json'}
            res = requests.post(url, json='', headers=headers,timeout=30)
            
            return payment

        else :
            payment.paymentRef = 'katalis'
            cek_order["orderHistory"]["paymentData"] = payment.dict()
            cek_order["orderHistory"]["nfcId"] = nfcId
            cek_order["orderHistory"]["deviceId"] = deviceId
            mv = await  MGDB.toko_kasir.update_one(
                { "idAkun": ObjectId(current_user.accountId)}, 
                { "$addToSet": { "orderHistory": cek_order["orderHistory"]}}
            )
            if not mv.modified_count:
                await SelfCekOrAddKasir(current_user)

                mv2 = await  MGDB.toko_kasir.update_one(
                    { "idAkun": ObjectId(current_user.accountId)}, 
                    { "$addToSet": { "orderHistory": cek_order["orderHistory"]}}
                )
                if not mv2.modified_count: raise HTTPException(status_code=404, detail="error update db to tbl_kasir")    
            upd = await dbase.update_one(
                {"_id": ObjectId(posId), "orderHistory.id": ObjectId(orderId),"isVerified":False}, 
                {"$set":  { "isVerified": True}}
            )
            if not upd.modified_count: raise HTTPException(status_code=404, detail="error update db status verified")
            # stok produk berkurang
            prod =cek_order["orderHistory"]['productOrder']
            for i in range(len(prod)) :
                prid = prod[i]['productId']
                jml_order = prod[i]['jumlahOrder']
                stok_before = prod[i]['product']['stock']
                stok_after = stok_before -jml_order
                MGDB.toko_product.update_one({"_id":ObjectId(prid)},{'$set':{'stock': stok_after}})

            return payment
            



@router_pos.get("/history_transaksi")
async def history_transaksi(date_start:datetime=datetime.now(),date_end:datetime=datetime.now(),product_name:str = '', size: int = 10, page: int = 0,current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    date_start = date_start.replace(hour=0,minute=0,second=0)
    date_end = date_end.replace(hour=23,minute=59,second=59)
    skip = page * size
    limit = size

    match = {'$match':{'createTime':{'$gte':date_start,'$lte':date_end}}}
    paging = {'$facet':{'data':[{'$sort':{'createTime':-1}},{'$skip':skip},{'$limit':limit}]}}
    count = {'$count':'total'}
    if product_name != '': 
        product_name = product_name.upper()
        match = {'$match':{'createTime':{'$gte':date_start,'$lte':date_end},'productOrder.product.name':product_name}}

    datas_cursor = MGDB.toko_kasir.aggregate([{'$match':{'idAkun':ObjectId(current_user.accountId)}},{'$project':{'orderHistory':1,'_id':0}},{'$unwind':{'path':'$orderHistory'}},{'$replaceRoot':{'newRoot':'$orderHistory'}},{'$project':{'createTime':1,'totalItem':1,'totalPrice':1,'productOrder':1}},match,paging])
    datas = await datas_cursor.to_list(1000)
    datas=datas[0]

    for i in range(len(datas['data']))  :
        for j in range(len(datas['data'][i]['productOrder'])) :
            del datas['data'][i]['productOrder'][j]['id']
            del datas['data'][i]['productOrder'][j]['productId']
            del datas['data'][i]['productOrder'][j]['product']['_id']
    

    jml = MGDB.toko_kasir.aggregate([{'$match':{'idAkun':ObjectId(current_user.accountId)}},{'$project':{'orderHistory':1,'_id':0}},{'$unwind':{'path':'$orderHistory'}},{'$replaceRoot':{'newRoot':'$orderHistory'}},{'$project':{'createTime':1,'totalItem':1,'totalPrice':1,'productOrder':1}},match,count])
    jmls = await jml.to_list(1000)
    
    total = 0
    if len(jmls) > 0 : total = jmls[0]['total']
    

    datas['page'] = page
    datas['totalPage'] = math.ceil(total / size)
    datas['size'] = size
    datas['count'] = len(datas['data'])
    datas['total'] = total
    return datas
