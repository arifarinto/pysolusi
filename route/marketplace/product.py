from typing import List
from bson import ObjectId
from config.config import MGDB, REDATA
from fastapi import APIRouter, Depends, HTTPException, File, UploadFile, Security
from datetime import datetime
import pandas as pd
import math

from model.product import ProductOnDB, ProductComplete, ProductPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_product = APIRouter()
dbase = MGDB.toko_product

async def GetProductOr404(id: str):
    _id = ValidateObjectId(id)
    product = await dbase.find_one({"_id": _id})
    if product:
        return product
    else:
        raise HTTPException(status_code=404, detail="Product not found")

# =================================================================================
async def save_tags(current_user,data_in,image_url:str = ''):
    if len(data_in) < 1 : return
    cek_data = await MGDB.toko_product_tag.find_one({"creatorId":ObjectId(current_user.accountId)})
    if not cek_data:
        tags = {}
        tags['creatorId'] = ObjectId(current_user.accountId)
        tags['userName'] = current_user.userName
        
        tags['tags'] = []
        await MGDB.toko_product_tag.insert_one(tags)

    for data_tag in data_in :
        await MGDB.toko_product_tag.update_one(
        {"creatorId":ObjectId(current_user.accountId)},
        {"$pull":{"tags": {"tag":data_tag.strip().capitalize()}}}
        )
        await MGDB.toko_product_tag.update_one(
        {"creatorId":ObjectId(current_user.accountId)},
        {"$addToSet":{"tags": {"id_tag":str(ObjectId()),"tag": data_tag.strip().capitalize(),"image":image_url}}}
        )




@router_product.post("/productUpload/{creatorId}")
async def upload_product(creatorId:ObjectId = Depends(ValidateObjectId),file: UploadFile = File(...), current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    result = []
    data_in = ProductComplete()
    key = 'STORE.product_tags'
    for index,row in df.iterrows():
        product = IsiDefault(data_in, current_user)
        product.name = row['nama'].upper()
        product.tags = row['tags'].lower().split(',')
        product.price = row['harga']
        product.volume = row['stok']
        product.volType = "pcs"
        product.barcode = row['barcode']
        product.creatorId = ObjectId(creatorId)
        # REDATA.sadd(key,*product.tags)
        result.append(product.dict())
        await save_tags( current_user,row['tags'].lower().split(','))
    #aneh
    print(result)
    dbase.insert_many(result)
    return {"ok"}

@router_product.post("/product", response_model=ProductOnDB)
async def add_product(data_in: ProductComplete, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    product = IsiDefault(data_in, current_user)
    product.name = product.name.upper()
    product.tags = ListToUp(product.tags)
    key = 'STORE.product_tags'
    await save_tags(current_user,data_in.tags)
    # REDCON.sadd(key,*product.tags)
    # await dbase.update_one({"key":product.name},
    #     {"$set": product.dict(skip_defaults=False)},upsert=True)
    product_op = await dbase.insert_one(product.dict())
    if product_op.inserted_id:
        product = await GetProductOr404(product_op.inserted_id)
    return product


@router_product.post("/get_product/{creatorId}", response_model=dict)
async def get_product_by_creator(creatorId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='creatorId',key=ObjectId(creatorId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = ProductPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply

@router_product.post("/get_all_product", response_model=dict)
async def get_all_products_for_all_user(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = ProductPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply

@router_product.get("/product/{id}", response_model=ProductOnDB)
async def get_product_by_id(id: ObjectId = Depends(ValidateObjectId)):
    product = await dbase.find_one({"_id": id})
    if product:
        return product
    else:
        raise HTTPException(status_code=404, detail="Product not found")


@router_product.delete("/product/{id}", dependencies=[Depends(GetProductOr404)], response_model=dict)
async def delete_product_by_id(id: str,current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    product_op = await dbase.delete_one({"_id": ObjectId(id)})
    if product_op.deleted_count:
        return {"status": f"deleted count: {product_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_product.put("/product/{id_}", response_model=ProductOnDB)
async def update_product(id_: str, data_in: ProductComplete, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    product_data = IsiDefault(data_in, current_user, True)
    product = await dbase.find_one({"_id": ObjectId(id_)})
    if product:
        product_data.updateTime = dateTimeNow()
        product_op = await dbase.update_one(
            {"_id": ObjectId(id_)}, {"$set": product_data.dict(skip_defaults=True)}
        )
        await save_tags(current_user,data_in.tags)
        if product_op.modified_count:
            return await GetProductOr404(id_)
        else:
            raise HTTPException(status_code=304)
    else:
        raise HTTPException(status_code=304)


@router_product.post("/add_tag")
async def add_tag(tag_name: str,image_url: str='', current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    await save_tags(current_user,[tag_name],image_url)
    cek_data = await MGDB.toko_product_tag.find_one({"creatorId":ObjectId(current_user.accountId)})
    return cek_data['tags']


@router_product.get("/get_tag")
async def get_tag(current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    cek_data = await MGDB.toko_product_tag.find_one({"creatorId":ObjectId(current_user.accountId)})
    if not cek_data : return []
    return cek_data['tags']

@router_product.get("/get_tag_by_id")
async def get_tag_by_id(id_tag:str,current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):

    cek_data_cursor = MGDB.toko_product_tag.aggregate([{'$match':{'creatorId':ObjectId(current_user.accountId)}},{'$project':{'tags':1,'_id':0}},{'$unwind':{'path':'$tags'}},{'$replaceRoot':{'newRoot':'$tags'}},{'$match':{'id_tag':id_tag}}])
    cek_data = await cek_data_cursor.to_list(1000)
    if len(cek_data) < 1 : return {}
    return cek_data[0]


@router_product.put("/update_tag")
async def update_tag(tag_name: str,image_url: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    await save_tags(current_user,[tag_name],image_url)
    cek_data = await MGDB.toko_product_tag.find_one({"creatorId":ObjectId(current_user.accountId)})
    return cek_data['tags']

@router_product.put("/update_tag_by_id")
async def update_tag_by_id(id_tag: str,tag_name:str,image_url: str='', current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    await MGDB.toko_product_tag.update_one(
        {"creatorId":ObjectId(current_user.accountId)},
        {"$pull":{"tags": {"id_tag": id_tag}}}
        )

    await MGDB.toko_product_tag.update_one(
        {"creatorId":ObjectId(current_user.accountId)},
        {"$addToSet":{"tags": {"id_tag":id_tag,"tag": tag_name.strip().capitalize(),"image":image_url}}}
        )
    
    return {"id_tag":id_tag,"tag": tag_name.strip().capitalize(),"image":image_url}


@router_product.delete("/delete_tag")
async def delete_tag(tag_name: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    await MGDB.toko_product_tag.update_one(
        {"creatorId":ObjectId(current_user.accountId)},
        {"$pull":{"tags": {"tag":tag_name.strip().capitalize()}}}
        )
    cek_data = await MGDB.toko_product_tag.find_one({"creatorId":ObjectId(current_user.accountId)})
    return cek_data['tags']

@router_product.delete("/delete_tag_by_id")
async def delete_tag_by_id(id_tag: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    await MGDB.toko_product_tag.update_one(
        {"creatorId":ObjectId(current_user.accountId)},
        {"$pull":{"tags": {"id_tag":id_tag}}}
        )
    cek_data = await MGDB.toko_product_tag.find_one({"creatorId":ObjectId(current_user.accountId)})
    return cek_data['tags']


