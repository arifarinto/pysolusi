from bson import ObjectId
from config.config import MGDB, REDATA
from fastapi import APIRouter, Depends, HTTPException, File, UploadFile, Security
from datetime import datetime
import pandas as pd
import math

from model.product import ProductOnDB, ProductComplete, ProductPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_pembelian_stok = APIRouter()
dbase = MGDB.toko_product

# =================================================================================


@router_pembelian_stok.post("/pembelian_stok/{id_}", response_model=ProductOnDB)
async def pembelian_stok(id_: str, data_in: ProductComplete, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    product_data = IsiDefault(data_in, current_user, True)
    product = await dbase.find_one({"_id": ObjectId(id_)})
    if not product: raise HTTPException(status_code=404, detail="Product not found")
    product_data.updateTime = dateTimeNow()

    await MGDB.toko_pembelian_stok.insert_one(product_data.dict())

    product_data.price += product['price']
    product_data.stock += product['stock']
    product_op = await dbase.update_one(
        {"_id": ObjectId(id_)}, {"$set": product_data.dict(skip_defaults=True)}
    )
    if not product_op.modified_count: raise HTTPException(status_code=404, detail="error database")

    return product
 

