from bson import ObjectId
from config.config import MGDB, REDATA
from fastapi import APIRouter, Depends, HTTPException, File, UploadFile, Security
from datetime import datetime
import pandas as pd
import math

from model.product import ProductOnDB, ProductComplete, ProductPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_product_kasir = APIRouter()
dbase = MGDB.toko_product

async def GetProductOr404(id: str):
    _id = ValidateObjectId(id)
    product = await dbase.find_one({"_id": _id})
    if product:
        return product
    else:
        raise HTTPException(status_code=404, detail="Product not found")

# =================================================================================

@router_product_kasir.post("/product", response_model=ProductOnDB)
async def add_product(data_in: ProductComplete, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    product = IsiDefault(data_in, current_user)
    product.name = product.name.upper()
    product.tags = ListToUp(product.tags)
    key = 'STORE.product_tags'
    # REDCON.sadd(key,*product.tags)
    # await dbase.update_one({"key":product.name},
    #     {"$set": product.dict(skip_defaults=False)},upsert=True)
    product_op = await dbase.insert_one(product.dict())
    if product_op.inserted_id:
        product = await GetProductOr404(product_op.inserted_id)
    return product


@router_product_kasir.post("/get_product", response_model=dict)
async def get_all_products(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = ProductPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_product_kasir.get("/product/{id}", response_model=ProductOnDB)
async def get_product_by_id(id: ObjectId = Depends(ValidateObjectId)):
    product = await dbase.find_one({"_id": id})
    if product:
        return product
    else:
        raise HTTPException(status_code=404, detail="Product not found")


@router_product_kasir.delete("/product/{id}", dependencies=[Depends(GetProductOr404)], response_model=dict)
async def delete_product_by_id(id: str,current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    product_op = await dbase.delete_one({"_id": ObjectId(id)})
    if product_op.deleted_count:
        return {"status": f"deleted count: {product_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_product_kasir.put("/product/{id_}", response_model=ProductOnDB)
async def update_product(id_: str, data_in: ProductComplete, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    product_data = IsiDefault(data_in, current_user, True)
    product = await dbase.find_one({"_id": ObjectId(id_)})
    if product:
        product_data.updateTime = dateTimeNow()
        product_op = await dbase.update_one(
            {"_id": ObjectId(id_)}, {"$set": product_data.dict(skip_defaults=True)}
        )
        if product_op.modified_count:
            return await GetProductOr404(id_)
        else:
            raise HTTPException(status_code=304)
    else:
        raise HTTPException(status_code=304)
