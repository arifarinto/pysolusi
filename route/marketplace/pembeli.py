from os import mkdir
from bson import ObjectId
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime, timedelta
import math

from model.order import MerchantPaymentType, PaymentType, PembeliBase, PembeliOnDB, ProductOrder, OrderHistory, OrderStatus, DeliverAddr, PaymentData, PembeliPage
from model.product import ProductOrderBaseOut
from model.user import UserCreate
from model.auth import TokenData
from route.auth import get_current_user
from impl.user import CreateUser
from model.util import SearchRequest, FieldObjectIdRequest
from util.util import DollarDeliverAddr, IsiDefault, RandomString, ValidateObjectAkunId, ValidateObjectId, CreateCriteria
from util.util_waktu import dateTimeNow

router_pembeli = APIRouter()
dbase = MGDB.toko_pembeli

async def GetPembeliOr404(id: str):
    _id = ValidateObjectId(id)
    pembeli = await dbase.find_one({"_id": _id})
    if pembeli:
        return pembeli
    else:
        raise HTTPException(status_code=404, detail="Pembeli not found")

async def GetPembeliByIdAkun(id: str):
    _id = ValidateObjectId(id)
    pembeli = await dbase.find_one({"idAkun": _id})
    if pembeli:
        return pembeli
    else:
        return False

# =================================================================================

async def SelfCekOrAddPembeli(current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    cek_pembeli = await GetPembeliByIdAkun(current_user.accountId)
    if cek_pembeli:
        return cek_pembeli
    else:
        #insert ke table pembeli
        pembeli = PembeliBase()
        pembeli.creatorName = current_user.userName
        pembeli.creatorId = ObjectId(current_user.accountId)
        pembeli.createTime = dateTimeNow()
        pembeli.updateTime = dateTimeNow()
        pembeli.idAkun = ObjectId(current_user.accountId)
        pembeli.name = current_user.userName
        pembeli_op = await dbase.insert_one(pembeli.dict())
        if pembeli_op.inserted_id:
            return pembeli


@router_pembeli.post("/get_pembeli", response_model=dict)
async def get_all_pembelis(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = PembeliPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_pembeli.get("/pembeli/{id}", response_model=PembeliOnDB)
async def get_pembeli_by_id(id: ObjectId = Depends(ValidateObjectId)):
    pembeli = await dbase.find_one({"_id": id})
    if pembeli:
        return pembeli
    else:
        raise HTTPException(status_code=404, detail="Pembeli not found")

@router_pembeli.get("/pembeli_by_idakun/{id}", response_model=PembeliOnDB)
async def pembeli_by_idakun(id: ObjectId = Depends(ValidateObjectId)):
    pembeli = await dbase.find_one({"idAkun": id})
    if pembeli:
        return pembeli
    else:
        raise HTTPException(status_code=404, detail="Pembeli not found")


@router_pembeli.delete("/pembeli/{id}", dependencies=[Depends(GetPembeliOr404)], response_model=dict)
async def delete_pembeli_by_id(id: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    pembeli_op = await dbase.delete_one({"_id": ObjectId(id)})
    if pembeli_op.deleted_count:
        return {"status": f"deleted count: {pembeli_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_pembeli.put("/pembeli/{id_}", response_model=PembeliBase)
async def update_pembeli(id_: str, data_in: PembeliOnDB, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    pembeli_data = IsiDefault(data_in, current_user)
    pembeli = await dbase.find_one({"_id": ObjectId(id_)})
    if pembeli:
        pembeli_data.password = pembeli["password"]
        pembeli_op = await dbase.update_one(
            {"_id": ObjectId(id_)}, {"$set": pembeli_data.dict(skip_defaults=True)}
        )
        if pembeli_op.modified_count:
            return await GetPembeliOr404(id_)
        else:
            raise HTTPException(status_code=304)
    else:
        raise HTTPException(status_code=304)

# =================================================================================

@router_pembeli.get("/pembeli_favorit", response_model=List[ProductOrderBaseOut])
async def get_pembeli_favorit(current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    pipeline = [
        {"$lookup":{"from": "toko_product","localField": "favorite","foreignField": "_id","as": "favoriteList"}},
        {"$match": {"idAkun": ObjectId(current_user.accountId)}},
        {"$project": {"favoriteList": 1}}
    ]
    favorit_cursor = dbase.aggregate(pipeline)
    favorit = await favorit_cursor.to_list(1)
    print(favorit[0]["favoriteList"])
    return list(favorit[0]["favoriteList"])

@router_pembeli.get("/pembeli_address", response_model=List[DeliverAddr])
async def get_pembeli_address(current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    pembeli = await dbase.find_one({"idAkun": ObjectId(current_user.accountId)},{"deliverAddr":1})
    if pembeli:
        return pembeli["deliverAddr"]
    else:
        raise HTTPException(status_code=404, detail="Pembeli not found")

@router_pembeli.get("/pembeli_cart_add/{productId}", response_model=ProductOrder)
async def add_pembeli_cart(productId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    await SelfCekOrAddPembeli(current_user)
    cek_product = await MGDB.toko_product.find_one({"_id":ObjectId(productId)},
    {"name":1,"tags":1,"stokistId":1,"price":1,"volume":1,"volType":1,"barcode":1,"images":1})
    if cek_product:
        update_cart = await dbase.update_one(
            { "idAkun": ObjectId(current_user.accountId), "cartData.productId" : ObjectId(productId)},
            { "$set": { "cartData.$.product": cek_product, "cartData.$.updateTime": dateTimeNow()},
            "$inc":{"cartData.$.jumlahOrder" : +1 }}
        )
        print(update_cart.modified_count)
        if update_cart.modified_count:
            cek_cart = await dbase.find_one(
                { "idAkun": ObjectId(current_user.accountId), "cartData.productId" : ObjectId(productId)},{"cartData.$":1}
            )
            return cek_cart["cartData"][0]
        else:
            order = ProductOrder()
            order.id = ObjectId()
            order.createTime = dateTimeNow()
            order.updateTime = dateTimeNow()
            order.productId = ObjectId(productId)
            order.jumlahOrder = 1
            order.product = cek_product
            add_cart_op = await dbase.update_one(
                { "idAkun": ObjectId(current_user.accountId)},
                { "$addToSet": { "cartData": order.dict()}}
            )
            print(add_cart_op.modified_count)
            return order
    else:
        raise HTTPException(status_code=404, detail="Product ID not found")

@router_pembeli.get("/pembeli_cart_del/{productId}", response_model=ProductOrder)
async def del_pembeli_cart(productId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):

    update_cart = await dbase.update_one(
        { "idAkun": ObjectId(current_user.accountId), "cartData.productId" : ObjectId(productId)},
        { "$set": { "cartData.$.updateTime": dateTimeNow()},
        "$inc":{"cartData.$.jumlahOrder" : -1 }}
    )
    if update_cart.modified_count:
        cek_cart = await dbase.find_one(
            { "idAkun": ObjectId(current_user.accountId), "cartData.productId" : ObjectId(productId)},{"cartData.$":1}
        )
        if cek_cart["cartData"][0]["jumlahOrder"] == 0 :
            dbase.update_one(
                { "idAkun": ObjectId(current_user.accountId)},
                { "$pull": {"cartData": {"productId": ObjectId(productId)}}}
            )
        return cek_cart["cartData"][0]
    else:
        raise HTTPException(status_code=404, detail="Pembeli ID or Product ID not found")


@router_pembeli.get("/pembeli_cart_clear", response_model=dict)
async def pembeli_cart_clear(current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    await dbase.update_one(
            { "idAkun": ObjectId(current_user.accountId)},
            { "$set": { "cartData": []}}
        )
    return {"status":"ok"}
    

#cek data cart by pembeli
@router_pembeli.get("/pembeli_cek_cart_data", response_model=List[ProductOrder])
async def get_pembeli_cart_data(current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    cek_cart = await dbase.find_one(
        { "idAkun": ObjectId(current_user.accountId)},{"cartData.product.id":0,"orderHistory":0,"deliverBoyHistory":0,"password":0}
    )
    print(cek_cart)
    return list(cek_cart["cartData"])


@router_pembeli.get("/pembeli_cart_count", response_model=dict)
async def get_pembeli_cart_count(current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    pipeline = [
        {"$match": {"idAkun" : ObjectId(current_user.accountId)}},
        {"$project": { "totalCart" : {"$sum" : "$cartData.jumlahOrder"}}} 
    ]
    cursor = dbase.aggregate(pipeline)
    cek = await cursor.to_list(1)
    return {"totalCart": cek[0]["totalCart"]}

# =================================================================================
# MULAI TRANSAKSI 

@router_pembeli.get("/pembeli_cart_checkout/step1_start", response_model=dict)
async def check_out_step1_start(current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    cek_cart = await dbase.find_one(
        { "idAkun": ObjectId(current_user.accountId)},{"cartData.product.id":0,"orderHistory":0,"deliverBoyHistory":0,"password":0}
    )
    totalItem = 0
    totalPrice = 0
    for x in cek_cart["cartData"]:
        totalItem += x["jumlahOrder"]
        totalPrice += x["jumlahOrder"] * x["product"]["price"]

    if totalItem > 0:
        orderStatus = OrderStatus()
        orderStatus.createTime = dateTimeNow()
        orderStatus.updateTime = dateTimeNow()
        orderStatus.status = "start"

        dataOrder = OrderHistory()
        dataOrder.id = ObjectId()
        dataOrder.productOrder = cek_cart["cartData"]
        dataOrder.createTime = dateTimeNow()
        dataOrder.updateTime = dateTimeNow()
        dataOrder.userWhoOrder = ObjectId(current_user.accountId)
        dataOrder.totalItem = totalItem
        dataOrder.totalPrice = totalPrice
        dataOrder.orderStatus = [orderStatus]
        dataOrder.lastOrderStatus = "start"
        
        dbase.update_one(
            { "idAkun": ObjectId(current_user.accountId)},
            { "$set": {"cartData":[]},"$addToSet": { "orderHistory": dataOrder.dict()}}
        )
        return {"totalItem":totalItem, "totalPrice": totalPrice, "orderStatus" : "start", "orderId": str(dataOrder.id)}

    else:
        raise HTTPException(status_code=404, detail="Cart Data is empty")

@router_pembeli.post("/pembeli_cart_checkout/step2_address/{orderId}", response_model=DeliverAddr)
async def check_out_step2_address(address: DeliverAddr, orderId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    #isi alamat baru atau pilih alamat existing dengan cek id_
    try :
        ValidateObjectId(address.id)
        cek_addr = await dbase.find_one(
            { "idAkun": ObjectId(current_user.accountId),"deliverAddr.id":ObjectId(address.id)},{"deliverAddr.$":1}
        )
        if cek_addr:
            await dbase.update_one(
                {"idAkun": ObjectId(current_user.accountId), "orderHistory.id": ObjectId(orderId)},
                { "$set": {"orderHistory.$.deliverAddr": cek_addr["deliverAddr"][0],"orderHistory.$.lastOrderStatus": "setAddress"}}
            )
            return cek_addr["deliverAddr"][0]
        else:
            raise HTTPException(status_code=404, detail="Address Id Not Found")

    except :
        address.id = ObjectId()
        address.description = address.description.upper()
        address.createTime = dateTimeNow()
        address.updateTime = dateTimeNow()
        print("cek")
        await dbase.update_one(
            {"idAkun": ObjectId(current_user.accountId), "orderHistory.id": ObjectId(orderId)},
            { "$set": {
                "orderHistory.$.deliverAddr": address.dict(),
                "orderHistory.$.lastOrderStatus": "setAddress",
                "lastAddr":address.dict()}
            }
        )
        cek_addr = await dbase.find_one(
            { "idAkun": ObjectId(current_user.accountId),"deliverAddr.description":address.description},{"deliverAddr.$":1}
        )
        if not cek_addr:
             await dbase.update_one(
                {"idAkun": ObjectId(current_user.accountId)},
                {"$addToSet": {"deliverAddr":address.dict()}}
            )
        else:
            updateAlamat = DollarDeliverAddr(address.dict())
            await dbase.update_one(
                {"idAkun": ObjectId(current_user.accountId), "deliverAddr.description": address.description},
                {"$set": updateAlamat}
            )
        return address

@router_pembeli.post("/pembeli_cart_checkout/step3_payment/{orderId}", response_model=PaymentData)
async def check_out_step3_payment(paymentType: PaymentType, merchantPaymentType: MerchantPaymentType, orderId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    #isi cara bayar
    cek_order = await dbase.find_one(
        { "idAkun": ObjectId(current_user.accountId),"orderHistory.id":ObjectId(orderId)},{"name":1,"orderHistory.$":1}
    )
    if cek_order:
        payment = PaymentData()
        payment.paymentType = paymentType
        payment.merchant = merchantPaymentType
        print(cek_order["orderHistory"][0]["totalPrice"])
        payment.amount = cek_order["orderHistory"][0]["totalPrice"]
        payment.totalAmount = payment.amount + payment.ongkir
        payment.finalAmount = payment.totalAmount - payment.diskon
        payment.paid = 0
        payment.id = ObjectId()
        payment.createTime = dateTimeNow()
        payment.updateTime = dateTimeNow()
        payment.paymentTime = dateTimeNow()
        defMaxPayTime = 24
        payment.maxPaymentTime = dateTimeNow() + timedelta(hours=defMaxPayTime)
        payment.paymentRef = 'none'

        await dbase.update_one(
            {"idAkun": ObjectId(current_user.accountId), "orderHistory.id": ObjectId(orderId)},
            { "$set": {
                "orderHistory.$.paymentData": payment.dict(),
                "orderHistory.$.lastOrderStatus": "setPayment"
                }
            }
        )
        return payment
    else:
        raise HTTPException(status_code=404, detail="Order Id Not Found")

#update pembayaran
@router_pembeli.post("/pembeli_cart_checkout/step3a_payment_update/{orderId}", response_model=dict)
async def check_out_step3a_payment_update(payment: PaymentData, orderId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    #isi cara bayar
    print('update pembayaran nya')

@router_pembeli.get("/pembeli_cart_checkout/step4_packing/{idAkun}/{orderId}", response_model=dict)
async def check_out_step4_packing(idAkun:str, orderId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    #update status packing pada saat orderan dicetak download word orderan by petugas per kecamatan
    cek_order = await dbase.find_one(
        { "idAkun": ObjectId(idAkun),"orderHistory.id":ObjectId(orderId)},{"orderHistory.$":1}
    ) 
    if cek_order:
        await dbase.update_one(
            {"idAkun": ObjectId(idAkun), "orderHistory.id": ObjectId(orderId)},
            { "$set": {
                "orderHistory.$.lastOrderStatus": "setPacking",
                "orderHistory.$.userWhoDeliver": ObjectId(current_user.accountId)
                }
            }
        )
        # kirim notif bahwa barang sudah dipacking
        return {"response":"ok"}
    else:
        raise HTTPException(status_code=404, detail="Order Id Not Found")

#step5_otw_mulai berangkat ngirim


#step6_barang_diterima


#step7_review