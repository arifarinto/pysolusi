# backend/tancho/infos/routes.py

from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.support import InfoBase, InfoInput, InfoOnDB, InfoPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_info = APIRouter()
dbase = MGDB.tbl_info


async def GetInfoOr404(id_: str):
    _id = ValidateObjectId(id_)
    info = await dbase.find_one({"_id": _id})
    if info:
        return info
    else:
        raise HTTPException(status_code=404, detail="Info not found")


# =================================================================================


@router_info.post("/info", response_model=InfoOnDB)
async def add_info(
    data_in: InfoInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    data_in = InfoBase(**data_in.dict())
    info = IsiDefault(data_in, current_user)
    info.title = info.title.upper()
    cinfo = await dbase.find_one(
        {"companyId": ObjectId(current_user.companyId), "title": info.title}
    )
    if cinfo:
        raise HTTPException(
            status_code=400, detail="Info tidak boleh dengan judul yang sama"
        )
    info.isHeadLine = False
    info.isPublish = False
    info_op = await dbase.insert_one(info.dict())
    if info_op.inserted_id:
        info = await GetInfoOr404(info_op.inserted_id)
        return info


@router_info.post("/get_info", response_model=dict)
async def get_all_infos(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    skip = page * size
    search.defaultObjectId.append(
        FieldObjectIdRequest(field="companyId", key=ObjectId(current_user.companyId))
    )
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = InfoPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_info.get("/info/{id_}", response_model=InfoOnDB)
async def get_info_by_id(
    id_: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    info = await dbase.find_one(
        {"_id": id_, "companyId": ObjectId(current_user.companyId)}
    )
    if info:
        return info
    else:
        raise HTTPException(status_code=404, detail="Info not found")


@router_info.delete(
    "/info/{id_}", dependencies=[Depends(GetInfoOr404)], response_model=dict
)
async def delete_info_by_id(
    id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    info_op = await dbase.delete_one(
        {"_id": ObjectId(id_), "companyId": ObjectId(current_user.companyId)}
    )
    if info_op.deleted_count:
        return {"status": f"deleted count: {info_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_info.put("/info/{id_}", response_model=InfoOnDB)
async def update_info(
    id_: str,
    data_in: InfoBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    info = IsiDefault(data_in, current_user, True)
    info.updateTime = dateTimeNow()
    info_op = await dbase.update_one(
        {"_id": ObjectId(id_), "companyId": ObjectId(current_user.companyId)},
        {"$set": info.dict(skip_defaults=True)},
    )
    if info_op.modified_count:
        return await GetInfoOr404(id_)
    else:
        raise HTTPException(status_code=304)
