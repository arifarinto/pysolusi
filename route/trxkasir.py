from function.inquiry import CreateInquiry, GetInquiry
from model.default import InquiryBase, JwtToken
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response,
    Security,
    BackgroundTasks,
)
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
from route.auth import get_current_user, login_for_access_token
from util.util_trx import transaksi_universal
from function.user import CheckMemberByUserId
from util.util import RandomString

router_trxkasir = APIRouter()
CHANNEL = "KASIR"


@router_trxkasir.post("/inquiry_data_kartu", response_model=dict)
async def inquiry_data_kartu(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    trxName = "inquiry_data_kartu"


@router_trxkasir.post("/transaksi_dengan_kartu", response_model=dict)
async def transaksi_dengan_kartu(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "transaksi_dengan_kartu"
    noreff = RandomString(8)
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxkasir.post("/transaksi_kartu_unlimited", response_model=dict)
async def transaksi_kartu_unlimited(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "transaksi_kartu_unlimited"
    noreff = RandomString(8)
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxkasir.post("/topup_saldo_dengan_kartu", response_model=dict)
async def topup_saldo_dengan_kartu(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "topup_saldo_dengan_kartu"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxkasir.post("/inquiry_qr_code_cpm", response_model=dict)
async def inquiry_qr_code_cpm(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    trxName = "inquiry_qr_code_cpm"


@router_trxkasir.post("/transaksi_qr_code_cpm", response_model=dict)
async def transaksi_qr_code_cpm(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "transaksi_qr_code_cpm"
    noreff = RandomString(8)
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxkasir.post("/inquiry_userid", response_model=dict)
async def inquiry_userid(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    trxName = "inquiry_userid"
    return await CreateInquiry(current_user, userIdTujuan, amount)


@router_trxkasir.post("/topup_saldo_dengan_userid", response_model=dict)
async def topup_saldo_dengan_userid(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "topup_saldo_dengan_userid"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxkasir.post("/inquiry_invoice_internal", response_model=dict)
async def inquiry_invoice_internal(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    # question:
    # 1. Belum paham alurnya seperti apa, dan fungsinya apa?
    trxName = "inquiry_invoice_internal"


@router_trxkasir.post("/pembayaran_invoice_internal", response_model=dict)
async def pembayaran_invoice_internal(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "pembayaran_invoice_internal"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxkasir.post("/pengantian_kartu_hilang", response_model=dict)
async def pengantian_kartu_hilang(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "pengantian_kartu_hilang"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxkasir.post("/blokir_unblokir_kartu", response_model=dict)
async def blokir_unblokir_kartu(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):

    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    # question:
    # ini transaksinya seperti apa
    trxName = "blokir_unblokir_kartu"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1
