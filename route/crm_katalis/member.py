from model.support import InvoiceInput
from function.invoice import CreateInvoice
from definitions import CRM_KATALIS_ROOT
from util.util import IsiDefault, ValidateObjectId
from config.config import MGDB
from model.crm_katalis.katalis_lama import MemberTempOnDB
from typing import List
from starlette.responses import HTMLResponse, RedirectResponse
from starlette.templating import Jinja2Templates
from util.util_waktu import convertStrDateToDate
from fastapi.encoders import jsonable_encoder
from model.membership.member import MemberBase
from bson.objectid import ObjectId
from route.membership.member import get_member_by_userid, update_member_by_userid
from function.crm_katalis.member import (
    DeleteMember,
    GetClientBasedOnMemberId,
    GetMemberOr404,
    GetMembersOnCompanyId,
    UpdateMember,
)
from model.default import JwtToken
from route.auth import get_current_user
from fastapi import (
    APIRouter,
    Form,
    Request,
    Depends,
    HTTPException,
    status,
    Response,
    Security,
    BackgroundTasks,
)

from model.crm_katalis.crmkatalis import (
    CetakKartuBase,
    CetakKartuOnDB,
    MemberInput,
    MemberKartu,
    # MemberBasic,
)

from function.user import checkIfUserAuthorized

router_member = APIRouter()
templates = Jinja2Templates(directory="template-html")

HARGA_PER_KARTU = 10000


@router_member.get("/{memberId}", response_model=MemberBase)
async def get_member_by_id_member(
    memberId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk mengambil data member berdasarkan id member. Wajib login sebagai admin, atau client.

    Client tidak bisa mengakses data member client lain.
    """
    client = await GetClientBasedOnMemberId(memberId)
    checkIfUserAuthorized(current_user, str(client["userId"]))
    return await GetMemberOr404(memberId)


@router_member.get("/", response_model=List[MemberTempOnDB])
async def get_members_by_company_id(
    companyId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    return await GetMembersOnCompanyId(companyId)


@router_member.put("/{memberId}", response_model=MemberBase)
async def update_member(
    memberId: str,
    data: MemberInput,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk update data member berdasarkan id member. Wajib login sebagai admin, atau client.

    Client tidak bisa update data member client lain.
    """
    client = await GetClientBasedOnMemberId(memberId)
    checkIfUserAuthorized(current_user, str(client["userId"]))
    return await UpdateMember(memberId, data)


@router_member.delete("/{memberId}")
async def delete_member(
    memberId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk hapus data member berdasarkan id member. Wajib login sebagai admin, atau client.

    Client tidak bisa hapus data member client lain.
    """
    client = await GetClientBasedOnMemberId(memberId)
    checkIfUserAuthorized(current_user, str(client["userId"]))
    await DeleteMember(memberId)
    return "Berhasil Hapus Member dengan id " + str(memberId)


@router_member.get("/cetak_kartu/", response_class=HTMLResponse)
async def pilih_member(companyId: str, request: Request):
    parameters = {}
    parameters["request"] = request
    parameters["companyId"] = companyId

    members = await GetMembersOnCompanyId(companyId)
    parameters["members"] = members
    return templates.TemplateResponse(
        "pilih_member_cetak_kartu.html",
        parameters,
    )


@router_member.get(
    "/checkout_pesanan_kartu/{cetakKartuId}", response_class=HTMLResponse
)
async def checkout_pesanan_kartu(cetakKartuId: str, request: Request):
    cetakKartuId = ValidateObjectId(cetakKartuId)
    # parameters["companyId"] = companyId

    dbase = MGDB.cetak_kartu
    pesanan = await dbase.find_one({"_id": cetakKartuId})
    print("pesanan: " + str(pesanan))

    parameters = {}
    parameters["request"] = request
    parameters["pesanan"] = pesanan

    # members = await GetMembersOnCompanyId(companyId)
    # parameters["members"] = members
    return templates.TemplateResponse(
        "checkout_cetak_kartu.html",
        parameters,
    )


@router_member.post("/cetak_kartu/", response_class=RedirectResponse, status_code=302)
async def proses_hasil_pilih_member(companyId: str, members: List[str] = Form(...)):
    ck = CetakKartuBase()
    ck.companyId = ObjectId(companyId)
    ck.jumlah = len(members)
    ck.total = ck.jumlah * HARGA_PER_KARTU
    for member in members:
        id = member.split("-")[0]
        name = member.split("-")[1]
        ck.members.append(MemberKartu(id=id, name=name))
    dbase = MGDB.cetak_kartu
    pesanan = await dbase.insert_one(ck.dict())
    if pesanan.inserted_id:

        # setup invoice
        token_data = JwtToken()
        token_data.userId = None
        token_data.companyId = None
        token_data.isAdmin = None
        token_data.roles = None
        token_data.exp = None

        data = InvoiceInput()
        data.title = "Pembayaran Pesanan Cetak Kartu"
        data.total = ck.total
        data.userId = None

        await CreateInvoice(data, token_data)

        pesanan = await dbase.find_one({"_id": pesanan.inserted_id})
        return (
            CRM_KATALIS_ROOT + "/member/checkout_pesanan_kartu/" + str(pesanan["_id"])
        )
    else:
        raise HTTPException(status_code=500, detail="Gagal Tambah Pesanan Kartu")
