from function.crm_katalis.partner import (
    DeleteFollowUp,
    GetFollowUpOr404,
    GetPartnerBasedOnFollowUpId,
    UpdateFollowUp,
)
from function.user import checkIfUserAuthorized
from model.default import JwtToken
from fastapi.routing import APIRouter
from model.crm_katalis.crmkatalis import (
    FollowUpBasic,
    FollowUpInput,
    LeadsBasic,
    LeadsInput,
)
from fastapi import Security
from route.auth import get_current_user

router_followup = APIRouter()


@router_followup.get("/{followUpId}", response_model=FollowUpBasic)
async def get_followup_by_id_followup(
    followUpId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk mengambil data follow up berdasarkan id follow up. Wajib login sebagai admin, atau partner.

    Partner tidak bisa mengakses data follow up untuk lead yang bukan miliknya.
    """

    partner = await GetPartnerBasedOnFollowUpId(followUpId)
    checkIfUserAuthorized(current_user, str(partner["userId"]))
    followUp = await GetFollowUpOr404(followUpId)
    return followUp


@router_followup.put("/{followUpId}", response_model=FollowUpBasic)
async def update_followup(
    followUpId: str,
    data: FollowUpInput,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk update data follow up berdasarkan id follow up. Wajib login sebagai admin, atau partner.

    Partner tidak bisa update data follow up untuk lead yang bukan miliknya.
    """
    partner = await GetPartnerBasedOnFollowUpId(followUpId)
    checkIfUserAuthorized(current_user, str(partner["userId"]))
    return await UpdateFollowUp(followUpId, data)


@router_followup.delete("/{followUpId}")
async def delete_followup(
    followUpId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk hapus data follow up berdasarkan id follow up. Wajib login sebagai admin, atau partner.

    Partner tidak bisa hapus data follow up untuk lead yang bukan miliknya.
    """
    partner = GetPartnerBasedOnFollowUpId
    checkIfUserAuthorized(current_user, str(partner["userId"]))
    await DeleteFollowUp(followUpId)
    return "Berhasil Hapus Follow Up dengan id: " + str(followUpId)
