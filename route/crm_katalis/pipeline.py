from os import pipe
from function.crm_katalis.client import (
    DeleteKeyValuePairPipelineForms,
    GetPipelineByIdPipelineOr404,
    SetPipelineIsPassedStatus,
    SetPipelineIsValidatedStatus,
    UpdatePipelineForms,
)
from model.support import ParameterData, PipelineData
from fastapi import APIRouter
from model.default import JwtToken
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response,
    Security,
    BackgroundTasks,
)
from route.auth import get_current_user

router_pipeline = APIRouter()


@router_pipeline.get("/{pipelineId}", response_model=PipelineData)
async def get_pipeline_by_id(
    pipelineId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk mengambil data pipeline berdasarkan id pipeline.
    """
    return await GetPipelineByIdPipelineOr404(pipelineId)


@router_pipeline.put("/{pipelineId}/is_validated", response_model=PipelineData)
async def set_pipeline_is_validated_status(
    validated: bool,
    pipelineId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk update status isValidated pada pipeline.
    """
    return await SetPipelineIsValidatedStatus(pipelineId, validated)


@router_pipeline.put("/{pipelineId}/is_passed", response_model=PipelineData)
async def set_pipeline_is_passed_status(
    passed: bool,
    pipelineId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk update status isPassed pada pipeline.
    """
    return await SetPipelineIsPassedStatus(pipelineId, passed)


@router_pipeline.put("/{pipelineId}/form", response_model=PipelineData)
async def add_pipeline_forms_element(
    pipelineId: str,
    formData: ParameterData,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk update key-value pairs pada atribut forms di pipeline.
    elemen di dalam array atribut forms memiliki atribut sebagai berikut:
    - key
    - tipe
    - choice
    - isMultipleChoice
    - value

    Setiap elemen di dalam array forms diidentifikasi secara unik oleh key.
    Apabila data masukan formData memiliki key yang sama dengan data yang sudah ada di dalam database, maka sistem akan mengupdate elemen form yang bersangkutan.
    Apabila data masukan formData memiliki key yang belum ada di dalam database, maka sistem akan membuat entri baru di dalam array forms.
    """
    return await UpdatePipelineForms(pipelineId, formData)


@router_pipeline.delete("/{pipelineId}/form/{key}")
async def delete_pipeline_forms_element(
    pipelineId: str,
    key: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    API untuk delete elemen di dalam forms pipeline berdasarkan key.
    """
    await DeleteKeyValuePairPipelineForms(pipelineId, key)
    return "Berhasil delete key " + str(key)
