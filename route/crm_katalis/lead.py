import math
from typing import List
from util.util import IsiDefault
from function.user import checkIfUserAuthorized
from route.auth import get_current_user
from fastapi import Security
from model.default import JwtToken
from model.crm_katalis.crmkatalis import (
    FollowUpBasic,
    FollowUpInput,
    FollowUpPage,
    LeadsBasic,
    LeadsInput,
)
from fastapi.routing import APIRouter
from function.crm_katalis.partner import (
    CreatePartner,
    GetPartnerBasedOnFollowUpId,
    GetPartnerBasedOnLeadId,
    GetPartnerOr404,
    DeletePartner,
    UpdatePartner,
    CreateLead,
    GetLeadOr404,
    UpdateLead,
    DeleteLead,
    CreateFollowUp,
    GetAllFollowUp,
    GetFollowUpOr404,
    UpdateFollowUp,
    DeleteFollowUp,
    GetAllPartner,
)

router_lead = APIRouter()


@router_lead.get("/{leadId}", response_model=LeadsBasic)
async def get_lead(
    leadId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk mengambil data lead tunggal berdasarkan id lead. Wajib login sebagai admin, atau partner.

    Partner tidak bisa mengakses data lead partner lain.
    """
    partner = await GetPartnerBasedOnLeadId(leadId)
    checkIfUserAuthorized(current_user, str(partner["userId"]))
    return await GetLeadOr404(leadId)


@router_lead.put("/{leadId}", response_model=LeadsBasic)
async def update_lead(
    leadId: str,
    data: LeadsInput,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk update data lead tunggal berdasarkan id lead. Wajib login sebagai admin, atau partner.

    Partner tidak bisa update data lead partner lain.
    """
    partner = await GetPartnerBasedOnLeadId(leadId)
    checkIfUserAuthorized(current_user, str(partner["userId"]))
    return await UpdateLead(leadId, data)


@router_lead.delete("/{leadId}")
async def delete_lead(
    leadId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk hapus data lead tunggal berdasarkan id lead. Wajib login sebagai admin, atau partner.

    Partner tidak bisa hapus data lead partner lain.
    """
    partner = await GetPartnerBasedOnLeadId(leadId)
    checkIfUserAuthorized(current_user, str(partner["userId"]))
    await DeleteLead(leadId)
    return "Berhasil Hapus Lead dengan id " + str(leadId)


# CRUD FOLLOWUP
@router_lead.post("/{leadId}/follow_up", response_model=FollowUpBasic)
async def add_followup(
    leadId: str,
    followUpInput: FollowUpInput,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk menambah data follow up untuk satu lead. Wajib login sebagai admin, atau partner.

    Partner tidak bisa menambah data follow up untuk lead yang bukan miliknya.
    """
    partner = await GetPartnerBasedOnLeadId(leadId)
    checkIfUserAuthorized(current_user, str(partner["userId"]))
    followUpInput = FollowUpBasic(**followUpInput.dict())
    followUpInput = IsiDefault(followUpInput, current_user)
    return await CreateFollowUp(leadId, followUpInput)


@router_lead.get("/{leadId}/follow_up", response_model=FollowUpPage)
async def get_all_followup_of_a_lead(
    leadId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    # search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk mengambil semua follow up untuk satu lead. Wajib login sebagai admin, atau partner.

    Partner tidak bisa mengakses data follow up untuk lead yang bukan miliknya.
    """

    partner = await GetPartnerBasedOnLeadId(leadId)
    checkIfUserAuthorized(current_user, str(partner["userId"]))
    followUps = await GetAllFollowUp(leadId)
    skip = page * size
    datas = followUps[skip : skip + size]
    totalElements = len(followUps)
    totalPages = math.ceil(totalElements / size)
    reply = FollowUpPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


# @router_lead.get("/follow_up/{followUpId}", response_model=FollowUpBasic)
# async def get_followup_by_id_followup(
#     followUpId: str,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk mengambil data follow up berdasarkan id follow up. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa mengakses data follow up untuk lead yang bukan miliknya.
#     """

#     partner = await GetPartnerBasedOnFollowUpId(followUpId)
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     followUp = await GetFollowUpOr404(followUpId)
#     return followUp


# @router_lead.put("/follow_up/{followUpId}", response_model=FollowUpBasic)
# async def update_followup(
#     followUpId: str,
#     data: FollowUpInput,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk update data follow up berdasarkan id follow up. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa update data follow up untuk lead yang bukan miliknya.
#     """
#     partner = GetPartnerBasedOnFollowUpId
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     return await UpdateFollowUp(followUpId, data)


# @router_lead.delete("/follow_up/{followUpId}")
# async def delete_followup(
#     followUpId: str,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk hapus data follow up berdasarkan id follow up. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa hapus data follow up untuk lead yang bukan miliknya.
#     """
#     partner = GetPartnerBasedOnFollowUpId
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     await DeleteFollowUp(followUpId)
#     return "Berhasil Hapus Follow Up dengan id: " + str(followUpId)
