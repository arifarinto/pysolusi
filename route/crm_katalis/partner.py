import math
from model.util import SearchRequest
from util.util import IsiDefault
from bson.objectid import ObjectId
from function.crm_katalis.partner import (
    CreatePartner,
    GetPartnerBasedOnFollowUpId,
    GetPartnerBasedOnLeadId,
    GetPartnerOr404,
    DeletePartner,
    UpdatePartner,
    CreateLead,
    GetLeadOr404,
    UpdateLead,
    DeleteLead,
    CreateFollowUp,
    GetAllFollowUp,
    GetFollowUpOr404,
    UpdateFollowUp,
    DeleteFollowUp,
    GetAllPartner,
)
from model.crm_katalis.crmkatalis import (
    LeadPage,
    PartnerBasic,
    PartnerCredential,
    PartnerInput,
    LeadsInput,
    LeadsBasic,
    FollowUpInput,
    FollowUpBasic,
)
from typing import Dict, List
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response,
    Security,
    BackgroundTasks,
)
from config.config import MGDB, CONF, PROJECT_NAME
from model.default import JwtToken
from route.auth import get_current_user
from function.user import CreateUser, checkIfNoIdIsAlreadyRegistered
from function.user import checkIfUserAuthorized

router_partner = APIRouter()


# CRUD PARTNER
@router_partner.post("", response_model=PartnerBasic)
async def add_partner(
    data: PartnerInput,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Endpoint untuk menambahkan data partner baru. Wajib login sebagai admin.
    """
    #
    dbase = MGDB.user_crmkatalis_partner
    await checkIfNoIdIsAlreadyRegistered(
        dbase, current_user.companyId, data.noId, "partner"
    )
    # cpartner = await dbase.find_one(
    #     {"companyId": ObjectId(current_user.companyId), "noId": data.noId}
    # )
    # if cpartner:
    #     raise HTTPException(
    #         status_code=400, detail="Partner with NIP : " + data.noId + " is registered"
    #     )

    data = PartnerCredential(**data.dict())
    data = IsiDefault(data, current_user)
    partner = await CreatePartner(
        tblName="user_crmkatalis_partner",
        user=data,
        password="pass",
        isFirstLogin="false",
        role=["partner"],
    )

    return partner
    # pass


@router_partner.get("", response_model=List[PartnerBasic])
async def get_all_partners(
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"])
):
    """
    Endpoint untuk request seluruh data partner yang ada di database. Wajib login sebagai admin.
    """
    partners = await GetAllPartner()
    return partners


@router_partner.get("/my_account", response_model=PartnerBasic)
async def get_partner_my_account(
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk mendapatkan data partner berdasarkan user id. Hanya mengembalikan satu buah objek.
    """

    partnerId = current_user.userId
    return await GetPartnerOr404(partnerId)


@router_partner.get("/{userId}", response_model=PartnerBasic)
async def get_partner(
    userId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk mendapatkan data partner berdasarkan user id. Hanya mengembalikan satu buah objek.

    Wajib login sebagai admin, atau partner. Admin bisa mengakses data partner manapun, sedangkan partner hanya dapat mengakses data dirinya sendiri.
    """
    checkIfUserAuthorized(current_user, userId)
    partner = await GetPartnerOr404(userId)
    return partner


@router_partner.put("/{userId}", response_model=PartnerBasic)
async def update_partner(
    userId: str,
    data: PartnerInput,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Endpoint untuk update data partner berdasarkan user id.

    Wajib login sebagai admin, atau partner. Admin bisa mengakses data partner manapun, sedangkan partner hanya dapat mengakses data dirinya sendiri.

    Response adalah data partner yang telah terupdate.
    """
    checkIfUserAuthorized(current_user, userId)
    return await UpdatePartner(userId, data)


@router_partner.put("/my_account", response_model=PartnerBasic)
async def update_partner_my_account(
    data: PartnerInput,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk update data partner berdasarkan user id.

    Wajib login sebagai admin, atau partner. Admin bisa mengakses data partner manapun, sedangkan partner hanya dapat mengakses data dirinya sendiri.

    Response adalah data partner yang telah terupdate.
    """
    # checkIfUserAuthorized(current_user, userId)
    return await UpdatePartner(current_user.userId, data)


@router_partner.delete("/{userId}")
async def delete_partner(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["", "*"])
):
    """
    Endpoint untuk update data partner berdasarkan user id. Wajib login sebagai admin.
    """
    checkIfUserAuthorized(current_user, userId)
    await DeletePartner(userId)
    return "Partner dengan id " + str(userId) + " berhasil dihapus"


# === CRUD LEAD ===


@router_partner.post("/my_account/leads", response_model=LeadsBasic)
async def add_lead_my_account(
    data: LeadsInput,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk menambah data lead partner. Wajib login sebagai admin, atau partner.
    """
    data = LeadsBasic(**data.dict())
    data = IsiDefault(data, current_user)
    return await CreateLead(data, current_user.userId)


@router_partner.post("/{userId}/leads", response_model=LeadsBasic)
async def add_lead(
    data: LeadsInput,
    userId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk menambah data lead partner. Wajib login sebagai admin, atau partner.
    """
    checkIfUserAuthorized(current_user, userId)
    partner = await GetPartnerOr404(userId)
    data = LeadsBasic(**data.dict())
    data = IsiDefault(data, current_user)
    data.companyId = partner["companyId"]
    print("Add lead")
    print("Lead: " + str(data))
    return await CreateLead(data, userId)


@router_partner.get("/my_account/leads", response_model=LeadPage)
async def get_all_leads_of_a_partner_my_account(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    # search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk mengambil semua data lead partner. Wajib login sebagai admin, atau partner.
    """
    # checkIfUserAuthorized(current_user, userId)
    partner = await GetPartnerOr404(current_user.userId)
    leads = partner["leads"]
    skip = page * size
    datas = leads[skip : skip + size]
    totalElements = len(leads)
    totalPages = math.ceil(totalElements / size)
    reply = LeadPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_partner.get("/{userId}/leads", response_model=LeadPage)
async def get_all_leads_of_a_partner(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    # search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Endpoint untuk mengambil semua data lead partner. Wajib login sebagai admin, atau partner.
    """
    checkIfUserAuthorized(current_user, userId)
    partner = await GetPartnerOr404(userId)
    leads = partner["leads"]
    skip = page * size
    datas = leads[skip : skip + size]
    totalElements = len(leads)
    totalPages = math.ceil(totalElements / size)
    reply = LeadPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


# @router_partner.get("/leads/{leadId}", response_model=LeadsBasic)
# async def get_lead(
#     leadId: str,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk mengambil data lead tunggal berdasarkan id lead. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa mengakses data lead partner lain.
#     """
#     partner = await GetPartnerBasedOnLeadId(leadId)
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     return await GetLeadOr404(leadId)


# @router_partner.put("/leads/{leadId}", response_model=LeadsBasic)
# async def update_lead(
#     leadId: str,
#     data: LeadsInput,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk update data lead tunggal berdasarkan id lead. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa update data lead partner lain.
#     """
#     partner = await GetPartnerBasedOnLeadId(leadId)
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     return await UpdateLead(leadId, data)


# @router_partner.delete("/leads/{leadId}")
# async def delete_lead(
#     leadId: str,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk hapus data lead tunggal berdasarkan id lead. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa hapus data lead partner lain.
#     """
#     partner = await GetPartnerBasedOnLeadId(leadId)
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     await DeleteLead(leadId)
#     return "Berhasil Hapus Lead dengan id " + str(leadId)


# # CRUD FOLLOWUP
# @router_partner.post("/leads/{leadId}/follow_up", response_model=FollowUpBasic)
# async def add_followup(
#     leadId: str,
#     followUpInput: FollowUpInput,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk menambah data follow up untuk satu lead. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa menambah data follow up untuk lead yang bukan miliknya.
#     """
#     partner = await GetPartnerBasedOnLeadId(leadId)
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     return await CreateFollowUp(leadId, followUpInput)


# @router_partner.get("/leads/{leadId}/follow_up", response_model=List[FollowUpBasic])
# async def get_all_followup_of_a_lead(
#     leadId: str,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk mengambil semua follow up untuk satu lead. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa mengakses data follow up untuk lead yang bukan miliknya.
#     """

#     partner = await GetPartnerBasedOnLeadId(leadId)
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     return await GetAllFollowUp(leadId)


# @router_partner.get("/leads/follow_up/{followUpId}", response_model=FollowUpBasic)
# async def get_followup_by_id_followup(
#     followUpId: str,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk mengambil data follow up berdasarkan id follow up. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa mengakses data follow up untuk lead yang bukan miliknya.
#     """

#     partner = await GetPartnerBasedOnFollowUpId(followUpId)
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     followUp = await GetFollowUpOr404(followUpId)
#     return followUp


# @router_partner.put("/leads/follow_up/{followUpId}", response_model=FollowUpBasic)
# async def update_followup(
#     followUpId: str,
#     data: FollowUpInput,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk update data follow up berdasarkan id follow up. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa update data follow up untuk lead yang bukan miliknya.
#     """
#     partner = GetPartnerBasedOnFollowUpId
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     return await UpdateFollowUp(followUpId, data)


# @router_partner.delete("/leads/follow_up/{followUpId}")
# async def delete_followup(
#     followUpId: str,
#     current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
# ):
#     """
#     Endpoint untuk hapus data follow up berdasarkan id follow up. Wajib login sebagai admin, atau partner.

#     Partner tidak bisa hapus data follow up untuk lead yang bukan miliknya.
#     """
#     partner = GetPartnerBasedOnFollowUpId
#     checkIfUserAuthorized(current_user, str(partner["userId"]))
#     await DeleteFollowUp(followUpId)
#     return "Berhasil Hapus Follow Up dengan id: " + str(followUpId)
