# from fastapi.datastructures import UploadFile
import os
import shutil
from util.util_excel import getString
from definitions import TEMPLATE_DOC_PATH
from fastapi.exceptions import HTTPException
from function.crm_katalis.partner import UpdateLead
import math
from model.util import FieldObjectIdRequest, SearchRequest
from model.membership.member import MemberBase, MemberCredential, MemberOnDB, MemberPage
from function.crm_katalis.member import CreateMember
from function.company import CreateCompany
from util.util import CreateCriteria, IsiDefault
from function.document import GetDocumentsOnUserId
from function.invoice import GetInvoicesOnUserId
from os import pipe
from bson.objectid import ObjectId
from model.support import (
    DocumentData,
    DocumentPage,
    InvoiceData,
    InvoicePage,
    PipelineData,
)
from function.user import (
    checkIfNoIdIsAlreadyRegistered,
    checkIfUserAuthorized,
)
from route.auth import get_current_user
from model.default import CompanyBasic, CompanyInput, JwtToken
from model.crm_katalis.katalis_lama import (
    CompanyOnDB,
    MemberTempBase,
    MemberTempOnDB,
    MemberTempPage,
)
from typing import Dict, List
from fastapi import APIRouter, Security, UploadFile, File, BackgroundTasks
from config.config import MGDB, CONF, MGDB_KATALIS, PROJECT_NAME
from model.crm_katalis.crmkatalis import (
    ClientBasic,
    ClientCredential,
    ClientInput,
    ClientPage,
    LeadsInput,
    MemberInput,
    # MemberBasic,
)
from function.crm_katalis.client import (
    CreateClient,
    CreateCompanyForClient,
    GetPipelineByNameOr404,
    GetPipelineByNomorOr404,
    GetClientOr404,
    GetAllClient,
    SetCompanyIdAndCompanyInitialForClient,
    UpdateClient,
    DeleteClient,
)

from function.crm_katalis.member import (
    GetMembersOnCompanyId,
)

import pandas as pd

from route.file import deleteGeneratedDocument

router_client = APIRouter()

# CRUD CLIENT
@router_client.post("", response_model=ClientBasic)
async def add_client(
    data: ClientInput,
    leadId: str = None,
    current_user: JwtToken = Security(get_current_user, scopes=["partner", "*"]),
):
    """
    Endpoint untuk menambahkan data client baru. Wajib login sebagai (super)admin, atau partner.
    """
    dbase = MGDB.user_crmkatalis_client
    await checkIfNoIdIsAlreadyRegistered(
        dbase, current_user.companyId, data.noId, "client"
    )

    data = ClientCredential(**data.dict())
    data = IsiDefault(data, current_user)
    client = await CreateClient(
        tblName="user_crmkatalis_client",
        user=data,
        password="pass",
        isFirstLogin="false",
        role=["client"],
    )

    if leadId:
        leadUpdate = LeadsInput()
        leadUpdate.convertedToClient = True
        await UpdateLead(leadId, leadUpdate)

    return client


@router_client.post("/get_all_clients", response_model=ClientPage)
async def get_all_client(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Endpoint untuk request seluruh data client yang ada di database. Wajib login sebagai admin.
    """
    skip = page * size
    criteria = CreateCriteria(search)
    dbase = MGDB.user_crmkatalis_client
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = ClientPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_client.get("/my_account", response_model=ClientBasic)
async def get_client_my_account(
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk mendapatkan data client. Wajib login sebagai client.
    """

    clientId = current_user.userId
    return await GetClientOr404(clientId)


@router_client.get("/{clientId}", response_model=ClientBasic)
async def get_client(
    clientId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Endpoint untuk mendapatkan data client berdasarkan user id. Hanya mengembalikan satu buah objek.

    Wajib login sebagai admin.
    """
    checkIfUserAuthorized(current_user, clientId)
    return await GetClientOr404(clientId)


@router_client.post("/{clientId}/create_company", response_model=CompanyOnDB)
async def create_company_for_client(
    data: CompanyInput,
    clientId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Endpoint untuk membuat company client. Digunakan saat client berada pada tahap "Create Company".

    Wajib login sebagai (super)admin.

    Response adalah data company yang baru dibuat.
    """
    return await CreateCompanyForClient(data, clientId, current_user)


@router_client.put("/my_account", response_model=ClientBasic)
async def update_client_my_account(
    data: ClientInput,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk update data client (untuk user client).
    Wajib login sebagai client. Client hanya dapat mengubah datanya sendiri.

    Response adalah data client yang telah terupdate.
    """
    clientId = current_user.userId
    return await UpdateClient(clientId, data)


@router_client.put("/{clientId}", response_model=ClientBasic)
async def update_client(
    clientId: str,
    data: ClientInput,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk update data client berdasarkan user id.

    Wajib login sebagai admin.

    Response adalah data client yang telah terupdate.
    """
    checkIfUserAuthorized(current_user, clientId)
    return await UpdateClient(clientId, data)


@router_client.delete("/{clientId}")
async def delete_client(
    clientId: str, current_user: JwtToken = Security(get_current_user, scopes=["", "*"])
):
    """
    Endpoint untuk update data client berdasarkan user id. Wajib login sebagai admin.
    """
    checkIfUserAuthorized(current_user, clientId)
    await DeleteClient(clientId)
    return "Client dengan id " + str(clientId) + " berhasil dihapus"


# === CRUD MEMBER ===


async def CreateMemberTemp(data: MemberTempBase):
    dbase = MGDB_KATALIS.crm_katalis_member_temp
    member = await dbase.insert_one(data.dict())
    if member.inserted_id:
        return await dbase.find_one({"_id": member.inserted_id})
    else:
        raise HTTPException(status_code=500, detail="Gagal Tambah Member")


async def AddMember(
    companyId: str, data: MemberInput, current_user: JwtToken, tipe: str
):
    dbase = MGDB.user_membership_member
    await checkIfNoIdIsAlreadyRegistered(dbase, companyId, data.noId, "member")
    data = data.dict()
    data["address"] = None
    data = MemberTempBase(**data)
    data.companyId = ObjectId(companyId)
    return await CreateMemberTemp(data)


@router_client.post("/my_account/members", response_model=MemberTempOnDB)
async def add_member_for_my_account(
    data: MemberInput,
    tipe: str = "python",
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Endpoint untuk menambah data member client. Wajib login sebagai client.
    """
    return await AddMember(current_user.companyId, data, current_user, "python")


@router_client.post("/{clientId}/members", response_model=MemberTempOnDB)
async def add_member(
    clientId: str,
    data: MemberInput,
    tipe: str = "python",
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Endpoint untuk menambah data member dari satu client. Wajib login sebagai admin.
    """
    client = await GetClientOr404(clientId)
    companyId = client["companyId"]
    return await AddMember(companyId, data, current_user, "python")


async def UploadMember(
    file: UploadFile,
    tipe: str,
    file_location: str,
    current_user: JwtToken,
    companyId: str,
):
    file_object = file.file
    upload = open(file_location, "wb+")
    shutil.copyfileobj(file_object, upload)
    upload.close()

    # get the data
    df = pd.read_excel(file_location)
    result = []
    for index, row in df.iterrows():
        name = getString(row["name"])
        member = MemberInput(name=name)
        member = await AddMember(companyId, member, current_user, tipe)
        member = MemberTempOnDB(**member)
        result.append(member)
    print("result: " + str(len(result)))
    return result


@router_client.post("/my_account/members_upload")
async def upload_member_for_my_account(
    background_tasks: BackgroundTasks,
    file: UploadFile = File(...),
    tipe: str = "python",
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    arrName = file.filename.split(".")
    if arrName[1] == "xls" or arrName[1] == "xlsx":
        originName = file.filename
        file_location = os.path.join(TEMPLATE_DOC_PATH, originName)
        background_tasks.add_task(deleteGeneratedDocument, file_location)
        return await UploadMember(
            file, "python", file_location, current_user, current_user.companyId
        )
    else:
        raise HTTPException(
            status_code=400, detail="Format file hanya boleh xls atau xlsx"
        )

@router_client.post("/{clientId}/members_upload")
async def upload_member_for_client_id(
    clientId:str,
    background_tasks: BackgroundTasks,
    file: UploadFile = File(...),
    tipe: str = "python",
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    arrName = file.filename.split(".")
    if arrName[1] == "xls" or arrName[1] == "xlsx":
        originName = file.filename
        file_location = os.path.join(TEMPLATE_DOC_PATH, originName)
        background_tasks.add_task(deleteGeneratedDocument, file_location)
        client = await GetClientOr404(clientId)
        companyId = client["companyId"]
        return await UploadMember(
            file, "python", file_location, current_user, companyId
        )
    else:
        raise HTTPException(
            status_code=400, detail="Format file hanya boleh xls atau xlsx"
        )


@router_client.post(
    "/my_account/members/get_all_members", response_model=MemberTempPage
)
async def get_all_members_of_client_my_account(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Endpoint untuk mengambil semua data member dari client. Wajib login sebagai client
    """

    members = await GetMembersOnCompanyId(current_user.companyId)
    skip = page * size
    datas = members[skip : skip + size]
    totalElements = len(members)
    totalPages = math.ceil(totalElements / size)
    reply = MemberTempPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_client.post("/{clientId}/members/get_all_members", response_model=MemberPage)
async def get_all_members_of_client(
    clientId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Endpoint untuk mengambil semua data member dari client. Wajib login sebagai admin.
    """
    checkIfUserAuthorized(current_user, clientId)
    client = await GetClientOr404(clientId)
    companyId = client["companyId"]
    members = await GetMembersOnCompanyId(companyId)
    skip = page * size
    datas = members[skip : skip + size]
    totalElements = len(members)
    totalPages = math.ceil(totalElements / size)
    reply = MemberTempPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_client.get("/my_account/pipelines", response_model=List[PipelineData])
async def get_all_pipelines_my_account(
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Get data pipelines client. Wajib login sebagai client.
    """
    client = await GetClientOr404(current_user.userId)
    pipelines = client["pipelines"]
    return pipelines


@router_client.get("/{clientId}/pipelines", response_model=List[PipelineData])
async def get_all_pipelines(
    clientId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Get data pipelines client. Wajib login sebagai admin.
    """
    client = await GetClientOr404(clientId)
    pipelines = client["pipelines"]
    return pipelines


@router_client.get("/{clientId}/pipeline/nomor/{nomor}", response_model=PipelineData)
async def get_pipeline_by_nomor(
    clientId: str,
    nomor: int,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Get data pipeline tunggal berdasarkan nomor pipeline. Wajib login sebagai admin.
    """
    return await GetPipelineByNomorOr404(clientId, nomor)


@router_client.get("/{clientId}/pipeline/name/{name}", response_model=PipelineData)
async def get_pipeline_by_name(
    clientId: str,
    name: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Get data pipeline tunggal berdasarkan nama pipeline. Wajib login sebagai admin.
    """
    return await GetPipelineByNameOr404(clientId, name)


@router_client.get("/my_account/pipeline/nomor/{nomor}", response_model=PipelineData)
async def get_pipeline_by_nomor(
    clientId: str,
    nomor: int,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Get data pipeline tunggal berdasarkan nomor pipeline. Wajib login sebagai client.
    """
    return await GetPipelineByNomorOr404(current_user.userId, nomor)


@router_client.get("/my_account/pipeline/name/{name}", response_model=PipelineData)
async def get_pipeline_by_name(
    clientId: str,
    name: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Get data pipeline tunggal berdasarkan nama pipeline. Wajib login sebagai client.
    """
    return await GetPipelineByNameOr404(current_user.userId, name)


@router_client.post("/my_account/invoice", response_model=InvoicePage)
async def get_client_invoices_my_account(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Get data invoice client. Wajib login sebagai client.
    """
    invoices = await GetInvoicesOnUserId(current_user.userId)
    skip = page * size
    datas = invoices[skip : skip + size]
    totalElements = len(invoices)
    totalPages = math.ceil(totalElements / size)
    reply = InvoicePage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_client.post("/{clientId}/invoice", response_model=InvoicePage)
async def get_client_invoices(
    clientId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Get data invoice client. Wajib login sebagai admin.
    """
    invoices = await GetInvoicesOnUserId(clientId)
    skip = page * size
    datas = invoices[skip : skip + size]
    totalElements = len(invoices)
    totalPages = math.ceil(totalElements / size)
    reply = InvoicePage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_client.post("/my_account/document", response_model=DocumentPage)
async def get_client_documents_my_account(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    """
    Get data dokumen client. Wajib login sebagai client.
    """
    documents = await GetDocumentsOnUserId(current_user.userId)
    skip = page * size
    datas = documents[skip : skip + size]
    totalElements = len(documents)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_client.post("/{clientId}/document", response_model=DocumentPage)
async def get_client_documents(
    clientId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Get data dokumen client. Wajib login sebagai admin.
    """
    documents = await GetDocumentsOnUserId(clientId)
    skip = page * size
    datas = documents[skip : skip + size]
    totalElements = len(documents)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply
