from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user

router_gtwclient = APIRouter()


# =================================================================================

@router_gtwclient.get("/cek_va_status", response_model=dict)
async def cek_va_status():
    # cek va number
    return {"status":"ok"}

@router_gtwclient.post("/create_va", response_model=dict)
async def create_va():
    # create va number
    return {"status":"ok"}

@router_gtwclient.put("/update_va", response_model=dict)
async def update_va():
    # update va number
    return {"status":"ok"}

@router_gtwclient.post("/delete_va", response_model=dict)
async def delete_va():
    # delete va number
    return {"status":"ok"}