from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.vasystem.institution import InstitutionBase, InstitutionOnDB, InstitutionPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user

router_institution = APIRouter()
dbase = MGDB.tbl_institution

async def GetInstitutionOr404(id: str):
    _id = ValidateObjectId(id)
    institution = await dbase.find_one({"_id": _id})
    if institution:
        return institution
    else:
        raise HTTPException(status_code=404, detail="Institution not found")

# =================================================================================

@router_institution.post("/institution", response_model=InstitutionOnDB)
async def add_institution(data_in: InstitutionBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    institution = IsiDefault(data_in, current_user)
    institution_op = await dbase.insert_one(institution.dict())
    if institution_op.inserted_id:
        institution = await GetInstitutionOr404(institution_op.inserted_id)
        return institution


@router_institution.post("/get_institution", response_model=dict)
async def get_all_institutions(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='userId',key=ObjectId(current_user.userId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = InstitutionPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_institution.get("/institution/{id}", response_model=InstitutionOnDB)
async def get_institution_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    institution = await dbase.find_one({"_id": id})
    if institution:
        return institution
    else:
        raise HTTPException(status_code=404, detail="Institution not found")


@router_institution.delete("/institution/{id}", dependencies=[Depends(GetInstitutionOr404)], response_model=dict)
async def delete_institution_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    institution_op = await dbase.delete_one({"_id": ObjectId(id)})
    if institution_op.deleted_count:
        return {"status": f"deleted count: {institution_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_institution.put("/institution/{id_}", response_model=InstitutionOnDB)
async def update_institution(id_: str, data_in: InstitutionBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    institution = IsiDefault(data_in, current_user, True)
    institution.updateTime = dateTimeNow()
    institution_op = await dbase.update_one(
        {"_id": ObjectId(id_)}, 
        {"$set": institution.dict(skip_defaults=True)}
    )
    if institution_op.modified_count:
        return await GetInstitutionOr404(id_)
    else:
        raise HTTPException(status_code=304)