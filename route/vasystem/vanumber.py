from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.vasystem.vanumber import VaNumberBase, VaNumberOnDB
from model.vasystem.institution import VaNumber, VaNumberPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import CreateArrayCriteria, ValidateObjectId, IsiDefault, CreateCriteria, DollarVaNumber
from route.auth import get_current_user

router_vanumber = APIRouter()
dbase = MGDB.tbl_institution

async def GetVaNumberOr404(id: str):
    _id = ValidateObjectId(id)
    vanumber = await dbase.find_one({"_id": _id})
    if vanumber:
        return vanumber
    else:
        raise HTTPException(status_code=404, detail="VaNumber not found")

# =================================================================================

@router_vanumber.post("/vanumber/{prefix}", response_model=VaNumber)
async def add_vanumber(prefix:str, vanumber: VaNumber, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    vanumber.id = ObjectId()
    vanumber.createTime = dateTimeNow()
    vanumber.updateTime = dateTimeNow()
    await dbase.update_one(
        {"userId": ObjectId(current_user.userId),"prefix":prefix},
        {"$addToSet": {"vaNumbers":vanumber.dict()}}
    )
    vaInsert = VaNumberBase()
    dataIn = IsiDefault(vaInsert, current_user)
    dataIn.vaNumber = vanumber.vaNumber
    #cek dulu, sebelum di insert, kalo uda ada di update aja
    await MGDB.tbl_vanumber.insert_one(dataIn.dict())
    return vanumber


@router_vanumber.post("/get_vanumber", response_model=dict)
async def get_all_vanumbers(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    criteria = CreateArrayCriteria('vaNumbers',search)
    sort = 'vaNumbers.'+sort
    paging = {'$facet':{'data':[{'$sort':{sort:dir}},{'$skip':skip},{'$limit':size}]}}
    count = {'$count':'total'}
    # print(criteria)
    # print([{'$unwind':{'path':'$vaNumbers'}},{'$project':{'_id':0,'vaNumbers':1}},{'$match':criteria},paging])
    datas_cursor = dbase.aggregate([{'$match':{"userId": ObjectId(current_user.userId)}},{'$unwind':{'path':'$vaNumbers'}},{'$project':{'_id':0,'vaNumbers':1}},{'$match':criteria},paging])
    datas = await datas_cursor.to_list(length=size)

    jml = dbase.aggregate([{'$match':{"userId": ObjectId(current_user.userId)}},{'$unwind':{'path':'$vaNumbers'}},{'$match':criteria},count])
    jmls = await jml.to_list(1000)
    total = 0
    if len(jmls) > 0 : total = jmls[0]['total']

    totalPages = math.ceil(total / size)
    reply = VaNumberPage(content=datas[0]['data'],size=size,page=page,totalElements=total,totalPages=totalPages,sortDirection=dir)
    return reply

@router_vanumber.get("/byvanumber/{vanumber}", response_model=VaNumber)
async def get_vanumber_by_number(vanumber: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    vanumber = await dbase.find_one({"userId": ObjectId(current_user.userId), "vaNumbers.vaNumber": vanumber},{"vaNumbers.$":1})
    if vanumber:
        return vanumber['vaNumbers'][0]
    else:
        raise HTTPException(status_code=404, detail="VaNumber not found")

@router_vanumber.get("/vanumber/{id}", response_model=VaNumber)
async def get_vanumber_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    vanumber = await dbase.find_one({"userId": ObjectId(current_user.userId), "vaNumbers.id": id},{"vaNumbers.$":1})
    if vanumber:
        return vanumber['vaNumbers'][0]
    else:
        raise HTTPException(status_code=404, detail="VaNumber not found")


@router_vanumber.delete("/vanumber/{id}", response_model=dict)
async def delete_vanumber_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    print({"userId": ObjectId(current_user.userId), "vaNumbers.id": id})
    vanumber_op = await dbase.update_one(
        {"userId": ObjectId(current_user.userId), "vaNumbers.id": id},
        {"$pull":{"vaNumbers": {"id": id}}}
    )
    if vanumber_op.modified_count:
        return {"status": f"deleted count: {vanumber_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_vanumber.put("/vanumber/{id}", response_model=dict)
async def update_vanumber(id: str, vanumber: VaNumber, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    vanumber.updateTime = dateTimeNow()
    updateData = DollarVaNumber(vanumber.dict(skip_defaults=True))
    print(updateData)
    vanumber_op = await dbase.update_one(
        {"userId": ObjectId(current_user.userId), "vaNumbers.id": ObjectId(id)},
        {"$set": updateData}
    )
    if vanumber_op.modified_count:
        return {"status": f"modified count: {vanumber_op.modified_count}"}
    else:
        raise HTTPException(status_code=304)