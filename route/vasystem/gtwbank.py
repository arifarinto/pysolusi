from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user

router_gtwbank = APIRouter()


# =================================================================================

@router_gtwbank.get("/inquiry", response_model=dict)
async def get_inquiry_va():
    # cek va number
    return {"status":"ok"}

@router_gtwbank.post("/payment", response_model=dict)
async def post_payment_va():
    # cek va number
    return {"status":"ok"}

@router_gtwbank.post("/advice", response_model=dict)
async def post_advice_va():
    # cek va number
    return {"status":"ok"}

@router_gtwbank.post("/reversal", response_model=dict)
async def post_reversal_va():
    # cek va number
    return {"status":"ok"}