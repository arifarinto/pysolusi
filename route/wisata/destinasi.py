# backend/tancho/destinasis/routes.py

from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.wisata.destinasi import DestinasiBase, DestinasiOnDB, DestinasiPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_destinasi = APIRouter()
dbase = MGDB.wisata_destinasi

async def GetDestinasiOr404(id_: str):
    _id = ValidateObjectId(id_)
    destinasi = await dbase.find_one({"_id": _id})
    if destinasi:
        return destinasi
    else:
        raise HTTPException(status_code=404, detail="Destinasi not found")

# =================================================================================

@router_destinasi.post("/destinasi", response_model=DestinasiOnDB)
async def add_destinasi(dataIn: DestinasiBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    destinasi = IsiDefault(dataIn, current_user)
    destinasi.name = destinasi.name.upper()
    cdestinasi = await dbase.find_one({
        "companyId": ObjectId(current_user.companyId), 
        "name":destinasi.name
    })
    if cdestinasi:
        raise HTTPException(status_code=400, detail="Destinasi is registered")
    destinasi_op = await dbase.insert_one(destinasi.dict())
    if destinasi_op.inserted_id:
        destinasi = await GetDestinasiOr404(destinasi_op.inserted_id)
        return destinasi

@router_destinasi.post("/get_destinasi", response_model=dict)
async def get_all_destinasis(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = DestinasiPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_destinasi.get("/destinasi/{id_}", response_model=DestinasiBase)
async def get_destinasi_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    destinasi = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if destinasi:
        return destinasi
    else:
        raise HTTPException(status_code=404, detail="Destinasi not found")


@router_destinasi.delete("/destinasi/{id_}", dependencies=[Depends(GetDestinasiOr404)], response_model=dict)
async def delete_destinasi_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    destinasi_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if destinasi_op.deleted_count:
        return {"status": f"deleted count: {destinasi_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_destinasi.put("/destinasi/{id_}", response_model=DestinasiOnDB)
async def update_destinasi(id_: str, dataIn: DestinasiBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    destinasi = IsiDefault(dataIn, current_user, True)
    destinasi.updateTime = dateTimeNow()
    destinasi_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": destinasi.dict(skip_defaults=True)}
    )
    if destinasi_op.modified_count:
        return await GetDestinasiOr404(id_)
    else:
        raise HTTPException(status_code=304)