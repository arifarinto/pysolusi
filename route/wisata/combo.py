from io import BytesIO
from starlette.responses import Response
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime, timedelta
import logging
import math
import pandas as pd

from model.wisata.pengunjung import comboPengunjung
from model.wisata.gateway import comboGateway
from model.wisata.kasir import comboKasir
from model.wisata.miniatm import comboMiniatm
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_combo_wisata = APIRouter()

# ========================================================================================
# combo pengunjung
@router_combo_wisata.get("/get_combo_pengunjung", response_model=List[comboPengunjung])
async def get_combo_pengunjung(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)

    pengunjungs = (
        await MGDB.user_wisata_pengunjung.find(
            criteria, {"userId": 1, "name": 1, "noId": 1}
        )
        .sort("name", 1)
        .to_list(1000)
    )
    return pengunjungs


@router_combo_wisata.get(
    "/get_combo_pengunjung/{userId}", response_model=comboPengunjung
)
async def get_combo_pengunjung_by_user_id_pengunjung(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    criteria["userId"] = ObjectId(userId)

    pengunjungs = await MGDB.user_wisata_pengunjung.find_one(
        criteria, {"userId": 1, "name": 1, "noId": 1}
    )
    return pengunjungs


# ========================================================================================
# combo kasir
@router_combo_wisata.get("/get_combo_kasir", response_model=List[comboKasir])
async def get_combo_kasir(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)

    kasirs = (
        await MGDB.user_wisata_kasir.find(criteria, {"userId": 1, "name": 1, "noId": 1})
        .sort("name", 1)
        .to_list(1000)
    )
    return kasirs


@router_combo_wisata.get("/get_combo_kasir/{userId}", response_model=comboKasir)
async def get_combo_kasir_by_user_id_kasir(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    criteria["userId"] = ObjectId(userId)

    kasirs = await MGDB.user_wisata_kasir.find_one(
        criteria, {"userId": 1, "name": 1, "noId": 1}
    )
    return kasirs


# ========================================================================================
# combo miniatm
@router_combo_wisata.get("/get_combo_miniatm", response_model=List[comboMiniatm])
async def get_combo_miniatm(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)

    miniatms = (
        await MGDB.user_wisata_miniatm.find(
            criteria, {"userId": 1, "name": 1, "noId": 1}
        )
        .sort("name", 1)
        .to_list(1000)
    )
    return miniatms


@router_combo_wisata.get("/get_combo_miniatm/{userId}", response_model=comboMiniatm)
async def get_combo_miniatm_by_user_id_miniatm(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    criteria["userId"] = ObjectId(userId)

    miniatms = await MGDB.user_wisata_miniatm.find_one(
        criteria, {"userId": 1, "name": 1, "noId": 1}
    )
    return miniatms


# ========================================================================================
# combo gateway
@router_combo_wisata.get("/get_combo_gateway", response_model=List[comboGateway])
async def get_combo_gateway(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)

    gateways = (
        await MGDB.user_wisata_gateway.find(
            criteria, {"userId": 1, "name": 1, "noId": 1}
        )
        .sort("name", 1)
        .to_list(1000)
    )
    return gateways


@router_combo_wisata.get("/get_combo_gateway/{userId}", response_model=comboGateway)
async def get_combo_gateway_by_user_id_gateway(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    criteria["userId"] = ObjectId(userId)

    gateways = await MGDB.user_wisata_gateway.find_one(
        criteria, {"userId": 1, "name": 1, "noId": 1}
    )
    return gateways
