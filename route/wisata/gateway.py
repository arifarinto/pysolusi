# backend/tancho/gateways/routes.py

from function.user import CreateUser
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.wisata.gateway import GatewayBase, GatewayCredential, GatewayOnDB, GatewayPage, comboGateway
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import IdentityData, JwtToken, UserInput
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import convertDateToStrPassword, dateTimeNow

router_gateway = APIRouter()
dbase = MGDB.user_wisata_gateway

async def GetGatewayOr404(userId: str):
    userId = ValidateObjectId(userId)
    gateway = await dbase.find_one({"userId": userId})
    if gateway:
        return gateway
    else:
        raise HTTPException(status_code=404, detail="Gateway not found")

# =================================================================================


@router_gateway.post("/gateway", response_model=GatewayBase)
async def add_gateway(dataIn: UserInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    gateway = GatewayCredential(**dataIn.dict())
    gateway = IsiDefault(gateway, current_user)
    gateway.userId = ObjectId()

    cgateway = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "noId": gateway.noId})
    if cgateway:
        raise HTTPException(status_code=400, detail="Gateway with NIP : "+gateway.noId+" is registered")
    
    gateway.name = gateway.name.upper()
    gateway.tags = ListToUp(gateway.tags)
    gateway.username = gateway.noId

    identity = IdentityData()
    if gateway.identity.dateOfBirth:
        identity.dateOfBirth = gateway.identity.dateOfBirth
        password = convertDateToStrPassword(gateway.identity.dateOfBirth)
    else:
        password = gateway.noId

    gateway.identity = identity
    gateway_op = await CreateUser("user_wisata_gateway", gateway, password, True, ['gateway'])
    return gateway_op

@router_gateway.post("/upload_gateway")
async def upload_gateway(file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    dataIn = GatewayCredential()
    for index,row in df.iterrows():
        gateway = IsiDefault(dataIn, current_user)
        gateway.noId = getString(row['noid'])
        gateway.name = getString(row['name'].upper())
        gateway.note = getString(row['note'])
        gateway.identity.dateOfBirth = getString(row['dateOfBirth'])
        gateway.tags = getString(row['tags'])
        if gateway.tags:
            gateway.tags = getString(row['Tags']).split(",")
            gateway.tags = ListToUp(gateway.tags)
        else:
            gateway.tags = []

        cgateway = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "noId": gateway.noId})
        if cgateway:
            rowNonvalid = rowNonvalid + 1
            unValid.append({
                "name":gateway.name,
                "noId":gateway.noId,
                "note":"telah terdaftar"
                })
            pass
        else:
            gateway.userId = ObjectId()
            identity = IdentityData()
            if gateway.identity.dateOfBirth:
                identity.dateOfBirth = gateway.identity.dateOfBirth
                password = convertDateToStrPassword(gateway.identity.dateOfBirth)
            else:
                password = gateway.noId

            gateway.identity = identity
            await CreateUser("user_wisata_gateway", gateway, password, True, ['gateway'])

    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}

@router_gateway.post("/get_gateway", response_model=dict)
async def get_all_gateways(size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = GatewayPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_gateway.get("/get_gateway_by_userid/{userId}", response_model=GatewayBase)
async def get_gateway_by_userid(userId: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    gateway = await dbase.find_one({"userId": userId,"companyId": ObjectId(current_user.companyId)})
    if gateway:
        return gateway
    else:
        raise HTTPException(status_code=404, detail="Gateway not found")


@router_gateway.delete("/delete_gateway_by_userid/{userId}", dependencies=[Depends(GetGatewayOr404)], response_model=dict)
async def delete_gateway_by_userid(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    gateway_op = await dbase.delete_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)})
    if gateway_op.deleted_count:
        return {"status": f"deleted count: {gateway_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_gateway.put("/update_gateway_by_userid/{userId}", response_model=GatewayOnDB)
async def update_gateway_by_userid(userId: str, dataIn: GatewayBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # gateway = IsiDefault(dataIn, current_user, True)
    dataIn.updateTime = dateTimeNow()
    gateway_op = await dbase.update_one(
        {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": dataIn.dict(skip_defaults=True)}
    )
    if gateway_op.modified_count or gateway_op.matched_count:
        return await GetGatewayOr404(userId)
    else:
        raise HTTPException(status_code=304)