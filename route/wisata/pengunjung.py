# backend/tancho/pengunjungs/routes.py

from function.user import CreateUser
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd
from model.support import InvoiceData, InvoicePage, PipelineData, InvoiceInput, InvoiceOnDB, PaymentStatusEnum
from function.invoice import (
    CreateInvoice,
    DeleteInvoice,
    GetInvoiceBaseOnMonthAndYear,
    GetInvoiceOr404,
    GetInvoicesByCompanyId,
    GetInvoicesOnUserId,
    UpdateInvoice,
)

from model.wisata.pengunjung import PengunjungBase, PengunjungCredential, PengunjungOnDB, PengunjungPage, comboPengunjung
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import IdentityData, JwtToken, UserInput
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import convertDateToStrPassword, dateTimeNow

router_pengunjung = APIRouter()
dbase = MGDB.user_wisata_pengunjung

async def GetPengunjungOr404(userId: str):
    userId = ValidateObjectId(userId)
    pengunjung = await dbase.find_one({"userId": userId})
    if pengunjung:
        return pengunjung
    else:
        raise HTTPException(status_code=404, detail="Pengunjung not found")

# =================================================================================


@router_pengunjung.post("/pengunjung", response_model=PengunjungBase)
async def add_pengunjung(dataIn: UserInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengunjung = PengunjungCredential(**dataIn.dict())
    pengunjung = IsiDefault(pengunjung, current_user)
    pengunjung.userId = ObjectId()

    cpengunjung = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "noId": pengunjung.noId})
    if cpengunjung:
        raise HTTPException(status_code=400, detail="Pengunjung with NIP : "+pengunjung.noId+" is registered")
    
    pengunjung.name = pengunjung.name.upper()
    pengunjung.tags = ListToUp(pengunjung.tags)
    pengunjung.username = pengunjung.noId

    identity = IdentityData()
    if pengunjung.identity.dateOfBirth:
        identity.dateOfBirth = pengunjung.identity.dateOfBirth
        password = convertDateToStrPassword(pengunjung.identity.dateOfBirth)
    else:
        password = pengunjung.noId

    pengunjung.identity = identity
    pengunjung_op = await CreateUser("user_wisata_pengunjung", pengunjung, password, True, ['pengunjung'])
    return pengunjung_op


@router_pengunjung.post("/get_pengunjung", response_model=dict)
async def get_all_pengunjungs(size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = PengunjungPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_pengunjung.get("/get_pengunjung_by_userid/{userId}", response_model=PengunjungBase)
async def get_pengunjung_by_userid(userId: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengunjung = await dbase.find_one({"userId": userId,"companyId": ObjectId(current_user.companyId)})
    if pengunjung:
        return pengunjung
    else:
        raise HTTPException(status_code=404, detail="Pengunjung not found")


@router_pengunjung.delete("/delete_pengunjung_by_userid/{userId}", dependencies=[Depends(GetPengunjungOr404)], response_model=dict)
async def delete_pengunjung_by_userid(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengunjung_op = await dbase.delete_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)})
    if pengunjung_op.deleted_count:
        return {"status": f"deleted count: {pengunjung_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_pengunjung.put("/update_pengunjung_by_userid/{userId}", response_model=PengunjungOnDB)
async def update_pengunjung_by_userid(userId: str, dataIn: PengunjungBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # pengunjung = IsiDefault(dataIn, current_user, True)
    dataIn.updateTime = dateTimeNow()
    pengunjung_op = await dbase.update_one(
        {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": dataIn.dict(skip_defaults=True)}
    )
    if pengunjung_op.modified_count or pengunjung_op.matched_count:
        return await GetPengunjungOr404(userId)
    else:
        raise HTTPException(status_code=304)