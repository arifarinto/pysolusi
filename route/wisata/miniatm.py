# backend/tancho/miniatms/routes.py

from function.user import CreateUser
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd
from model.support import InvoiceData, InvoicePage, PipelineData, InvoiceInput, InvoiceOnDB, PaymentStatusEnum
from function.invoice import (
    CreateInvoice,
    DeleteInvoice,
    GetInvoiceBaseOnMonthAndYear,
    GetInvoiceOr404,
    GetInvoicesByCompanyId,
    GetInvoicesOnUserId,
    UpdateInvoice,
)

from model.wisata.miniatm import MiniatmBase, MiniatmCredential, MiniatmOnDB, MiniatmPage, comboMiniatm
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import IdentityData, JwtToken, UserInput
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import convertDateToStrPassword, dateTimeNow

router_miniatm = APIRouter()
dbase = MGDB.user_wisata_miniatm

async def GetMiniatmOr404(userId: str):
    userId = ValidateObjectId(userId)
    miniatm = await dbase.find_one({"userId": userId})
    if miniatm:
        return miniatm
    else:
        raise HTTPException(status_code=404, detail="Miniatm not found")

# =================================================================================


@router_miniatm.post("/miniatm", response_model=MiniatmBase)
async def add_miniatm(dataIn: UserInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    miniatm = MiniatmCredential(**dataIn.dict())
    miniatm = IsiDefault(miniatm, current_user)
    miniatm.userId = ObjectId()

    cminiatm = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "noId": miniatm.noId})
    if cminiatm:
        raise HTTPException(status_code=400, detail="Miniatm with NIP : "+miniatm.noId+" is registered")
    
    miniatm.name = miniatm.name.upper()
    miniatm.tags = ListToUp(miniatm.tags)
    miniatm.username = miniatm.noId

    identity = IdentityData()
    if miniatm.identity.dateOfBirth:
        identity.dateOfBirth = miniatm.identity.dateOfBirth
        password = convertDateToStrPassword(miniatm.identity.dateOfBirth)
    else:
        password = miniatm.noId

    miniatm.identity = identity
    miniatm_op = await CreateUser("user_wisata_miniatm", miniatm, password, True, ['miniatm'])
    return miniatm_op

@router_miniatm.post("/upload_miniatm")
async def upload_miniatm(file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    dataIn = MiniatmCredential()
    for index,row in df.iterrows():
        miniatm = IsiDefault(dataIn, current_user)
        miniatm.noId = getString(row['noid'])
        miniatm.name = getString(row['name'].upper())
        miniatm.note = getString(row['note'])
        miniatm.identity.dateOfBirth = getString(row['dateOfBirth'])
        miniatm.tags = getString(row['tags'])
        if miniatm.tags:
            miniatm.tags = getString(row['Tags']).split(",")
            miniatm.tags = ListToUp(miniatm.tags)
        else:
            miniatm.tags = []

        cminiatm = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "noId": miniatm.noId})
        if cminiatm:
            rowNonvalid = rowNonvalid + 1
            unValid.append({
                "name":miniatm.name,
                "noId":miniatm.noId,
                "note":"telah terdaftar"
                })
            pass
        else:
            miniatm.userId = ObjectId()
            identity = IdentityData()
            if miniatm.identity.dateOfBirth:
                identity.dateOfBirth = miniatm.identity.dateOfBirth
                password = convertDateToStrPassword(miniatm.identity.dateOfBirth)
            else:
                password = miniatm.noId

            miniatm.identity = identity
            await CreateUser("user_wisata_miniatm", miniatm, password, True, ['miniatm'])

    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}

@router_miniatm.post("/get_miniatm", response_model=dict)
async def get_all_miniatms(size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = MiniatmPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_miniatm.get("/get_miniatm_by_userid/{userId}", response_model=MiniatmBase)
async def get_miniatm_by_userid(userId: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    miniatm = await dbase.find_one({"userId": userId,"companyId": ObjectId(current_user.companyId)})
    if miniatm:
        return miniatm
    else:
        raise HTTPException(status_code=404, detail="Miniatm not found")


@router_miniatm.delete("/delete_miniatm_by_userid/{userId}", dependencies=[Depends(GetMiniatmOr404)], response_model=dict)
async def delete_miniatm_by_userid(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    miniatm_op = await dbase.delete_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)})
    if miniatm_op.deleted_count:
        return {"status": f"deleted count: {miniatm_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_miniatm.put("/update_miniatm_by_userid/{userId}", response_model=MiniatmOnDB)
async def update_miniatm_by_userid(userId: str, dataIn: MiniatmBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # miniatm = IsiDefault(dataIn, current_user, True)
    dataIn.updateTime = dateTimeNow()
    miniatm_op = await dbase.update_one(
        {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": dataIn.dict(skip_defaults=True)}
    )
    if miniatm_op.modified_count or miniatm_op.matched_count:
        return await GetMiniatmOr404(userId)
    else:
        raise HTTPException(status_code=304)