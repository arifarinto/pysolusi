# backend/tancho/kasirs/routes.py

from function.user import CreateUser
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd
from model.support import InvoiceData, InvoicePage, PipelineData, InvoiceInput, InvoiceOnDB, PaymentStatusEnum
from function.invoice import (
    CreateInvoice,
    DeleteInvoice,
    GetInvoiceBaseOnMonthAndYear,
    GetInvoiceOr404,
    GetInvoicesByCompanyId,
    GetInvoicesOnUserId,
    UpdateInvoice,
)

from model.wisata.pengunjung import PengunjungBase, TiketData, TiketEnum, TiketInput
from model.wisata.kasir import KasirBase, KasirCredential, KasirOnDB, KasirPage, comboKasir
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import IdentityData, JwtToken, UserInput
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import convertDateToStrPassword, dateTimeNow

router_kasir = APIRouter()
dbase = MGDB.user_wisata_kasir

async def GetKasirOr404(userId: str):
    userId = ValidateObjectId(userId)
    kasir = await dbase.find_one({"userId": userId})
    if kasir:
        return kasir
    else:
        raise HTTPException(status_code=404, detail="Kasir not found")

# =================================================================================

@router_kasir.post("/kasir", response_model=KasirBase)
async def add_kasir(dataIn: UserInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    kasir = KasirCredential(**dataIn.dict())
    kasir = IsiDefault(kasir, current_user)
    kasir.userId = ObjectId()

    ckasir = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "noId": kasir.noId})
    if ckasir:
        raise HTTPException(status_code=400, detail="Kasir with NIP : "+kasir.noId+" is registered")
    
    kasir.name = kasir.name.upper()
    kasir.tags = ListToUp(kasir.tags)
    kasir.username = kasir.noId

    identity = IdentityData()
    if kasir.identity.dateOfBirth:
        identity.dateOfBirth = kasir.identity.dateOfBirth
        password = convertDateToStrPassword(kasir.identity.dateOfBirth)
    else:
        password = kasir.noId

    kasir.identity = identity
    kasir_op = await CreateUser("user_wisata_kasir", kasir, password, True, ['kasir'])
    return kasir_op

@router_kasir.post("/upload_kasir")
async def upload_kasir(file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    dataIn = KasirCredential()
    for index,row in df.iterrows():
        kasir = IsiDefault(dataIn, current_user)
        kasir.noId = getString(row['noid'])
        kasir.name = getString(row['name'].upper())
        kasir.note = getString(row['note'])
        kasir.identity.dateOfBirth = getString(row['dateOfBirth'])
        kasir.tags = getString(row['tags'])
        if kasir.tags:
            kasir.tags = getString(row['Tags']).split(",")
            kasir.tags = ListToUp(kasir.tags)
        else:
            kasir.tags = []

        ckasir = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "noId": kasir.noId})
        if ckasir:
            rowNonvalid = rowNonvalid + 1
            unValid.append({
                "name":kasir.name,
                "noId":kasir.noId,
                "note":"telah terdaftar"
                })
            pass
        else:
            kasir.userId = ObjectId()
            identity = IdentityData()
            if kasir.identity.dateOfBirth:
                identity.dateOfBirth = kasir.identity.dateOfBirth
                password = convertDateToStrPassword(kasir.identity.dateOfBirth)
            else:
                password = kasir.noId

            kasir.identity = identity
            await CreateUser("user_wisata_kasir", kasir, password, True, ['kasir'])

    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}

@router_kasir.post("/get_kasir", response_model=dict)
async def get_all_kasirs(size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = KasirPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_kasir.get("/get_kasir_by_userid/{userId}", response_model=KasirBase)
async def get_kasir_by_userid(userId: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    kasir = await dbase.find_one({"userId": userId,"companyId": ObjectId(current_user.companyId)})
    if kasir:
        return kasir
    else:
        raise HTTPException(status_code=404, detail="Kasir not found")


@router_kasir.delete("/delete_kasir_by_userid/{userId}", dependencies=[Depends(GetKasirOr404)], response_model=dict)
async def delete_kasir_by_userid(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    kasir_op = await dbase.delete_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)})
    if kasir_op.deleted_count:
        return {"status": f"deleted count: {kasir_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_kasir.put("/update_kasir_by_userid/{userId}", response_model=KasirOnDB)
async def update_kasir_by_userid(userId: str, dataIn: KasirBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # kasir = IsiDefault(dataIn, current_user, True)
    dataIn.updateTime = dateTimeNow()
    kasir_op = await dbase.update_one(
        {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": dataIn.dict(skip_defaults=True)}
    )
    if kasir_op.modified_count or kasir_op.matched_count:
        return await GetKasirOr404(userId)
    else:
        raise HTTPException(status_code=304)