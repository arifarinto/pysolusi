# backend/tancho/wahanas/routes.py

from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.wisata.destinasi import WahanaData, WahanaJadwalData, DestinasiBase, DestinasiOnDB, DestinasiPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, RandomString, ValidateObjectId, IsiDefault, CreateCriteria, CreateArrayCriteria
from route.auth import get_current_user
from util.util_waktu import convertStrDateToDate, dateTimeNow

router_wahana = APIRouter()
dbase = MGDB.wisata_destinasi

async def GetWahanaOr404(id_: str):
    _id = ValidateObjectId(id_)
    wahana = await dbase.find_one({"_id": _id})
    if wahana:
        return wahana
    else:
        raise HTTPException(status_code=404, detail="Wahana not found")

# =================================================================================
# wahana

@router_wahana.post("/wahana/{idDestinasi}", response_model=WahanaData)
async def add_wahana(idDestinasi:str, dataIn: WahanaData, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    destinasi_op = await dbase.update_one(
                { "_id": ObjectId(idDestinasi)},
                { "$addToSet": { "wahana": dataIn.dict()}}
            )
    if destinasi_op.modified_count:
        return dataIn
    else:
        raise HTTPException(status_code=304)


@router_wahana.get("/get_all_wahana/{companyId}/{idDestinasi}", response_model=dict)
async def get_all_wahana_destinasi(companyId: str ,idDestinasi: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pipeline = [
            {"$unwind": {"path": "$wahana"}},
            {"$match": {"companyId": ObjectId(companyId), "_id": ObjectId(idDestinasi)}},
            {"$replaceRoot": {"newRoot": "$wahana"}},
            {
                "$project": {
                    "id": "$id",
                    "namaWahana": "$namaWahana"
                }
            }
        ]
    wahana = await dbase.aggregate(pipeline).to_list(10000)
    if not wahana:
        raise HTTPException (status_code=404, detail= "wahana tidak ditemukan")
    data_wahana = {}
    data_wahana["destinasi"] = idDestinasi
    data_wahana['wahana'] = wahana
    return data_wahana


@router_wahana.get("/get_wahana/{idDestinasi}/{idWahana}", response_model=WahanaData)
async def get_wahana_destinasi(idDestinasi: str, idWahana: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    # wahana = await dbase.find_one({"idDestinasi": ObjectId(idDestinasi), "wahana.id": id})
    pipeline = [
        {"$match": {"_id": ObjectId(idDestinasi)}},
        {"$unwind": {"path": "$wahana"}},
        {"$replaceRoot": {"newRoot": "$wahana"}},
        {"$match": {"id": idWahana}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        wahana = result[0]
        return wahana
    else:
        raise HTTPException (status_code=404, detail= "wahana tidak ditemukan")


@router_wahana.delete("/wahana/{idDestinasi}/{idWahana}", response_model=dict)
async def delete_wahana(idDestinasi: str, idWahana: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pipeline = [
        {"$match": {"_id": ObjectId(idDestinasi)}},
        {"$unwind": {"path": "$wahana"}},
        {"$replaceRoot": {"newRoot": "$wahana"}},
        {"$match": {"id": idWahana}}
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if not result:
        raise HTTPException (status_code=404, detail= "wahana tidak ditemukan")

    wahana_op = await dbase.update_one(
        {"_id": ObjectId(idDestinasi),"wahana.id": idWahana},
        {"$pull":{"wahana": {"id": idWahana}}}
    )
    if wahana_op.modified_count:
        return {"status": f"deleted count: {wahana_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_wahana.put("/wahana/{idDestinasi}/{idWahana}", response_model=dict)
async def update_wahana(idDestinasi: str, idWahana:str, dataIn: WahanaData, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pipeline = [
        {"$match": {"_id": ObjectId(idDestinasi)}},
        {"$unwind": {"path": "$wahana"}},
        {"$replaceRoot": {"newRoot": "$wahana"}},
        {"$match": {"id": idWahana}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if not result:
        raise HTTPException (status_code=404, detail= "wahana tidak ditemukan")
    wahana = result[0]
    dataIn = dataIn.dict(skip_defaults=True)
    dataIn = {k: v for k, v in dataIn.items() if v is not None}
    wahana.update(dataIn)
    wahana_op = await dbase.update_one(
        {"_id": ObjectId(idDestinasi), "wahana.id": idWahana},
        {"$set": {"wahana.$": wahana}},
    )
    if wahana_op.modified_count:
        return wahana
    else:
        raise HTTPException(status_code=304)

# =================================================================================
# jadwal wahana

@router_wahana.post("/jadwal_wahana/{idDestinasi}/{idWahana}", response_model=WahanaJadwalData)
async def add_jadwal_wahana(idDestinasi : str, idWahana:str, dataIn: WahanaJadwalData, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    jadwal_op = await dbase.update_one(
        {"_id": ObjectId(idDestinasi), "wahana.id": idWahana},
        {"$addToSet": {"wahana.$.jadwal": dataIn.dict()}},
    )
    if jadwal_op.modified_count:
        return dataIn
    else:
        raise HTTPException(status_code=304)


@router_wahana.get("/get_all_jadwal/{companyId}/{idDestinasi}/{idWahana}", response_model=dict)
async def get_all_wahana_destinasi(companyId: str ,idDestinasi: str, idWahana: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pipeline = [
            {"$match": {"companyId": ObjectId(companyId), "_id": ObjectId(idDestinasi)}},
            {"$unwind": {"path": "$wahana"}},
            {"$replaceRoot": {"newRoot": "$wahana"}},
            {"$match": {"id": idWahana}},
            {"$unwind": {"path":"$jadwal"}},
            {"$replaceRoot": {"newRoot": "$jadwal"}},
            {
                "$project": {
                    "idJadwal": "$idJadwal",
                    "tanggal": "$tanggal",
                    "isOpen" : "$isOpen",
                    "capacity" : "$capacity",
                    "price" : "$price"
                }
            }
        ]
    jadwal = await dbase.aggregate(pipeline).to_list(10000)
    if not jadwal:
        raise HTTPException (status_code=404, detail= "data jadwal wahana tidak ditemukan")
    data_jadwal = {}
    data_jadwal["destinasi"] = idDestinasi
    data_jadwal["wahana"] = idWahana
    data_jadwal['jadwals'] = jadwal
    return data_jadwal


@router_wahana.get("/get_jadwal_wahana/{idDestinasi}/{idWahana}/{idJadwal}", response_model=WahanaJadwalData)
async def get_jadwal_wahana_by_idJadwal(idDestinasi: str, idWahana: str, idJadwal :str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    pipeline = [
            {"$match": {"_id": ObjectId(idDestinasi)}},
            {"$unwind": {"path": "$wahana"}},
            {"$replaceRoot": {"newRoot": "$wahana"}},
            {"$match": {"id": idWahana}},
            {"$unwind": {"path":"$jadwal"}},
            {"$replaceRoot": {"newRoot": "$jadwal"}},
            {"$match": {"idJadwal": idJadwal}},   
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        jadwalwahana = result[0]
        return jadwalwahana
    else:
        raise HTTPException (status_code=404, detail= "jadwal wahana tidak ditemukan")   


@router_wahana.delete("/jadwal_wahana/{idDestinasi}/{idWahana}/{idjadwal}", response_model=dict)
async def delete_jadwal_wahana(idDestinasi: str, idWahana: str, idJadwal: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pipeline = [
            {"$match": {"_id": ObjectId(idDestinasi)}},
            {"$unwind": {"path": "$wahana"}},
            {"$replaceRoot": {"newRoot": "$wahana"}},
            {"$match": {"id": idWahana}},
            {"$unwind": {"path":"$jadwal"}},
            {"$replaceRoot": {"newRoot": "$jadwal"}},
            {"$match": {"idJadwal": idJadwal}}, 
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if not result:
        raise HTTPException (status_code=404, detail= "jadwal wahana tidak ditemukan")
    jadwal_op = await dbase.update_one(
        {},
        {"$pull": {"wahana.$[].jadwal": {"idJadwal": idJadwal}}},
    )
    if jadwal_op.modified_count:
        return {"status": f"deleted count: {jadwal_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_wahana.put("/jadwal_wahana/{idDestinasi}/{idWahana}/{idJadwal}", response_model=dict)
async def update_jadwal_wahana(idDestinasi: str, idWahana: str, idJadwal: str, dataIn: WahanaJadwalData, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pipeline = [
            {"$match": {"_id": ObjectId(idDestinasi)}},
            {"$unwind": {"path": "$wahana"}},
            {"$replaceRoot": {"newRoot": "$wahana"}},
            {"$match": {"id": idWahana}},
            {"$unwind": {"path":"$jadwal"}},
            {"$replaceRoot": {"newRoot": "$jadwal"}},
            {"$match": {"idJadwal": idJadwal}},  
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if not result:
        raise HTTPException (status_code=404, detail= "jadwal wahana tidak ditemukan")
    jadwal = result[0]
    dataIn = dataIn.dict(skip_defaults=True)
    dataIn = {k: v for k, v in dataIn.items() if v is not None}
    jadwal.update(dataIn)
    jadwal_op = await dbase.update_one(
        {"_id": ObjectId(idDestinasi)},
        {"$set": {"wahana.$[w].jadwal.$[j]": jadwal}},
        array_filters=[
            {"w.id": idWahana},
            {"j.idJadwal": idJadwal},
        ],
    )
    if jadwal_op.modified_count:
        return jadwal
    else:
        raise HTTPException(status_code=304)