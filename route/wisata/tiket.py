from io import BytesIO
from pydantic.networks import HttpUrl
from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Response ,Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.wisata.miniatm import MiniatmBase, MiniatmCredential, MiniatmOnDB, MiniatmPage, comboMiniatm
from model.wisata.kasir import KasirBase, KasirCredential, KasirOnDB, KasirPage, comboKasir
from model.wisata.pengunjung import PengunjungBase, TiketData, TiketEnum, TiketInput
from model.wisata.destinasi import WahanaData, WahanaJadwalData, DestinasiBase, DestinasiOnDB, DestinasiPage
from model.util import ObjectIdStr, SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria, CreateArrayCriteria
from route.auth import get_current_user
from util.util_waktu import convertStrDateToDate


router_tiket = APIRouter()
# =================================================================================

@router_tiket.get("/combo_jenis_tiket",response_model=list)
async def get_combo_jenis_tiket():
    return list(TiketEnum)


@router_tiket.post("/create_tiket/{tipe}/{userId}", response_model=TiketData)
async def add_tiket_every_type(tipe: str, userId: str,dataIn: TiketInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    """
    Types:
    > *pjg untuk pengunjung, maka userId adalah userId pengunjung dan wajib login sebagai pengunjung.
    
    > *ksr untuk kasir, maka userId adalah userId kasir dan wajib login sebagai kasir.
    
    > *atm untuk miniatm, maka userId adalah userId miniatm dan wajib login sebagai miniatm.
    """ 
    # cek data destinasi dan data wahana utk dapat harga, nama destinasi, dan wahana
    tiket = TiketData(**dataIn.dict())
    tiket.id = ObjectId()
    jumlah = tiket.jumlah
    pipeline = [
        {"$match": {"_id": ObjectId(tiket.idDestinasi)}},
        {"$unwind": {"path": "$wahana"}},
        {"$replaceRoot": {"newRoot": "$wahana"}},
        {"$match": {"id": tiket.idWahana}},
        {"$unwind": {"path": "$jadwal"}},
        {"$replaceRoot": {"newRoot": "$jadwal"}},
        {"$match": {"idJadwal": tiket.idJadwal}}
    ]
    results = await MGDB.wisata_destinasi.aggregate(pipeline).to_list(1000)
    wahanaJadwalData = None
    if results:
        wahanaJadwalData = results[0]
    else:
        raise HTTPException (status_code=404, detail= "wahana tidak ditemukan")
    price = wahanaJadwalData["price"]
    tiket.totalHarga = price*jumlah

    if tipe == "pjg":
        tiket_op = await MGDB.user_wisata_pengunjung.update_one(
                { "userId": ObjectId(userId)},
                { "$addToSet": { "tikets": tiket.dict()}}
            )
    elif tipe == "ksr":
        tiket_op = await MGDB.user_wisata_kasir.update_one(
                { "userId": ObjectId(userId)},
                { "$addToSet": { "tikets": tiket.dict()}}
            )
    elif tipe == "atm":
        tiket_op = await MGDB.user_wisata_miniatm.update_one(
                { "userId": ObjectId(userId)},
                { "$addToSet": { "tikets": tiket.dict()}}
            )
    else:
        raise HTTPException(status_code=404, detail="tipe yang dimasukkan salah, data tidak ditemukan")

    if tiket_op.modified_count:
        return tiket
    else:
        raise HTTPException(status_code=304)


@router_tiket.get("/get_all_tiket/{companyId}/{tipe}/{userId}", response_model=List[TiketData])
async def get_all_tiket_pengunjung(companyId: str, tipe: str, userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    if tipe == "pjg":
        pipeline = [
                {"$unwind": {"path": "$tikets"}},
                {"$match": {"companyId": ObjectId(companyId), "userId": ObjectId(userId)}},
                {"$replaceRoot": {"newRoot": "$tikets"}},
            ]
        tiket = await MGDB.user_wisata_pengunjung.aggregate(pipeline).to_list(10000)
        if tiket:
            return tiket
        else:
            raise HTTPException (status_code=404, detail= "data tiket tidak ditemukan")
    
    elif tipe == "ksr":
        pipeline = [
                {"$unwind": {"path": "$tikets"}},
                {"$match": {"companyId": ObjectId(companyId), "userId": ObjectId(userId)}},
                {"$replaceRoot": {"newRoot": "$tikets"}},
            ]
        tiket = await MGDB.user_wisata_kasir.aggregate(pipeline).to_list(10000)
        if tiket:
            return tiket
        else:
            raise HTTPException (status_code=404, detail= "data tiket tidak ditemukan")

    elif tipe == "atm":
        pipeline = [
                {"$unwind": {"path": "$tikets"}},
                {"$match": {"companyId": ObjectId(companyId), "userId": ObjectId(userId)}},
                {"$replaceRoot": {"newRoot": "$tikets"}},
            ]
        tiket = await MGDB.user_wisata_miniatm.aggregate(pipeline).to_list(10000)
        if tiket:
            return tiket
        else:
            raise HTTPException (status_code=404, detail= "data tiket tidak ditemukan")
    
    else:
        raise HTTPException (status_code=404, detail="tipe yang dimasukkan salah, data tidak ditemukan")


@router_tiket.get("/get_tiket/{tipe}/{userId}/{idTiket}", response_model=TiketData)
async def get_tiket_every_type(tipe: str, userId: str, idTiket: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    """
    Types:
    > *pjg untuk pengunjung
    
    > *ksr untuk kasir
    
    > *atm untuk miniatm
    """    
    if tipe == "pjg":
        # tiket = await MGDB.user_wisata_pengunjung.find_one({"tipe": str(tipe), "tikets.id": id},{"tikets.$":1})
        pipeline = [
            {"$match": {"userId": ObjectId(userId)}},
            {"$unwind": {"path": "$tikets"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"id": ObjectId(idTiket)}}
        ]
        result = await MGDB.user_wisata_pengunjung.aggregate(pipeline).to_list(1000)
        if result:
            tiket = result[0]
        else:
            raise HTTPException (status_code=404, detail= "tiket tidak ditemukan")

    elif tipe == "ksr":
        # tiket = await MGDB.user_wisata_kasir.find_one({"tipe": str(tipe), "tiket.id": id},{"tiket.$":1})
        pipeline = [
            {"$match": {"userId": ObjectId(userId)}},
            {"$unwind": {"path": "$tikets"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"id": ObjectId(idTiket)}}
        ]
        result = await MGDB.user_wisata_kasir.aggregate(pipeline).to_list(1000)
        if result:
            tiket = result[0]
        else:
            raise HTTPException (status_code=404, detail= "tiket tidak ditemukan")

    elif tipe == "atm":
        # tiket = await MGDB.user_wisata_miniatm.find_one({"tipe": str(tipe), "tiket.id": id},{"tiket.$":1})
        pipeline = [
            {"$match": {"userId": ObjectId(userId)}},
            {"$unwind": {"path": "$tikets"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"id": ObjectId(idTiket)}}
        ]
        result = await MGDB.user_wisata_miniatm.aggregate(pipeline).to_list(1000)
        if result:
            tiket = result[0]
        else:
            raise HTTPException (status_code=404, detail= "tiket tidak ditemukan")
    
    else:
        raise HTTPException(status_code=404, detail="tipe yang dimasukkan salah, data tidak ditemukan")
    
    if tiket:
        return tiket
    else:
        raise HTTPException(status_code=404, detail="tiket not found")


@router_tiket.delete("/delete_tiket/{tipe}/{userId}/{idTiket}", response_model=dict)
async def delete_tiket_every_type(tipe: str, userId: str, idTiket: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    """
    Types:
    > *pjg untuk pengunjung
    
    > *ksr untuk kasir
    
    > *atm untuk miniatm
    """ 
    if tipe == "pjg":
        pipeline = [
            {"$match": {"userId": ObjectId(userId)}},
            {"$unwind": {"path": "$tikets"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"id": ObjectId(idTiket)}}
        ]
        result = await MGDB.user_wisata_pengunjung.aggregate(pipeline).to_list(1000)
        if not result:
            raise HTTPException (status_code=404, detail= "tiket tidak ditemukan")

        tiket_op = await MGDB.user_wisata_pengunjung.update_one(
            {"tikets.id": ObjectId(idTiket)},
            {"$pull":{"tikets": {"id": ObjectId(idTiket)}}}
        )

    elif tipe == "ksr":
        pipeline = [
            {"$match": {"userId": ObjectId(userId)}},
            {"$unwind": {"path": "$tikets"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"id": ObjectId(idTiket)}}
        ]
        result = await MGDB.user_wisata_kasir.aggregate(pipeline).to_list(1000)
        if not result:
            raise HTTPException (status_code=404, detail= "tiket tidak ditemukan")
        tiket_op = await MGDB.user_wisata_kasir.update_one(
            {"tikets.id": ObjectId(idTiket)},
            {"$pull":{"tikets": {"id": ObjectId(idTiket)}}}
        )
    
    elif tipe == "atm":
        pipeline = [
            {"$match": {"userId": ObjectId(userId)}},
            {"$unwind": {"path": "$tikets"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"id": ObjectId(idTiket)}}
        ]
        result = await MGDB.user_wisata_miniatm.aggregate(pipeline).to_list(1000)
        if not result:
            raise HTTPException (status_code=404, detail= "tiket tidak ditemukan")
        tiket_op = await MGDB.user_wisata_miniatm.update_one(
            {"tikets.id": ObjectId(idTiket)},
            {"$pull":{"tikets": {"id": ObjectId(idTiket)}}}
        )

    else:
        raise HTTPException(status_code=404, detail="tipe yang dimasukkan salah, data tidak ditemukan")

    if tiket_op.modified_count:
        return {"status": f"deleted count: {tiket_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_tiket.put("/update_tiket/{tipe}/{userId}/{idTiket}", response_model=TiketData)
async def update_tiket_every_type(tipe: str, userId: str, idTiket:str, dataIn: TiketInput,current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    """
    Types:
    > *pjg untuk pengunjung
    
    > *ksr untuk kasir
    
    > *atm untuk miniatm
    """ 
    tiket = TiketData(**dataIn.dict())
    tiket.id = ObjectId()
    jumlah = tiket.jumlah
    pipeline = [
        {"$match": {"_id": ObjectId(tiket.idDestinasi)}},
        {"$unwind": {"path": "$wahana"}},
        {"$replaceRoot": {"newRoot": "$wahana"}},
        {"$match": {"id": tiket.idWahana}},
        {"$unwind": {"path": "$jadwal"}},
        {"$replaceRoot": {"newRoot": "$jadwal"}},
        {"$match": {"idJadwal": tiket.idJadwal}}
    ]
    results = await MGDB.wisata_destinasi.aggregate(pipeline).to_list(1000)
    wahanaJadwalData = None
    if results:
        wahanaJadwalData = results[0]
    else:
        raise HTTPException (status_code=404, detail= "wahana tidak ditemukan")
    price = wahanaJadwalData["price"]
    tempTotalHarga = price*jumlah

    if tipe == "pjg":
        pipeline = [
            {"$match": {"userId": ObjectId(userId)}},
            {"$unwind": {"path": "$tikets"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"id": ObjectId(idTiket)}}
        ]
        result = await MGDB.user_wisata_pengunjung.aggregate(pipeline).to_list(1000)
        if not result:
            raise HTTPException (status_code=404, detail= "tiket tidak ditemukan")
        tiket = result[0]
        dataIn = dataIn.dict(skip_defaults=True)
        dataIn = {k: v for k, v in dataIn.items() if v is not None}
        tiket.update(dataIn)

        tiket["totalHarga"] = tempTotalHarga
        tiket_op = await MGDB.user_wisata_pengunjung.update_one(
            {"tikets.id": ObjectId(idTiket)},
            {"$set": {"tikets.$": tiket}},
        )

    elif tipe == "ksr":
        pipeline = [
            {"$match": {"userId": ObjectId(userId)}},
            {"$unwind": {"path": "$tikets"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"id": ObjectId(idTiket)}}
        ]
        result = await MGDB.user_wisata_kasir.aggregate(pipeline).to_list(1000)
        if not result:
            raise HTTPException (status_code=404, detail= "tiket tidak ditemukan")
        tiket = result[0]
        dataIn = dataIn.dict(skip_defaults=True)
        dataIn = {k: v for k, v in dataIn.items() if v is not None}
        tiket.update(dataIn)

        tiket["totalHarga"] = tempTotalHarga
        tiket_op = await MGDB.user_wisata_kasir.update_one(
            {"tikets.id": ObjectId(idTiket)},
            {"$set": {"tikets.$": tiket}},
        )

    elif tipe == "atm":
        pipeline = [
            {"$match": {"userId": ObjectId(userId)}},
            {"$unwind": {"path": "$tikets"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"id": ObjectId(idTiket)}}
        ]
        result = await MGDB.user_wisata_miniatm.aggregate(pipeline).to_list(1000)
        if not result:
            raise HTTPException (status_code=404, detail= "tiket tidak ditemukan")
        tiket = result[0]
        dataIn = dataIn.dict(skip_defaults=True)
        dataIn = {k: v for k, v in dataIn.items() if v is not None}
        tiket.update(dataIn)

        tiket["totalHarga"] = tempTotalHarga
        tiket_op = await MGDB.user_wisata_miniatm.update_one(
            {"tikets.id": ObjectId(idTiket)},
            {"$set": {"tikets.$": tiket}},
        )

    else:
        raise HTTPException(status_code=404, detail="tipe yang dimasukkan salah, data tidak ditemukan")

    if tiket_op.modified_count:
        return tiket
    else:
        raise HTTPException(status_code=304)


# =================================================================================
# report

@router_tiket.get("/report_tiket_visit/{tipe}", response_model=dict)
async def report_tiket_visit_every_type(tipe: str, companyId: str, userId:str,from_tgl:str,to_tgl:str):
    """
    Types:
    > *pjg untuk pengunjung, maka userId adalah userId pengunjung.
    
    > *ksr untuk kasir, maka userId adalah userId kasir.
    
    > *atm untuk miniatm, maka userId adalah userId miniatm.
    """ 
    from_tgl = datetime(
        int(from_tgl.split("-")[0]),
        int(from_tgl.split("-")[1]),
        int(from_tgl.split("-")[2]),
    )
    to_tgl = datetime(
        int(to_tgl.split("-")[0]),
        int(to_tgl.split("-")[1]),
        int(to_tgl.split("-")[2]),
    )
    pipeline = [
            {"$unwind": {"path": "$tikets"}},
            {"$match": {"companyId": ObjectId(companyId), "userId": ObjectId(userId)}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"visitDate": {"$gte": from_tgl, "$lte": to_tgl}}},
            {
                "$project": {
                    "id": "$id",
                    "namaDestinasi": "$namaDestinasi",
                    "namaWahana": "$namaWahana",
                    "jenisTiket": "$jenisTiket",
                    "jumlah": "$jumlah",
                    "visitDate": "$visitDate",
                    "totalHarga": "$totalHarga",
                }
            }
        ]

    if tipe == "pjg":
        tiket = await MGDB.user_wisata_pengunjung.aggregate(pipeline).to_list(10000)
        if not tiket:
            raise HTTPException (status_code=404, detail= f"tidak ada kunjungan dari tanggal {from_tgl} hingga tanggal {to_tgl}")
    
    elif tipe == "ksr":
        tiket = await MGDB.user_wisata_kasir.aggregate(pipeline).to_list(10000)
        if not tiket:
            raise HTTPException (status_code=404, detail= f"tidak ada kunjungan dari tanggal {from_tgl} hingga tanggal {to_tgl}")

    elif tipe == "atm":
        tiket = await MGDB.user_wisata_miniatm.aggregate(pipeline).to_list(10000)
        if not tiket:
            raise HTTPException (status_code=404, detail= f"tidak ada kunjungan dari tanggal {from_tgl} hingga tanggal {to_tgl}")
   
    else:
        raise HTTPException(status_code=404, detail="tipe yang dimasukkan salah, data tidak ditemukan")

    report = {}
    report['from_tgl'] = str(from_tgl)
    report['to_tgl'] = str(to_tgl)
    report['tikets'] = tiket
    return report


@router_tiket.get("/report_tiket_order/{tipe}", response_model=dict)
async def report_tiket_order_every_type(tipe: str, companyId: str, userId:str,from_tgl:str,to_tgl:str):
    """
    Types:
    > *pjg untuk pengunjung, maka userId adalah userId pengunjung.
    
    > *ksr untuk kasir, maka userId adalah userId kasir.
    
    > *atm untuk miniatm, maka userId adalah userId miniatm.
    """ 
    from_tgl = datetime(
        int(from_tgl.split("-")[0]),
        int(from_tgl.split("-")[1]),
        int(from_tgl.split("-")[2]),
    )
    to_tgl = datetime(
        int(to_tgl.split("-")[0]),
        int(to_tgl.split("-")[1]),
        int(to_tgl.split("-")[2]),
    )
    pipeline = [
            {"$unwind": {"path": "$tikets"}},
            {"$match": {"companyId": ObjectId(companyId), "userId": ObjectId(userId)}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"orderTime": {"$gte": from_tgl, "$lte": to_tgl}}},
            {
                "$project": {
                    "id": "$id",
                    "namaDestinasi": "$namaDestinasi",
                    "namaWahana": "$namaWahana",
                    "jenisTiket": "$jenisTiket",
                    "jumlah": "$jumlah",
                    "orderTime": "$orderTime",
                    "totalHarga": "$totalHarga",
                }
            }
        ]

    if tipe == "pjg":
        tiket = await MGDB.user_wisata_pengunjung.aggregate(pipeline).to_list(10000)
        if not tiket:
            raise HTTPException (status_code=404, detail= f"tidak ada tiket yang diorder dari tanggal {from_tgl} hingga tanggal {to_tgl}")
    
    elif tipe == "ksr":
        tiket = await MGDB.user_wisata_kasir.aggregate(pipeline).to_list(10000)
        if not tiket:
            raise HTTPException (status_code=404, detail= f"tidak ada tiket yang diorder dari tanggal {from_tgl} hingga tanggal {to_tgl}")

    elif tipe == "atm":
        tiket = await MGDB.user_wisata_miniatm.aggregate(pipeline).to_list(10000)
        if not tiket:
            raise HTTPException (status_code=404, detail= f"tidak ada tiket yang diorder dari tanggal {from_tgl} hingga tanggal {to_tgl}")
   
    else:
        raise HTTPException(status_code=404, detail="tipe yang dimasukkan salah, data tidak ditemukan")

    report = {}
    report['from_tgl'] = str(from_tgl)
    report['to_tgl'] = str(to_tgl)
    report['tikets'] = tiket
    return report


@router_tiket.get("/download_all_pengunjung.xls")
async def download_all_penggunjung(companyId: str,from_tgl:str,to_tgl:str):
    from_tgl = datetime(
        int(from_tgl.split("-")[0]),
        int(from_tgl.split("-")[1]),
        int(from_tgl.split("-")[2]),
    )
    to_tgl = datetime(
        int(to_tgl.split("-")[0]),
        int(to_tgl.split("-")[1]),
        int(to_tgl.split("-")[2]),
    )
    data_tiket = await MGDB.user_wisata_pengunjung.find({'companyId':ObjectId(companyId)},{'userId':0,'userId':1,'name':1,'tikets':3}).to_list(10000)
    for b in range(len(data_tiket)):
        # idPengunjung = data_tiket[b]['userId']
        pipeline = [
            {"$unwind": {"path": "$tikets"}},
            {"$match": {"companyId": ObjectId(companyId)}},
            {"$addFields": {"tikets.id_pengunjung": "$userId"}},
            {"$addFields": {"tikets.nama_pengunjung": "$name"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"visitDate": {"$gte": from_tgl, "$lte": to_tgl}}},
            {
                "$project": {
                    "namaPengunjung":"$nama_pengunjung",
                    "id_tiket": "$id",
                    "namaWahana": "$namaWahana",
                    "jenisTiket": "$jenisTiket",
                    "jumlah": "$jumlah",
                    "visitDate": "$visitDate",
                    "totalHarga": "$totalHarga",
                }
            },
        ]
        data_tiket = await MGDB.user_wisata_pengunjung.aggregate(pipeline).to_list(10000)
        # data_tiket[b]['from_tgl'] = str(from_tgl)
        # data_tiket[b]['to_tgl'] = str(to_tgl)
        # data_tiket[b]['tikets'] = data_tiket

    df =  pd.DataFrame(list(data_tiket))
    with BytesIO() as b:
        writer = pd.ExcelWriter(b, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='data_tiket')
        writer.save()
        return Response(content=b.getvalue(), media_type='application/vnd.ms-excel')


@router_tiket.get("/download_tiket_all_kasir.xls")
async def download_tiket_all_kasir(companyId: str,from_tgl:str,to_tgl:str):
    from_tgl = datetime(
        int(from_tgl.split("-")[0]),
        int(from_tgl.split("-")[1]),
        int(from_tgl.split("-")[2]),
    )
    to_tgl = datetime(
        int(to_tgl.split("-")[0]),
        int(to_tgl.split("-")[1]),
        int(to_tgl.split("-")[2]),
    )
    data_tiket = await MGDB.user_wisata_kasir.find({'companyId':ObjectId(companyId)},{'userId':0,'userId':1,'name':1,'tikets':1}).to_list(10000)
    for b in range(len(data_tiket)):
        # idKasir = data_tiket[b]['userId']
        pipeline = [
            {"$unwind": {"path": "$tikets"}},
            {"$match": {"companyId": ObjectId(companyId)}},
            {"$addFields": {"tikets.id_kasir": "$userId"}},
            {"$addFields": {"tikets.nama_kasir": "$name"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"visitDate": {"$gte": from_tgl, "$lte": to_tgl}}},
            {
                "$project": {
                    "namaKasir":"$nama_kasir",
                    "id_tiket": "$id",
                    "namaWahana": "$namaWahana",
                    "jenisTiket": "$jenisTiket",
                    "jumlah": "$jumlah",
                    "visitDate": "$visitDate",
                    "totalHarga": "$totalHarga",
                }
            },
        ]
        data_tiket = await MGDB.user_wisata_kasir.aggregate(pipeline).to_list(10000)

    df =  pd.DataFrame(list(data_tiket))
    with BytesIO() as b:
        writer = pd.ExcelWriter(b, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='data_tiket')
        writer.save()
        return Response(content=b.getvalue(), media_type='application/vnd.ms-excel')


@router_tiket.get("/download_tiket_all_miniatm.xls")
async def download_tiket_all_miniatm(companyId: str,from_tgl:str,to_tgl:str):
    from_tgl = datetime(
        int(from_tgl.split("-")[0]),
        int(from_tgl.split("-")[1]),
        int(from_tgl.split("-")[2]),
    )
    to_tgl = datetime(
        int(to_tgl.split("-")[0]),
        int(to_tgl.split("-")[1]),
        int(to_tgl.split("-")[2]),
    )
    data_tiket = await MGDB.user_wisata_miniatm.find({'companyId':ObjectId(companyId)},{'userId':0,'userId':1,'name':1,'tikets':1}).to_list(10000)
    for b in range(len(data_tiket)):
        # idAtm = data_tiket[b]['userId']
        pipeline = [
            {"$unwind": {"path": "$tikets"}},
            {"$match": {"companyId": ObjectId(companyId)}},
            {"$addFields": {"tikets.id_miniatm": "$userId"}},
            {"$addFields": {"tikets.nama_miniatm": "$name"}},
            {"$replaceRoot": {"newRoot": "$tikets"}},
            {"$match": {"visitDate": {"$gte": from_tgl, "$lte": to_tgl}}},
            {
                "$project": {
                    "namaMiniatm":"$nama_miniatm",
                    "id_tiket": "$id",
                    "namaWahana": "$namaWahana",
                    "jenisTiket": "$jenisTiket",
                    "jumlah": "$jumlah",
                    "visitDate": "$visitDate",
                    "totalHarga": "$totalHarga",
                }
            },
        ]
        data_tiket = await MGDB.user_wisata_miniatm.aggregate(pipeline).to_list(10000)

    df =  pd.DataFrame(list(data_tiket))
    with BytesIO() as b:
        writer = pd.ExcelWriter(b, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='data_tiket')
        writer.save()
        return Response(content=b.getvalue(), media_type='application/vnd.ms-excel')