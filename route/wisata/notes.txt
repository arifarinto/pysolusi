CONTOH BUAT TIKET
id jadwal : 102afc95077b9852f0edbddb
id pengunjung : 613b01dbfee8171643c98dee
{
  "id" : "212adc95077b9852f0edbddb",
  "idDestinasi": "613afc95077b9852f0edbddb",
  "idWahana": "303afc95077b9852f0edbddb",
  "jenisTiket": "manca_dewasa",
  "jumlah": 2,
  "visitDate": "2021-09-14T06:45:15.506Z",
  "isPaid": false
}

CONTOH BUAT WAHANA
id destiny : 613afc95077b9852f0edbddb
{
  "id": "222afc95077b9852f0edbddb",
  "namaWahana": "contoh barulagi",
  "note": "string",
  "images": [],
  "jadwal": [
    {
      "tanggal": "2021-09-14T04:04:39.267Z",
      "isOpen": true,
      "capacity": 750,
      "pengunjungOrder": 0,
      "pengunjungBayar": 0,
      "pengunjungDatang": 0,
      "price": 25000
    }
  ]
}

contoh id tiket
# FORMAT TIKET tipe(pjg/ksr/mat)#id#tgl
# CONTOH TIKET pjg#6135b5d1a82fa2ffdf9fc211#2021-09-09


#CONTOH CREATE DISTRIBUSI DONASI
idDonasi : 61552f710a3664cfc319e807
idCompany : 613ade5588f8aa7022d8418e
{
  "id": "40552f710a3664cfc319e807",
  "namaKegiatan": "beli",
  "waktu": "2021-10-05T02:16:26.015Z",
  "dana": 2000000,
  "images": [],
  "keterangan": "string"
}

#CONTOH CREATE MUTASI DONASI
{
  "id": "90552f710a3664cfc319e807",
  "namaDonatur": "dona",
  "nominal": 2000000,
  "tgl": "2021-10-05T02:03:51.758Z"
}

# ===========================================================================

error:
requests.exceptions.ConnectionError: HTTPConnectionPool(host='localhost', port=8000): Max retries exceeded with url: /svc_sql/user/cek_all_user_data (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7effdbce4400>: Failed to establish a new connection: [Errno 111] Connection refused'))

setting yml
==== before:
services:
  sql_url: "http://localhost:8000"

=== after:
services:
  sql_url: "http://sundev.duckdns.org:8000"

user.py
   # insert to mysql
    reqUser = {
        "companyId" : str(user.companyId),
        "userId" : str(user.companyId),
        "name" : user.name
        }
    # url = URL_SQL+'/svc_sql/user/cek_all_user_data'
    # x = requests.post(url, json = reqUser)
    # print(x.text)

    3 ini dicomment

================================================================================

=== mob_user for wisata ===

from route.wisata.pengunjung import *
from route.wisata.tiket import *

@app.post("/mob_wisata/pengunjung/add_pengunjung", response_model=PengunjungBase, tags=["wisata-pengunjung"])
async def mob_add_pengunjung(dataIn: UserInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    return await add_pengunjung(dataIn, current_user)

@app.post("/mob_wisata/pengunjung/get_pengunjung", response_model=dict, tags=["wisata-pengunjung"])
async def mob_get_all_pengunjungs(size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    return await get_all_pengunjungs(size, page, sort, dir, search, current_user)

@app.get("/mob_wisata/pengunjung/get_pengunjung/{userId}", response_model=PengunjungBase, tags=["wisata-pengunjung"])
async def mob_get_pengunjung_by_userid(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    return await get_pengunjung_by_userid(userId, current_user)

@app.delete("/mob_wisata/pengunjung/delete_pengunjung/{userId}", response_model=dict, tags=["wisata-pengunjung"])
async def mob_delete_pengunjung_by_userid(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    return await delete_pengunjung_by_userid(userId, current_user)

@app.put("/mob_wisata/pengunjung/update_pengunjung/{userId}", response_model=dict, tags=["wisata-pengunjung"])
async def mob_update_pengunjung_by_userid(userId: str, dataIn: PengunjungBase, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    return await update_pengunjung_by_userid(userId, dataIn,current_user)

@app.post("/mob_wisata/pengunjung/add_tiket/{tipe}/{idCreator}", response_model=TiketData, tags=["wisata-pengunjung"])
async def mob_pengunjung_add_tiket(tipe: str, idCreator: str,dataIn: TiketInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    return await add_tiket_every_type(tipe, idCreator ,dataIn, current_user)

@app.get("/mob_wisata/pengunjung/get_tiket/{tipe}/{idTiket}", response_model=TiketData, tags=["wisata-pengunjung"])
async def mob_pengunjung_get_tiket(tipe: str, idTiket: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    return await get_tiket_every_type(tipe, idTiket, current_user)

@app.delete("/mob_wisata/pengunjung/delete_tiket/{tipe}/{idTiket}", response_model=dict, tags=["wisata-pengunjung"])
async def mob_pengunjung_delete_tiket(tipe: str, idTiket: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    return await delete_tiket_every_type(tipe, idTiket, current_user)

@app.put("/mob_wisata/pengunjung/update_tiket/{tipe}/{idTiket}", response_model=TiketData, tags=["wisata-pengunjung"])
async def mob_pengunjung_update_tiket(tipe: str, idTiket: str,dataIn: TiketInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    return await update_tiket_every_type(tipe, idTiket ,dataIn, current_user)