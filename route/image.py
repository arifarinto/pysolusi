from bson.objectid import ObjectId
from config.config import MGDB, MGIMAGE, CONF
from fastapi import APIRouter, Depends, HTTPException, File, UploadFile, BackgroundTasks, Security
from fastapi.responses import FileResponse
from typing import List
import shutil
import os
from PIL import Image
from autocrop import Cropper
import math
from os import path
import io
import base64

from route.auth import get_current_user
from model.default import JwtToken
from model.image import MediaBase, MediaPage, ActionType, ImageType, UploadBase64
from util.util import SearchRequest, RandomString, ValidateSizePage, CreateCriteria, FieldObjectIdRequest, IsiDefault
from util.util_waktu import dateTimeNow, dateNowStr

FOLDER = "./upload-image/"
IMAGE = "./image/"
dbase = MGIMAGE.tbl_image

router_image = APIRouter()

def image_process(dm: MediaBase):
    image = Image.open(FOLDER+dm.originName)
    # resize std
    image.thumbnail((800, 800))
    image.save(FOLDER+dm.name + 'std.' + dm.fileType)
    # resize pas
    image.thumbnail((400, 400))
    image.save(FOLDER + dm.name + 'pas.' + dm.fileType)
    horizontal, vertical = image.size
    if horizontal > vertical:
        ico_max = 150 * horizontal / vertical
        image.thumbnail((ico_max, ico_max))
        horizontal, vertical = image.size
        jarak1 = (horizontal - 150) / 2
        jarak2 = jarak1 + 150
        box = (jarak1, 0, jarak2, 150)
        image_crop = image.crop(box)
        image_crop.save(FOLDER+dm.name + 'ico.' + dm.fileType)
    else:
        ico_max = 150 * vertical / horizontal
        image.thumbnail((ico_max, ico_max))
        horizontal, vertical = image.size
        jarak1 = (vertical - 150) / 2
        jarak2 = jarak1 + 150
        box = (0, jarak1, 150, jarak2)
        image_crop = image.crop(box)
        image_crop.save(FOLDER+dm.name + 'ico.' + dm.fileType)
    os.remove(FOLDER+dm.originName)

def big_image_process(dm: MediaBase):
    image = Image.open(FOLDER+dm.originName)
    # resize std
    image.thumbnail((1400, 1400))
    image.save(FOLDER+dm.name + 'std.' + dm.fileType)
    # resize pas
    image.thumbnail((600, 600))
    image.save(FOLDER + dm.name + 'pas.' + dm.fileType)
    horizontal, vertical = image.size
    if horizontal > vertical:
        ico_max = 150 * horizontal / vertical
        image.thumbnail((ico_max, ico_max))
        horizontal, vertical = image.size
        jarak1 = (horizontal - 150) / 2
        jarak2 = jarak1 + 150
        box = (jarak1, 0, jarak2, 150)
        image_crop = image.crop(box)
        image_crop.save(FOLDER+dm.name + 'ico.' + dm.fileType)
    else:
        ico_max = 150 * vertical / horizontal
        image.thumbnail((ico_max, ico_max))
        horizontal, vertical = image.size
        jarak1 = (vertical - 150) / 2
        jarak2 = jarak1 + 150
        box = (0, jarak1, 150, jarak2)
        image_crop = image.crop(box)
        image_crop.save(FOLDER+dm.name + 'ico.' + dm.fileType)
    os.remove(FOLDER+dm.originName)

def image_foto(dm: MediaBase):
    image = Image.open(FOLDER+dm.originName)
    # resize pas
    image.thumbnail((400, 400))
    image.save(FOLDER + dm.name + 'pas.' + dm.fileType)
    os.remove(FOLDER+dm.originName)

def image_pasphoto(dm: MediaBase):
    image = Image.open(FOLDER+dm.originName)
    image.thumbnail((400, 400))
    if image.mode != 'RGB':
        image = image.convert('RGB')
    image.save(FOLDER+dm.originName)
    cropper = Cropper()
    try:
        cropper.face_percent = 55
        cropped_array = cropper.crop(FOLDER+dm.originName)
        cropped_image = Image.fromarray(cropped_array)
        cropped_image.save(FOLDER + dm.name + 'pas.' + dm.fileType)
        os.remove(FOLDER+dm.originName)
        print("cropped_array")
    except Exception:
        print("uncropped")
        shutil.copyfile(FOLDER+dm.originName, FOLDER + dm.name + 'pas.' + dm.fileType)
        os.remove(FOLDER+dm.originName)

# =================================================================================

@router_image.post("/try_base_enampat")
async def try_base_enampat(data:UploadBase64):
    image = base64.b64decode(str(data.base64text))       
    fileName = 'test.png'
    imagePath = FOLDER + fileName
    img = Image.open(io.BytesIO(image))
    img.save(imagePath, 'png')
    return fileName

@router_image.get("/get/{file_name}/{jenis}")
async def get_image(file_name: str, jenis: ImageType):
    if jenis == 'thumb':
        jenis = 'pas'
    image = await dbase.find_one({"name": file_name})
    filepath_empty = IMAGE+"no-image.png"
    if image:
        try:
            filepath = FOLDER+image["name"] + jenis + '.' + image["fileType"]
            print(filepath)
            if(path.exists(filepath)):            
                return FileResponse(filepath)
            else:
                print('no file 1')
                return FileResponse(filepath_empty) 
        except Exception:
            print('no file 2')
            return FileResponse(filepath_empty)
    else:
        print('no data')
        return FileResponse(filepath_empty)

@router_image.get("/cek_image/{file_name}")
async def cek_image(file_name: str):
    image = await dbase.find_one({"name": file_name})
    filepath_empty = IMAGE+"no-image.png"
    if image:
        try:
            filepath = FOLDER+image["name"] + 'pas' + '.' + image["fileType"]
            print(filepath)
            if(path.exists(filepath)):            
                return {"status":"ok"}
            else:
                print('no file 1')
                return {"status":"data ada, file tidak ada"}
        except Exception:
            print('no file 2')
            return {"status":"data ada, file tidak ada"}
    else:
        print('no data')
        return {"status":"data tidak ada, file tidak ada"}

@router_image.get("/logo_company/{companyId}")
async def get_image(companyId: str):
    home = await MGDB.tbl_company.find_one({"companyId": ObjectId(companyId)})
    print(home)
    jenis = 'pas'
    file_name = home['logoUrl']
    image = await dbase.find_one({"name": file_name})
    filepath_empty = IMAGE+"no-image.png"
    if image:
        try:
            filepath = FOLDER+image["name"] + jenis + '.' + image["fileType"]
            if(path.exists(filepath)):
                return FileResponse(filepath)
            else:
                return FileResponse(filepath_empty) 
        except Exception:
            return FileResponse(filepath_empty)
    else:
        return FileResponse(filepath_empty)



@router_image.post("/get_images", response_model=dict)
async def get_all_images(size: int = Depends(ValidateSizePage), page: int = 0, sort: str = "createTime", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    if current_user.isAdmin:
        search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    else:
        search.defaultObjectId.append(FieldObjectIdRequest(field='idAkun',key=ObjectId(current_user.accountId)))

    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = MediaPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_image.post("/upload_image", response_model=MediaBase)
async def upload_image(background_tasks: BackgroundTasks, file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    content_type = file.content_type
    print(content_type)
    if content_type == "image/jpeg" or content_type == "image/svg+xml" or content_type == "image/jpg" or content_type == "image/png" or content_type == "image/gif":
        random = RandomString(8)
        tmp_name = dateNowStr() + '-' + random + '-'
        originName = tmp_name + '-' + file.filename
        file_object = file.file
        upload = open(os.path.join(FOLDER, originName), 'wb+')
        shutil.copyfileobj(file_object, upload)
        upload.close()
        extention = content_type.replace('image/', '')
        dm = MediaBase(name=tmp_name,createTime=dateTimeNow(),originName=originName,fileName=file.filename,fileType=extention)
        dm_insert = IsiDefault(dm, current_user)
        dm_insert.userId=ObjectId(current_user.userId)
        background_tasks.add_task(image_process, dm)
        dbase.insert_one(dm_insert.dict())
        return dm
    else:
        raise HTTPException(status_code=400, detail="Unknown image type")


@router_image.post("/big_upload_image", response_model=MediaBase)
async def big_upload_image(background_tasks: BackgroundTasks, file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    content_type = file.content_type
    if content_type == "image/jpeg" or content_type == "image/svg" or content_type == "image/jpg" or content_type == "image/png" or content_type == "image/gif":
        random = RandomString(8)
        tmp_name = dateNowStr() + '-' + random + '-'
        originName = tmp_name + '-' + file.filename
        file_object = file.file
        upload = open(os.path.join(FOLDER, originName), 'wb+')
        shutil.copyfileobj(file_object, upload)
        upload.close()
        extention = content_type.replace('image/', '')
        dm = MediaBase(name=tmp_name,createTime=dateTimeNow(),originName=originName,fileName=file.filename,fileType=extention)
        dm_insert = IsiDefault(dm, current_user)
        dm_insert.userId=ObjectId(current_user.userId)
        background_tasks.add_task(big_image_process, dm)
        dbase.insert_one(dm_insert.dict())
        return dm
    else:
        raise HTTPException(status_code=400, detail="Unknown image type")


@router_image.post("/bulk_upload_images", response_model=dict)
async def bulk_upload_images(background_tasks: BackgroundTasks, files: List[UploadFile] = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    for gambar in files:
        content_type = gambar.content_type
        if content_type == "image/jpeg" or content_type == "image/svg" or content_type == "image/jpg" or content_type == "image/png" or content_type == "image/gif":
            random = RandomString(8)
            tmp_name = dateNowStr() + '-' + random + '-'
            originName = tmp_name + '-' + gambar.filename
            file_object = gambar.file
            upload = open(os.path.join(FOLDER, originName), 'wb+')
            shutil.copyfileobj(file_object, upload)
            upload.close()
            extention = content_type.replace('image/', '')
            dm = MediaBase(name=tmp_name,createTime=dateTimeNow(),originName=originName,fileName=gambar.filename,fileType=extention)
            dm_insert = IsiDefault(dm, current_user)
            dm_insert.userId=ObjectId(current_user.userId)
            background_tasks.add_task(image_process, dm)
            dbase.insert_one(dm_insert.dict())
        else:
            raise HTTPException(status_code=400, detail="Unknown image type")
    return {"ok": "ok"}


@router_image.delete("/image/{name}", response_model=dict)
async def delete_image_by_id(name: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    image_op = await dbase.delete_one({"name": name, "companyId": ObjectId(current_user.companyId)})
    if image_op.deleted_count:
        return {"status": f"deleted count: {image_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Not Found")


@router_image.post("/upload_pasphoto", response_model=MediaBase)
async def upload_pasphoto(background_tasks: BackgroundTasks, file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    content_type = file.content_type
    if content_type == "image/jpeg" or content_type == "image/svg" or content_type == "image/jpg" or content_type == "image/png" or content_type == "image/gif":
        random = RandomString(8)
        tmp_name = dateNowStr() + '-' + random + '-'
        originName = tmp_name + '-' + file.filename
        file_object = file.file
        upload = open(os.path.join(FOLDER, originName), 'wb+')
        shutil.copyfileobj(file_object, upload)
        upload.close()
        extention = content_type.replace('image/', '')
        dm = MediaBase(name=tmp_name,createTime=dateTimeNow(),originName=originName,fileName=file.filename,fileType=extention)
        dm_op = await MGDB.tbl_user.update_one(
            {"_id": ObjectId(current_user.userId)},
            {"$set": {"image": {"imageAvatar": dm.name}}}
        )
        print(dm_op.modified_count)
        background_tasks.add_task(image_pasphoto, dm)
        dm_insert = IsiDefault(dm, current_user)
        dm_insert.userId=ObjectId(current_user.userId)
        dbase.insert_one(dm_insert.dict())
        return dm
    else:
        raise HTTPException(status_code=400, detail="Unknown image type")


@router_image.post("/upload_user/{action}/{userId}", response_model=MediaBase)
async def upload_user(action: ActionType, userId: str, background_tasks: BackgroundTasks, file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    if current_user.isAdmin == False:
        userId = current_user.userId
    content_type = file.content_type
    dtUser = await MGDB.tbl_user.find_one({"_id": ObjectId(userId)})
    #baca text ektp, opencv + pytesserac
    #TODO compare foto ektp with selfi
    if not dtUser:
        raise HTTPException(status_code=404, detail="ID Not Found")
    if content_type == "image/jpeg" or content_type == "image/svg" or content_type == "image/jpg" or content_type == "image/png" or content_type == "image/gif":
        random = RandomString(8)
        tmp_name = dateNowStr() + '-' + random + '-'
        originName = tmp_name + file.filename
        file_object = file.file
        upload = open(os.path.join(FOLDER, originName), 'wb+')
        shutil.copyfileobj(file_object, upload)
        upload.close()
        extention = content_type.replace('image/', '')
        dm = MediaBase(name=tmp_name,createTime=dateTimeNow(),originName=originName,fileName=file.filename,fileType=extention)
        MGDB.tbl_user.update_one(
            {"_id": ObjectId(userId)},
            {"$set": {"image": {action: dm.name}}}
        )
        background_tasks.add_task(image_process, dm)
        dm_insert = IsiDefault(dm, current_user)
        dm_insert.userId=ObjectId(userId)
        dbase.insert_one(dm_insert.dict())
        return dm
    else:
        raise HTTPException(status_code=400, detail="Unknown image type")


@router_image.post("/image_aja", response_model=dict)
async def image_aja(file: UploadFile = File(...)):
    upload = open(os.path.join(FOLDER, file.filename), 'wb+')
    shutil.copyfileobj(file.file, upload)
    upload.close()
    return {"status":"ok"}