# backend/tancho/devices/routes.py

from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.akses.device import DeviceBase, DeviceOnDB, DevicePage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_device = APIRouter()
dbase = MGDB.akses_device

async def GetDeviceOr404(id_: str):
    _id = ValidateObjectId(id_)
    device = await dbase.find_one({"_id": _id})
    if device:
        return device
    else:
        raise HTTPException(status_code=404, detail="device not found")

# =================================================================================

@router_device.post("/device", response_model=DeviceOnDB)
async def add_device(dataIn: DeviceBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    device = IsiDefault(dataIn, current_user)
    device.name = device.name.upper()
    cdevice = await dbase.find_one({
        "companyId": ObjectId(current_user.companyId), 
        "name":device.name
    })
    if cdevice:
        raise HTTPException(status_code=400, detail="Device is registered")
    device_op = await dbase.insert_one(device.dict())
    if device_op.inserted_id:
        device = await GetDeviceOr404(device_op.inserted_id)
        return device


@router_device.post("/upload_device")
async def upload_device(file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    dataIn = DeviceBase()
    for index,row in df.iterrows():
        device = IsiDefault(dataIn, current_user)
        device.namaDevice = getString(str(row['NamaDevice']))
        device.tipe = getString(row['Tipe'])
        if None in [device.namaDevice, device.tipe]:
            raise HTTPException(status_code=404, detail="NamaDevice dan Tipe harus diisi")
        else:
            device.namaDevice = getString(str(row['NamaDevice']).upper())
            device.tipe = getString(row['Tipe'].upper())
            device.tags = getString(row['Tags'])
            if device.tags:
                device.tags = getString(row['Tags']).split(",")
                device.tags = ListToUp(device.tags)
            else:
                device.tags = []
            cruang = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "name": device.namaDevice})
            if cruang:
                rowNonvalid = rowNonvalid + 1
                unValid.append({
                    "name":device.namaDevice,
                    "note":"telah terdaftar"
                    })
                pass
            else:
                rowValid = rowValid + 1
                result.append(device.dict())
    print(result)
    dbase.insert_many(result)
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


@router_device.post("/get_device", response_model=dict)
async def get_all_devices(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = DevicePage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_device.get("/device/{id_}", response_model=DeviceBase)
async def get_device_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    device = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if device:
        return device
    else:
        raise HTTPException(status_code=404, detail="Device not found")


@router_device.delete("/device/{id_}", dependencies=[Depends(GetDeviceOr404)], response_model=dict)
async def delete_device_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    device_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if device_op.deleted_count:
        return {"status": f"deleted count: {device_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_device.put("/device/{id_}", response_model=DeviceOnDB)
async def update_device(id_: str, dataIn: DeviceBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    device = IsiDefault(dataIn, current_user, True)
    device.updateTime = dateTimeNow()
    device_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": device.dict(skip_defaults=True)}
    )
    if device_op.modified_count:
        return await GetDeviceOr404(id_)
    else:
        raise HTTPException(status_code=304)