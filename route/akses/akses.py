from datetime import date, datetime
from typing import List

from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import PlainTextResponse
from model.default import AksesBase, TanggalAbsen
from model.akses.pengguna import PenggunaBase
# from route.akses.kartu import GetCardOr404
from route.akses.device import GetDeviceOr404
from route.trx.transaction import payment_device
from route.company import GetCompanyOr404
from util.util import CekNfcLen, CheckList, ValidateObjectId
from util.util_user import GetAccountOr404
from util.util_waktu import dateTimeNow
router_akses = APIRouter()
dbase = MGDB.akses_pengguna


async def GetPenggunaOr404(id: str):
    _id = ValidateObjectId(id)
    pengguna = await dbase.find_one({"_id": _id})
    if pengguna:
        return pengguna
    else:
        raise HTTPException(status_code=404, detail="Pengguna not found")

async def GetPenggunaByIdAkun(idAkun: str):
    idAk = ValidateObjectId(idAkun)
    pengguna = await dbase.find_one({"idAkun": idAk})
    if pengguna:
        return pengguna
    else:
        return False

# =================================================================================
@router_akses.get("/proses_log_absensi/{companyId}/{idPengguna}/{namaPengguna}/{waktu}/{idDevice}/{suhu}", response_model=dict)
async def proses_log_absensi(companyId: str, idPengguna: str, namaPengguna: str, waktu: str, idDevice: str, suhu: float = 0):
    print()


@router_akses.get("/proses_log_absen/{companyId}/{idPengguna}/{namaPengguna}/{waktu}/{idDevice}/{suhu}", response_model=dict)
async def proses_log_absensi(companyId: str, idPengguna: str, namaPengguna: str, waktu: str, idDevice: str, suhu: float = 0):

    cekIdPengguna = await dbase.find_one({"companyId": ObjectId(companyId),"idPengguna":idPengguna})
    if not cekIdPengguna:
        pengguna = PenggunaBase()
        pengguna.namaShift = "DEFAULT"
        pengguna.createTime = dateTimeNow()
        pengguna.updateTime = dateTimeNow()
        pengguna.companyId = ObjectId(companyId)
        pengguna.idPengguna = idPengguna
        cek_company = await GetCompanyOr404(companyId)
        pengguna.companyName = cek_company["companyName"]
        pengguna.creatorName = namaPengguna 
        pengguna.levelTipe = []
        pengguna.isDeleted = False
        pengguna.name = namaPengguna.capitalize()
        await dbase.insert_one(pengguna.dict())

    note = ''
    # device = await MGDB.tbl_device.find_one({"companyId":ObjectId(companyId),"_id": ObjectId(idDevice)})
    # print(device)
    # if not device:
    #     raise HTTPException(status_code=404, detail="Device not found")

    if len(waktu) > 10:
        # print(waktu)
        tanggal = waktu[0:10]
        jam = waktu[11:16]
    else:
        tanggal = str(date.today())
        jam = waktu
    # print(tanggal + " " + jam)
    akses = AksesBase()
    akses.createTime = dateTimeNow()
    akses.waktuAkses = datetime.strptime(tanggal + " " + jam, '%Y-%m-%d %H:%M')
    akses.suhuTubuh = suhu
    akses.namaDevice =  "" #device['namaDevice']
    absen = TanggalAbsen()
    absen.tanggal = datetime.strptime(tanggal, '%Y-%m-%d')

    pipeline = [
        {
            '$unwind': {
                'path': '$absen'
            }
        }, {
            '$project': {
                'companyId':1,
                'idPengguna':1,
                'namaShift':1,
                'absen': 1,
                'idAkun':1,
                '_id':1
            }
        }, {
            '$match': {
                'companyId': ObjectId(companyId),
                'idPengguna': idPengguna,
                'absen.tanggal': absen.tanggal
            }
        }
    ]
    
    cursor = dbase.aggregate(pipeline)
    data_pengguna = await cursor.to_list(1)
    print(data_pengguna)
    if data_pengguna:
        print('pertama')
        urut = [
            (data_pengguna[0]['absen']['first'],data_pengguna[0]['absen']['suhuFirst']),
            (data_pengguna[0]['absen']['last'],data_pengguna[0]['absen']['suhuLast']),
            (jam,suhu)]
        urut.sort()
        print(urut)
        absen.first = urut[0][0]
        absen.last = urut[2][0]
        absen.suhuFirst = urut[0][1]
        absen.suhuLast = urut[2][1]
        pengguna_op = await dbase.update_one(
            {"companyId": ObjectId(companyId), "idPengguna": idPengguna},
            {"$addToSet": {"akses": akses.dict()}}
        )
        # MGDB.tbl_device.update_one(
        #     {"companyId":ObjectId(companyId),"_id": ObjectId(idDevice)},
        #     {"$addToSet": {"logDevice": akses.dict()}}
        # )

        # idAkun = str(data_pengguna[0]['idAkun'])
        # dAkun = await GetAccountOr404(idAkun)

        shift_cursor = MGDB.akses_shift.find(
            {"companyId": ObjectId(companyId), "hari": absen.tanggal.strftime("%A")}
        )
        
        shift = await shift_cursor.to_list(10)
        pilihan_shift = []

        if shift:
            for x in shift:
                skor = 0
                if x['sblmMasuk'] < absen.first and absen.first <= x['jamMasuk']:
                    absen.masuk = "datang_tepat"
                    skor = skor + 2

                if x['jamMasuk'] < absen.first and absen.first  <= x['stlhMasuk']:

                    absen.masuk = "datang_terlambat"
                    skor = skor + 1
                if x['sblmPulang'] < absen.last and absen.last  <= x['jamPulang']:
                    absen.pulang = "pulang_awal"
                    skor = skor + 1
                if x['jamPulang'] < absen.last and absen.last  <= x['stlhPulang']:
                    absen.pulang = "pulang_tepat"
                    skor = skor + 2
                if absen.first > x['jamPulangLembur']:
                    absen.pulang = "lembur"
                    skor = skor + 2
                absen.namaShift = x['namaShift']
                pilihan_shift.append((skor,absen))
            pilihan_shift.sort(reverse=True)
            print(pilihan_shift)
            note = 'absen ideal, ada shift nya yang sesuai, yaitu :'+ str(pilihan_shift[0][1])
            await dbase.update_one(
                {"companyId": ObjectId(companyId), "idPengguna": idPengguna,'absen.tanggal': absen.tanggal},
                {"$set": {"absen.$": pilihan_shift[0][1].dict()}}
            )
        else:
            shift_cursor = MGDB.akses_shift.find(
                {"companyId": ObjectId(companyId),"namaShift": "DEFAULT", "hari": absen.tanggal.strftime("%A")}
            )

            shift = await shift_cursor.to_list(3)
            print(shift)
            pilihan_shift = []

            if shift:
                for x in shift:
                    skor = 0
                    if x['sblmMasuk'] < absen.first and absen.first  <= x['jamMasuk']:
                        absen.masuk = "datang_tepat"
                        skor = skor + 2
                    if x['jamMasuk'] < absen.first and absen.first <= x['stlhMasuk']:
                        absen.masuk = "datang_terlambat"
                        skor = skor + 1
                    if x['sblmPulang'] < absen.last and absen.last  <= x['jamPulang']:
                        absen.pulang = "pulang_awal"
                        skor = skor + 1
                    if x['jamPulang'] < absen.last and absen.last <= x['stlhPulang']:
                        absen.pulang = "pulang_tepat"
                        skor = skor + 2
                    if absen.first > x['jamPulangLembur']:
                        absen.pulang = "lembur"
                        skor = skor + 2
                    absen.namaShift = x['namaShift']
                    pilihan_shift.append((skor,absen))
                pilihan_shift.sort(reverse=True)
                print(pilihan_shift)
                note = 'absen ideal pakai shift DEFAULT, ada shift nya yang sesuai, yaitu :'+pilihan_shift[0][1].dict()
                pengguna_op = await dbase.update_one(
                    {"companyId": ObjectId(companyId), "_id": data_pengguna[0]["_id"],'absen.tanggal': absen.tanggal},
                    {"$set": {"absen.$": pilihan_shift[0][1].dict()}}
                )
            else:
                note = 'hanya menambahkan data absen di tanggal yang sama, karena gak ada shift nya'
                pengguna_op = await dbase.update_one(
                    {"companyId": ObjectId(companyId), "idPengguna": idPengguna,'absen.tanggal': absen.tanggal},
                    {"$set": {"absen.$": absen.dict()}}
                )
    else:
        print('kedua')
        note = 'hanya insert aja ke data absen'
        absen.first = jam
        absen.suhuFirst = suhu
        absen.last = jam
        absen.suhuLast = suhu
        pengguna_op = await dbase.update_one(
            {"companyId": ObjectId(companyId), "idPengguna": idPengguna},
            {"$addToSet": {"akses": akses.dict(),"absen":absen.dict()}}
        )
    print(note)
    if pengguna_op.modified_count:
        return {"ok": "ok"}
    else:
        raise HTTPException(status_code=404, detail="Failed to add log")


@router_akses.get("/card_access/{idDevice}/{nfcId}/{suhu}", response_class=PlainTextResponse)
async def card_access(idDevice: str, nfcId: str, suhu: float):
    nfcId = CekNfcLen(nfcId)
    print('tap card at device')
    dDevice = await GetDeviceOr404(idDevice)
    companyId = str(dDevice['companyId'])
    
    if dDevice['biayaTap'] > 0:
        # cek source of fund nya dari tbl company
        #potong saldo
        await payment_device(idDevice,nfcId)

    dCard = await GetCardOr404(companyId,nfcId)
    idAkun = str(dCard['idAkun'])

    dataAkun = await GetAccountOr404(idAkun)

    if dDevice['tags'] == []:
        print('Device tanpa tags, bisa diakses siapa aja')
    else:
        if not CheckList(dDevice['tags'],dataAkun['tags']):
            raise HTTPException(status_code=400, detail="Anda tidak memiliki hak akses")

    #ABSENSI, jika device nya juga sebagai device absen, lakukan proses absensi
    if dDevice['isAbsen'] == True:
        print('lakukan proses absensi')
        #cek apakah data pengguna sudah ada
        cPengguna = GetPenggunaByIdAkun(idAkun)
        if not cPengguna:
            cekIdPengguna = await dbase.find_one({"companyId": ObjectId(companyId),"idPengguna":dataAkun['nomorIdentitas']})
            if cekIdPengguna :
                raise HTTPException(status_code=400, detail="ID Pengguna is exist")
            pengguna = PenggunaBase()
            pengguna.namaShift = "DEFAULT"
            pengguna.createTime = dateTimeNow()
            pengguna.updateTime = dateTimeNow()
            pengguna.companyId = ObjectId(companyId)
            pengguna.companyName = dDevice['companyName']
            pengguna.creatorId = ObjectId(idAkun)
            pengguna.creatorName = dataAkun['name']
            pengguna.levelTipe = []
            pengguna.isDeleted = False
            pengguna.name = pengguna.name.upper()
            await dbase.insert_one(pengguna.dict())
        # await proses_log_absen(companyId, idAkun, pengguna.name ,'', dDevice['id_'], dDevice['namaDevice'], suhu)

    #ABSENSI EDU, jika device nya juga sebagai device absen edumedia, lakukan proses absensi ke edumedia
    if dDevice['isEduAbsen'] == True:
        print('lakukan proses absensi ke edumedia')
        # await proses_log_edumedia_absen(companyId, idAkun, '', dDevice['id_'], dDevice['namaDevice'], 0)
    return "OK" 
