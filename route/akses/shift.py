from bson.objectid import ObjectId
from config.config import MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
import math

from model.akses.shift import ShiftBase, ShiftOnDB, ShiftPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import IsiDefault, ValidateObjectId, CreateCriteria
from route.auth import get_current_user

router_shift = APIRouter()
dbase = MGDB.akses_shift

async def GetShiftOr404(id: str):
    _id = ValidateObjectId(id)
    shift = await dbase.find_one({"_id": _id})
    if shift:
        return shift
    else:
        raise HTTPException(status_code=404, detail="Shift not found")


# =================================================================================

@router_shift.post("/shift", response_model=ShiftOnDB)
async def add_shift(data_in: ShiftBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    data_in.namaShift = data_in.namaShift.upper()
    shift_cek = await dbase.find_one({"companyId": ObjectId(current_user.companyId),"namaShift":data_in.namaShift,"hari":data_in.hari})
    shift = IsiDefault(data_in, current_user)
    shift_op = await dbase.insert_one(shift.dict())
    if shift_op.inserted_id:
        shift = await GetShiftOr404(shift_op.inserted_id)
        return shift

@router_shift.post("/get_shift", response_model=dict)
async def get_all_shifts(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = ShiftPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply

@router_shift.get("/shift/{id}", response_model=ShiftOnDB)
async def get_shift_by_id(id: ObjectId = Depends(ValidateObjectId)):
    shift = await dbase.find_one({"_id": id})
    if shift:
        return shift
    else:
        raise HTTPException(status_code=404, detail="Shift not found")

@router_shift.delete("/shift/{id}", dependencies=[Depends(GetShiftOr404)], response_model=dict)
async def delete_shift_by_id(id: str):
    shift_op = await dbase.delete_one({"_id": ObjectId(id)})
    if shift_op.deleted_count:
        return {"status": f"deleted count: {shift_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")

@router_shift.put("/shift/{id_}", response_model=ShiftOnDB)
async def update_shift(id_: str, data_in: ShiftBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    shift_data = IsiDefault(data_in, current_user, True)
    shift = await dbase.find_one({"_id": ObjectId(id_)})
    if shift:
        shift_op = await dbase.update_one(
            {"_id": ObjectId(id_)}, {"$set": shift_data.dict(skip_defaults=True)}
        )
        if shift_op.modified_count:
            return await GetShiftOr404(id_)
        else:
            raise HTTPException(status_code=304)
    else:
        raise HTTPException(status_code=304)