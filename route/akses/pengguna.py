import math
from datetime import datetime, timedelta
from io import BytesIO
from typing import List, NamedTuple

import pandas as pd
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Response, File, UploadFile, Security, datastructures
from model.akses.pengguna import CutiBase, PenggunaBase, PenggunaOnDB,PenggunaPage
from model.default import IdentityData, JwtToken, UserInput, RoleEnum
# from model.akses.kartu import CardBase
# from model.user import RoleAkun, UserCreate
from model.util import FieldObjectIdRequest, SearchRequest
from route.auth import get_current_user
from route.trx.transaction import TopupNfc
from util.util import (CekNfcLen, CreateCriteria, IsiDefault, RandomString, ValidateObjectId)
from function.user import CreateUser
from util.util_waktu import convertDateToStrDate, dateTimeNow

router_pengguna = APIRouter()
dbase = MGDB.akses_pengguna

async def GetPenggunaOr404(id: str):
    _id = ValidateObjectId(id)
    pengguna = await dbase.find_one({"_id": _id})
    if pengguna:
        return pengguna
    else:
        raise HTTPException(status_code=404, detail="Pengguna not found")

async def GetPenggunaByIdAkun(idAkun: str):
    idAk = ValidateObjectId(idAkun)
    pengguna = await dbase.find_one({"idAkun": idAk})
    if pengguna:
        return pengguna
    else:
        return False

# =================================================================================


@router_pengguna.post("/pengguna/{role}", response_model=PenggunaOnDB)
async def add_pengguna(data_in: PenggunaBase, role: RoleEnum = 'ROLE_CUSTOMER',  current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    if role == 'ROLE_CUSTOMER' or role == 'ROLE_USER':
        #cek idPengguna, pakai customer jika yang akses adalah customer tempat wisata, pakai role user jika yang akses adalah karyawan
        cekIdPengguna = await dbase.find_one({"companyId": ObjectId(current_user.companyId),"idPengguna":data_in.idPengguna})
        if cekIdPengguna :
            raise HTTPException(status_code=400, detail="ID Pengguna is exist")
        data_in.namaShift = data_in.namaShift.upper()
        pengguna = IsiDefault(data_in, current_user)
        pengguna.name = pengguna.name.upper()
        pengguna_op = await dbase.insert_one(pengguna.dict())
        if pengguna_op.inserted_id:
            userCreate = CreateUser()
            userCreate.companyId = current_user.companyId
            userCreate.companyName= current_user.companyName
            userCreate.tags = data_in.tags
            userCreate.role.append(role)
            userCreate.name = pengguna.name
            userCreate.email = data_in.email
            userCreate.phone = data_in.phone
            akun = await CreateUser(userCreate, 'default')
            print(akun)
            await dbase.update_one(
                {"_id": ObjectId(pengguna_op.inserted_id)}, 
                {"$set": {"idAkun":akun.object_id}}
            )
            pengguna = await GetPenggunaOr404(pengguna_op.inserted_id)
            return pengguna
        
    else:
        raise HTTPException(status_code=400, detail="ROLE is not allowed")

@router_pengguna.post("/fast_register/{nfcId}/{amount}", response_model=dict)
async def fast_register(data_in: PenggunaBase, nfcId: str, amount: int,  current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    role = 'ROLE_CUSTOMER'
    pengguna = IsiDefault(data_in, current_user)
    pengguna.name = pengguna.name.upper()
    pengguna_op = await dbase.insert_one(pengguna.dict())
    if pengguna_op.inserted_id:
        userCreate = CreateUser()
        userCreate.companyId = current_user.companyId
        userCreate.companyName= current_user.companyName
        userCreate.tags = data_in.tags
        userCreate.role.append(role)
        userCreate.name = pengguna.name
        userCreate.email = data_in.email
        userCreate.phone = data_in.phone
        akun = await CreateUser(userCreate, 'default')
        print(akun)
        await dbase.update_one(
            {"_id": ObjectId(pengguna_op.inserted_id)}, 
            {"$set": {"idAkun":akun.object_id}}
        )
        #add nfc
        card_in = CardBase()
        card_in.idNfc = CekNfcLen(nfcId)
        card_in.idAkun = akun.object_id
        cardNfc = IsiDefault(card_in, current_user)
        await MGDB.tbl_card.insert_one(cardNfc.dict())
        #top up
        await TopupNfc(card_in.idNfc,amount,current_user)
        respon = {'balance':amount,'nfc':card_in.idNfc,'name':pengguna.name,'idAkun':str(akun.object_id)}
        return respon

@router_pengguna.post("/get_pengguna", response_model=dict)
async def get_all_penggunas(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    # search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    # criteria = CreateCriteria(search)
    criteria = {"companyId": ObjectId(current_user.companyId)}
    datas_cursor = dbase.find(criteria,{'akses':0,'absen':0}).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = PenggunaPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply

@router_pengguna.get("/get_pengguna_and_id")
async def get_pengguna_and_id(name:str ='',current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    
    name = name.lower().strip()
    if name =='' :
        datas_cursor = dbase.find({},{'_id':0,'name':1,'idPengguna':1}).sort('name',1)
    else :
        pipeline = [{ '$addFields': { 'regex': { '$regexMatch': { 'input': "$name", 'regex':name,'options':'i' } } } },
                    {
                        '$project': {
                        '_id': 0,
                        'name': 1,
                        'idPengguna': 1,
                        'regex':1
                        }
                    },{
                        '$match':{
                            'regex': True
                        }
                    },
                    {
                        '$sort': {
                        'name': 1
                        }
                    },{
                        '$project':{'regex':0}
                    }
                    ]

        datas_cursor = dbase.aggregate(pipeline)

    datas = await datas_cursor.to_list(200000)
    print(datas)
    return datas



@router_pengguna.get("/pengguna/{id}", response_model=PenggunaOnDB)
async def get_pengguna_by_id(id: ObjectId = Depends(ValidateObjectId)):
    pengguna = await dbase.find_one({"_id": id},{"akses":0})
    if pengguna:
        return pengguna
    else:
        raise HTTPException(status_code=404, detail="Pengguna not found")


@router_pengguna.delete("/pengguna/{id}", dependencies=[Depends(GetPenggunaOr404)], response_model=dict)
async def delete_pengguna_by_id(id: str):
    pengguna_op = await dbase.delete_one({"_id": ObjectId(id)})
    if pengguna_op.deleted_count:
        return {"status": f"deleted count: {pengguna_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_pengguna.put("/pengguna/{id_}", response_model=PenggunaOnDB)
async def update_pengguna(id_: str, data_in: PenggunaBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengguna_data = IsiDefault(data_in, current_user, True)
    pengguna = await dbase.find_one({"_id": ObjectId(id_)})
    if pengguna:
        pengguna_op = await dbase.update_one(
            {"_id": ObjectId(id_)}, {
                "$set": pengguna_data.dict(skip_defaults=True)}
        )
        if pengguna_op.modified_count:
            return await GetPenggunaOr404(id_)
        else:
            raise HTTPException(status_code=304)
    else:
        raise HTTPException(status_code=404)

@router_pengguna.post("/cuti/{id_}", response_model=CutiBase)
async def add_cuti(id_: str, cuti: CutiBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    cuti.createTime = dateTimeNow()
    cuti.updateTime = dateTimeNow()
    cuti.tglCuti = convertDateToStrDate(cuti.tglCuti)
    pengguna = await dbase.find_one({"_id": ObjectId(id_), "cuti.tglCuti" : cuti.tglCuti})
    if not pengguna:
        pengguna_op = await dbase.update_one(
            {"_id": ObjectId(id_)}, 
            {"$addToSet": {"cuti":cuti.dict()}}
        )
        if pengguna_op.modified_count:
            return cuti
        else:
            raise HTTPException(status_code=404, detail="Anda tidak memiliki akun pengguna absensi")
    else:
        raise HTTPException(status_code=400, detail="Date is exist, select another date")

@router_pengguna.get("/cuti/{id_}", response_model=PenggunaOnDB)
async def get_cuti_by_id_pengguna(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengguna = await dbase.find_one({"_id": ObjectId(id_)},{'akses':0,'absen':0})
    return pengguna

@router_pengguna.put("/cuti/{id_}/{tgl}", response_model=CutiBase)
async def update_cuti(id_: str, tgl: str, cuti: CutiBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengguna = await dbase.find_one({"_id": ObjectId(id_), "cuti.tglCuti" : tgl})
    if pengguna:
        pengguna_op = await dbase.update_one(
            {"_id": ObjectId(id_),"cuti.tglCuti":tgl}, 
            {"$set": {"cuti.$":cuti.dict(skip_defaults=True)}}
        )
        if pengguna_op.modified_count:
            return cuti
        else:
            raise HTTPException(status_code=304)
    else:
        raise HTTPException(status_code=404, detail="Date is not found")

@router_pengguna.delete("/cuti/{id_}/{tgl}", response_model=dict)
async def delete_cuti(id_: str, tgl: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengguna = await dbase.find_one({"_id": ObjectId(id_), "cuti.tglCuti" : tgl})
    if pengguna:
        pengguna_op = await dbase.update_one(
            {"_id": ObjectId(id_)}, 
            {"$pull": {"cuti" : {"tglCuti":tgl}}}
        )
        if pengguna_op.modified_count:
            return {"ok":"ok"}
        else:
            raise HTTPException(status_code=304)
    else:
        raise HTTPException(status_code=404, detail="Date is not found")

# cuti =====================================================================
# post data cuti, kemudian acc oleh hrd untuk ubah status menjadi isPermitted
# post data cuti bisa range tanggal
# update data cuti next aja yang bisa, jika ubah date, isPermitted juga berubah

# absensi ==================================================================

@router_pengguna.get("/get_tanggal_awal_minggu/{tanggal}")
async def get_tanggal_awal_minggu(tanggal: str):
    tgl = datetime.strptime(tanggal, '%Y-%m-%d')
    tglAwal = tgl - timedelta(days=tgl.weekday() % 7)
    return tglAwal.strftime('%Y-%m-%d')


@router_pengguna.get("/get_data_absen/{companyId}/{tanggal}",response_model=list)
async def get_data_absen(companyId: str,tanggal: str):
    tgl = datetime.strptime(tanggal, '%Y-%m-%d')
    tglAwal = tgl - timedelta(days=tgl.weekday() % 7)
    tglAkhir = tglAwal + timedelta(6)
    pipeline = [
        {
            '$match': {
                'companyId': ObjectId(companyId)
            }
        }, {
            '$unwind': {
                'path': '$absen'
            }
        }, {
            '$project': {
                '_id': 0,
                'name': 1, 
                'idPengguna': 1, 
                'absen': 1,
                'hari': {'$isoDayOfWeek':'$absen.tanggal'}
            }
        }, {
            '$match': {
                'absen.tanggal': {
                    '$gte': tglAwal, 
                    '$lte': tglAkhir
                }
            }
        }, {
            '$sort': {
                'name': 1, 
                'absen.tanggal': 1
            }
        }
    ]
    cursor = dbase.aggregate(pipeline)
    data_absen = await cursor.to_list(10000)
    return data_absen


@router_pengguna.get("/get_data_absen2")
async def get_data_absen2(companyId:str,tanggal:str,idPengguna:int=0,tipe_absensi:str='all',page:int=1,size:int=10):
    tgl = datetime.strptime(tanggal, '%Y-%m-%d')
    tglAwal = tgl - timedelta(days=tgl.weekday() % 7)
    tglAkhir = tglAwal + timedelta(6)
    skip = page * size

    filterp = {'$match': {'idPengguna':idPengguna,'absen.tanggal': {'$gte': tglAwal,'$lte': tglAkhir}}}
    if idPengguna == 0 : filterp = {'$match': {'absen.tanggal': {'$gte': tglAwal,'$lte': tglAkhir}}}

    filterp2 = {'$match': {'companyId': ObjectId(companyId)}}
    if tipe_absensi != 'all': filterp2 = {'$match': {"$or":[{'absen.masuk' : tipe_absensi},{'absen.pulang' : tipe_absensi}]}}

    pipeline = [
        {'$match': {'companyId': ObjectId(companyId)}},
        {'$unwind': {'path': '$absen'}},
        filterp,filterp2,
        {'$sort': {'name': 1}},
        {'$skip': skip},
        {'$limit': size},
        {'$project': {'_id': 0,'name': 1,'idPengguna': 1,'absen': 1,'hari': {'$dayOfWeek':'$absen.tanggal'} }},
        {'$group':{'_id':{'name':'$name','idPengguna':'$idPengguna'},'absen':{'$push':'$absen'}} }
        ]
    cursor = dbase.aggregate(pipeline)
    data_absen = await cursor.to_list(10000)
    jml_cursor = dbase.aggregate([{'$match': {'companyId': ObjectId(companyId)}},{'$unwind': {'path': '$absen'}},filterp,filterp2])
    jmls = await jml_cursor.to_list(10000)
    jml = len(jmls)

    data = {}
    data['data'] = data_absen
    data['page'] = page
    data['size'] = size
    data['total_page'] = math.ceil(jml/size)
    data['total_data'] = jml
    return data


@router_pengguna.get("/get_data_absen3")
async def get_data_absen3(companyId:str,tanggal:str,idPengguna:str='',tipe_absensi:str='all',page:int=1,size:int=10):
    idPengguna = f"{idPengguna}"
    if idPengguna == "0" : idPengguna =''

    tglAwal = datetime.strptime(tanggal, '%Y-%m-%d')
    tglAkhir = tglAwal + timedelta(6)

    skip = page * size

    filterp = {'$match': {'idPengguna':idPengguna,'absen.tanggal': {'$gte': tglAwal,'$lte': tglAkhir}}}
    if idPengguna == '' : filterp = {'$match': {'absen.tanggal': {'$gte': tglAwal,'$lte': tglAkhir}}}

    filterp2 = {'$match': {'companyId': ObjectId(companyId)}}
    if tipe_absensi != 'all': filterp2 = {'$match': {"$or":[{'absen.masuk' : tipe_absensi},{'absen.pulang' : tipe_absensi}]}}

    pipeline = [
        {'$match': {'companyId': ObjectId(companyId)}},
        {'$unwind': {'path': '$absen'}},
        filterp,filterp2,
        {'$sort': {'name': 1}},
        {'$skip': skip},
        {'$limit': size},
        {'$project': {'_id': 0,'name': 1,'idPengguna': 1,'absen': 1,'hari': {'$dayOfWeek':'$absen.tanggal'} }},
        {'$group':{'_id':{'name':'$name','idPengguna':'$idPengguna'},'absen':{'$push':'$absen'}} }
        ]
    cursor = dbase.aggregate(pipeline)
    data_absen = await cursor.to_list(10000)
    jml_cursor = dbase.aggregate([{'$match': {'companyId': ObjectId(companyId)}},{'$unwind': {'path': '$absen'}},filterp,filterp2])
    jmls = await jml_cursor.to_list(10000)
    jml = len(jmls)

    data = {}
    data['data'] = data_absen
    data['tgl_awal'] = tglAwal
    data['tgl_akhir'] = tglAkhir
    data['page'] = page
    data['size'] = size
    data['total_page'] = math.ceil(jml/size)
    data['total_data'] = jml
    return data



@router_pengguna.get("/get_data_absen_person/{companyId}/{tanggal}/{idPengguna}/{tipe_absensi}",response_model=list)
async def get_data_absen_person(companyId: str,tanggal: str,idPengguna : str,tipe_absensi:str ):
    tgl = datetime.strptime(tanggal, '%Y-%m-%d')
    tglAwal = tgl - timedelta(days=tgl.weekday() % 7)
    tglAkhir = tglAwal + timedelta(6)

    # tglAwal = datetime.strptime(tgl_awal, '%Y-%m-%d')
    # tglAkhir = datetime.strptime(tgl_akhir, '%Y-%m-%d')
    # tipe_absensi: all, datang_tepat,datang_terlambat,pulang_tepat,pulang_awal,lembur

    if tipe_absensi == 'all' and int(idPengguna) != 0  : 
        pl_tipe = {'$match': {'idPengguna': int(idPengguna)}}
    elif tipe_absensi == 'all' and int(idPengguna) == 0 :
        pl_tipe = {'$match': {'absen.tanggal': {'$gte': tglAwal} }}
    elif tipe_absensi != 'all' and int(idPengguna) == 0 :
        pl_tipe = {'$match': {"$or":[{'absen.masuk' : tipe_absensi},{'absen.pulang' : tipe_absensi}]}}
    else :
        pl_tipe = {'$match': {"$or":[{'idPengguna': int(idPengguna),'absen.masuk' : tipe_absensi },
            {'idPengguna': int(idPengguna),'absen.pulang' : tipe_absensi}]}}


    pipeline = [
        {'$match': {'companyId': ObjectId(companyId)}},
        {'$unwind': {'path': '$absen' }},
        {'$project': {'_id': 0,'name': 1,'idPengguna': 1,'absen': 1,'hari': {'$isoDayOfWeek':'$absen.tanggal'}}},
        {'$match': {'absen.tanggal': {'$gte': tglAwal,'$lte': tglAkhir }}},
        pl_tipe,
        {'$sort': {'name': 1,'absen.tanggal': 1}}
    ]

    if tipe_absensi =='tidak_masuk' :
        pipeline = [
                {
                    '$match': {
                        'companyId': ObjectId(companyId),
                        'idPengguna': int(idPengguna)
                    }
                }, {
                    '$unwind': {
                        'path': '$cuti'
                    }
                }, {
                    '$project': {
                        '_id': 0,
                        'name': 1, 
                        'idPengguna': 1, 
                        'cuti': 1,
                        'tanggal':'$cuti.createTime',
                        'first' : '',
                        'masuk' : '$cuti.keterangan',
                        'last' : '',
                        'pulang': '$cuti.keterangan',
                        'hari': {'$isoDayOfWeek':'$cuti.createTime'}
                    }
                }, {
                    '$match': {
                        'cuti.createTime': {
                            '$gte': tglAwal, 
                            '$lte': tglAkhir
                        }
                    }
                },{
                    '$sort': {
                        'cuti.createTime': 1
                    }
                },{
                    '$project':{
                        'cuti':0
                    }
                }
            ]

    cursor = dbase.aggregate(pipeline)
    data_absen = await cursor.to_list(10000)
    return data_absen


@router_pengguna.get("/download_data_absen_person/{companyId}/{tanggal}/{idPengguna}/{tipe_absensi}",response_model=list)
async def download_data_absen_person(companyId: str,tanggal: str,idPengguna : str,tipe_absensi:str ):
    tgl = datetime.strptime(tanggal, '%Y-%m-%d')
    tglAwal = tgl - timedelta(days=tgl.weekday() % 7)
    tglAkhir = tglAwal + timedelta(6)

    # tglAwal = datetime.strptime(tgl_awal, '%Y-%m-%d')
    # tglAkhir = datetime.strptime(tgl_akhir, '%Y-%m-%d')

    if tipe_absensi == 'all' : 
        pl_tipe = {
            '$match': {
                'idPengguna': int(idPengguna)
            }
        }
    else :
        pl_tipe = {
            '$match': {"$or":[{
                'idPengguna': int(idPengguna),
                'absen.masuk' : tipe_absensi
            },
            {
                'idPengguna': int(idPengguna),
                'absen.pulang' : tipe_absensi
            }
            ]}
        }

    pipeline = [
        {
            '$match': {
                'companyId': ObjectId(companyId),
                'idPengguna': int(idPengguna)
            }
        }, {
            '$unwind': {
                'path': '$absen'
            }
        }, {
            '$project': {
                '_id': 0,
                'name': 1, 
                'idPengguna': 1, 
                'absen': 1,
                'first' : '$absen.first',
                'masuk' : '$absen.masuk',
                'last' : '$absen.last',
                'pulang': '$absen.pulang',
                'hari': {'$isoDayOfWeek':'$absen.tanggal'}
            }
        }, {
            '$match': {
                'absen.tanggal': {
                    '$gte': tglAwal, 
                    '$lte': tglAkhir
                }
            }
        },pl_tipe, {
            '$sort': {
                'name': 1, 
                'absen.tanggal': 1
            }
        },{
            '$project':{
                'absen':0
            }
        }
    ]

    if tipe_absensi =='tidak_masuk' :
        pipeline = [
                {
                    '$match': {
                        'companyId': ObjectId(companyId),
                        'idPengguna': int(idPengguna)
                    }
                }, {
                    '$unwind': {
                        'path': '$cuti'
                    }
                }, {
                    '$project': {
                        '_id': 0,
                        'name': 1, 
                        'idPengguna': 1, 
                        'cuti': 1,
                        'tanggal':'$cuti.createTime',
                        'first' : '',
                        'masuk' : '$cuti.keterangan',
                        'last' : '',
                        'pulang': '$cuti.keterangan',
                        'hari': {'$isoDayOfWeek':'$cuti.createTime'}
                    }
                }, {
                    '$match': {
                        'cuti.createTime': {
                            '$gte': tglAwal, 
                            '$lte': tglAkhir
                        }
                    }
                },{
                    '$sort': {
                        'cuti.createTime': 1
                    }
                },{
                    '$project':{
                        'cuti':0
                    }
                }
            ]

    cursor = dbase.aggregate(pipeline)
    data_absen = await cursor.to_list(10000)
    

    df =  pd.DataFrame(data_absen)
    with BytesIO() as b:
        # Use the StringIO object as the filehandle.
        writer = pd.ExcelWriter(b, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='data_absen')
        writer.save()
        return Response(content=b.getvalue(), media_type='application/vnd.ms-excel')



@router_pengguna.get("/get_data_bulan_absen/{companyId}/{tahun}/{bulan}",response_model=list)
async def get_data_bulan_absen(companyId: str,tahun: str, bulan: str):
    tglAwal = datetime.strptime(tahun + '-' + bulan + '-01','%Y-%m-%d')
    bulanAkhir = int(bulan) + 1
    if bulanAkhir == 13:
        tahunAkhir = str(int(tahun) + 1)
        bulanAkhir = '01'
    else:
        tahunAkhir = tahun
        bulanAkhir = (str(bulanAkhir).zfill(2))
    
    tglAkhir = datetime.strptime(tahunAkhir + '-' + bulanAkhir + '-01','%Y-%m-%d')
    print(tglAkhir)
    pipeline = [
        {
            '$match': {
                'companyId': ObjectId(companyId)
            }
        }, {
            '$unwind': {
                'path': '$absen'
            }
        }, {
            '$project': {
                '_id': 0,
                'companyId': 1, 
                'name': 1, 
                'idPengguna': 1, 
                'absen': 1
            }
        }, {
            '$match': {
                'absen.tanggal': {
                    '$gte': tglAwal, 
                    '$lt': tglAkhir
                }
            }
        }, {
            '$sort': {
                'name': 1, 
                'absen.tanggal': 1
            }
        }
    ]
    cursor = dbase.aggregate(pipeline)
    data_absen = await cursor.to_list(10000)
    return data_absen

@router_pengguna.get("/download_absen.xls") #action download excel
async def download_absen(companyId: str,tahun: str, bulan: str):
    absensi = await get_data_bulan_absen(companyId, tahun, bulan)
    df =  pd.DataFrame(list(absensi))
    with BytesIO() as b:
        # Use the StringIO object as the filehandle.
        writer = pd.ExcelWriter(b, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='data_absen')
        writer.save()
        return Response(content=b.getvalue(), media_type='application/vnd.ms-excel')


@router_pengguna.post("/upload_pengguna")
async def upload_penggunaa(file: UploadFile = File(...),current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    for index,item in df.iterrows():
        data_in = PenggunaBase()
        data_in.name = item['name']
        data_in.companyId = ObjectId("60a4938b32d7481e2137b1be")
        data_in.idPengguna = int(item['nip'])
        data_in.idAkun = item['_id']
        data_in.email = f"pdam{index}pdamsalatiga.id"
        data_in.phone = ''
        data_in.namaShift = ''
        await add_pengguna(data_in,'ROLE_CUSTOMER',current_user)


@router_pengguna.get("/report_absen_by_pengguna")
async def report_absen_by_pengguna(companyId: str, idPengguna:str,from_tgl:str,to_tgl:str):
    from_tgl = datetime.strptime( from_tgl, '%Y-%m-%d')
    to_tgl = datetime.strptime( to_tgl, '%Y-%m-%d')
    pipeline = [{'$match':{'companyId':ObjectId(companyId),'idPengguna':idPengguna}},
    {'$unwind':{'path':'$absen'}},
    {'$project':{'_id':0,'tanggal':'$absen.tanggal','year':{'$year':'$absen.tanggal'},'month':{'$month':'$absen.tanggal'},'first':'$absen.first','last':'$absen.last','masuk':'$absen.masuk','pulang':'$absen.pulang'}},
    {'$match':{'tanggal':{'$gte':from_tgl,'$lte':to_tgl}}}]
    
    cursor = dbase.aggregate(pipeline)
    absen_masuk = await cursor.to_list(10000)
    jumlah_datang_terlambat = 0
    jumlah_pulang_awal = 0
    jumlah_lembur = 0
    for x in absen_masuk:
        if x['masuk'] == 'datang_terlambat' : jumlah_datang_terlambat += 1
        if x['pulang'] == 'pulang_awal' : jumlah_pulang_awal += 1
        if x['pulang'] == 'lembur' : jumlah_lembur += 1



    pipeline = [{'$match':{'companyId':ObjectId(companyId),'idPengguna':idPengguna}},
    {'$unwind':{'path':'$cuti'}},
    {'$project':{'_id':0,'tanggal':'$cuti.tglCuti','year':{'$substr':['$cuti.tglCuti',0,4]},'month':{'$substr':['$cuti.tglCuti',5,2]},'keterangan':'$cuti.keterangan'}},
    {'$match':{'tanggal':{'$gte':from_tgl,'$lte':to_tgl}}}]
    
    cursor = dbase.aggregate(pipeline)
    absen_tidak_masuk = await cursor.to_list(10000)

    cek_data = await dbase.find_one({'companyId':ObjectId(companyId),'idPengguna':idPengguna})

    report = {}
    report['company'] = cek_data['companyName']
    report['name'] = cek_data['name']
    report['from_tgl'] = str(from_tgl)
    report['to_tgl'] = str(to_tgl)
    report['data_absen_masuk'] = absen_masuk
    report['jumlah_absen_masuk'] = len(absen_masuk)
    report['data_absen_tidak_masuk'] = absen_tidak_masuk
    report['jumlah_absen_tidak_masuk'] = len(absen_tidak_masuk)
    report['jumlah_datang_terlambat'] = jumlah_datang_terlambat
    report['jumlah_pulang_awal'] = jumlah_pulang_awal
    report['jumlah_lembur'] = jumlah_lembur
    
    return report


@router_pengguna.get("/download_absen_all_pengguna.xls")
async def download_absen_all_pengguna(companyId: str,from_tgl:str,to_tgl:str):
    from_tgl = datetime.strptime( from_tgl, '%Y-%m-%d')
    to_tgl = datetime.strptime( to_tgl, '%Y-%m-%d')

    data_penggunax = dbase.find({'companyId':ObjectId(companyId)},{'_id':0,'idPengguna':1,'name':1})
    data_pengguna = await data_penggunax.to_list(10000)

    for b in range(len(data_pengguna)):
        idPengguna = data_pengguna[b]['idPengguna']
        pipeline = [{'$match':{'companyId':ObjectId(companyId),'idPengguna':idPengguna}},
        {'$unwind':{'path':'$absen'}},
        {'$project':{'_id':0,'tanggal':'$absen.tanggal','year':{'$year':'$absen.tanggal'},'month':{'$month':'$absen.tanggal'},'first':'$absen.first','last':'$absen.last','masuk':'$absen.masuk','pulang':'$absen.pulang'}},
        {'$match':{'tanggal':{'$gte':from_tgl,'$lte':to_tgl}}}]
        cursor = dbase.aggregate(pipeline)
        absen_masuk = await cursor.to_list(10000)
        jumlah_datang_terlambat = 0
        jumlah_pulang_awal = 0
        jumlah_lembur = 0
        for x in absen_masuk:
            if x['masuk'] == 'datang_terlambat' : jumlah_datang_terlambat += 1
            if x['pulang'] == 'pulang_awal' : jumlah_pulang_awal += 1
            if x['pulang'] == 'lembur' : jumlah_lembur += 1
        pipeline = [{'$match':{'companyId':ObjectId(companyId),'idPengguna':idPengguna}},
        {'$unwind':{'path':'$cuti'}},
        {'$project':{'_id':0,'tanggal':'$cuti.tglCuti','year':{'$substr':['$cuti.tglCuti',0,4]},'month':{'$substr':['$cuti.tglCuti',5,2]},'keterangan':'$cuti.keterangan'}},
        {'$match':{'tanggal':{'$gte':from_tgl,'$lte':to_tgl}}}]
        cursor = dbase.aggregate(pipeline)
        absen_tidak_masuk = await cursor.to_list(10000)

        data_pengguna[b]['from_tgl'] = str(from_tgl)
        data_pengguna[b]['to_tgl'] = str(to_tgl)
        data_pengguna[b]['jumlah_absen_masuk'] = len(absen_masuk)
        data_pengguna[b]['jumlah_absen_tidak_masuk'] = len(absen_tidak_masuk)
        data_pengguna[b]['jumlah_datang_terlambat'] = jumlah_datang_terlambat
        data_pengguna[b]['jumlah_pulang_awal'] = jumlah_pulang_awal
        data_pengguna[b]['jumlah_lembur'] = jumlah_lembur

    
    
    df =  pd.DataFrame(list(data_pengguna))
    with BytesIO() as b:
        # Use the StringIO object as the filehandle.
        writer = pd.ExcelWriter(b, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='data_absen')
        writer.save()
        return Response(content=b.getvalue(), media_type='application/vnd.ms-excel')

