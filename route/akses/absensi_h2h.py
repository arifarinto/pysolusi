from bson.objectid import ObjectId
from fastapi import APIRouter, Depends, HTTPException, Security
from route.auth import get_current_user
from config.config import ENVIRONMENT
from model.default import JwtToken
import requests
import json


absensi_h2h = APIRouter()
domain = 'https://api.dev.katalis.info'
if ENVIRONMENT == 'production': domain = 'https://api.katalis.info'



@absensi_h2h.get("/school/{school_code}/attendance/history")
async def get_history_absensi_school(school_code:str,start_date:str,end_date:str,current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    url = f"{domain}/main_a/pengguna/school/{school_code}/attendance/history?start_date={start_date}&end_date={end_date}"
    response = requests.request("GET", url)
    jsonq = json.loads(response.content)
    return jsonq


@absensi_h2h.get("/school/{school_code}/student/{nis}/attendance/history")
async def get_history_absensi_student(school_code:str,nis:str,start_date:str,end_date:str,current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    url = f"{domain}/main_a/pengguna/school/{school_code}/student/{nis}/attendance/history?start_date={start_date}&end_date={end_date}"
    response = requests.request("GET", url)
    jsonq = json.loads(response.content)
    return jsonq


@absensi_h2h.put("/school/{school_code}/student/{nis}")
async def update_absensi(school_code:str,nis:str,data_in: dict,current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    url = f"{domain}/main_a/pengguna/school/{school_code}/student/{nis}"
    response = requests.request("PUT", url,json=data_in,headers={'content-type': 'application/json'})
    jsonq = json.loads(response.content)
    return jsonq