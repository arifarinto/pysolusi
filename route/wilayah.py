from typing import List
from config.config import MGDB
from fastapi.routing import APIRouter
from model.support import DistrictBase, ProvinceBase, RegencyBase


router_wilayah = APIRouter()


@router_wilayah.get("/getAllProvinces", response_model=List[ProvinceBase])
async def get_all_provinces():
    provinces = MGDB["provinsi"].find()
    provinces = await provinces.to_list(length=1000)
    return provinces


@router_wilayah.get("/getRegencies/{idProvince}", response_model=List[RegencyBase])
async def get_regencies(idProvince: str):
    regencies = MGDB["kabupaten_kota"].find({"province_id": idProvince})
    regencies = await regencies.to_list(length=1000)
    return regencies


@router_wilayah.get("/getDistricts/{idRegency}", response_model=List[DistrictBase])
async def get_districts(idRegency: str):
    districts = MGDB["kecamatan"].find({"regency_id": idRegency})
    districts = await districts.to_list(length=1000)
    return districts
