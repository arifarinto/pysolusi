# backend/tancho/sliders/routes.py

from bson.objectid import ObjectId
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
from datetime import datetime
import math

from model.web.slider import SliderBase, SliderOnDB, SliderPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ListToUp, ValidateObjectId, CreateCriteria, FieldObjectIdRequest
from util.util_waktu import dateTimeNow

router_slider = APIRouter()
dbase = MGDB.website_slider

async def GetSliderOr404(id: str):
    _id = ValidateObjectId(id)
    slider = await dbase.find_one({"_id": _id})
    if slider:
        return slider
    else:
        raise HTTPException(status_code=404, detail="Slider not found")

# =================================================================================

@router_slider.post("/slider", response_model=SliderOnDB)
async def add_slider(data_in: SliderBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    slider = IsiDefault(data_in, current_user)
    slider.tags = ListToUp(slider.tags)
    slider_op = await dbase.insert_one(slider.dict())
    if slider_op.inserted_id:
        slider = await GetSliderOr404(slider_op.inserted_id)
        return slider


@router_slider.post("/get_slider/{companyId}", response_model=dict)
async def get_all_sliders(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = SliderPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_slider.get("/slider/{id}", response_model=SliderOnDB)
async def get_slider_by_id(id: ObjectId = Depends(ValidateObjectId)):
    slider = await dbase.find_one({"_id": id})
    if slider:
        return slider
    else:
        raise HTTPException(status_code=404, detail="Slider not found")


@router_slider.delete("/slider/{id}", dependencies=[Depends(GetSliderOr404)], response_model=dict)
async def delete_slider_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    slider_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if slider_op.deleted_count:
        return {"status": f"deleted count: {slider_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_slider.put("/slider/{id_}", response_model=SliderOnDB)
async def update_slider(id_: str, data_in: SliderBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    slider = IsiDefault(data_in, current_user, True)
    slider.updateTime = dateTimeNow()
    if slider.tags : slider.tags = ListToUp(slider.tags)
    slider_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": slider.dict(skip_defaults=True)}
    )
    if slider_op.modified_count:
        return await GetSliderOr404(id_)
    else:
        raise HTTPException(status_code=304)