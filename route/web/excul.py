from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.excul import ExculBase, ExculOnDB, ExculPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import convertStrTimeToTime, dateTimeNow

router_excul = APIRouter()
dbase = MGDB.website_excul

async def GetExculOr404(id: str):
    _id = ValidateObjectId(id)
    excul = await dbase.find_one({"_id": _id})
    if excul:
        return excul
    else:
        raise HTTPException(status_code=404, detail="Excul not found")

# =================================================================================

@router_excul.post("/excul", response_model=ExculOnDB)
async def add_excul(data_in: ExculBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    excul = IsiDefault(data_in, current_user)
    excul.tags = ListToUp(excul.tags)
    data_in.jam = str(data_in.jam)
    excul_op = await dbase.insert_one(excul.dict())
    if excul_op.inserted_id:
        excul = await GetExculOr404(excul_op.inserted_id)
        return excul


@router_excul.post("/get_excul/{companyId}", response_model=dict)
async def get_all_exculs(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = ExculPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_excul.get("/excul/{id}", response_model=ExculOnDB)
async def get_excul_by_id(id: ObjectId = Depends(ValidateObjectId)):
    excul = await dbase.find_one({"_id": id})
    if excul:
        return excul
    else:
        raise HTTPException(status_code=404, detail="Excul not found")


@router_excul.delete("/excul/{id}", dependencies=[Depends(GetExculOr404)], response_model=dict)
async def delete_excul_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    excul_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if excul_op.deleted_count:
        return {"status": f"deleted count: {excul_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_excul.put("/excul/{id_}", response_model=ExculOnDB)
async def update_excul(id_: str, data_in: ExculBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    data_in.jam = str(data_in.jam)
    excul = IsiDefault(data_in, current_user, True)
    excul.updateTime = dateTimeNow()
    if excul.tags : excul.tags = ListToUp(excul.tags)
    excul_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": excul.dict(skip_defaults=True)}
    )
    if excul_op.modified_count:
        return await GetExculOr404(id_)
    else:
        raise HTTPException(status_code=304)
