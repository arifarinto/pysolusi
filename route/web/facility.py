# backend/tancho/facilitys/routes.py

from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from datetime import datetime
import math

from model.web.facility import FacilityBase, FacilityOnDB, FacilityPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ListToUp, ValidateObjectId, CreateCriteria, FieldObjectIdRequest
from util.util_waktu import dateTimeNow

router_facility = APIRouter()
dbase = MGDB.website_facility

async def GetFacilityOr404(id: str):
    _id = ValidateObjectId(id)
    facility = await dbase.find_one({"_id": _id})
    if facility:
        return facility
    else:
        raise HTTPException(status_code=404, detail="Facility not found")

# =================================================================================

@router_facility.post("/facility", response_model=FacilityOnDB)
async def add_facility(data_in: FacilityBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    facility = IsiDefault(data_in, current_user)
    facility.tags = ListToUp(facility.tags)
    facility_op = await dbase.insert_one(facility.dict())
    if facility_op.inserted_id:
        facility = await GetFacilityOr404(facility_op.inserted_id)
        return facility


@router_facility.post("/get_facility/{companyId}", response_model=dict)
async def get_all_facilitys(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = FacilityPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_facility.get("/facility/{id}", response_model=FacilityOnDB)
async def get_facility_by_id(id: ObjectId = Depends(ValidateObjectId)):
    facility = await dbase.find_one({"_id": id})
    if facility:
        return facility
    else:
        raise HTTPException(status_code=404, detail="Facility not found")


@router_facility.delete("/facility/{id}", dependencies=[Depends(GetFacilityOr404)], response_model=dict)
async def delete_facility_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    facility_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if facility_op.deleted_count:
        return {"status": f"deleted count: {facility_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_facility.put("/facility/{id_}", response_model=FacilityOnDB)
async def update_facility(id_: str, data_in: FacilityBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    facility = IsiDefault(data_in, current_user, True)
    facility.updateTime = dateTimeNow()
    if facility.tags : facility.tags = ListToUp(facility.tags)
    facility_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": facility.dict(skip_defaults=True)}
    )
    if facility_op.modified_count:
        return await GetFacilityOr404(id_)
    else:
        raise HTTPException(status_code=304)