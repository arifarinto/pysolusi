from bson.objectid import ObjectId
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.default import JwtToken
from model.web.list import ListDetail, menuEnum
from route.auth import get_current_user
from util.util import RandomString, DollarListDetail
from util.util_waktu import dateTimeNow

router_listdetail = APIRouter()

# =================================================================================

@router_listdetail.post("/list/{menu_name}/{data_id}", response_model=ListDetail)
async def add_list(menu_name:menuEnum, data_id:str, data_in: ListDetail, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    colName = 'website_'+menu_name
    data_in.listId = RandomString()
    data_in.createTime = dateTimeNow()
    await MGDB[colName].update_one(
        {"_id": ObjectId(data_id),"companyId": ObjectId(current_user.companyId)}, 
        {"$addToSet": {"listDetail":data_in.dict()}}
    )
    return data_in.dict()


@router_listdetail.get("/get_list/{menu_name}/{data_id}", response_model=list)
async def get_all_list(menu_name:menuEnum, data_id:str):
    colName = 'website_'+menu_name
    datas = await MGDB[colName].find_one({"_id": ObjectId(data_id)},{"listDetail":1})
    return datas['listDetail']


@router_listdetail.get("/list/{menu_name}/{data_id}/{list_id}", response_model=dict)
async def get_list_by_id(menu_name:menuEnum, data_id:str,list_id:str):
    colName = 'website_'+menu_name
    datas = await MGDB[colName].find_one({"_id": ObjectId(data_id),"listDetail.listId":list_id},{"listDetail.$":1})
    return datas['listDetail'][0]


@router_listdetail.put("/list/{menu_name}/{data_id}/{list_id}", response_model=dict)
async def put_list_by_id(menu_name:menuEnum, data_id:str, list_id:str, data_in: ListDetail, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    colName = 'website_'+menu_name
    data_in.createTime = dateTimeNow()
    data = DollarListDetail(data_in.dict(skip_defaults=True))
    print(data)
    await MGDB[colName].update_one(
        {"_id": ObjectId(data_id),"listDetail.listId":list_id},
        {"$set":data}
    )
    return data_in.dict()


@router_listdetail.delete("/list/{menu_name}/{data_id}/{list_id}", response_model=dict)
async def delete_list_by_id(menu_name:menuEnum, data_id:str, list_id:str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    colName = 'website_'+menu_name
    await MGDB[colName].update_one(
        {"_id": ObjectId(data_id),"listDetail.listId":list_id},
        {"$pull":{"listDetail": {"listId": list_id}}}
    )
    return {"status":"ok"}