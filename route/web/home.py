# backend/tancho/homes/routes.py

from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
from dateutil import relativedelta
import math

from model.web.home import HomeBase, HomeOnDB, HomePage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_home = APIRouter()
dbase = MGDB.website_home

async def GetHomeOr404(id: str):
    _id = ValidateObjectId(id)
    home = await dbase.find_one({"_id": _id})
    if home:
        return home
    else:
        raise HTTPException(status_code=404, detail="Home not found")

# =================================================================================

@router_home.get("/for_home/{id}", response_model=dict)
async def get_home_by_company_id(id: ObjectId = Depends(ValidateObjectId)):
    home = await dbase.find_one({"companyId": id},sort=[('_id', -1)])
    if home:
        print(home)
        home['_id'] = str(home['_id'])
        del home['companyId']
        del home['creatorUserId']
        #get 5 last slider by update, get 5 last news by update, get agenda bulan ini, get 5 last comment
        slider_cursor = MGDB.website_slider.find({"companyId": id},{"imageUrl":1})
        dslider = await slider_cursor.to_list(length=5)
        print(dslider)
        for x in dslider:
            x['_id'] = str(x['_id'])
        home['slider'] = dslider

        news_cursor = MGDB.website_news.find({"companyId": id})
        dnews = await news_cursor.to_list(length=5)
        print(dnews)
        for x in dnews:
            x['_id'] = str(x['_id'])
            del x['companyId']
            del x['creatorUserId']
        home['news'] = dnews

        firstDate = datetime.today().replace(day=1,hour=0,minute=0,second=0)
        lastDate = datetime.today() + relativedelta.relativedelta(months=1, day=1,hour=0,minute=0,second=0)
        filterBulan = {"timePlan": {"$gte":firstDate,"$lt":lastDate}}
        agenda_cursor = MGDB.website_agenda.find({"$and": [{"companyId": id},filterBulan]})
        dagenda = await agenda_cursor.to_list(length=10)
        print(dagenda)
        for x in dagenda:
            x['_id'] = str(x['_id'])
            del x['companyId']
            del x['creatorUserId']
        home['agendas'] = dagenda

        testimony_cursor = MGDB.website_testimony.find({"companyId": id})
        dtestimony = await testimony_cursor.to_list(length=5)
        print(dtestimony)
        for x in dtestimony:
            x['_id'] = str(x['_id'])
            del x['companyId']
            del x['creatorUserId']
        home['comments'] = dtestimony

        return home
    else:
        raise HTTPException(status_code=404, detail="Home not found")


@router_home.post("/home", response_model=HomeOnDB)
async def add_home(data_in: HomeBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    home = IsiDefault(data_in, current_user)
    home_op = await dbase.insert_one(home.dict())
    if home_op.inserted_id:
        home = await GetHomeOr404(home_op.inserted_id)
        return home


@router_home.post("/get_home", response_model=dict)
async def get_all_homes(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    # search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    # criteria = CreateCriteria(search)
    criteria = {}
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = HomePage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_home.post("/get_home_by_company_id/{company_id}", response_model=dict)
async def get_home_by_company_id(company_id: str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    # search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    # criteria = CreateCriteria(search)
    criteria = {"companyId": ObjectId(company_id)}
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = HomePage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_home.get("/home/{id}", response_model=HomeOnDB)
async def get_home_by_id(id: ObjectId = Depends(ValidateObjectId)):
    home = await dbase.find_one({"_id": id})
    if home:
        return home
    else:
        raise HTTPException(status_code=404, detail="Home not found")


@router_home.delete("/home/{id}", dependencies=[Depends(GetHomeOr404)], response_model=dict)
async def delete_home_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    home_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if home_op.deleted_count:
        return {"status": f"deleted count: {home_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_home.put("/home/{id_}", response_model=HomeOnDB)
async def update_home(id_: str, data_in: HomeBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    home = IsiDefault(data_in, current_user, True)
    home.updateTime = dateTimeNow()
    home_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": home.dict(skip_defaults=True)}
    )
    if home_op.modified_count:
        return await GetHomeOr404(id_)
    else:
        raise HTTPException(status_code=304)