# backend/tancho/weeklys/routes.py

from bson.objectid import ObjectId
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime,date
from dateutil import relativedelta
import math

from model.web.weekly import WeeklyBase, WeeklyOnDB, WeeklyPage, MonthType
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import convertStrDateToDate, dateTimeNow

import calendar
import numpy as np
calendar.setfirstweekday(6)

router_weekly = APIRouter()
dbase = MGDB.website_weekly

async def GetWeeklyOr404(id: str):
    _id = ValidateObjectId(id)
    weekly = await dbase.find_one({"_id": _id})
    if weekly:
        return weekly
    else:
        raise HTTPException(status_code=404, detail="Weekly not found")

# =================================================================================

@router_weekly.post("/weekly", response_model=WeeklyOnDB)
async def add_weekly(data_in: WeeklyBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    weekly = IsiDefault(data_in, current_user)
    weekly.tags = ListToUp(weekly.tags)
    weekly.dateStart = convertStrDateToDate(weekly.dateStart)
    weekly.dateFinish = convertStrDateToDate(weekly.dateFinish)
    weekly_op = await dbase.insert_one(weekly.dict())
    if weekly_op.inserted_id:
        weekly = await GetWeeklyOr404(weekly_op.inserted_id)
        return weekly


@router_weekly.post("/get_weekly/{companyId}", response_model=dict)
async def get_all_weeklys(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = WeeklyPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_weekly.post("/get_by_month/{companyId}/{month}", response_model=List)
async def get_all_weeklys(companyId:str, month: MonthType, filter_key: str ='', filter_value:str=''):
    search = {}
    search[filter_key] = filter_value
    if filter_key == '' or filter_value == '' : search = {}
 
    firstDate = datetime.today()
    lastDate = datetime.today()
    if month == 'last':
        firstDate = lastDate + relativedelta.relativedelta(months=-1, day=1,hour=0,minute=0,second=0)
        lastDate = lastDate.replace(day=1,hour=0,minute=0,second=0)
    if month == 'now':
        firstDate = firstDate.replace(day=1,hour=0,minute=0,second=0)
        lastDate = lastDate + relativedelta.relativedelta(months=1, day=1,hour=0,minute=0,second=0)
    if month == 'next':
        firstDate = firstDate + relativedelta.relativedelta(months=1, day=1,hour=0,minute=0,second=0)
        lastDate = lastDate + relativedelta.relativedelta(months=2, day=1,hour=0,minute=0,second=0)

    print(firstDate)
    kal = calendar.monthcalendar(firstDate.year,firstDate.month)
    # print(get_week_of_month(firstDate.year, firstDate.month, firstDate.day))
    # print(calendar.monthcalendar(firstDate.year,firstDate.month))
    # print(len(calendar.monthcalendar(firstDate.year,firstDate.month)))
    # print(calendar.monthcalendar(firstDate.year,firstDate.month)[4][0])

    mingguan = []
    for i in range(len(kal)):
        data = {}
        data['tahun'] = firstDate.year
        data['bulan'] = firstDate.month
        data['range_week'] = kal[i]
        tg = kal[i][0]
        if tg == 0 : tg=1
        fd = firstDate.replace(day=tg,hour=0,minute=0,second=0)
        tg2 = kal[i][-1]
        if tg2 == 0 : tg2=kal[i][-2]
        if tg2 == 0 : tg2=kal[i][-3]
        if tg2 == 0 : tg2=kal[i][-4]
        if tg2 == 0 : tg2=kal[i][-5]
        if tg2 == 0 : tg2=kal[i][-6]
        if tg2 == 0 : tg2=kal[i][-7]
        ld = firstDate.replace(day=tg2,hour=0,minute=0,second=0)

        w_cursor = MGDB.website_weekly.find({"$and": [{"companyId": ObjectId(companyId)},search,{"dateStart": {"$gte":fd,"$lte":ld}}]}).sort("dateStart",1)
        w_c = await w_cursor.to_list(100)

        for x in w_c:
            x['_id'] = str(x['_id'])
            del x['companyId']
            del x['creatorUserId']

        data['data'] = w_c
        mingguan.append(data)
    return mingguan


@router_weekly.get("/weekly/{id}", response_model=WeeklyOnDB)
async def get_weekly_by_id(id: ObjectId = Depends(ValidateObjectId)):
    weekly = await dbase.find_one({"_id": id})
    if weekly:
        return weekly
    else:
        raise HTTPException(status_code=404, detail="Weekly not found")


@router_weekly.delete("/weekly/{id}", dependencies=[Depends(GetWeeklyOr404)], response_model=dict)
async def delete_weekly_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    weekly_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if weekly_op.deleted_count:
        return {"status": f"deleted count: {weekly_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_weekly.put("/weekly/{id_}", response_model=WeeklyOnDB)
async def update_weekly(id_: str, data_in: WeeklyBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    weekly = IsiDefault(data_in, current_user, True)
    weekly.updateTime = dateTimeNow()
    if weekly.tags : weekly.tags = ListToUp(weekly.tags)
    if weekly.dateStart : weekly.dateStart = datetime.fromisoformat(f"{weekly.dateStart}")
    if weekly.dateFinish : weekly.dateFinish = datetime.fromisoformat(f"{weekly.dateFinish}")

    weekly_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": weekly.dict(skip_defaults=True)}
    )

    if weekly_op.modified_count:
        return await GetWeeklyOr404(id_)

    else:
        raise HTTPException(status_code=304)