# backend/tancho/faqs/routes.py

from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.faq import FaqBase, FaqOnDB, FaqPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from route.auth import get_current_user
from util.util import ValidateObjectId, IsiDefault, CreateCriteria

router_faq = APIRouter()
dbase = MGDB.tbl_faq

async def GetFaqOr404(id: str):
    _id = ValidateObjectId(id)
    faq = await dbase.find_one({"_id": _id})
    if faq:
        return faq
    else:
        raise HTTPException(status_code=404, detail="Faq not found")

# =================================================================================

@router_faq.post("/faq", response_model=FaqOnDB)
async def add_faq(data_in: FaqBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    faq = IsiDefault(data_in, current_user)
    faq_op = await dbase.insert_one(faq.dict())
    if faq_op.inserted_id:
        faq = await GetFaqOr404(faq_op.inserted_id)
        return faq


@router_faq.post("/get_faq", response_model=dict)
async def get_all_faqs(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    # search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    # criteria = CreateCriteria(search)
    criteria = {}
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = FaqPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_faq.get("/faq/{id}", response_model=FaqOnDB)
async def get_faq_by_id(id: ObjectId = Depends(ValidateObjectId)):
    faq = await dbase.find_one({"_id": id})
    if faq:
        return faq
    else:
        raise HTTPException(status_code=404, detail="Faq not found")


@router_faq.post("/faq_by_company_id/{company_id}", response_model=dict)
async def get_faq_by_company_id(company_id:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    criteria = {"companyId": ObjectId(company_id)}
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = FaqPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_faq.delete("/faq/{id}", dependencies=[Depends(GetFaqOr404)], response_model=dict)
async def delete_faq_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    faq_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if faq_op.deleted_count:
        return {"status": f"deleted count: {faq_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_faq.put("/faq/{id_}", response_model=FaqOnDB)
async def update_faq(id_: str, data_in: FaqBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    faq = IsiDefault(data_in, current_user, True)
    faq.updateTime = dateTimeNow()
    faq_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": faq.dict(skip_defaults=True)}
    )
    if faq_op.modified_count:
        return await GetFaqOr404(id_)
    else:
        raise HTTPException(status_code=304)