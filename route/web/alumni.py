from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.alumni import AlumniBase, AlumniOnDB, AlumniPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import dateTimeNow

router_alumni = APIRouter()
dbase = MGDB.website_alumni

async def GetAlumniOr404(id: str):
    _id = ValidateObjectId(id)
    alumni = await dbase.find_one({"_id": _id})
    if alumni:
        return alumni
    else:
        raise HTTPException(status_code=404, detail="Alumni not found")

# =================================================================================

@router_alumni.post("/alumni", response_model=AlumniOnDB)
async def add_alumni(data_in: AlumniBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    alumni = IsiDefault(data_in, current_user)
    alumni.tags = ListToUp(alumni.tags)
    alumni_op = await dbase.insert_one(alumni.dict())
    if alumni_op.inserted_id:
        alumni = await GetAlumniOr404(alumni_op.inserted_id)
        return alumni


@router_alumni.post("/get_alumni/{companyId}", response_model=dict)
async def get_all_alumnis(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = AlumniPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_alumni.get("/alumni/{id}", response_model=AlumniOnDB)
async def get_alumni_by_id(id: ObjectId = Depends(ValidateObjectId)):
    alumni = await dbase.find_one({"_id": id})
    if alumni:
        return alumni
    else:
        raise HTTPException(status_code=404, detail="Alumni not found")


@router_alumni.delete("/alumni/{id}", dependencies=[Depends(GetAlumniOr404)], response_model=dict)
async def delete_alumni_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    alumni_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if alumni_op.deleted_count:
        return {"status": f"deleted count: {alumni_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_alumni.put("/alumni/{id_}", response_model=AlumniOnDB)
async def update_alumni(id_: str, data_in: AlumniBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    alumni = IsiDefault(data_in, current_user, True)
    alumni.updateTime = dateTimeNow()
    if alumni.tags : alumni.tags = ListToUp(alumni.tags)
    alumni_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": alumni.dict(skip_defaults=True)}
    )
    if alumni_op.modified_count:
        return await GetAlumniOr404(id_)
    else:
        raise HTTPException(status_code=304)
