from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.curiculum import CuriculumBase, CuriculumOnDB, CuriculumPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import dateTimeNow

router_curiculum = APIRouter()
dbase = MGDB.website_curiculum

async def GetCuriculumOr404(id: str):
    _id = ValidateObjectId(id)
    curiculum = await dbase.find_one({"_id": _id})
    if curiculum:
        return curiculum
    else:
        raise HTTPException(status_code=404, detail="Curiculum not found")

# =================================================================================

@router_curiculum.post("/curiculum", response_model=CuriculumOnDB)
async def add_curiculum(data_in: CuriculumBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    curiculum = IsiDefault(data_in, current_user)
    curiculum.tags = ListToUp(curiculum.tags)
    curiculum_op = await dbase.insert_one(curiculum.dict())
    if curiculum_op.inserted_id:
        curiculum = await GetCuriculumOr404(curiculum_op.inserted_id)
        return curiculum


@router_curiculum.post("/get_curiculum/{companyId}", response_model=dict)
async def get_all_curiculums(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = CuriculumPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_curiculum.get("/curiculum/{id}", response_model=CuriculumOnDB)
async def get_curiculum_by_id(id: ObjectId = Depends(ValidateObjectId)):
    curiculum = await dbase.find_one({"_id": id})
    if curiculum:
        return curiculum
    else:
        raise HTTPException(status_code=404, detail="Curiculum not found")


@router_curiculum.delete("/curiculum/{id}", dependencies=[Depends(GetCuriculumOr404)], response_model=dict)
async def delete_curiculum_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    curiculum_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if curiculum_op.deleted_count:
        return {"status": f"deleted count: {curiculum_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_curiculum.put("/curiculum/{id_}", response_model=CuriculumOnDB)
async def update_curiculum(id_: str, data_in: CuriculumBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    curiculum = IsiDefault(data_in, current_user, True)
    curiculum.updateTime = dateTimeNow()
    if curiculum.tags : curiculum.tags = ListToUp(curiculum.tags)
    curiculum_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": curiculum.dict(skip_defaults=True)}
    )
    if curiculum_op.modified_count:
        return await GetCuriculumOr404(id_)
    else:
        raise HTTPException(status_code=304)
