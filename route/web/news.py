# backend/tancho/newss/routes.py

from bson.objectid import ObjectId
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.news import NewsBase, NewsOnDB, NewsPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ListToUp, ValidateObjectId, CreateCriteria, FieldObjectIdRequest
from util.util_waktu import dateTimeNow

router_news = APIRouter()
dbase = MGDB.website_news

async def GetNewsOr404(id: str):
    _id = ValidateObjectId(id)
    news = await dbase.find_one({"_id": _id})
    if news:
        return news
    else:
        raise HTTPException(status_code=404, detail="News not found")

# =================================================================================

@router_news.post("/news", response_model=NewsOnDB)
async def add_news(data_in: NewsBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    news = IsiDefault(data_in, current_user)
    news.tags = ListToUp(news.tags)
    news_op = await dbase.insert_one(news.dict())
    if news_op.inserted_id:
        news = await GetNewsOr404(news_op.inserted_id)
        return news


@router_news.post("/get_news/{companyId}", response_model=dict)
async def get_all_news(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = NewsPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_news.get("/news/{id}", response_model=NewsOnDB)
async def get_news_by_id(id: ObjectId = Depends(ValidateObjectId)):
    news = await dbase.find_one({"_id": id})
    if news:
        return news
    else:
        raise HTTPException(status_code=404, detail="News not found")


@router_news.delete("/news/{id}", dependencies=[Depends(GetNewsOr404)], response_model=dict)
async def delete_news_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    news_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if news_op.deleted_count:
        return {"status": f"deleted count: {news_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_news.put("/news/{id_}", response_model=NewsOnDB)
async def update_news(id_: str, data_in: NewsBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    news = IsiDefault(data_in, current_user, True)
    news.updateTime = dateTimeNow()
    if news.tags : news.tags = ListToUp(news.tags)
    news_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": news.dict(skip_defaults=True)}
    )
    if news_op.modified_count:
        return await GetNewsOr404(id_)
    else:
        raise HTTPException(status_code=304)