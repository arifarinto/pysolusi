# backend/tancho/agendas/routes.py

from bson.objectid import ObjectId
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
from dateutil import relativedelta
import math

from model.web.agenda import AgendaBase, AgendaOnDB, AgendaPage, MonthType
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import convertStrDateTimeToDateTime, dateTimeNow

router_agenda = APIRouter()
dbase = MGDB.website_agenda

async def GetAgendaOr404(id: str):
    _id = ValidateObjectId(id)
    agenda = await dbase.find_one({"_id": _id})
    if agenda:
        return agenda
    else:
        raise HTTPException(status_code=404, detail="Agenda not found")

# =================================================================================

@router_agenda.post("/agenda", response_model=AgendaOnDB)
async def add_agenda(data_in: AgendaBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    agenda = IsiDefault(data_in, current_user)
    agenda.tags = ListToUp(agenda.tags)
    # if not agenda.timePlan:
    #     raise HTTPException(status_code=400, detail="Waktu atau Time Plan harus di isi dengan format 2020-12-01 08:00:00")
    # agenda.timePlan = convertStrDateTimeToDateTime(agenda.timePlan)
    agenda_op = await dbase.insert_one(agenda.dict())
    if agenda_op.inserted_id:
        agenda = await GetAgendaOr404(agenda_op.inserted_id)
        return agenda


@router_agenda.post("/get_agenda/{companyId}", response_model=dict)
async def get_all_agendas(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = AgendaPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_agenda.get("/get_by_month/{companyId}/{month}", response_model=list)
async def get_all_agendas(companyId:str, month: MonthType):
    firstDate = datetime.today()
    lastDate = datetime.today()
    if month == 'last':
        firstDate = lastDate + relativedelta.relativedelta(months=-1, day=1,hour=0,minute=0,second=0)
        lastDate = lastDate.replace(day=1,hour=0,minute=0,second=0)
    if month == 'now':
        firstDate = firstDate.replace(day=1,hour=0,minute=0,second=0)
        lastDate = lastDate + relativedelta.relativedelta(months=1, day=1,hour=0,minute=0,second=0)
    if month == 'next':
        firstDate = firstDate + relativedelta.relativedelta(months=1, day=1,hour=0,minute=0,second=0)
        lastDate = lastDate + relativedelta.relativedelta(months=2, day=1,hour=0,minute=0,second=0)
    print(firstDate)
    print(lastDate)
    filterBulan = {"timePlan": {"$gte":firstDate,"$lt":lastDate}}
    agenda_cursor = MGDB.website_agenda.find({"$and": [{"companyId": ObjectId(companyId)},filterBulan]}).sort("timePlan",1)
    dagenda = await agenda_cursor.to_list(length=100)
    print(dagenda)
    for x in dagenda:
        x['_id'] = str(x['_id'])
        del x['companyId']
        del x['creatorUserId']
    return dagenda


@router_agenda.get("/agenda/{id}", response_model=AgendaOnDB)
async def get_agenda_by_id(id: ObjectId = Depends(ValidateObjectId)):
    agenda = await dbase.find_one({"_id": id})
    if agenda:
        return agenda
    else:
        raise HTTPException(status_code=404, detail="Agenda not found")


@router_agenda.delete("/agenda/{id}", dependencies=[Depends(GetAgendaOr404)], response_model=dict)
async def delete_agenda_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    agenda_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if agenda_op.deleted_count:
        return {"status": f"deleted count: {agenda_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_agenda.put("/agenda/{id_}", response_model=AgendaOnDB)
async def update_agenda(id_: str, data_in: AgendaBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    agenda = IsiDefault(data_in, current_user, True)
    agenda.updateTime = dateTimeNow()
    if agenda.tags : agenda.tags = ListToUp(agenda.tags)
    # if agenda.timePlan : agenda.timePlan = convertStrDateTimeToDateTime(agenda.timePlan)
    agenda_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": agenda.dict(skip_defaults=True)}
    )
    if agenda_op.modified_count:
        return await GetAgendaOr404(id_)
    else:
        raise HTTPException(status_code=304)