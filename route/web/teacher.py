from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.teacher import TeacherBase, TeacherOnDB, TeacherPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import dateTimeNow

router_teacher = APIRouter()
dbase = MGDB.website_teacher

async def GetTeacherOr404(id: str):
    _id = ValidateObjectId(id)
    teacher = await dbase.find_one({"_id": _id})
    if teacher:
        return teacher
    else:
        raise HTTPException(status_code=404, detail="Teacher not found")

# =================================================================================

@router_teacher.post("/teacher", response_model=TeacherOnDB)
async def add_teacher(data_in: TeacherBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    teacher = IsiDefault(data_in, current_user)
    teacher.tags = ListToUp(teacher.tags)
    teacher_op = await dbase.insert_one(teacher.dict())
    if teacher_op.inserted_id:
        teacher = await GetTeacherOr404(teacher_op.inserted_id)
        return teacher


@router_teacher.post("/get_teacher/{companyId}", response_model=dict)
async def get_all_teachers(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = TeacherPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_teacher.get("/teacher/{id}", response_model=TeacherOnDB)
async def get_teacher_by_id(id: ObjectId = Depends(ValidateObjectId)):
    teacher = await dbase.find_one({"_id": id})
    if teacher:
        return teacher
    else:
        raise HTTPException(status_code=404, detail="Teacher not found")


@router_teacher.delete("/teacher/{id}", dependencies=[Depends(GetTeacherOr404)], response_model=dict)
async def delete_teacher_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    teacher_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if teacher_op.deleted_count:
        return {"status": f"deleted count: {teacher_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_teacher.put("/teacher/{id_}", response_model=TeacherOnDB)
async def update_teacher(id_: str, data_in: TeacherBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    teacher = IsiDefault(data_in, current_user, True)
    teacher.updateTime = dateTimeNow()
    if teacher.tags : teacher.tags = ListToUp(teacher.tags)
    teacher_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": teacher.dict(skip_defaults=True)}
    )
    if teacher_op.modified_count:
        return await GetTeacherOr404(id_)
    else:
        raise HTTPException(status_code=304)
