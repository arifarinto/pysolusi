# backend/tancho/programs/routes.py

from bson.objectid import ObjectId
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
from datetime import datetime
import math

from model.web.program import ProgramBase, ProgramOnDB, ProgramPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ListToUp, ValidateObjectId, CreateCriteria, FieldObjectIdRequest
from util.util_waktu import dateTimeNow

router_program = APIRouter()
dbase = MGDB.website_program

async def GetProgramOr404(id: str):
    _id = ValidateObjectId(id)
    program = await dbase.find_one({"_id": _id})
    if program:
        return program
    else:
        raise HTTPException(status_code=404, detail="Program not found")

# =================================================================================

@router_program.post("/program", response_model=ProgramOnDB)
async def add_program(data_in: ProgramBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    program = IsiDefault(data_in, current_user)
    program.tags = ListToUp(program.tags)
    program_op = await dbase.insert_one(program.dict())
    if program_op.inserted_id:
        program = await GetProgramOr404(program_op.inserted_id)
        return program


@router_program.post("/get_program/{companyId}", response_model=dict)
async def get_all_program(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = ProgramPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_program.get("/program/{id}", response_model=ProgramOnDB)
async def get_program_by_id(id: ObjectId = Depends(ValidateObjectId)):
    program = await dbase.find_one({"_id": id})
    if program:
        return program
    else:
        raise HTTPException(status_code=404, detail="Program not found")


@router_program.delete("/program/{id}", dependencies=[Depends(GetProgramOr404)], response_model=dict)
async def delete_program_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    program_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if program_op.deleted_count:
        return {"status": f"deleted count: {program_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_program.put("/program/{id_}", response_model=ProgramOnDB)
async def update_program(id_: str, data_in: ProgramBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    program = IsiDefault(data_in, current_user, True)
    program.updateTime = dateTimeNow()
    if program.tags : program.tags = ListToUp(program.tags)
    program_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": program.dict(skip_defaults=True)}
    )
    if program_op.modified_count:
        return await GetProgramOr404(id_)
    else:
        raise HTTPException(status_code=304)