# backend/tancho/pemarkirs/routes.py
from function.user import CreateUser
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile, datastructures
from typing import List
from datetime import datetime, timedelta
import logging
import math
import pandas as pd
from model.support import (
    InvoiceData,
    InvoicePage,
    PipelineData,
    InvoiceInput,
    InvoiceOnDB,
    PaymentStatusEnum,
)
from function.invoice import (
    CreateInvoice,
    DeleteInvoice,
    GetInvoiceBaseOnMonthAndYear,
    GetInvoiceOr404,
    GetInvoicesByCompanyId,
    GetInvoicesOnUserId,
    UpdateInvoice,
)
from model.parkir.user_parkir import (
    UserParkirBase,
    UserParkirOnDB,
    UserParkirPage,
    PemarkirTempData,
)
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import IdentityData, JwtToken, UserInput
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import convertDateToStrDate, convertDateToStrPassword, dateTimeNow

router_user_temp = APIRouter()
dbase = MGDB.user_parkir_data


async def GetDataUserOr404(id_: str):
    _id = ValidateObjectId(id_)
    dataTemp = await dbase.find_one({"_id": _id})
    if dataTemp:
        return dataTemp
    else:
        raise HTTPException(status_code=404, detail="Data user not found")


# data user=================================================================================


@router_user_temp.post("/create_user_data", response_model=UserParkirOnDB)
async def add_user_data_tiap_hari(
    dataIn: UserParkirBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    dataTemp = IsiDefault(dataIn, current_user)
    dataTemp.tanggal = convertDateToStrDate(dataTemp.tanggal)
    cUserTemp = await dbase.find_one(
        {"companyId": ObjectId(current_user.companyId),
        "tanggal": dataTemp.tanggal}
    )
    if cUserTemp:
        raise HTTPException(status_code=400, detail="Tanggal sudah ada")
    dataTemp_op = await dbase.insert_one(dataTemp.dict())
    if dataTemp_op.inserted_id:
        dataTemp = await GetDataUserOr404(dataTemp_op.inserted_id)
        return dataTemp


@router_user_temp.post("/get_all_data_user_temp", response_model=dict)
async def get_all_data_user_temp(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = UserParkirPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_user_temp.get("/get_data_user_temp/{id_}", response_model=UserParkirBase)
async def get_data_user_temp_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    dataTemp = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if dataTemp:
        return dataTemp
    else:
        raise HTTPException(status_code=404, detail="Data user temp not found")


@router_user_temp.put("/data_user_temp/{_id}", response_model=UserParkirOnDB)
async def update_data_user_temp(_id: str, dataIn: UserParkirBase, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    dataTemp = IsiDefault(dataIn, current_user, True)
    dataTemp.tanggal = convertDateToStrDate(dataTemp.tanggal)
    dataTemp.updateTime = dateTimeNow()
    dataTemp_op = await dbase.update_one(
        {"_id": ObjectId(_id), "companyId": ObjectId(current_user.companyId)},
        {"$set": dataTemp.dict(skip_defaults=True)}
    )
    if dataTemp_op.modified_count:
        return await GetDataUserOr404(_id)
    else:
        raise HTTPException(status_code=304)


# user temporary =================================================================================


@router_user_temp.post(
    "/create_pemarkir_temp/{id_data}", response_model=PemarkirTempData
)
async def add_pemarkir_temp(
    id_data: str,
    data_in: PemarkirTempData,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):

    data_in.karcis.id = ObjectId()
    data_in.karcis.waktuMasuk = dateTimeNow()
    userTemp_op = await dbase.update_one(
        {"_id": ObjectId(id_data)}, {"$addToSet": {"users": data_in.dict()}}
    )
    if userTemp_op.modified_count:
        return data_in
    else:
        raise HTTPException(status_code=304)


@router_user_temp.get("/get_all_userTemps/{companyId}/{id_data}", response_model=List[PemarkirTempData])
async def get_all_user_temp(companyId: str, id_data: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),):
    pipeline = [
        {"$unwind": {"path": "$users"}},
        {"$match": {"companyId": ObjectId(companyId), "_id": ObjectId(id_data)}},
        {"$replaceRoot": {"newRoot": "$users"}}
    ]
    userTemp = await dbase.aggregate(pipeline).to_list(10000)
    if not userTemp:
        raise HTTPException(status_code=404, detail="pemarkir tidak ditemukan")
    return userTemp


@router_user_temp.get(
    "/get_userTemp_by_userId/{id_data}/{userTempId}", response_model=PemarkirTempData
)
async def get_user_temp_by_userId(
    id_data: str,
    userTempId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$match": {"_id": ObjectId(id_data)}},
        {"$unwind": {"path": "$users"}},
        {"$replaceRoot": {"newRoot": "$users"}},
        {"$match": {"userTempId": userTempId}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        userTemp = result[0]
        return userTemp
    else:
        raise HTTPException(status_code=404, detail="pemarkir tidak ditemukan")


@router_user_temp.delete("/userTemp/{id_data}/{userTempId}", response_model=dict)
async def delete_user_temp(
    id_data: str,
    userTempId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$match": {"_id": ObjectId(id_data)}},
        {"$unwind": {"path": "$users"}},
        {"$replaceRoot": {"newRoot": "$users"}},
        {"$match": {"userTempId": userTempId}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if not result:
        raise HTTPException(status_code=404, detail="pemarkir tidak ditemukan")

    userTemp_op = await dbase.update_one(
        {"_id": ObjectId(id_data), "users.userTempId": userTempId},
        {"$pull": {"users": {"userTempId": userTempId}}},
    )
    if userTemp_op.modified_count:
        return {"status": f"deleted count: {userTemp_op.modified_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_user_temp.put(
    "/userTemp/{id_data}/{userTempId}", response_model=PemarkirTempData
)
async def update_user_temp(
    id_data: str,
    userTempId: str,
    data_in: PemarkirTempData,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$match": {"_id": ObjectId(id_data)}},
        {"$unwind": {"path": "$users"}},
        {"$replaceRoot": {"newRoot": "$users"}},
        {"$match": {"userTempId": userTempId}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if not result:
        raise HTTPException(status_code=404, detail="pemarkir tidak ditemukan")
    userTemp = result[0]
    data_in = data_in.dict(skip_defaults=True)
    data_in = {k: v for k, v in data_in.items() if v is not None}
    userTemp.update(data_in)
    userTemp_op = await dbase.update_one(
        {"_id": ObjectId(id_data), "users.userTempId": userTempId},
        {"$set": {"users.$": userTemp}},
    )
    if userTemp_op.modified_count:
        return userTemp
    else:
        raise HTTPException(status_code=304)
