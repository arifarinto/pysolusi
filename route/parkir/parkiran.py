from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.parkir.parkiran import (
    HargaData,
    ParkiranBase,
    ParkiranOnDB,
    ParkiranPage,
    TipeEnum,
    comboParkiran)
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_parkiran = APIRouter()
dbase = MGDB.parkir_parkiran

async def GetParkiranOr404(id_: str):
    _id = ValidateObjectId(id_)
    parkiran = await dbase.find_one({"_id": _id})
    if parkiran:
        return parkiran
    else:
        raise HTTPException(status_code=404, detail="Parkiran not found")

# =================================================================================
# parkiran

@router_parkiran.post("/parkiran", response_model=ParkiranOnDB)
async def add_parkiran(data_in: ParkiranBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    parkiran = IsiDefault(data_in, current_user)
    parkiran.name = parkiran.name.upper()
    cparkiran = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "name": parkiran.name})
    if cparkiran:
        raise HTTPException(status_code=400, detail="Parkiran with Name : "+parkiran.name+" is registered")
    parkiran.kode = parkiran.kode.upper()
    parkiran.tags = ListToUp(parkiran.tags)
    parkiran_op = await dbase.insert_one(parkiran.dict())
    if parkiran_op.inserted_id:
        parkiran = await GetParkiranOr404(parkiran_op.inserted_id)
        return parkiran


@router_parkiran.post("/get_parkiran", response_model=dict)
async def get_all_parkirans(size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    print(datas)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = ParkiranPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_parkiran.get("/parkiran/{id_}", response_model=ParkiranOnDB)
async def get_parkiran_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    parkiran = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if parkiran:
        return parkiran
    else:
        raise HTTPException(status_code=404, detail="Parkiran not found")


@router_parkiran.delete("/parkiran/{id_}", dependencies=[Depends(GetParkiranOr404)], response_model=dict)
async def delete_parkiran_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    parkiran_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if parkiran_op.deleted_count:
        return {"status": f"deleted count: {parkiran_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_parkiran.put("/parkiran/{id_}", response_model=ParkiranOnDB)
async def update_parkiran(id_: str, data_in: ParkiranBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # parkiran = IsiDefault(data_in, current_user, True)
    parkiran = data_in
    parkiran.updateTime = dateTimeNow()
    parkiran_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": parkiran.dict(skip_defaults=True)}
    )
    if parkiran_op.modified_count or parkiran_op.matched_count:
        return await GetParkiranOr404(id_)
    else:
        raise HTTPException(status_code=304)


# =================================================================================
# harga parkir

@router_parkiran.get("/combo_tipe_kendaraan",response_model=list)
async def get_combo_tipe_kendaraam():
    return list(TipeEnum)


@router_parkiran.post("/create_harga_parkir/{idParkiran}", response_model=HargaData)
async def add_harga_parkir(idParkiran: str, data_in: HargaData, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    # cek apakah di db sudah ada harga untuk tipe kendaraan tsb
    pipeline = [
        {"$match": {"_id": ObjectId(idParkiran)}},
        {"$unwind": {"path": "$daftarHarga"}},
        {"$replaceRoot": {"newRoot": "$daftarHarga"}},
        {"$match": {"tipe": data_in.tipe}},
    ]
    charga = await dbase.aggregate(pipeline).to_list(1000)
    if charga:
        raise HTTPException(status_code=400, detail=f"daftar harga dengan tipe kendaraan {data_in.tipe} sudah ada")

    harga_op =  await dbase.update_one(
        {"_id": ObjectId(idParkiran)},
        {"$addToSet": { "daftarHarga":data_in.dict()}}
    )
    if harga_op.modified_count:
        return data_in
    else:
        raise HTTPException(status_code=304)
    

@router_parkiran.get("/get_harga/{companyId}/{idParkiran}", response_model= dict)
async def get_daftar_harga(companyId: str, idParkiran: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    pipeline = [
                {"$unwind": {"path": "$daftarHarga"}},
                {"$match": {"companyId": ObjectId(companyId), "_id": ObjectId(idParkiran)}},
                {"$replaceRoot": {"newRoot": "$daftarHarga"}},
                {
                    "$project": {
                        "tipe": "$tipe",
                        "harga": "$harga"
                        }
                }
            ]
    harga = await dbase.aggregate(pipeline).to_list(10000)
    if not harga:
        raise HTTPException (status_code=404, detail= "daftar harga tidak ditemukan")
    data_harga = {}
    data_harga["parkiran"] = idParkiran
    data_harga['daftar harga'] = harga
    return data_harga


@router_parkiran.get("/get_harga/{idParkiran}")
async def get_harga_by_tipe(idParkiran: str, tipe: TipeEnum, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),):
    pipeline = [
        {"$match": {"_id": ObjectId(idParkiran)}},
        {"$unwind": {"path": "$daftarHarga"}},
        {"$replaceRoot": {"newRoot": "$daftarHarga"}},
        {"$match": {"tipe": tipe}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        harga = result[0]
        return harga
    else:
        raise HTTPException(
            status_code=404,
            detail=f"harga dengan tipe kendaraan {tipe} tidak ditemukan",
        )


@router_parkiran.put("/update_harga/{idParkiran}", response_model= HargaData)
async def update_harga(idParkiran: str, tipe: TipeEnum, data_in: HargaData, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    pipeline = [
        {"$match": {"_id": ObjectId(idParkiran)}},
        {"$unwind": {"path": "$daftarHarga"}},
        {"$replaceRoot": {"newRoot": "$daftarHarga"}},
        {"$match": {"tipe": tipe}}
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if not result:
        raise HTTPException (status_code=404, detail= f"harga dengan tipe kendaraan {tipe} tidak ditemukan")
    harga = result[0]
    harga["harga"] = data_in.harga
    harga_op = await dbase.update_one(
        {"_id": ObjectId(idParkiran), "daftarHarga.tipe": tipe},
        {"$set": {"daftarHarga.$": harga}},
    )
    if harga_op.modified_count:
        return harga
    else:
        raise HTTPException(status_code=304)