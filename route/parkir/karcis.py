from io import BytesIO
import datetime
from time import strftime
from pydantic.networks import HttpUrl
from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import (
    APIRouter,
    Response,
    Depends,
    HTTPException,
    Security,
    File,
    UploadFile,
)
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.parkir.parkiran import TipeEnum
from model.parkir.user_parkir import (
    KarcisData,
    KarcisInput,
    KarcisGatewayData,
    PemarkirTempData,
    UserParkirBase,
    UserParkirOnDB,
    UserParkirPage,
)
from model.util import ObjectIdStr, SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import (
    ListToUp,
    ValidateObjectId,
    IsiDefault,
    CreateCriteria,
    CreateArrayCriteria,
    cleanNullTerms,
)
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_karcis = APIRouter()
dbase = MGDB.user_parkir_data

# ============================================================================================                                                                                             ===============================================


@router_karcis.post("/create_karcis/{id_data}/{userTempId}", response_model=KarcisData)
async def add_karcis(
    id_data: str,
    userTempId: str,
    tipe: TipeEnum,
    dataIn: KarcisInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # get data karcis
    pipeline = [
        {"$unwind": {"path": "$users"}},
        {"$replaceRoot": {"newRoot": "$users"}},
        {"$match": {"userTempId": userTempId}},
        {"$replaceRoot": {"newRoot": "$karcis"}},
    ]
    results = await MGDB.user_parkir_data.aggregate(pipeline).to_list(1000)
    if results:
        dataIn = dataIn.dict()
        results[0] = cleanNullTerms(results[0])
        dataIn.update(results[0])
        dataIn["waktuKeluar"] = dateTimeNow()

    print("datain: " + str(dataIn))
    karcis = KarcisData(**dataIn)
    print("karcis: " + str(karcis))
    karcis.tipeKendaraan = tipe

    # get data harga sesuai tipe kendaraan 
    pipeline = [
        {"$match": {"_id": ObjectId(karcis.idParkiran)}},
        {"$unwind": {"path": "$daftarHarga"}},
        {"$replaceRoot": {"newRoot": "$daftarHarga"}},
        {"$match": {"tipe": tipe}},
    ]
    results = await MGDB.parkir_parkiran.aggregate(pipeline).to_list(1000)
    charga = None
    if results:
        charga = results[0]
    else:
        raise HTTPException(
            status_code=404,
            detail=f"harga dengan tipe kendaraan {tipe} tidak ditemukan",
        )
    harga = charga["harga"]

    masuk = karcis.waktuMasuk
    print("masuk: " + str(masuk))
    d1 = int(masuk.strftime("%d"))
    H1 = int(masuk.strftime("%H"))
    M1 = int(masuk.strftime("%M"))

    keluar = karcis.waktuKeluar
    print("keluar: " + str(keluar))
    d2 = int(keluar.strftime("%d"))
    H2 = int(keluar.strftime("%H"))
    M2 = int(keluar.strftime("%M"))

    hari = d2 - d1
    jam = H2 - H1
    jam = abs(jam)

    # menghitung menit
    if M1 >= M2:
        menit = 60 - (M1 - M2)
    else:
        menit = M2 - M1

    # gratis min 5 menit
    if hari == 0 and jam == 0 and menit <= 5:
        jam_total = jam
        karcis.totalBayar = 0
    
    # sama hari lebih 5 menit
    elif hari == 0 and jam == 0 and menit <= 30:
        jam_total = jam
        karcis.totalBayar = harga * 1

    # beda hari dan jam dibawah 30 menit
    elif jam > 0 and menit <= 30:
        if hari >= 1:
            jam_beda_lebih = (hari - 1) *24
            jam_beda_sehari = (24 - H1) + H2
            jam_total = jam_beda_lebih + jam_beda_sehari
            karcis.totalBayar = harga * jam_total
        else:
            jam_total = jam
            karcis.totalBayar = harga * jam_total  

    # beda hari dan jam diatas 30 menit
    elif jam >= 0 and menit > 30:
        if hari >= 1:
            jam_beda_lebih = (hari - 1) *24
            jam_beda_sehari = (24 - H1) + H2
            jam_total = jam_beda_lebih + jam_beda_sehari
            karcis.totalBayar = harga * (jam_total + 1)
        else:
            jam_total = jam
            karcis.totalBayar = harga * (jam_total + 1) 

    print ("waktu parkir: " + str(jam_total)+" jam "+str(menit)+" menit ")
    karcis_op = await dbase.update_one(
        {"_id": ObjectId(id_data), "users.userTempId": userTempId},
        {"$set": {"users.$.karcis": karcis.dict()}},
    )
    if karcis_op.modified_count:
        return karcis
    else:
        raise HTTPException(status_code=304)


@router_karcis.get("/get_karcis/{companyId}/{id_data}/{userTempId}", response_model=dict)
async def get_karcis_pemarkir(
    companyId: str,
    id_data: str,
    userTempId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$match": {"companyId": ObjectId(companyId), "_id": ObjectId(id_data)}},
        {"$unwind": {"path": "$users"}},
        {"$replaceRoot": {"newRoot": "$users"}},
        {"$match": {"userTempId": userTempId}},
        {"$unwind": {"path": "$karcis"}},
        {"$replaceRoot": {"newRoot": "$karcis"}},
        {
            "$project": {
                "id": "$id",
                "namaParkiran": "$namaParkiran",
                "waktuMasuk": "$waktuMasuk",
                "waktuKeluar": "$waktuKeluar",
                "tipeKendaraan": "$tipeKendaraan",
                "totalBayar": "$totalBayar",
            }
        },
    ]
    karcis = await dbase.aggregate(pipeline).to_list(10000)
    if not karcis:
        raise HTTPException(status_code=404, detail="data karcis tidak ditemukan")
    data_karcis = {}
    data_karcis["id data"] = id_data
    data_karcis["pemarkir"] = userTempId
    data_karcis["karcis"] = karcis
    return data_karcis


# @router_karcis.get("/get_karcis/{id_data}/{userTempId}/{idKarcis}", response_model=KarcisData)
# async def get_karcis_by_idKarcis(id_data: str,userTempId: str,idKarcis: str,current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),):
#     pipeline = [
#         {"$match": {"_id": ObjectId(id_data)}},
#         {"$unwind": {"path": "$users"}},
#         {"$replaceRoot": {"newRoot": "$users"}},
#         {"$match": {"userTempId": userTempId}},
#         {"$unwind": {"path": "$karcis"}},
#         {"$replaceRoot": {"newRoot": "$karcis"}},
#         {"$match": {"id": idKarcis}},
#     ]
#     result = await dbase.aggregate(pipeline).to_list(1000)
#     if result:
#         karcis = result[0]
#         return karcis
#     else:
#         raise HTTPException(status_code=404, detail="karcis tidak ditemukan")


# @router_karcis.delete("/delete_karcis/{id_data}/{userTempId}/{idKarcis}", response_model=dict)
# async def delete_karcis(id_data: str,userTempId: str,idKarcis: str,current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),):
#     pipeline = [
#         {"$match": {"_id": ObjectId(id_data)}},
#         {"$unwind": {"path": "$users"}},
#         {"$replaceRoot": {"newRoot": "$users"}},
#         {"$match": {"userTempId": userTempId}},
#         {"$unwind": {"path": "$karcis"}},
#         {"$replaceRoot": {"newRoot": "$karcis"}},
#         {"$match": {"id": ObjectId(idKarcis)}},
#     ]
#     result = await dbase.aggregate(pipeline).to_list(1000)
#     if not result:
#         raise HTTPException(status_code=404, detail="karcis tidak ditemukan")

#     karcis_op = await dbase.update_one(
#         {},
#         {"$pull": {"users.$[].karcis": {"id": idKarcis}}},
#     )
#     if karcis_op.modified_count:
#         return {"status": f"deleted count: {karcis_op.modified_count}"}
#     else:
#         raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


# @router_karcis.put("/update_karcis/{id_data}/{userTempId}/{idKarcis}", response_model=KarcisData)
# async def update_karcis_every_type(id_data: str,userTempId: str,tipe: TipeEnum,idKarcis: str,dataIn: KarcisInput,
#     current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
# ):
#     # get data karcis
#     pipeline = [
#         {"$unwind": {"path": "$users"}},
#         {"$replaceRoot": {"newRoot": "$users"}},
#         {"$match": {"userTempId": userTempId}},
#         {"$replaceRoot": {"newRoot": "$karcis"}},
#     ]
#     results = await MGDB.user_parkir_data.aggregate(pipeline).to_list(1000)
#     if results:
#         dataIn = dataIn.dict()
#         results[0] = cleanNullTerms(results[0])
#         dataIn.update(results[0])
#         dataIn["waktuKeluar"] = dateTimeNow()

#     print("datain: " + str(dataIn))
#     karcis = KarcisData(**dataIn)
#     print("karcis: " + str(karcis))
#     karcis.tipeKendaraan = tipe

#     # get data harga sesuai tipe kendaraan 
#     pipeline = [
#         {"$match": {"_id": ObjectId(karcis.idParkiran)}},
#         {"$unwind": {"path": "$daftarHarga"}},
#         {"$replaceRoot": {"newRoot": "$daftarHarga"}},
#         {"$match": {"tipe": tipe}},
#     ]
#     results = await MGDB.parkir_parkiran.aggregate(pipeline).to_list(1000)
#     charga = None
#     if results:
#         charga = results[0]
#     else:
#         raise HTTPException(
#             status_code=404,
#             detail=f"harga dengan tipe kendaraan {tipe} tidak ditemukan",
#         )
#     harga = charga["harga"]

#     masuk = karcis.waktuMasuk
#     print("masuk: " + str(masuk))
#     d1 = int(masuk.strftime("%d"))
#     H1 = int(masuk.strftime("%H"))
#     M1 = int(masuk.strftime("%M"))

#     keluar = karcis.waktuKeluar
#     print("keluar: " + str(keluar))
#     d2 = int(keluar.strftime("%d"))
#     H2 = int(keluar.strftime("%H"))
#     M2 = int(keluar.strftime("%M"))

#     hari = d2 - d1
#     jam = H2 - H1
#     jam = abs(jam)

#     # menghitung menit
#     if M1 >= M2:
#         menit = 60 - (M1 - M2)
#     else:
#         menit = M2 - M1

#     # gratis min 5 menit
#     if hari == 0 and jam == 0 and menit <= 5:
#         jam_total = jam
#         tempTotalBayar = 0
    
#     # sama hari lebih 5 menit
#     elif hari == 0 and jam == 0 and menit <= 30:
#         jam_total = jam
#         tempTotalBayar = harga * 1

#     # beda hari dan jam dibawah 30 menit
#     elif jam > 0 and menit <= 30:
#         if hari >= 1:
#             jam_beda_lebih = (hari - 1) *24
#             jam_beda_sehari = (24 - H1) + H2
#             jam_total = jam_beda_lebih + jam_beda_sehari
#             tempTotalBayar = harga * jam_total
#         else:
#             jam_total = jam
#             tempTotalBayar = harga * jam_total  

#     # beda hari dan jam diatas 30 menit
#     elif jam >= 0 and menit > 30:
#         if hari >= 1:
#             jam_beda_lebih = (hari - 1) *24
#             jam_beda_sehari = (24 - H1) + H2
#             jam_total = jam_beda_lebih + jam_beda_sehari
#             tempTotalBayar = harga * (jam_total + 1)
#         else:
#             jam_total = jam
#             tempTotalBayar = harga * (jam_total + 1) 

#     print ("waktu parkir: " + str(jam_total)+" jam "+str(menit)+" menit ")

#     # get data yang akan di update
#     pipeline = [
#         {"$match": {"_id": ObjectId(id_data)}},
#         {"$unwind": {"path": "$users"}},
#         {"$replaceRoot": {"newRoot": "$users"}},
#         {"$match": {"userTempId": userTempId}},
#         {"$unwind": {"path": "$karcis"}},
#         {"$replaceRoot": {"newRoot": "$karcis"}},
#         {"$match": {"id": ObjectId(idKarcis)}},
#     ]
#     result = await dbase.aggregate(pipeline).to_list(1000)
#     if not result:
#         raise HTTPException(status_code=404, detail="karcis tidak ditemukan")
#     karcis = result[0]
#     dataInKarcis = dataIn.dict(skip_defaults=True)
#     dataInKarcis = {k: v for k, v in dataInKarcis.items() if v is not None}
#     karcis.update(dataInKarcis)

#     karcis["totalBayar"] = tempTotalBayar
#     karcis_op = await dbase.update_one(
#         {"_id": ObjectId(id_data)},
#         {"$set": {"users.$[u].karcis.$[k]": karcis}},
#         array_filters=[
#             {"u.userTempId": userTempId},
#             {"k.id": idKarcis},
#         ],
#     )
#     if karcis_op.modified_count:
#         return karcis
#     else:
#         raise HTTPException(status_code=304)