# TODO:
# 1. Implement ulang GetDocSettingByCompanyId -- di pysolusi, nama fungsinya, GetDocumentSettingByCompanyId, baru sebagian
# 2. Periksa fungsi SendDocEmail
# 3. Perhatikan bagian yang menggunakan Invoice
# 4. Perhatikan CreateUser
# 5. Buat fungsi generate_invoice_by_id

# from route.ppdb.ppdbconfig import GetPPDBConfigOnCompanyId
# from function.invoice import CreateInvoice
from route.ppdb.ppdbconfig import GetPPDBConfigOnCompanyId
from route.file import deleteGeneratedDocument
from definitions import TEMPLATE_DOC_PATH
from docxtpl.template import DocxTemplate
from function.company import UpdateCompany
from route.company import add_company
from function.ppdb.pendaftar import (
    CreateDocumentPPDB,
    CreateInvoicePPDB,
    CreatePendaftar,
    DeleteDocumentPPDB,
    DeleteInvoicePPDB,
    DeletePendaftar,
    GetAllPendaftar,
    GetDocumentHasilSeleksi,
    GetDocumentPPDBByCompanyId,
    GetDocumentPPDBByUserId,
    GetDocumentPPDBOr404,
    GetInvoicePPDBOr404,
    GetInvoicesPPDBByCompanyId,
    GetInvoicesPPDBByUserId,
    GetPendaftarByCompanyId,
    GetPendaftarOr404,
    GetPendaftarUnverifiedOr404,
    SetPendaftarVerified,
    SetupDocumentPPDBHasilSeleksi,
    UpdateDocumentPPDB,
    UpdateInvoicePPDB,
    UpdatePendaftar,
)

# from route.document import add_document, generate_document_by_id
from route.document import add_document

# from route.docsetting import GetDocSettingByCompanyId
from function.document import (
    CreateDocument,
    GetDocSettingCompanyIdTitle,
    GetDocumentOr404,
    GetDocumentSettingByCompanyId,
    GetDocumentSettingByCompanyIdAndTitle,
    GetDocumentSettingOr404,
)
from docx.api import Document
from starlette.responses import FileResponse

# from util.send_email import SendDocEmail
from svc_email import sendEmail

# from model.invoice import InvoiceBase
from model.support import (
    DocumentInput,
    DocumentPage,
    InvoiceBase,
    InvoiceData,
    InvoiceInput,
    InvoicePage,
    ParameterData,
)
from config.config import MGDB, MGIMAGE, REDQUE
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    Security,
    Response,
    BackgroundTasks,
    UploadFile,
    File,
)
from typing import List
from datetime import datetime, date, timedelta
import math
from bson.objectid import ObjectId
import shutil
import os
import pandas as pd
from io import BytesIO
from rq import Queue

from model.ppdb.pendaftar import (
    ImageActionType,
    ImagePpdb,
    PendaftarBase,
    PendaftarCredential,
    PendaftarInput,
    PendaftarOnDB,
    PendaftarPage,
    StatusPendaftaran,
    TemplateProsesBase,
)
from model.default import CompanyInput, CompanyOnDB, JwtToken
from model.image import MediaBase
from model.support import DocumentData
from route.auth import get_current_user
from util.util import (
    CreateCriteria,
    IsiDefault,
    RandomString,
    ValidateObjectId,
    ValidateSizePage,
    SearchRequest,
    FieldBoolRequest,
    FieldObjectIdRequest,
    ConvertToMongodb,
    cleanNullTerms,
)
from util.util_waktu import convertStrDateToDate, dateNow, dateNowStr, dateTimeNow
from util.util_user import CreateUser, GetAccountOr404
from util.util_excel import getString, getNumber
from route.image import FOLDER, image_process
from route.invoice import add_invoice, generate_invoice_by_id

q = Queue("new_erp_queue", connection=REDQUE)

# dbase = MGDB.crm_ppdb_pendaftar
dbase = MGDB.user_crmppdb_pendaftar
router_pendaftar = APIRouter()

# =================================================================================

# === CRUD PENDAFTAR ===


@router_pendaftar.post("/get_all_pendaftar", response_model=PendaftarPage)
async def get_all_pendaftar(
    size: int = Depends(ValidateSizePage),
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pendaftars = await GetAllPendaftar()
    skip = page * size
    datas = pendaftars[skip : skip + size]
    totalElements = len(pendaftars)
    totalPages = math.ceil(totalElements / size)
    reply = PendaftarPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.post("/get_pendaftar_my_company", response_model=PendaftarPage)
async def get_pendaftar_my_company(
    size: int = Depends(ValidateSizePage),
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pendaftars = await GetPendaftarByCompanyId(current_user.companyId)
    skip = page * size
    datas = pendaftars[skip : skip + size]
    totalElements = len(pendaftars)
    totalPages = math.ceil(totalElements / size)
    reply = PendaftarPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.post(
    "/get_pendaftar_by_company_id/{companyId}", response_model=PendaftarPage
)
async def get_pendaftar_by_company_id(
    companyId: str,
    size: int = Depends(ValidateSizePage),
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pendaftars = await GetPendaftarByCompanyId(companyId)
    skip = page * size
    datas = pendaftars[skip : skip + size]
    totalElements = len(pendaftars)
    totalPages = math.ceil(totalElements / size)
    reply = PendaftarPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.get("/get_pendaftar/my_account", response_model=PendaftarOnDB)
async def get_pendaftar_my_account(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    pendaftar = await GetPendaftarOr404(current_user.userId)
    return pendaftar


@router_pendaftar.get("/get_pendaftar/{userId}", response_model=PendaftarOnDB)
async def get_pendaftar(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    pendaftar = await GetPendaftarOr404(userId)
    return pendaftar


@router_pendaftar.post("/admin_add_pendaftar", response_model=PendaftarBase)
async def admin_add_pendaftar(
    data: PendaftarInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    if current_user.isAdmin == False:
        raise HTTPException(
            status_code=400, detail="Anda tidak berhak mengakses menu ini"
        )

    # create pendaftar dan invoice dan dokumen ppdb(?)
    data = PendaftarCredential(**data.dict())
    data = IsiDefault(data, current_user)
    pendaftar = await CreatePendaftar(
        tblName="user_crmppdb_pendaftar",
        user=data,
        password="pass",
        isFirstLogin="false",
        role=["pendaftar"],
        current_user=current_user,
    )
    return pendaftar


@router_pendaftar.put(
    "/update_pendaftar/my_account",
    response_model=PendaftarOnDB,
)
async def update_pendaftar(
    pendaftar: PendaftarInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await UpdatePendaftar(current_user.userId, pendaftar)


@router_pendaftar.put(
    "/update_pendaftar/{userId}",
    response_model=PendaftarOnDB,
)
async def update_pendaftar(
    userId: str,
    pendaftar: PendaftarInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await UpdatePendaftar(userId, pendaftar)


@router_pendaftar.delete("/delete_pendaftar/{userId}", response_model=dict)
async def delete_pendaftar(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    await DeletePendaftar(userId)
    return "Berhasil hapus data pendaftar dengan userId: " + userId


# === CRUD Invoice PPDB


@router_pendaftar.post("/create_invoice", response_model=InvoiceData)
async def create_invoice(
    invoiceInput: InvoiceInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await CreateInvoicePPDB(invoiceInput, current_user)


@router_pendaftar.get("/get_invoice/{invoiceId}", response_model=InvoiceData)
async def get_invoice(
    invoiceId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await GetInvoicePPDBOr404(invoiceId)


@router_pendaftar.get("/get_invoices_user_id/my_account", response_model=InvoicePage)
async def get_invoices_by_user_id_my_account(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    invoices = await GetInvoicesPPDBByUserId(current_user.userId)
    skip = page * size
    datas = invoices[skip : skip + size]
    totalElements = len(invoices)
    totalPages = math.ceil(totalElements / size)
    reply = InvoicePage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.get("/get_invoices_user_id/{userId}", response_model=InvoicePage)
async def get_invoices_by_user_id(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    invoices = await GetInvoicesPPDBByUserId(userId)
    skip = page * size
    datas = invoices[skip : skip + size]
    totalElements = len(invoices)
    totalPages = math.ceil(totalElements / size)
    reply = InvoicePage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.get("/get_invoices_company_id/my_account", response_model=InvoicePage)
async def get_invoices_by_company_id_my_account(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    invoices = await GetInvoicesPPDBByCompanyId(current_user.companyId)
    skip = page * size
    datas = invoices[skip : skip + size]
    totalElements = len(invoices)
    totalPages = math.ceil(totalElements / size)
    reply = InvoicePage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.get(
    "/get_invoices_company_id/{companyId}", response_model=InvoicePage
)
async def get_invoices_by_company_id(
    companyId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    invoices = await GetInvoicesPPDBByCompanyId(companyId)
    skip = page * size
    datas = invoices[skip : skip + size]
    totalElements = len(invoices)
    totalPages = math.ceil(totalElements / size)
    reply = InvoicePage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.put("/update_invoice/{idInvoice}", response_model=InvoiceData)
async def update_invoice(
    idInvoice: str,
    invoiceInput: InvoiceInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    data = invoiceInput.dict()

    # pastikan data userId nggak ikut terupdate
    data["userId"] = None

    data = cleanNullTerms(data)
    return await UpdateInvoicePPDB(idInvoice, data)


@router_pendaftar.delete("/delete_invoice/{idInvoice}")
async def delete_invoice(
    idInvoice: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    await DeleteInvoicePPDB(idInvoice)
    return "Hapus data invoice dengan id: " + idInvoice


# === CRUD Document PPDB


@router_pendaftar.post(
    "/create_document_ppdb_diterima/{userId}", response_model=DocumentData
)
async def create_document_ppdb_diterima(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    doc = await SetupDocumentPPDBHasilSeleksi(userId, current_user, "TERIMA")
    document = await CreateDocumentPPDB(doc, current_user)
    return document


@router_pendaftar.post(
    "/create_document_ppdb_menunggu/{userId}", response_model=DocumentData
)
async def create_document_ppdb_menunggu(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    doc = await SetupDocumentPPDBHasilSeleksi(userId, current_user, "MENUNGGU")
    document = await CreateDocumentPPDB(doc, current_user)
    return document


@router_pendaftar.post(
    "/create_document_ppdb_ditolak/{userId}", response_model=DocumentData
)
async def create_document_ppdb_ditolak(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    doc = await SetupDocumentPPDBHasilSeleksi(userId, current_user, "TOLAK")
    document = await CreateDocumentPPDB(doc, current_user)
    return document


@router_pendaftar.get(
    "/get_document_ppdb_hasil_seleksi/my_account", response_model=DocumentData
)
async def get_document_hasil_seleksi_my_account(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await GetDocumentHasilSeleksi(current_user.userId)


@router_pendaftar.get(
    "/get_document_ppdb_hasil_seleksi/{userId}", response_model=DocumentData
)
async def get_document_hasil_seleksi(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await GetDocumentHasilSeleksi(userId)


@router_pendaftar.post("/create_document", response_model=DocumentData)
async def create_document_ppdb(
    doc: DocumentInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    document = await CreateDocumentPPDB(doc, current_user)
    return document


@router_pendaftar.get("/get_document/{documentId}", response_model=DocumentData)
async def get_document_ppdb_by_document_id(
    documentId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await GetDocumentPPDBOr404(documentId)


@router_pendaftar.get("/get_documents_user_id/my_account", response_model=DocumentPage)
async def get_documents_ppdb_by_user_id_my_account(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    documents = await GetDocumentPPDBByUserId(current_user.userId)
    skip = page * size
    datas = documents[skip : skip + size]
    totalElements = len(documents)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.get("/get_documents_user_id/{userId}", response_model=DocumentPage)
async def get_documents_ppdb_by_user_id(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    documents = await GetDocumentPPDBByUserId(userId)
    skip = page * size
    datas = documents[skip : skip + size]
    totalElements = len(documents)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.get(
    "/get_documents_company_id/my_account", response_model=DocumentPage
)
async def get_documents_ppdb_by_company_id_my_account(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    documents = await GetDocumentPPDBByCompanyId(current_user.companyId)
    skip = page * size
    datas = documents[skip : skip + size]
    totalElements = len(documents)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.get(
    "/get_documents_company_id/{companyId}", response_model=DocumentPage
)
async def get_documents_ppdb_by_company_id(
    companyId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    documents = await GetDocumentPPDBByCompanyId(companyId)
    skip = page * size
    datas = documents[skip : skip + size]
    totalElements = len(documents)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_pendaftar.put("/update_document/{documentId}", response_model=InvoiceData)
async def update_document(
    documentId: str,
    docInput: DocumentInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    data = docInput.dict()

    # pastikan data userId nggak ikut terupdate
    data["userId"] = None

    data = cleanNullTerms(data)
    return await UpdateDocumentPPDB(documentId, data)


@router_pendaftar.delete("/delete_document/{documentId}")
async def delete_document(
    documentId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    await DeleteDocumentPPDB(documentId)
    return "Hapus data dokumen dengan id: " + documentId


@router_pendaftar.get("/generate_document/{documentId}", response_class=FileResponse)
async def generate_document(documentId: str, background_tasks: BackgroundTasks):
    document = await GetDocumentPPDBOr404(documentId)
    docSetting = await GetDocumentSettingOr404(str(document["docSettingId"]))
    filename = docSetting["templateFilename"]
    if filename is None:
        raise HTTPException(
            status_code=404, detail="File template document setting tidak ditemukan"
        )
    template = DocxTemplate(TEMPLATE_DOC_PATH + "/" + filename)
    parameter = {}
    for param in document["parameter"]:
        parameter[param["key"]] = param["value"]
    template.render(parameter)
    filename = document["title"] + ".docx"
    file_location = TEMPLATE_DOC_PATH + "/" + filename
    template.save(TEMPLATE_DOC_PATH + "/" + filename)
    background_tasks.add_task(deleteGeneratedDocument, file_location)
    return FileResponse(
        file_location,
        media_type="application/octet-stream",
        filename=filename,
    )


@router_pendaftar.post("/upload_image/{action}/{pendaftarId}", response_model=MediaBase)
async def upload_image(
    action: ImageActionType,
    pendaftarId: str,
    background_tasks: BackgroundTasks,
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    await GetPendaftarOr404(pendaftarId)
    content_type = file.content_type
    if (
        content_type == "image/jpeg"
        or content_type == "image/svg"
        or content_type == "image/jpg"
        or content_type == "image/png"
        or content_type == "image/gif"
    ):
        random = RandomString(8)
        tmp_name = dateNowStr() + "-" + random + "-"
        originName = tmp_name + file.filename
        file_object = file.file
        upload = open(os.path.join(FOLDER, originName), "wb+")
        shutil.copyfileobj(file_object, upload)
        upload.close()
        extention = content_type.replace("image/", "")
        dm = MediaBase(
            name=tmp_name,
            createTime=dateTimeNow(),
            originName=originName,
            fileName=file.filename,
            fileType=extention,
        )
        dm_insert = dm
        dm_insert.userId = ObjectId(pendaftarId)
        imageAct = "image." + action
        dbase.update_one(
            {"userId": ObjectId(pendaftarId)}, {"$set": {imageAct: dm.name}}
        )
        background_tasks.add_task(image_process, dm)
        dm_insert = IsiDefault(dm, current_user)
        MGIMAGE.tbl_image.insert_one(dm_insert.dict())
        return dm
    else:
        raise HTTPException(status_code=400, detail="Unknown image type")


@router_pendaftar.put(
    "/update_pipeline_to_pendaftar/{idPendaftar}/{noProcess}",
    response_model=TemplateProsesBase,
)
async def update_pipeline_to_pendaftar(
    idPendaftar: str,
    noProcess: int,
    process: TemplateProsesBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # cek apakah pendaftar ada
    await GetPendaftarOr404(idPendaftar)

    # update pipeline sesuai nomor proses
    nomorArray = str(noProcess - 1)
    data_proses = {}
    jproses = process.dict(skip_defaults=True)
    jproses = cleanNullTerms(jproses)
    for x, y in jproses.items():
        data_proses["pipelines." + nomorArray + "." + x] = y
    await dbase.update_one({"userId": ObjectId(idPendaftar)}, {"$set": data_proses})

    pendaftar = await GetPendaftarOr404(idPendaftar)
    return pendaftar["pipelines"][int(nomorArray)]


# @router_pendaftar.put(
#     "/{idPendaftar}/{noProcess}/form", response_model=TemplateProsesBase
# )
# async def add_pipeline_forms_element(
#     idPendaftar: str,
#     noProcess: int,
#     value: str,
#     current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
# ):
#     """
#     Endpoint untuk update key-value pairs pada atribut forms di pipeline.
#     elemen di dalam array atribut forms memiliki atribut sebagai berikut:
#     - key
#     - tipe
#     - choice
#     - isMultipleChoice
#     - value

#     Setiap elemen di dalam array forms diidentifikasi secara unik oleh key.
#     Apabila data masukan formData memiliki key yang sama dengan data yang sudah ada di dalam database, maka sistem akan mengupdate elemen form yang bersangkutan.
#     Apabila data masukan formData memiliki key yang belum ada di dalam database, maka sistem akan membuat entri baru di dalam array forms.
#     """
#     pendaftar = await GetPendaftarOr404(idPendaftar)
#     pipelines = pendaftar["pipelines"]
#     pipeline = pipelines[noProcess - 1]
#     pipeline["value"] = value
#     print("Pipeline: " + str(pipeline))
#     process = TemplateProsesBase(**pipeline)
#     return await update_pipeline_to_pendaftar(
#         idPendaftar, noProcess, process, current_user
#     )

# return await UpdatePipelineForms(pipelineId, formData)


# @router_pendaftar.get("/download_excel/{companyId}/data_pendaftar.xls")
# async def download_pendaftar(companyId:str):
#     size = 10000
#     sort = "createTime"
#     dir = 1
#     search = SearchRequest()
#     search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
#     criteria = CreateCriteria(search)
#     print(criteria)
#     datas_cursor = dbase.find(criteria,
#         {"_id":1,"createTime":1,"nomor":1,"name":1,"jenjang":1,"jenjangDetail":1,"phone":1,"status":1,"pipeline":{"form":{"name":1,"value":1}}}
#         ).limit(size).sort(sort, dir)
#     datas = await datas_cursor.to_list(length=size)
#     df =  pd.DataFrame(list(datas))
#     with BytesIO() as b:
#         # Use the StringIO object as the filehandle.
#         writer = pd.ExcelWriter(b, engine='xlsxwriter')
#         df.to_excel(writer, sheet_name='data_pendaftar')
#         writer.save()
#         return Response(content=b.getvalue(), media_type='application/vnd.ms-excel')


@router_pendaftar.get(
    "/download_excel/{companyId}/data_pendaftar.xls", response_class=FileResponse
)  # TEST DOWNLOAD
async def download_excel(companyId: str, background_tasks: BackgroundTasks):
    "test companyId 6007a586406d12b2601d496e or 605da70c2f20299429cb940d"
    size = 10000
    sort = "createTime"
    dir = 1
    search = SearchRequest()
    search.defaultObjectId.append(
        FieldObjectIdRequest(field="companyId", key=ObjectId(companyId))
    )
    criteria = CreateCriteria(search)

    # setup df
    pendaftars = await GetPendaftarByCompanyId(companyId)
    datas = pendaftars
    df = pd.DataFrame(list(datas))

    # generate excel
    filename = "pendaftar.xlsx"
    file_location = TEMPLATE_DOC_PATH + "/" + filename
    df.to_excel(file_location, index=False, header=True)
    background_tasks.add_task(deleteGeneratedDocument, file_location)
    return FileResponse(
        file_location,
        media_type="application/octet-stream",
        filename=filename,
    )


# @router_pendaftar.get("/download_all_pendaftar/{companyId}/data_semua_pendaftar.xls")
# async def download_all_pendaftar(companyId: str):
#     size = 10000
#     sort = "createTime"
#     dir = 1
#     criteria = {"companyId": ObjectId(companyId)}
#     datas_cursor = (
#         dbase.find(
#             criteria,
#             {
#                 "_id": 1,
#                 "createTime": 1,
#                 "nomor": 1,
#                 "name": 1,
#                 "jenjang": 1,
#                 "jenjangDetail": 1,
#                 "phone": 1,
#                 "isDeleted": 1,
#             },
#         )
#         .limit(size)
#         .sort(sort, dir)
#     )
#     datas = await datas_cursor.to_list(length=size)
#     df = pd.DataFrame(list(datas))
#     with BytesIO() as b:
#         # Use the StringIO object as the filehandle.
#         writer = pd.ExcelWriter(b, engine="xlsxwriter")
#         df.to_excel(writer, sheet_name="data_pendaftar")
#         writer.save()
#         return Response(content=b.getvalue(), media_type="application/vnd.ms-excel")


@router_pendaftar.put(
    "/update_hasil_test/{idPendaftar}/{status}", response_model=dict()
)
async def update_hasil_test(
    idPendaftar: str,
    status: StatusPendaftaran,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    hasil = status
    pendaftar = await GetPendaftarUnverifiedOr404(idPendaftar)

    nama = "-"
    nomor = "-"

    if "name" in pendaftar:
        nama = pendaftar["name"]

    if "nomor" in pendaftar:
        nomor = pendaftar["nomor"]

    if hasil.upper() == "TERIMA":
        document = await create_document_ppdb_diterima(idPendaftar, current_user)

        # TODO: send document email

        # make invoice
        data = InvoiceInput()
        data.title = "Pendaftaran Siswa Baru (Diterima)"
        ppdb = await GetPPDBConfigOnCompanyId(current_user.companyId)
        data.total = ppdb["harga"]
        expiredDate = 60
        data.expiredDate = dateTimeNow() + timedelta(days=expiredDate)
        data.tags = ["PPDB"]
        data.userId = idPendaftar

        invoice = await CreateInvoicePPDB(data, current_user)
        return {
            "id": idPendaftar,
            "nama": nama,
            # "nomor": nomor,
            "status": "BERHASIL DIUPDATE - DITERIMA",
        }

    elif hasil.upper() == "TUNGGU":
        document = await create_document_ppdb_menunggu(idPendaftar, current_user)

        # TODO: kirim email

        # nggak ada create invoice

        return {"id": idPendaftar, "nama": nama, "status": "BERHASIL DIUPDATE - TUNGGU"}
    elif hasil.upper() == "TOLAK":
        document = await create_document_ppdb_ditolak(idPendaftar, current_user)

        # TODO: kirim email

        # nggak ada invoice

        return {
            "id": idPendaftar,
            "nama": nama,
            "status": "BERHASIL DIUPDATE - DITOLAK",
        }
    else:
        return {
            "id": idPendaftar,
            "nama": nama,
            "status": "DIPROSES",
        }


@router_pendaftar.post("/uploadHasilTest")
async def upload_hasil_test(
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    originName = file.filename
    file_object = file.file
    file_location = os.path.join(TEMPLATE_DOC_PATH, originName)
    upload = open(file_location, "wb+")
    shutil.copyfileobj(file_object, upload)
    upload.close()
    df = pd.read_excel(file_location)
    result = []
    for index, row in df.iterrows():
        idPendaftar = getString(row["id pendaftar"])
        hasil = getString(row["hasil"].upper())
        update = await update_hasil_test(idPendaftar, hasil, current_user)
        result.append(update)
    return result
