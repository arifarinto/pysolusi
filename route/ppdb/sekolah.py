from route.ppdb.ppdbconfig import AddDocumentSettingPPDB
from model.default import CompanyInput, CompanyOnDB, JwtToken
from fastapi import APIRouter, Security
from route.auth import get_current_user
from function.company import UpdateCompany
from route.company import add_company

# dbase = MGDB.user_crmppdb_pendaftar
router_sekolah = APIRouter()

# === CRUD COMPANY SEKOLAH
@router_sekolah.post("/admin_add_sekolah", response_model=CompanyOnDB)
async def admin_add_sekolah(
    data: CompanyInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    company = await add_company(data, current_user)

    # add ppdb to company solution
    company["solution"].append("crmppdb")
    data = {"solution": company["solution"]}

    # add company document setting
    await AddDocumentSettingPPDB("PPDB Diterima", company["companyId"], current_user)
    await AddDocumentSettingPPDB("PPDB Menunggu", company["companyId"], current_user)
    await AddDocumentSettingPPDB("PPDB Ditolak", company["companyId"], current_user)

    return await UpdateCompany(company["companyId"], data)

    # return company
