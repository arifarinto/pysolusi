from function.document import (
    AddDocumentSetting,
    GetDocSettingCompanyIdTitle,
    GetDocumentSettingByCompanyId,
    GetDocumentSettingByCompanyIdAndTitle,
)
from fastapi.datastructures import UploadFile
from fastapi.param_functions import File
from route.document import (
    add_doc_setting,
    get_doc_setting_by_companyId,
    upload_doc_template,
)
from typing import List
from model.support import (
    DocumentSettingBase,
    DocumentSettingOnDB,
    DocumentSettingPage,
    ParameterData,
)
from bson.objectid import ObjectId
from util.util import IsiDefault, ValidateObjectId
from fastapi.exceptions import HTTPException
from model.ppdb.ppdbconfig import ConfigPPDB, ConfigPPDBInput

# from route.ppdb.pendaftar import GetPendaftarOr404
from route.auth import get_current_user
from model.default import JwtToken
from config.config import MGDB
from fastapi.routing import APIRouter
from fastapi import Security
import math

dbase = MGDB.ppdb_config
router_ppdb_config = APIRouter()


async def GetConfig(id: str):
    _id = ValidateObjectId(id)
    return await dbase.find_one({"companyId": _id})


async def GetPPDBConfigOnCompanyId(id: str):
    config = await GetConfig(id)
    if config:
        return config
    else:
        raise HTTPException(status_code=404, detail="Config PPDB tidak ditemukan")


async def UpdateConfigPPDB(id: str, data: dict):
    id = ValidateObjectId(id)
    try:
        await dbase.update_one({"companyId": id}, {"$set": data})
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500, detail="Gagal Update Config")
    return await GetPPDBConfigOnCompanyId(id)


async def AddParameter(companyId: str, parameter: ParameterData):
    config = await GetPPDBConfigOnCompanyId(companyId)
    parameter.value = None

    # ubah key dari 'Lower Case' jadi 'lower_case', capital case jadi lower case, spasi diganti sama '_'
    parameter.key = parameter.key.lower()
    parameter.key = parameter.key.replace(" ", "_")

    parameter = parameter.dict()
    config["parameters"].append(parameter)
    data = {"parameters": config["parameters"]}
    return await UpdateConfigPPDB(companyId, data)


async def UpdateParameter(companyId: str, parameter: ParameterData, key: str):
    config = await GetPPDBConfigOnCompanyId(companyId)
    for i in range(len(config["parameters"])):
        if config["parameters"][i]["key"] == key:
            config["parameters"][i] = parameter.dict()

    return await UpdateConfigPPDB(companyId, {"parameters": config["parameters"]})


async def DeleteParameter(companyId: str, key: str):
    config = await GetPPDBConfigOnCompanyId(companyId)
    for i in range(len(config["parameters"])):
        if config["parameters"][i]["key"] == key:
            config["parameters"].remove(config["parameters"][i])
            await UpdateConfigPPDB(companyId, {"parameters": config["parameters"]})
            return "Berhasil hapus parameter dengan key " + key

    return "Parameter dengan key " + key + " tidak ditemukan"


async def SetConfig(input: ConfigPPDBInput, companyId: str):
    input = ConfigPPDB(**input.dict())
    input.companyId = ObjectId(companyId)

    config = await GetConfig(companyId)

    # data config sudah ada, update
    if config:
        input = input.dict()
        config = await UpdateConfigPPDB(companyId, input)
        return config

    # data config belum ada, insert
    else:
        config = await dbase.insert_one(input.dict())
        if config.inserted_id:
            return await GetPPDBConfigOnCompanyId(companyId)
        raise HTTPException(status_code=500, detail="Gagal insert data")


async def AddDocumentSettingPPDB(title: str, companyId: str, current_user):
    # cek apakah document setting ppdb sudah ada
    docSetting = await GetDocSettingCompanyIdTitle(companyId, title)
    if docSetting:
        raise HTTPException(
            status_code=400, detail="Document Setting " + title + " sudah ada"
        )

    docSetting = DocumentSettingBase()
    docSetting.documentSettingName = title
    docSetting.serviceName = "PPDB"
    docSetting = IsiDefault(docSetting, current_user)
    docSetting.companyId = ObjectId(companyId)
    docSetting = await AddDocumentSetting(docSetting)
    return docSetting


async def UploadTemplatePPDB(
    companyId: str, file: UploadFile, current_user: JwtToken, title: str
):
    docSetting = await GetDocumentSettingByCompanyIdAndTitle(companyId, title)
    return await upload_doc_template(str(docSetting["_id"]), file, current_user)


# === MULAI DEFINISI ROUTE ===


@router_ppdb_config.post("/my_account/set_config", response_model=ConfigPPDB)
async def set_config_my_account(
    input: ConfigPPDBInput,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    set_config ini untuk ADD config, atau UPDATE.
    Backend akan secara automatis memeriksa apakah config ppdb sudah ada atau belum.
    Apabila belum, maka backend akan melakukan operasi ADD.
    Apabila sudah, maka backend akan melakukan operasi UPDATE.
    """
    return await SetConfig(input, current_user.companyId)


@router_ppdb_config.post("/{companyId}/set_config", response_model=ConfigPPDB)
async def set_config(
    input: ConfigPPDBInput,
    companyId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    IDEM set_config_my_account
    """
    return await SetConfig(input, companyId)


@router_ppdb_config.get("/my_account/get_config", response_model=ConfigPPDB)
async def get_my_config(
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await GetPPDBConfigOnCompanyId(current_user.companyId)


@router_ppdb_config.get("/{companyId}/get_config/", response_model=ConfigPPDB)
async def get_config_by_companyId(
    companyId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await GetPPDBConfigOnCompanyId(companyId)


@router_ppdb_config.post("/my_account/add_parameter", response_model=ConfigPPDB)
async def add_parameter_my_account(
    parameter: ParameterData,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await AddParameter(current_user.companyId, parameter)


@router_ppdb_config.post("/{companyId}/add_parameter", response_model=ConfigPPDB)
async def add_parameter(
    companyId: str,
    parameter: ParameterData,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await AddParameter(companyId, parameter)


@router_ppdb_config.put("/my_account/update_parameter/{key}", response_model=ConfigPPDB)
async def update_parameter_my_account(
    key: str,
    parameter: ParameterData,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await UpdateParameter(current_user.companyId, parameter, key)


@router_ppdb_config.put(
    "/{companyId}/update_parameter/{key}", response_model=ConfigPPDB
)
async def update_parameter(
    key: str,
    companyId: str,
    parameter: ParameterData,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await UpdateParameter(current_user.companyId, parameter, key)


@router_ppdb_config.delete("/my_account/delete_parameter/{key}")
async def delete_my_account_parameter_by_key(
    key: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await DeleteParameter(current_user.companyId, key)


@router_ppdb_config.delete("/{companyId}/delete_parameter/{key}")
async def delete_parameter_by_key(
    key: str,
    companyId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await DeleteParameter(companyId, key)


# === CRUD Document Setting


@router_ppdb_config.post(
    "/my_account/add_doc_setting", response_model=DocumentSettingOnDB
)
async def add_document_setting(
    docSetting: DocumentSettingBase,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    docSetting = IsiDefault(docSetting, current_user)
    return await AddDocumentSetting(docSetting)


@router_ppdb_config.post(
    "/my_account/add_doc_setting/ppdb_diterima", response_model=DocumentSettingOnDB
)
async def add_document_setting_ppdb_diterima(
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    # query-nya sudah di-set case insensitive, jadi terserah "ppdb diterima" nya huruf besar, kecil, yang penting "ppdb diterima"
    return await AddDocumentSettingPPDB(
        "ppdb diterima", current_user.companyId, current_user
    )


@router_ppdb_config.post(
    "/my_account/add_doc_setting/ppdb_menunggu", response_model=DocumentSettingOnDB
)
async def add_document_setting_ppdb_menunggu(
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await AddDocumentSettingPPDB(
        "ppdb menunggu", current_user.companyId, current_user
    )


@router_ppdb_config.post(
    "/my_account/add_doc_setting/ppdb_ditolak", response_model=DocumentSettingOnDB
)
async def add_document_setting_ppdb_ditolak(
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await AddDocumentSettingPPDB(
        "ppdb ditolak", current_user.companyId, current_user
    )


@router_ppdb_config.post(
    "/{companyId}/add_doc_setting/ppdb_diterima", response_model=DocumentSettingOnDB
)
async def add_document_setting_ppdb_diterima(
    companyId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await AddDocumentSettingPPDB("PPDB Diterima", companyId, current_user)


@router_ppdb_config.post(
    "/{companyId}/add_doc_setting/ppdb_menunggu", response_model=DocumentSettingOnDB
)
async def add_document_setting_ppdb_menunggu(
    companyId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await AddDocumentSettingPPDB("PPDB Menunggu", companyId, current_user)


@router_ppdb_config.post(
    "/{companyId}/add_doc_setting/ppdb_ditolak", response_model=DocumentSettingOnDB
)
async def add_document_setting_ppdb_ditolak(
    companyId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await AddDocumentSettingPPDB("PPDB Ditolak", companyId, current_user)


@router_ppdb_config.post(
    "/my_account/upload_doc_template/ppdb_diterima", response_model=DocumentSettingOnDB
)
async def upload_doc_template_ppdb_diterima(
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await UploadTemplatePPDB(
        current_user.companyId, file, current_user, "PPDB Diterima"
    )


@router_ppdb_config.post(
    "/my_account/upload_doc_template/ppdb_menunggu", response_model=DocumentSettingOnDB
)
async def upload_doc_template_ppdb_menunggu(
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await UploadTemplatePPDB(
        current_user.companyId, file, current_user, "PPDB Menunggu"
    )


@router_ppdb_config.post(
    "/my_account/upload_doc_template/ppdb_ditolak", response_model=DocumentSettingOnDB
)
async def upload_doc_template_ppdb_ditolak(
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await UploadTemplatePPDB(
        current_user.companyId, file, current_user, "PPDB Ditolak"
    )


@router_ppdb_config.post(
    "/{companyId}/upload_doc_template/ppdb_diterima", response_model=DocumentSettingOnDB
)
async def upload_doc_template_ppdb_diterima(
    companyId: str,
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await UploadTemplatePPDB(companyId, file, current_user, "PPDB Diterima")


@router_ppdb_config.post(
    "/{companyId}/upload_doc_template/ppdb_menunggu", response_model=DocumentSettingOnDB
)
async def upload_doc_template_ppdb_menunggu(
    companyId: str,
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await UploadTemplatePPDB(companyId, file, current_user, "PPDB Menunggu")


@router_ppdb_config.post(
    "/{companyId}/upload_doc_template/ppdb_ditolak", response_model=DocumentSettingOnDB
)
async def upload_doc_template_ppdb_ditolak(
    companyId: str,
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await UploadTemplatePPDB(companyId, file, current_user, "PPDB Ditolak")


@router_ppdb_config.get(
    "/my_account/get_document_settings", response_model=DocumentSettingPage
)
async def get_document_settings(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    docSettings = await GetDocumentSettingByCompanyId(current_user.companyId)
    skip = page * size
    datas = docSettings[skip : skip + size]
    totalElements = len(docSettings)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentSettingPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply
