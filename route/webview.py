from bson.objectid import ObjectId
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from pydantic.main import BaseModel
from config.config import MGDB, ENVIRONMENT
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    Security,
    Response,
    BackgroundTasks,
    responses,
)
from datetime import datetime, timedelta

from model.default import IdentityData, RegisterBase, JwtToken, UserCredential
from function.user import CreateUser, CheckEmailWithCompany
from model.util import EmailData
from route.auth import (
    create_access_token,
    ACCESS_TOKEN_EXPIRE_MINUTES,
    get_current_user,
    login_for_access_token,
    login_for_access_webview,
)
from function.company import GetCompanyOr404
from util.util import (
    ValidateEmail,
    ValidateObjectId,
    RandomNumber,
    ValidPhoneNumber,
    RandomString,
    ValidPassword,
)
from util.util_waktu import dateNowStr, dateTimeNow
import jwt

router_webview = APIRouter()

ALGORITHM = "HS256"

dbase = MGDB.user_membership_member


class ThirdPartyToken(BaseModel):
    companyId: str = None
    jwtKey: str = None
    noid: str = None
    name: str = None
    exp: int = 15
    userKey: str = None


def create_access_token(data: ThirdPartyToken, expires_delta: timedelta = None):
    if expires_delta:
        expire = dateTimeNow() + expires_delta
    else:
        expire = dateTimeNow() + timedelta(minutes=10)
    data.exp = expire
    encoded_jwt = jwt.encode(data.dict(), data.jwtKey, algorithm=ALGORITHM)
    return encoded_jwt


@router_webview.post("/create_token", response_model=dict)
async def create_token(data_token: ThirdPartyToken):
    # create token
    config = await MGDB.tbl_company.find_one({"companyId": ObjectId(data_token.companyId)})
    if data_token.jwtKey != config["customKey"]:
        raise HTTPException(status_code=401, detail="Key is invalid")
    data_token.userKey = RandomString(10)
    await MGDB.tbl_temporary_login.insert_one(data_token.dict())
    access_token_expires = timedelta(minutes=5)
    access_token = create_access_token(data_token, expires_delta=access_token_expires)
    return {"access_token": access_token}


# cek data nya dulu ada di membership gak, kalo gak ada dicreate kan dulu trus diloginkan otomatis
@router_webview.get("/login_webview/{companyId}/{token}/{noid}", response_model=dict)
async def login_webview(companyId: str, token: str, noid: str):
    print(token)
    config = await MGDB.tbl_company.find_one({"companyId": ObjectId(companyId)})
    if config:
        jwtKey = config["customKey"]
        try:
            payload = jwt.decode(token, jwtKey, algorithms=[ALGORITHM])
            companyId = payload.get("companyId")
            noidJwt = payload.get("noid")
            dataJwtKey = payload.get("jwtKey")
            dataUserKey = payload.get("userKey")
            if noidJwt != noid or jwtKey != dataJwtKey:
                print(
                    "exception : "
                    + noidJwt
                    + " = "
                    + noid
                    + " :: "
                    + jwtKey
                    + " = "
                    + dataJwtKey
                )
                raise HTTPException(status_code=400, detail="Data is invalid")
        except jwt.PyJWTError:
            print("error decode")
            raise HTTPException(status_code=400, detail="Decrypt is invalid")
    else:
        raise HTTPException(status_code=400, detail="Company not found")

    cekTemp = await MGDB.tbl_temporary_login.find_one({"companyId": companyId,"noid":noid, "userKey":dataUserKey})

    if cekTemp:
        if cekTemp["userKey"] == dataUserKey:
            await MGDB.tbl_temporary_login.delete_one(
                {"_id": cekTemp["_id"]}
            )
        else:
            raise HTTPException(status_code=400, detail="User Key is invalid")
    else:
        raise HTTPException(status_code=400, detail="Key is not found")

    user = await dbase.find_one({"noId": noidJwt, "companyId": ObjectId(companyId)})

    if user:
        print("user found")  # diloginkan
        tempPassword = RandomString(20)
        await dbase.update_one(
            {"userId": ObjectId(user["userId"])},
            {"$set": {"credential.token": tempPassword}},
        )
        form_data = OAuth2PasswordRequestForm
        form_data.username = "user_membership_member*" + str(user["userId"])
        form_data.password = tempPassword
        return await login_for_access_webview(form_data)

    else:
        print("user not found")  # dibuatkan usernya kemudian diloginkan dengan userId
        user = UserCredential()
        user.name = payload.get("name")
        user.email = None
        user.username = payload.get("noid")
        user.companyId = ObjectId(companyId)
        user.noId = payload.get("noid")
        identity = IdentityData()
        identity.dateOfBirth = dateNowStr()
        user.identity = identity
        password = RandomString(6)
        user_op = await CreateUser(
            "user_membership_member", user, password, False, ["membership"]
        )
        tempPassword = RandomString(20)
        await dbase.update_one(
            {"userId": user_op.userId}, {"$set": {"credential.token": tempPassword}}
        )
        form_data = OAuth2PasswordRequestForm
        form_data.username = "user_membership_member*" + str(user_op.userId)
        form_data.password = tempPassword
        return await login_for_access_webview(form_data)


@router_webview.get("/test", response_model=dict)
async def test(current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    print(current_user)
    # pass
