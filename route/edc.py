import math
from typing import List
from bson.objectid import ObjectId
from config.config import MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from function.user import CreateUser, UpdateDateOfBirth
from model.default import IdentityData, JwtToken, UserBasic, UserCredential, UserInput
from model.membership.edc import EdcBase, EdcCredential, EdcPage
from model.util import ObjectIdStr, SearchRequest
from route.admin import GetAdminOr404
from route.auth import get_current_user
from util.util import RandomNumber, ValidateObjectId, cleanNullTerms
from util.util_waktu import convertStrDateToDate, dateNowStr

router_edc = APIRouter()
dbase = MGDB.user_membership_edc


# async def GetEdcOr404(userId: str = None):
#     if userId:
#         userId = ValidateObjectId(userId)
#         edc = await dbase.find_one({"userId": userId})

#         if "isDelete" in edc == False:
#             edc["isDelete"] = False

#         if edc and (edc["isDelete"] == False):
#             return edc
#         else:
#             raise HTTPException(status_code=404, detail="Edc tidak ditemukan")
#     else:
#         # edc = await dbase.find()
#         edcs = []
#         async for edc in dbase.find({"isDelete": False}):
#             edcs.append(edc)  # find student helper in helpers
#         return edcs


@router_edc.post("/edc", response_model=EdcBase)
async def add_edc(
    dataEdc: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    user = EdcCredential(**dataEdc.dict())
    arName = user.name.split(" ")
    user.username = arName[0] + RandomNumber(4)
    user.companyId = ObjectId(current_user.companyId)
    identity = IdentityData()
    identity.dateOfBirth = dateNowStr()
    user.identity = identity
    return await CreateUser("user_membership_edc", user, "pass", True, ["edc"])
    # pass


@router_edc.post("/edc")
async def get_all_edc(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    edcs = await GetEdcOr404()
    skip = page * size
    datas = edcs[skip : skip + size]
    totalElements = len(edcs)
    totalPages = math.ceil(totalElements / size)
    reply = EdcPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_edc.get("/edc/{userId}", response_model=UserBasic)
async def get_edc_by_user_id(
    userId: ObjectIdStr,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    edc = await GetEdcOr404(userId)
    return edc


@router_edc.put("/edc/{userId}", response_model=UserBasic)
async def update_edc_by_user_id(
    updateData: UserInput,
    userId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    await GetEdcOr404(userId)
    try:
        updateData = cleanNullTerms(updateData.dict())
        updateData = UpdateDateOfBirth(updateData)
        await dbase.update_one({"userId": ObjectId(userId)}, {"$set": updateData})
        return await GetEdcOr404(userId)
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")


@router_edc.delete("/edc/{userId}")
async def delete_edc_by_user_id(
    userId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    await GetEdcOr404(userId)
    try:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$set": {"isDelete": True}}
        )
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")
    return "EDC dengan userId " + userId + " telah dihapus"
