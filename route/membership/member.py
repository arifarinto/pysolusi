# backend/tancho/members/routes.py

import os
import shutil
from definitions import UPLOAD_PATH
from function.user import CreateUser, UpdateDateOfBirth
from route.file import deleteGeneratedDocument
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, ENVIRONMENT, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd
from fastapi import BackgroundTasks

from model.membership.member import (
    MemberBase,
    MemberCredential,
    MemberOnDB,
    MemberPage,
    comboMember,
)
from model.util import (
    FieldBoolRequest,
    ObjectIdStr,
    SearchRequest,
    FieldObjectIdRequest,
)
from model.default import IdentityData, JwtToken, UserInput
from util.util import (
    ValidateObjectId,
    IsiDefault,
    CreateCriteria,
    ListToUp,
    cleanNullTerms,
)
from route.auth import get_current_user
from util.util_waktu import convertDateToStrPassword, dateTimeNow

router_member = APIRouter()
dbase = MGDB.user_membership_member


async def GetMemberOr404(userId: str):
    userId = ValidateObjectId(userId)
    member = await dbase.find_one({"userId": userId, "isDelete": False})
    if member:
        return member
    else:
        raise HTTPException(status_code=404, detail="Member not found")


# =================================================================================


@router_member.post("/member", response_model=MemberBase)
async def add_member(
    dataIn: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Catatan:
    Phone number antara 8-13 karakter.
    """
    print("member: " + str(dataIn))
    member = MemberCredential(**dataIn.dict())
    member = IsiDefault(member, current_user)
    member.userId = ObjectId()

    cmember = await dbase.find_one(
        {
            "companyId": ObjectId(current_user.companyId),
            "noId": member.noId,
            "isDelete": False,
        }
    )
    if cmember:
        raise HTTPException(
            status_code=400,
            detail="Member with NIP : " + member.noId + " is registered",
        )

    member.name = member.name.upper()
    member.tags = ListToUp(member.tags)
    member.username = member.noId

    identity = IdentityData()
    if member.identity.dateOfBirth:
        identity.dateOfBirth = member.identity.dateOfBirth
        password = convertDateToStrPassword(member.identity.dateOfBirth)
    else:
        password = member.noId

    identity.gender = member.identity.gender
    identity.placeOfBirth = member.identity.placeOfBirth

    if ENVIRONMENT == "development":
        password = "pass"

    member.identity = identity
    member_op = await CreateUser(
        "user_membership_member", member, password, True, ["member"]
    )
    print("member_op: " + str(member_op))
    print("memebr_op type: " + str(type(member_op)))
    print("member_op automaticLogin " + member_op.automaticLogin)
    return member_op


@router_member.post("/upload_member")
async def upload_member(
    background_tasks: BackgroundTasks,
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # baca file excel ke panda dataframe
    if file.filename.split(".")[-1] != "xlsx":
        raise HTTPException(status_code=400, detail="Tipe file harus .xlsx")
    originName = file.filename
    file_object = file.file
    file_location = os.path.join(UPLOAD_PATH, originName)
    upload = open(file_location, "wb+")
    shutil.copyfileobj(file_object, upload)
    upload.close()
    df = pd.read_excel(file_location)

    # delete file yang diupload setelah pemrosesan selesai
    # biar nggak menuh2in
    background_tasks.add_task(deleteGeneratedDocument, file_location)

    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    dataIn = MemberCredential()
    for index, row in df.iterrows():
        member = MemberCredential()
        member = IsiDefault(dataIn, current_user)
        try:
            member.noId = getString(row["noid"])
            member.name = getString(row["name"].upper())
            member.note = getString(row["note"])
            member.identity = IdentityData()
            member.identity.dateOfBirth = getString(row["dateOfBirth"])
            member.tags = getString(row["tags"])
            if member.tags:
                member.tags = getString(row["tags"]).split(",")
                member.tags = ListToUp(member.tags)
            else:
                member.tags = []
        except Exception as e:
            raise HTTPException(
                status_code=400, detail="Atribut data excel tidak valid"
            )

        cmember = await dbase.find_one(
            {
                "companyId": ObjectId(current_user.companyId),
                "noId": member.noId,
                "isDelete": False,
            }
        )
        if cmember:
            rowNonvalid = rowNonvalid + 1
            unValid.append(
                {"name": member.name, "noId": member.noId, "note": "telah terdaftar"}
            )
            pass
        else:
            member.name = member.name.upper()
            member.userId = ObjectId()
            identity = IdentityData()
            if member.identity.dateOfBirth:
                identity.dateOfBirth = member.identity.dateOfBirth

                # slice 9 karakter terakhir supaya format date berubah dari
                # '1992-06-09 00:00:00' ke '1992-06-09'
                size = len(identity.dateOfBirth)
                identity.dateOfBirth = identity.dateOfBirth[: size - 9]

                password = convertDateToStrPassword(member.identity.dateOfBirth)
            else:
                password = member.noId
            # sementara, passwordnya 'pass'
            password = "pass"

            member.username = member.noId
            member.identity = identity
            await CreateUser(
                "user_membership_member", member, password, True, ["member"]
            )
            rowValid += 1

    return {"rowValid": rowValid, "rowInValid": rowNonvalid, "inValid": unValid}


@router_member.post("/get_member", response_model=dict)
async def get_all_members(
    size: int = 10,
    page: int = 0,
    sort: str = "name",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    skip = page * size
    if "superadmin" not in current_user.roles:
        search.defaultObjectId.append(
            FieldObjectIdRequest(
                field="companyId", key=ObjectId(current_user.companyId)
            )
        )
    search.defaultBool.append(FieldBoolRequest(field="isDelete", key=False))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = MemberPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_member.get("/get_member_by_userid/{userId}", response_model=MemberBase)
async def get_member_by_userid(
    userId: ObjectIdStr,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    ValidateObjectId(userId)
    return await GetMemberOr404(userId)


@router_member.delete(
    "/delete_member_by_userid/{userId}",
    dependencies=[Depends(GetMemberOr404)],
)
async def delete_member_by_userid(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    try:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$set": {"isDelete": True}}
        )
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")
    return "Member dengan userId " + userId + " telah dihapus"


@router_member.put("/update_member_by_userid/{userId}", response_model=MemberOnDB)
async def update_member_by_userid(
    userId: str,
    dataIn: MemberBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    dataIn = MemberBase(**dataIn.dict())
    dataIn.updateTime = dateTimeNow()
    updateData = dataIn.dict()
    updateData = cleanNullTerms(updateData)
    updateData = UpdateDateOfBirth(updateData)
    member_op = await dbase.update_one(
        {
            "userId": ObjectId(userId),
            "companyId": ObjectId(current_user.companyId),
            "isDelete": False,
        },
        {"$set": updateData},
    )
    if member_op.modified_count or member_op.matched_count:
        return await GetMemberOr404(userId)
    else:
        raise HTTPException(status_code=304)
