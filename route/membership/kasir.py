# backend/tancho/kasirs/routes.py

import os
import shutil
from requests.api import delete
from definitions import UPLOAD_PATH
from function.user import CreateUser, UpdateDateOfBirth
from model.membership.edc import EdcBase
from route.file import deleteGeneratedDocument
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd
from fastapi import BackgroundTasks

from model.membership.kasir import KasirBase, KasirCredential, KasirOnDB, KasirPage
from model.util import (
    FieldBoolRequest,
    ObjectIdStr,
    SearchRequest,
    FieldObjectIdRequest,
)
from model.default import IdentityData, JwtToken, UserInput
from util.util import (
    ValidateObjectId,
    IsiDefault,
    CreateCriteria,
    ListToUp,
    cleanNullTerms,
)
from route.auth import get_current_user
from util.util_waktu import convertDateToStrPassword, dateTimeNow

router_kasir = APIRouter()
dbase = MGDB.user_membership_kasir


async def GetKasirOr404(userId: str):
    userId = ValidateObjectId(userId)
    kasir = await dbase.find_one({"userId": userId, "isDelete": False})
    if kasir:
        return kasir
    else:
        raise HTTPException(status_code=404, detail="Kasir not found")


# =================================================================================


@router_kasir.post("/kasir", response_model=KasirBase)
async def add_kasir(
    dataIn: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Catatan:
    Phone number antara 8-13 karakter (minimal 8, maksimal 13).
    """
    print("kasir: " + str(dataIn))
    kasir = KasirCredential(**dataIn.dict())
    kasir = IsiDefault(kasir, current_user)
    kasir.userId = ObjectId()

    ckasir = await dbase.find_one(
        {
            "companyId": ObjectId(current_user.companyId),
            "noId": kasir.noId,
            "isDelete": False,
        }
    )
    if ckasir:
        raise HTTPException(
            status_code=400, detail="Kasir with NIP : " + kasir.noId + " is registered"
        )

    kasir.name = kasir.name.upper()
    kasir.tags = ListToUp(kasir.tags)
    kasir.username = kasir.noId

    identity = IdentityData()
    if kasir.identity.dateOfBirth:
        identity.dateOfBirth = kasir.identity.dateOfBirth
        password = convertDateToStrPassword(kasir.identity.dateOfBirth)
    else:
        password = kasir.noId

    identity.gender = kasir.identity.gender
    identity.placeOfBirth = kasir.identity.placeOfBirth

    kasir.identity = identity
    kasir_op = await CreateUser(
        "user_membership_kasir", kasir, password, True, ["kasir"]
    )
    return kasir_op


@router_kasir.post("/upload_kasir")
async def upload_kasir(
    background_tasks: BackgroundTasks,
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # baca file excel ke panda dataframe
    if file.filename.split(".")[-1] != "xlsx":
        raise HTTPException(status_code=400, detail="Tipe file harus .xlsx")
    originName = file.filename
    file_object = file.file
    file_location = os.path.join(UPLOAD_PATH, originName)
    upload = open(file_location, "wb+")
    shutil.copyfileobj(file_object, upload)
    upload.close()
    df = pd.read_excel(file_location)

    # delete file yang diupload setelah pemrosesan selesai
    # biar nggak menuh2in
    background_tasks.add_task(deleteGeneratedDocument, file_location)

    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    dataIn = KasirCredential()
    for index, row in df.iterrows():
        kasir = IsiDefault(dataIn, current_user)
        try:
            kasir.noId = getString(row["noid"])
            kasir.name = getString(row["name"].upper())
            kasir.note = getString(row["note"])
            kasir.identity = IdentityData()
            kasir.identity.dateOfBirth = getString(row["dateOfBirth"])
            kasir.tags = getString(row["tags"])
            if kasir.tags:
                kasir.tags = getString(row["Tags"]).split(",")
                kasir.tags = ListToUp(kasir.tags)
            else:
                kasir.tags = []
        except Exception as e:
            raise HTTPException(
                status_code=400, detail="Atribut data excel tidak valid"
            )
        ckasir = await dbase.find_one(
            {
                "companyId": ObjectId(current_user.companyId),
                "noId": kasir.noId,
                "isDelete": False,
            }
        )
        if ckasir:
            rowNonvalid = rowNonvalid + 1
            unValid.append(
                {"name": kasir.name, "noId": kasir.noId, "note": "telah terdaftar"}
            )
            pass
        else:
            kasir.userId = ObjectId()
            identity = IdentityData()
            if kasir.identity.dateOfBirth:
                identity.dateOfBirth = kasir.identity.dateOfBirth

                # slice 9 karakter terakhir supaya format date berubah dari
                # '1992-06-09 00:00:00' ke '1992-06-09'
                size = len(identity.dateOfBirth)
                identity.dateOfBirth = identity.dateOfBirth[: size - 9]

                password = convertDateToStrPassword(kasir.identity.dateOfBirth)
            else:
                password = kasir.noId
            # sementara, passwordnya 'pass'
            password = "pass"

            kasir.username = kasir.noId
            kasir.identity = identity
            await CreateUser("user_membership_kasir", kasir, password, True, ["kasir"])
            rowValid += 1

    return {"rowValid": rowValid, "rowInValid": rowNonvalid, "inValid": unValid}


@router_kasir.post("/get_kasir", response_model=dict)
async def get_all_kasirs(
    size: int = 10,
    page: int = 0,
    sort: str = "name",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    skip = page * size
    if "superadmin" not in current_user.roles:
        search.defaultObjectId.append(
            FieldObjectIdRequest(
                field="companyId", key=ObjectId(current_user.companyId)
            )
        )
    search.defaultBool.append(FieldBoolRequest(field="isDelete", key=False))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=1000)
    totalElements = len(datas)
    datas = datas[skip : skip + size]
    totalPages = math.ceil(totalElements / size)
    reply = KasirPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_kasir.get("/get_kasir_by_userid/{userId}", response_model=KasirBase)
async def get_kasir_by_userid(
    userId: ObjectIdStr,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await GetKasirOr404(userId)


@router_kasir.delete(
    "/delete_kasir_by_userid/{userId}",
    dependencies=[Depends(GetKasirOr404)],
)
async def delete_kasir_by_userid(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    try:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$set": {"isDelete": True}}
        )
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")
    return "Kasir dengan userId " + userId + " telah dihapus"


@router_kasir.put("/update_kasir_by_userid/{userId}", response_model=KasirOnDB)
async def update_kasir_by_userid(
    userId: str,
    dataIn: KasirBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    dataIn = EdcBase(**dataIn.dict())
    dataIn.updateTime = dateTimeNow()
    updateData = dataIn.dict()
    updateData = cleanNullTerms(updateData)
    updateData = UpdateDateOfBirth(updateData)
    kasir_op = await dbase.update_one(
        {
            "userId": ObjectId(userId),
            "companyId": ObjectId(current_user.companyId),
            "isDelete": False,
        },
        {"$set": updateData},
    )
    if kasir_op.modified_count or kasir_op.matched_count:
        return await GetKasirOr404(userId)
    else:
        raise HTTPException(status_code=304)
