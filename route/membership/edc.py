# backend/tancho/members/routes.py

import os
import shutil
from definitions import UPLOAD_PATH
from function.user import CreateUser, UpdateDateOfBirth
from route.file import deleteGeneratedDocument
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd
from fastapi import BackgroundTasks

from model.membership.edc import EdcBase, EdcCredential, EdcOnDB, EdcPage
from model.util import (
    FieldBoolRequest,
    ObjectIdStr,
    SearchRequest,
    FieldObjectIdRequest,
)
from model.default import IdentityData, JwtToken, UserInput
from util.util import (
    ValidateObjectId,
    IsiDefault,
    CreateCriteria,
    ListToUp,
    cleanNullTerms,
)
from route.auth import get_current_user
from util.util_waktu import convertDateToStrPassword, dateTimeNow

router_edc = APIRouter()
dbase = MGDB.user_membership_edc


async def GetEdcOr404(userId: str = None):
    if userId:
        userId = ValidateObjectId(userId)
        edc = await dbase.find_one({"userId": userId})

        if "isDelete" in edc == False:
            edc["isDelete"] = False

        if edc and (edc["isDelete"] == False):
            return edc
        else:
            raise HTTPException(status_code=404, detail="Edc tidak ditemukan")
    else:
        edcs = []
        async for edc in dbase.find({"isDelete": False}):
            edcs.append(edc)  # find student helper in helpers
        return edcs


# =================================================================================


@router_edc.post("/edc", response_model=EdcBase)
async def add_edc(
    dataIn: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Catatan:
    Phone number antara 8-13 karakter (minimal 8, maksimal 13).
    """
    member = EdcCredential(**dataIn.dict())
    member = IsiDefault(member, current_user)
    member.userId = ObjectId()

    cmember = await dbase.find_one(
        {
            "companyId": ObjectId(current_user.companyId),
            "noId": member.noId,
            "isDelete": False,
        }
    )
    if cmember:
        raise HTTPException(
            status_code=400, detail="Edc with NOID : " + member.noId + " is registered"
        )

    member.name = member.name.upper()
    member.tags = ListToUp(member.tags)
    member.username = member.noId

    identity = IdentityData()
    if member.identity.dateOfBirth:
        identity.dateOfBirth = member.identity.dateOfBirth
        password = convertDateToStrPassword(member.identity.dateOfBirth)
    else:
        password = member.noId

    identity.gender = member.identity.gender
    identity.placeOfBirth = member.identity.placeOfBirth

    member.identity = identity
    member_op = await CreateUser(
        "user_membership_edc", member, password, True, ["member"]
    )
    return member_op


@router_edc.post("/upload_edc")
async def upload_edc(
    background_tasks: BackgroundTasks,
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # baca file excel ke panda dataframe
    if file.filename.split(".")[-1] != "xlsx":
        raise HTTPException(status_code=400, detail="Tipe file harus .xlsx")
    originName = file.filename
    file_object = file.file
    file_location = os.path.join(UPLOAD_PATH, originName)
    upload = open(file_location, "wb+")
    shutil.copyfileobj(file_object, upload)
    upload.close()
    df = pd.read_excel(file_location)

    # delete file yang diupload setelah pemrosesan selesai
    # biar nggak menuh2in
    background_tasks.add_task(deleteGeneratedDocument, file_location)

    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    dataIn = EdcCredential()
    for index, row in df.iterrows():
        member = IsiDefault(dataIn, current_user)
        try:
            member.noId = getString(row["noid"])
            member.name = getString(row["name"].upper())
            member.note = getString(row["note"])
            member.identity = IdentityData()
            member.identity.dateOfBirth = getString(row["dateOfBirth"])
            member.tags = getString(row["tags"])
            if member.tags:
                member.tags = getString(row["tags"]).split(",")
                member.tags = ListToUp(member.tags)
            else:
                member.tags = []
        except Exception as e:
            raise HTTPException(
                status_code=400, detail="Atribut data excel tidak valid"
            )

        cmember = await dbase.find_one(
            {
                "companyId": ObjectId(current_user.companyId),
                "noId": member.noId,
                "isDelete": False,
            }
        )
        if cmember:
            rowNonvalid = rowNonvalid + 1
            unValid.append(
                {"name": member.name, "noId": member.noId, "note": "telah terdaftar"}
            )
            pass
        else:
            member.userId = ObjectId()
            identity = IdentityData()
            if member.identity.dateOfBirth:
                identity.dateOfBirth = member.identity.dateOfBirth

                # slice 9 karakter terakhir supaya format date berubah dari
                # '1992-06-09 00:00:00' ke '1992-06-09'
                size = len(identity.dateOfBirth)
                identity.dateOfBirth = identity.dateOfBirth[: size - 9]

                password = convertDateToStrPassword(member.identity.dateOfBirth)
            else:
                password = member.noId
            # sementara, passwordnya 'pass'
            password = "pass"

            member.username = member.noId
            member.identity = identity
            await CreateUser("user_membership_edc", member, password, True, ["member"])
            rowValid += 1

    return {"rowValid": rowValid, "rowInValid": rowNonvalid, "inValid": unValid}


@router_edc.post("/get_edc", response_model=dict)
async def get_all_edcs(
    size: int = 10,
    page: int = 0,
    sort: str = "name",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    skip = page * size
    if "superadmin" not in current_user.roles:
        search.defaultObjectId.append(
            FieldObjectIdRequest(
                field="companyId", key=ObjectId(current_user.companyId)
            )
        )
    search.defaultBool.append(FieldBoolRequest(field="isDelete", key=False))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = EdcPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_edc.get("/get_edc_by_userid/{userId}", response_model=EdcBase)
async def get_edc_by_userid(
    userId: ObjectIdStr,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Get EDC by userid. Company id edc mesti sama dengan company id current user. (i.e. user cuma bisa mengambil datanya sendiri)
    """
    userId = ValidateObjectId(userId)
    edc = await dbase.find_one(
        {
            "userId": userId,
            "companyId": ObjectId(current_user.companyId),
            "isDelete": False,
        }
    )
    if edc:
        return edc
    else:
        raise HTTPException(status_code=404, detail="Edc not found")


@router_edc.delete(
    "/delete_edc_by_userid/{userId}",
    dependencies=[Depends(GetEdcOr404)],
)
async def delete_edc_by_userid(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    try:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$set": {"isDelete": True}}
        )
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")
    return "EDC dengan userId " + userId + " telah dihapus"


@router_edc.put("/update_edc_by_userid/{userId}", response_model=EdcOnDB)
async def update_edc_by_userid(
    userId: str,
    dataIn: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    """
    Update edc by user id. current user's company id mesti sama dengan edc's company id.
    (i.e. si edc adalah kepunyaan company current user)
    """
    dataIn = EdcBase(**dataIn.dict())
    dataIn.updateTime = dateTimeNow()
    updateData = dataIn.dict()
    updateData = cleanNullTerms(updateData)
    updateData = UpdateDateOfBirth(updateData)
    member_op = await dbase.update_one(
        {
            "userId": ObjectId(userId),
            "companyId": ObjectId(current_user.companyId),
            "isDelete": False,
        },
        {"$set": updateData},
    )
    if member_op.modified_count or member_op.matched_count:
        return await GetEdcOr404(userId)
    else:
        raise HTTPException(status_code=304)
