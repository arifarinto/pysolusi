# backend/tancho/donasis/routes.py

from pydantic.networks import HttpUrl
from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.donasi import (
    DonasiOnDB,
    DonasiBase,
    DonasiPage,
    DistribusiData,
    DonaturData,
    MutasiDanaData,
    MutasiEnum,
)
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_donasi = APIRouter()
dbase = MGDB.tbl_donasi


async def GetDonasiOr404(id_: str):
    _id = ValidateObjectId(id_)
    donasi = await dbase.find_one({"_id": _id})
    if donasi:
        return donasi
    else:
        raise HTTPException(status_code=404, detail="Donasi not found")


# =================================================================================
# donasi


@router_donasi.post("/donasi", response_model=DonasiOnDB, tags=["donasi"])
async def add_donasi(
    data_in: DonasiBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    donasi = IsiDefault(data_in, current_user)
    donasi.namaDonasi = donasi.namaDonasi.upper()
    cdonasi = await dbase.find_one(
        {"companyId": ObjectId(current_user.companyId), "namaDonasi": donasi.namaDonasi}
    )
    if cdonasi:
        raise HTTPException(status_code=400, detail="Donasi is registered")
    donasi_op = await dbase.insert_one(donasi.dict())
    if donasi_op.inserted_id:
        donasi = await GetDonasiOr404(donasi_op.inserted_id)
        return donasi


@router_donasi.post("/get_donasi", response_model=dict, tags=["donasi"])
async def get_all_donasis(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    skip = page * size
    search.defaultObjectId.append(
        FieldObjectIdRequest(field="companyId", key=ObjectId(current_user.companyId))
    )
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = DonasiPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_donasi.get("/donasi/{id_}", response_model=DonasiOnDB, tags=["donasi"])
async def get_donasi_by_id(
    id_: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    donasi = await dbase.find_one(
        {"_id": id_, "companyId": ObjectId(current_user.companyId)}
    )
    if donasi:
        return donasi
    else:
        raise HTTPException(status_code=404, detail="Donasi not found")


@router_donasi.delete(
    "/donasi/{id_}",
    dependencies=[Depends(GetDonasiOr404)],
    response_model=dict,
    tags=["donasi"],
)
async def delete_donasi_by_id(
    id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    donasi_op = await dbase.delete_one(
        {"_id": ObjectId(id_), "companyId": ObjectId(current_user.companyId)}
    )
    if donasi_op.deleted_count:
        return {"status": f"deleted count: {donasi_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_donasi.put("/donasi/{id_}", response_model=DonasiOnDB, tags=["donasi"])
async def update_donasi(
    id_: str,
    data_in: DonasiBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    donasi = IsiDefault(data_in, current_user, True)
    donasi.updateTime = dateTimeNow()
    donasi_op = await dbase.update_one(
        {"_id": ObjectId(id_), "companyId": ObjectId(current_user.companyId)},
        {"$set": donasi.dict(skip_defaults=True)},
    )
    if donasi_op.modified_count:
        return await GetDonasiOr404(id_)
    else:
        raise HTTPException(status_code=304)


# =================================================================================
# distribusi


@router_donasi.post(
    "/distribusi/{idDonasi}", response_model=DistribusiData, tags=["distribusi"]
)
async def add_distribusi_donasi(
    idDonasi: str,
    dataIn: DistribusiData,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    donasi = await dbase.find_one({"_id": ObjectId(idDonasi)})
    danaSaldo = donasi["danaSaldo"]
    jumlah = dataIn.nominal
    danaSaldo = danaSaldo - jumlah
    if danaSaldo < 0:
        raise HTTPException(status_code=400, detail="Saldo tidak boleh minus")

    distribusi_op = await dbase.update_one(
        {"_id": ObjectId(idDonasi)}, {"$addToSet": {"distribusi": dataIn.dict()}}
    )
    if distribusi_op.modified_count:
        # mutasi dana langsung ter-create
        dataInMutasi = MutasiDanaData(
            id=dataIn.id,
            userId=current_user.userId,
            nama=dataIn.namaKegiatan,
            tipeMutasi="dana_keluar",
            nominal=dataIn.nominal,
            tgl=dataIn.waktu,
            keterangan=dataIn.keterangan,
        )
        await add_mutasi_donasi(idDonasi, dataInMutasi, current_user)
        return dataIn
    else:
        raise HTTPException(status_code=304)


@router_donasi.get(
    "/get_all_distribusi/{companyId}/{idDonasi}",
    response_model=dict,
    tags=["distribusi"],
)
async def get_all_distibusi_donasi(
    companyId: str,
    idDonasi: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$distribusi"}},
        {"$match": {"companyId": ObjectId(companyId), "_id": ObjectId(idDonasi)}},
        {"$replaceRoot": {"newRoot": "$distribusi"}},
        {
            "$project": {
                "id": "$id",
                "namaKegiatan": "$namaKegiatan",
                "tanggal": "$waktu",
                "nominal": "$nominal",
            }
        },
    ]
    distribusi = await dbase.aggregate(pipeline).to_list(10000)
    if not distribusi:
        raise HTTPException(status_code=404, detail="data distribusi tidak ditemukan")
    data_distribusi = {}
    data_distribusi["donasi"] = idDonasi
    data_distribusi["distribusi"] = distribusi
    return data_distribusi


@router_donasi.get(
    "/distribusi/{idDonasi}/{idDistribusi}",
    response_model=DistribusiData,
    tags=["distribusi"],
)
async def get_distribusi_donasi_by_id_distribusi(
    idDonasi: str,
    idDistribusi: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$distribusi"}},
        {"$replaceRoot": {"newRoot": "$distribusi"}},
        {"$match": {"id": idDistribusi}},
    ]
    distribusi = await dbase.aggregate(pipeline).to_list(1000)
    if distribusi:
        return distribusi[0]
    else:
        raise HTTPException(status_code=404, detail="distribusi tidak ditemukan")


@router_donasi.delete(
    "/distribusi/{idDonasi}/{idDistribusi}", response_model=dict, tags=["distribusi"]
)
async def delete_distribusi_donasi(
    idDonasi: str,
    idDistribusi: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$distribusi"}},
        {"$replaceRoot": {"newRoot": "$distribusi"}},
        {"$match": {"id": idDistribusi}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        dana = result[0]
    else:
        raise HTTPException(status_code=404, detail="distribusi tidak ditemukan")

    donasi = await dbase.find_one({"_id": ObjectId(idDonasi)})
    danaSaldo = donasi["danaSaldo"]
    danaSaldo = danaSaldo + dana["nominal"]

    distribusi_op = await dbase.update_one(
        {"_id": ObjectId(idDonasi), "distribusi.id": idDistribusi},
        {"$pull": {"distribusi": {"id": idDistribusi}}},
    )
    if distribusi_op.modified_count:
        # mutasi dana ikut ter-delete
        await delete_mutasi_donasi(idDonasi, idDistribusi, current_user)
        return {
            "status": f"distribusi dana dengan id {idDistribusi} berhasil dihapus. Saldo kembali menjadi sebesar {danaSaldo}"
        }
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_donasi.put(
    "/distribusi/{idDonasi}/{idDistribusi}",
    response_model=DistribusiData,
    tags=["distribusi"],
)
async def update_distribusi_donasi(
    idDonasi: str,
    idDistribusi: str,
    dataIn: DistribusiData,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$distribusi"}},
        {"$replaceRoot": {"newRoot": "$distribusi"}},
        {"$match": {"id": idDistribusi}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        distribusi = result[0]
    else:
        raise HTTPException(status_code=404, detail="distribusi tidak ditemukan")

    donasi = await dbase.find_one({"_id": ObjectId(idDonasi)})
    danaSaldo_before_update = donasi["danaSaldo"]
    danaSaldo_before_update = danaSaldo_before_update + distribusi["nominal"]

    jumlah = dataIn.nominal
    danaSaldo_after_update = danaSaldo_before_update - jumlah
    if danaSaldo_after_update < 0:
        raise HTTPException(status_code=400, detail="Saldo tidak boleh minus")

    dataInDistribusi = dataIn.dict(skip_defaults=True)
    dataInDistribusi = {k: v for k, v in dataInDistribusi.items() if v is not None}
    distribusi.update(dataInDistribusi)
    distribusi_op = await dbase.update_one(
        {"_id": ObjectId(idDonasi), "distribusi.id": idDistribusi},
        {"$set": {"distribusi.$": distribusi}},
    )
    if distribusi_op.modified_count:
        # mutasi dana ikut ter-update
        dataInMutasi = MutasiDanaData(
            id=dataIn.id,
            userId=current_user.userId,
            nama=dataIn.namaKegiatan,
            tipeMutasi="dana_keluar",
            nominal=dataIn.nominal,
            tgl=dataIn.waktu,
            keterangan=dataIn.keterangan,
        )
        await update_mutasi_donasi(idDonasi, idDistribusi, dataInMutasi, current_user)
        return dataInDistribusi
    else:
        raise HTTPException(status_code=304)


# =================================================================================
# donatur


@router_donasi.post("/donatur/{idDonasi}", response_model=DonaturData, tags=["donatur"])
async def add_donatur_donasi(
    idDonasi: str,
    dataIn: DonaturData,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    donasi = await dbase.find_one({"_id": ObjectId(idDonasi)})
    danaSaldo = donasi["danaSaldo"]
    danaTerkumpul = donasi["danaTerkumpul"]
    jumlah = dataIn.nominal
    danaTerkumpul = danaTerkumpul + jumlah
    danaSaldo = danaSaldo + jumlah

    if dataIn.EnableName == False:
        star = ""
        for c in dataIn.namaDonatur:
            star += "*"
            dataIn.namaDonatur = star

    donatur_op = await dbase.update_one(
        {"_id": ObjectId(idDonasi)}, {"$addToSet": {"donatur": dataIn.dict()}}
    )
    if donatur_op.modified_count:
        # mutasi dana langsung ter-create
        dataInMutasi = MutasiDanaData(
            id=dataIn.id,
            userId=current_user.userId,
            nama=dataIn.namaDonatur,
            EnableName=dataIn.EnableName,
            tipeMutasi="dana_masuk",
            nominal=dataIn.nominal,
            tgl=dataIn.tgl,
        )
        await add_mutasi_donasi(idDonasi, dataInMutasi, current_user)
        return dataIn
    else:
        raise HTTPException(status_code=304)


@router_donasi.get(
    "/get_all_donatur/{companyId}/{idDonasi}", response_model=dict, tags=["donatur"]
)
async def get_all_donatur_donasi(
    companyId: str,
    idDonasi: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$donatur"}},
        {"$match": {"companyId": ObjectId(companyId), "_id": ObjectId(idDonasi)}},
        {"$replaceRoot": {"newRoot": "$donatur"}},
        {
            "$project": {
                "id": "$id",
                "nama donatur": "$namaDonatur",
                "tanggal": "$tgl",
                "nominal": "$nominal",
            }
        },
    ]
    donatur = await dbase.aggregate(pipeline).to_list(10000)
    if not donatur:
        raise HTTPException(status_code=404, detail="data donatur tidak ditemukan")
    data_donatur = {}
    data_donatur["donasi"] = idDonasi
    data_donatur["donaturs"] = donatur
    return data_donatur


@router_donasi.get(
    "/donatur/{idDonasi}/{idDonatur}", response_model=DonaturData, tags=["donatur"]
)
async def get_donatur_donasi_by_id_donatur(
    idDonasi: str,
    idDonatur: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$donatur"}},
        {"$replaceRoot": {"newRoot": "$donatur"}},
        {"$match": {"id": idDonatur}},
    ]
    donatur = await dbase.aggregate(pipeline).to_list(1000)
    if donatur:
        return donatur[0]
    else:
        raise HTTPException(status_code=404, detail="donatur tidak ditemukan")


@router_donasi.delete(
    "/donatur/{idDonasi}/{idDonatur}", response_model=dict, tags=["donatur"]
)
async def delete_donatur_donasi(
    idDonasi: str,
    idDonatur: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$donatur"}},
        {"$replaceRoot": {"newRoot": "$donatur"}},
        {"$match": {"id": idDonatur}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        dana = result[0]
    else:
        raise HTTPException(status_code=404, detail="donatur tidak ditemukan")

    donasi = await dbase.find_one({"_id": ObjectId(idDonasi)})
    danaSaldo = donasi["danaSaldo"]
    danaTerkumpul = donasi["danaTerkumpul"]
    danaTerkumpul = danaTerkumpul - dana["nominal"]
    danaSaldo = danaSaldo - dana["nominal"]

    donatur_op = await dbase.update_one(
        {"_id": ObjectId(idDonasi), "donatur.id": idDonatur},
        {"$pull": {"donatur": {"id": idDonatur}}},
    )
    if donatur_op.modified_count:
        # mutasi dana ikut ter-delete
        await delete_mutasi_donasi(idDonasi, idDonatur, current_user)
        return {"status": f"distribusi dana dengan id {idDonatur} berhasil dihapus"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_donasi.put(
    "/donatur/{idDonasi}/{idDonatur}", response_model=DonaturData, tags=["donatur"]
)
async def update_donatur_donasi(
    idDonasi: str,
    idDonatur: str,
    dataIn: DonaturData,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$donatur"}},
        {"$replaceRoot": {"newRoot": "$donatur"}},
        {"$match": {"id": idDonatur}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        donatur = result[0]
    else:
        raise HTTPException(status_code=404, detail="donatur tidak ditemukan")

    donasi = await dbase.find_one({"_id": ObjectId(idDonasi)})
    danaSaldo_before_update = donasi["danaSaldo"]
    danaTerkumpul_before_update = donasi["danaTerkumpul"]
    danaTerkumpul_before_update = danaTerkumpul_before_update - donatur["nominal"]
    danaSaldo_before_update = danaSaldo_before_update - donatur["nominal"]

    jumlah = dataIn.nominal
    danaTerkumpul_after_update = danaSaldo_before_update + jumlah
    danaSaldo_after_update = danaSaldo_before_update + jumlah

    if dataIn.EnableName == False:
        star = ""
        for v in dataIn.namaDonatur:
            star += "*"
            dataIn.namaDonatur = star

    dataInDonatur = dataIn.dict(skip_defaults=True)
    dataInDonatur = {k: v for k, v in dataInDonatur.items() if v is not None}
    donatur.update(dataInDonatur)
    donatur_op = await dbase.update_one(
        {"_id": ObjectId(idDonasi), "donatur.id": idDonatur},
        {"$set": {"donatur.$": donatur}},
    )
    if donatur_op.modified_count:
        # mutasi dana ikut ter-update
        dataInMutasi = MutasiDanaData(
            id=dataIn.id,
            userId=current_user.userId,
            nama=dataIn.namaDonatur,
            tipeMutasi="dana_masuk",
            nominal=dataIn.nominal,
            tgl=dataIn.tgl,
        )
        await update_mutasi_donasi(idDonasi, idDonatur, dataInMutasi, current_user)
        return dataInDonatur
    else:
        raise HTTPException(status_code=304)


# =================================================================================
# endpoint mutasi dana


@router_donasi.get(
    "/get_all_mutasi/{companyId}/{idDonasi}/{tipe}",
    response_model=dict,
    tags=["mutasi"],
)
async def get_all_mutasi_donasi_every_type(
    companyId: str,
    idDonasi: str,
    tipe: MutasiEnum,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$mutasiDana"}},
        {"$match": {"companyId": ObjectId(companyId), "_id": ObjectId(idDonasi)}},
        {"$replaceRoot": {"newRoot": "$mutasiDana"}},
        {"$match": {"tipeMutasi": tipe}},
        {
            "$project": {
                "id": "$id",
                "nama": "$nama",
                "tanggal": "$tgl",
                "nominal": "$nominal",
            }
        },
    ]
    mutasi = await dbase.aggregate(pipeline).to_list(10000)
    if not mutasi:
        raise HTTPException(status_code=404, detail="data mutasi tidak ditemukan")
    data_mutasi = {}
    data_mutasi["donasi"] = idDonasi
    data_mutasi["tipe"] = tipe
    data_mutasi["mutasi"] = mutasi
    return data_mutasi


@router_donasi.get("/mutasi/{idMutasi}", response_model=MutasiDanaData, tags=["mutasi"])
async def get_mutasi_donasi_by_id(
    idMutasi: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$mutasiDana"}},
        {"$replaceRoot": {"newRoot": "$mutasiDana"}},
        {"$match": {"id": idMutasi}},
    ]
    mutasi = await dbase.aggregate(pipeline).to_list(1000)
    if mutasi:
        return mutasi[0]
    else:
        raise HTTPException(status_code=404, detail="mutasi tidak ditemukan")


# =================================================================================
# function mutasi dana


async def add_mutasi_donasi(
    idDonasi: str,
    dataIn: MutasiDanaData,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    donasi = await dbase.find_one({"_id": ObjectId(idDonasi)})
    danaTerkumpul = donasi["danaTerkumpul"]
    danaSaldo = donasi["danaSaldo"]
    jumlah = dataIn.nominal

    if dataIn.EnableName == False:
        star = ""
        for v in dataIn.nama:
            star += "*"
            dataIn.nama = star

    if dataIn.tipeMutasi == "dana_masuk":
        danaTerkumpul = danaTerkumpul + jumlah
        danaSaldo = danaSaldo + jumlah
        donasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi)},
            {"$set": {"danaTerkumpul": danaTerkumpul, "danaSaldo": danaSaldo}},
        )
        mutasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi)}, {"$addToSet": {"mutasiDana": dataIn.dict()}}
        )

    if dataIn.tipeMutasi == "dana_keluar":
        danaSaldo = danaSaldo - jumlah
        if danaSaldo < 0:
            raise HTTPException(status_code=400, detail="Saldo tidak boleh minus")
        donasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi)}, {"$set": {"danaSaldo": danaSaldo}}
        )
        mutasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi)}, {"$addToSet": {"mutasiDana": dataIn.dict()}}
        )

    if donasi_op.modified_count:
        if mutasi_op.modified_count:
            return dataIn
    else:
        raise HTTPException(status_code=304)


async def delete_mutasi_donasi(
    idDonasi: str,
    idMutasi: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$mutasiDana"}},
        {"$replaceRoot": {"newRoot": "$mutasiDana"}},
        {"$match": {"id": idMutasi}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        mutasi = result[0]
    else:
        raise HTTPException(status_code=400, detail="mutasi tiak ditemukan")

    tipeMutasi = mutasi["tipeMutasi"]
    jumlah = mutasi["nominal"]
    donasi = await dbase.find_one({"_id": ObjectId(idDonasi)})
    danaTerkumpul = donasi["danaTerkumpul"]
    danaSaldo = donasi["danaSaldo"]

    if tipeMutasi == "dana_masuk":
        danaTerkumpul = danaTerkumpul - jumlah
        danaSaldo = danaSaldo - jumlah
        donasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi)},
            {"$set": {"danaTerkumpul": danaTerkumpul, "danaSaldo": danaSaldo}},
        )
        mutasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi), "mutasiDana.id": idMutasi},
            {"$pull": {"mutasiDana": {"id": idMutasi}}},
        )

    if tipeMutasi == "dana_keluar":
        danaSaldo = danaSaldo + jumlah
        donasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi)}, {"$set": {"danaSaldo": danaSaldo}}
        )
        mutasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi), "mutasiDana.id": idMutasi},
            {"$pull": {"mutasiDana": {"id": idMutasi}}},
        )

    if donasi_op.modified_count:
        if mutasi_op.modified_count:
            return {"status": f"mutasi dana dengan id {idMutasi} berhasil dihapus"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


async def update_mutasi_donasi(
    idDonasi: str,
    idMutasi: str,
    dataIn: MutasiDanaData,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    pipeline = [
        {"$unwind": {"path": "$mutasiDana"}},
        {"$replaceRoot": {"newRoot": "$mutasiDana"}},
        {"$match": {"id": idMutasi}},
    ]
    result = await dbase.aggregate(pipeline).to_list(1000)
    if result:
        mutasi = result[0]
    else:
        raise HTTPException(status_code=404, detail="distribusi tidak ditemukan")
    tipeMutasi = mutasi["tipeMutasi"]
    donasi = await dbase.find_one({"_id": ObjectId(idDonasi)})
    danaTerkumpul_before_update = donasi["danaTerkumpul"]
    danaSaldo_before_update = donasi["danaSaldo"]

    if tipeMutasi == "dana_masuk":
        danaTerkumpul_before_update = danaTerkumpul_before_update - mutasi["nominal"]
        danaSaldo_before_update = danaSaldo_before_update - mutasi["nominal"]

        jumlah = dataIn.nominal
        danaTerkumpul_after_update = danaTerkumpul_before_update + jumlah
        danaSaldo_after_update = danaSaldo_before_update + jumlah

        if dataIn.EnableName == False:
            star = ""
            for v in dataIn.nama:
                star += "*"
                dataIn.nama = star

        dataIn = dataIn.dict(skip_defaults=True)
        dataIn = {k: v for k, v in dataIn.items() if v is not None}
        mutasi.update(dataIn)
        donasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi)},
            {
                "$set": {
                    "danaTerkumpul": danaTerkumpul_after_update,
                    "danaSaldo": danaSaldo_after_update,
                }
            },
        )
        mutasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi), "mutasiDana.id": idMutasi},
            {"$set": {"mutasiDana.$": mutasi}},
        )

    if tipeMutasi == "dana_keluar":
        danaSaldo_before_update = danaSaldo_before_update + mutasi["nominal"]
        jumlah = dataIn.nominal
        danaSaldo_after_update = danaSaldo_before_update - jumlah
        if danaSaldo_after_update < 0:
            raise HTTPException(status_code=400, detail="Saldo tidak boleh minus")

        dataIn = dataIn.dict(skip_defaults=True)
        dataIn = {k: v for k, v in dataIn.items() if v is not None}
        mutasi.update(dataIn)
        donasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi)}, {"$set": {"danaSaldo": danaSaldo_after_update}}
        )
        mutasi_op = await dbase.update_one(
            {"_id": ObjectId(idDonasi), "mutasiDana.id": idMutasi},
            {"$set": {"mutasiDana.$": mutasi}},
        )

    if donasi_op.modified_count:
        if mutasi_op.modified_count:
            return dataIn
    else:
        raise HTTPException(status_code=304)
