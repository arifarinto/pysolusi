from os import link
from config.config import MGDB
from definitions import CRM_KATALIS_ROOT, OPEN_FILE_ROOT, SVC_EMAIL_ROOT
import json
from model.util import SearchRequest
from svc_email import kirim_email_standard
from typing import List
from util.util import CreateCriteria, IsiDefault
from function.user import GetUserOr404, checkIfUserAuthorized
from function.invoice import (
    CreateInvoice,
    # CreateInvoice1,
    DeleteInvoice,
    GetInvoiceBaseOnMonthAndYear,
    GetInvoiceOr404,
    GetInvoicesAll,
    GetInvoicesByCompanyId,
    GetInvoicesOnUserId,
    UpdateInvoice,
)
from model.support import (
    InvoiceData,
    InvoiceInput,
    InvoiceOnDB,
    InvoicePage,
    PaymentStatusEnum,
)
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response,
    Security,
    BackgroundTasks,
)
from starlette.background import BackgroundTask
from route.auth import get_current_user
from model.default import JwtToken
from fastapi.testclient import TestClient
import requests
import math

router_invoice = APIRouter()
# client = TestClient(app)


@router_invoice.post("/get_invoices_all", response_model=InvoicePage)
async def get_all_invoices(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    invoices = await GetInvoicesAll()
    skip = page * size
    datas = invoices[skip : skip + size]
    totalElements = len(invoices)
    totalPages = math.ceil(totalElements / size)
    reply = InvoicePage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_invoice.post("", response_model=InvoiceData)
async def add_invoice(
    data: InvoiceInput,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Endpoint untuk menambahkan data invoice baru. Wajib login sebagai admin.
    """
    # invoice = await CreateInvoice(data, current_user)
    invoice = await CreateInvoice(data, current_user)
    linkInvoice = {
        "link_invoice": OPEN_FILE_ROOT + "/checkout_invoice/" + str(invoice["id"])
    }
    linkInvoice = json.dumps(linkInvoice, indent=4)
    user = await GetUserOr404(invoice["userId"])

    email = ""
    if "email" in user:
        email = user["email"]
    userEmail = email
    # userEmail = "ristiriantoadi@gmail.com"

    params = {
        "emailTo": userEmail,
        "subject": "Invoice " + invoice["title"],
        "templateName": "contoh_template_invoice.html",
        "environment": linkInvoice,
    }

    # fitur kirim emailnya dicomment dulu, untuk sementara
    # url = SVC_EMAIL_ROOT + "/kirim_email_standard"
    # requests.post(url, headers={"Accept": "application/json"}, params=params)
    return invoice


@router_invoice.get("/{idInvoice}", response_model=InvoiceData)
async def get_invoice_by_id_invoice(
    idInvoice: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    # checkIfUserAuthorized(current_user,userId)
    invoice = await GetInvoiceOr404(idInvoice)
    return invoice


@router_invoice.get("/company/{companyId}", response_model=InvoicePage)
async def get_invoices_by_company_id(
    companyId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    # checkIfUserAuthorized(current_user,userId)
    # invoice = await GetInvoiceOr404(idInvoice)
    invoices = await GetInvoicesByCompanyId(companyId)
    skip = page * size
    datas = invoices[skip : skip + size]
    totalElements = len(invoices)
    totalPages = math.ceil(totalElements / size)
    reply = InvoicePage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply
    # return invoices


@router_invoice.get("/user/{userId}", response_model=InvoicePage)
async def get_invoices_by_user_id(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    # checkIfUserAuthorized(current_user,userId)
    # invoice = await GetInvoiceOr404(idInvoice)
    invoices = await GetInvoicesOnUserId(userId)
    skip = page * size
    datas = invoices[skip : skip + size]
    totalElements = len(invoices)
    totalPages = math.ceil(totalElements / size)
    reply = InvoicePage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply
    # return invoices


@router_invoice.put("/{idInvoice}/update", response_model=InvoiceData)
async def update_invoice_general(
    idInvoice: str,
    data: InvoiceInput,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    data = data.dict()

    # pastikan data userId nggak terupdate
    data["userId"] = None

    # hapus key2 yg null/none
    newData = {}
    for key in data:
        if data[key]:
            newData[key] = data[key]
    return await UpdateInvoice(idInvoice, newData)


@router_invoice.put(
    "/{idInvoice}/payment_status/{paymentStatus}", response_model=InvoiceData
)
async def update_invoice_payment_status(
    idInvoice: str,
    paymentStatus: PaymentStatusEnum,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    data = {"paymentStatus": paymentStatus}
    return await UpdateInvoice(idInvoice, data)


@router_invoice.delete("/{idInvoice}")
async def delete_invoice(
    idInvoice: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    await DeleteInvoice(idInvoice)
    return "Berhasil hapus invoice dengan id " + idInvoice


async def generate_invoice_by_id():
    pass
