from route.queue.branch import GetBranchByIdAkun
from util.util_user import CreateUser
from model.user import UserCreate
from util.util_waktu import dateNow, dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.queue.loket import LoketBase, LoketOnDB, LoketPage
from model.queue.antrian import LoketOnDutyBase
from model.util import SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import RandomString, ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user

router_loket = APIRouter()
dbase = MGDB.queue_loket

async def GetLoketOr404(id: str):
    _id = ValidateObjectId(id)
    loket = await dbase.find_one({"_id": _id})
    if loket:
        return loket
    else:
        raise HTTPException(status_code=404, detail="Loket not found")

async def GetLoketByIdAkun(id: str):
    _id = ValidateObjectId(id)
    loket = await dbase.find_one({"idAkun": _id})
    if loket:
        return loket
    else:
        return False

# =================================================================================


@router_loket.post("/loket", response_model=LoketOnDB)
async def add_loket(data_in: LoketBase, current_user: TokenData = Security(get_current_user, scopes=["ROLE_BRANCH", "*"])):
    branch = await GetBranchByIdAkun(current_user.accountId)
    loket = IsiDefault(data_in, current_user)
    loket.tags = ListToUp(loket.tags)
    loket.name = loket.name.upper()
    loket.branchId = branch['_id']
    if not loket.layananId:
        raise HTTPException(status_code=400, detail="Loket harus memilih layanan")
    loket.layananId = ObjectId(loket.layananId)
    user = UserCreate()
    user.companyId = current_user.companyId
    user.companyName = current_user.companyName
    user.name = loket.name
    user.phone = loket.phone
    user.email = loket.email
    user.role = ["ROLE_LOKET"]
    user_op = await CreateUser(user, 'default')
    loket.idAkun = user_op.object_id
    
    loket_op = await dbase.insert_one(loket.dict())
    if loket_op.inserted_id:
        loket = await GetLoketOr404(loket_op.inserted_id)
        return loket


@router_loket.post("/get_loket", response_model=dict)
async def get_all_lokets(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = LoketPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_loket.get("/loket/{id}", response_model=LoketOnDB)
async def get_loket_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    loket = await dbase.find_one({"_id": id,"companyId": ObjectId(current_user.companyId)})
    if loket:
        return loket
    else:
        raise HTTPException(status_code=404, detail="Loket not found")


@router_loket.delete("/loket/{id}", dependencies=[Depends(GetLoketOr404)], response_model=dict)
async def delete_loket_by_id(id: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    loket_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if loket_op.deleted_count:
        return {"status": f"deleted count: {loket_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_loket.put("/loket/{id_}", response_model=LoketOnDB)
async def update_loket(id_: str, data_in: LoketBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    loket = IsiDefault(data_in, current_user, True)
    if loket.layananId:
        loket.layananId = ObjectId(loket.layananId)
    loket.updateTime = dateTimeNow()
    loket_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": loket.dict(skip_defaults=True)}
    )
    if loket_op.modified_count:
        return await GetLoketOr404(id_)
    else:
        raise HTTPException(status_code=304)


@router_loket.get("/checkin_loket", response_model=dict())
async def checkin_loket(current_user: TokenData = Security(get_current_user, scopes=["ROLE_LOKET", "*"])):
    cloket = await GetLoketByIdAkun(current_user.accountId)
    if not cloket:
        raise HTTPException(status_code=400, detail="Akun tidak valid")
    antrian = await MGDB.queue_antrian.find_one({"companyId":ObjectId(current_user.companyId),"layananId":cloket['layananId'],"tanggal":dateNow()})
    if not antrian:
        raise HTTPException(status_code=400, detail="Antrian tidak valid atau anda tidak berhak")

    #TODO cek pernah check in atau belum
    data = LoketOnDutyBase()
    data.idAkun = ObjectId(current_user.accountId)
    data.name = current_user.userName
    data.activeStatus = True
    data.startTime = dateTimeNow()
    
    print('OKOK')
  
    MGDB.queue_antrian.update_one({"companyId":ObjectId(current_user.companyId),"_id":antrian['_id']},
        {"$addToSet": {"loketOnDuty":data.dict()}}
    )
    return {"status":"ok"}

@router_loket.get("/checkout_loket", response_model=dict())
async def checkout_loket(current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    antrian = await MGDB.queue_antrian.find_one({"tanggal":dateNow(),"loketOnDuty.idAkun":ObjectId(current_user.accountId)})
    if not antrian :
        raise HTTPException(status_code=404, detail="Data Antrian Anda tidak ditemukan")

    await MGDB.queue_antrian.update_one({"_id":antrian['_id'],"loketOnDuty.idAkun":ObjectId(current_user.accountId)},
        {"$set": {"loketOnDuty.$.activeStatus": False}}
    )
    return {"status":"ok"}