# backend/tancho/audios/routes.py

from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.queue.audio import AudioBase, AudioOnDB, AudioPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user

router_audio = APIRouter()
dbase = MGDB.queue_audio

async def GetAudioOr404(id: str):
    _id = ValidateObjectId(id)
    audio = await dbase.find_one({"_id": _id})
    if audio:
        return audio
    else:
        raise HTTPException(status_code=404, detail="Audio not found")

# =================================================================================


@router_audio.post("/audio", response_model=AudioOnDB)
async def add_audio(data_in: AudioBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    audio = IsiDefault(data_in, current_user)
    audio.tags = ListToUp(audio.tags)
    audio_op = await dbase.insert_one(audio.dict())
    if audio_op.inserted_id:
        audio = await GetAudioOr404(audio_op.inserted_id)
        return audio


@router_audio.post("/get_audio", response_model=dict)
async def get_all_audios(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = AudioPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_audio.get("/audio/{id}", response_model=AudioOnDB)
async def get_audio_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    audio = await dbase.find_one({"_id": id,"companyId": ObjectId(current_user.companyId)})
    if audio:
        return audio
    else:
        raise HTTPException(status_code=404, detail="Audio not found")


@router_audio.delete("/audio/{id}", dependencies=[Depends(GetAudioOr404)], response_model=dict)
async def delete_audio_by_id(id: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    audio_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if audio_op.deleted_count:
        return {"status": f"deleted count: {audio_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_audio.put("/audio/{id_}", response_model=AudioOnDB)
async def update_audio(id_: str, data_in: AudioBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    audio = IsiDefault(data_in, current_user, True)
    audio.updateTime = dateTimeNow()
    audio_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": audio.dict(skip_defaults=True)}
    )
    if audio_op.modified_count:
        return await GetAudioOr404(id_)
    else:
        raise HTTPException(status_code=304)