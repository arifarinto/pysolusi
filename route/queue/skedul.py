from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.queue.skedul import SkedulBase, SkedulOnDB, SkedulPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user

router_skedul = APIRouter()
dbase = MGDB.queue_skedul

async def GetSkedulOr404(id: str):
    _id = ValidateObjectId(id)
    skedul = await dbase.find_one({"_id": _id})
    if skedul:
        return skedul
    else:
        raise HTTPException(status_code=404, detail="Skedul not found")

async def TambahSkedul(data_in:SkedulBase):
    data_in.title = data_in.title.upper()
    skedul_cek = await dbase.find_one({"companyId": data_in.companyId,"title":data_in.title,"hari":data_in.hari})
    if not skedul_cek:
        data_in.tags = ListToUp(data_in.tags)
        skedul_op = await dbase.insert_one(data_in.dict())
        if skedul_op.inserted_id:
            return "ok"
    else:
        return "duplicate"

# =================================================================================


@router_skedul.post("/skedul", response_model=SkedulOnDB)
async def add_skedul(data_in: SkedulBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    skedul = IsiDefault(data_in, current_user)
    skedul.title = skedul.title.upper()
    skedul.tags = ListToUp(skedul.tags)
    skedul_op = await dbase.insert_one(skedul.dict())
    if skedul_op.inserted_id:
        skedul = await GetSkedulOr404(skedul_op.inserted_id)
        return skedul

@router_skedul.post("/five_skedul", response_model=dict)
async def add_seven_shift(data_in: SkedulBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    data_in.namaSkedul = data_in.namaSkedul.upper()
    shift = IsiDefault(data_in, current_user)
    shift.hari = 'Monday'
    monday = await TambahSkedul(data_in)
    shift.hari = 'Tuesday'
    tuesday = await TambahSkedul(data_in)
    shift.hari = 'Wednesday'
    wednesday = await TambahSkedul(data_in)
    shift.hari = 'Thursday'
    thursday = await TambahSkedul(data_in)
    shift.hari = 'Friday'
    friday = await TambahSkedul(data_in)
    return {'monday':monday,'tuesday':tuesday,'wednesday':wednesday,'thursday':thursday,'friday':friday}


@router_skedul.post("/get_skedul", response_model=dict)
async def get_all_skeduls(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = SkedulPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_skedul.get("/skedul/{id}", response_model=SkedulOnDB)
async def get_skedul_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    skedul = await dbase.find_one({"_id": id,"companyId": ObjectId(current_user.companyId)})
    if skedul:
        return skedul
    else:
        raise HTTPException(status_code=404, detail="Skedul not found")


@router_skedul.delete("/skedul/{id}", dependencies=[Depends(GetSkedulOr404)], response_model=dict)
async def delete_skedul_by_id(id: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    skedul_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if skedul_op.deleted_count:
        return {"status": f"deleted count: {skedul_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_skedul.put("/skedul/{id_}", response_model=SkedulOnDB)
async def update_skedul(id_: str, data_in: SkedulBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    skedul = IsiDefault(data_in, current_user, True)
    skedul.updateTime = dateTimeNow()
    skedul_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": skedul.dict(skip_defaults=True)}
    )
    if skedul_op.modified_count:
        return await GetSkedulOr404(id_)
    else:
        raise HTTPException(status_code=304)