from route.queue.branch import GetBranchByIdAkun
from util.util_user import CreateUser
from model.user import UserCreate
from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.queue.kiosk import KioskBase, KioskOnDB, KioskPage
from model.util import ObjectIdStr, SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import RandomString, ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user

router_kiosk = APIRouter()
dbase = MGDB.queue_kiosk

async def GetKioskOr404(id: str):
    _id = ValidateObjectId(id)
    kiosk = await dbase.find_one({"_id": _id})
    if kiosk:
        return kiosk
    else:
        raise HTTPException(status_code=404, detail="Kiosk not found")

# =================================================================================


@router_kiosk.post("/kiosk", response_model=KioskOnDB)
async def add_kiosk(data_in: KioskBase, current_user: TokenData = Security(get_current_user, scopes=["ROLE_BRANCH", "*"])):
    branch = await GetBranchByIdAkun(current_user.accountId)
    kiosk = IsiDefault(data_in, current_user)
    kiosk.tags = ListToUp(kiosk.tags)
    kiosk.name = kiosk.name.upper()
    kiosk.branchId = branch['_id']
    user = UserCreate()
    user.companyId = current_user.companyId
    user.companyName = current_user.companyName
    user.name = kiosk.name
    if kiosk.jenis == 'layar':
        user.role = ["ROLE_MONITOR"]
    else:
        user.role = ["ROLE_KIOSK"]
    user_op = await CreateUser(user, 'default')
    kiosk.idAkun = user_op.object_id
    kiosk_op = await dbase.insert_one(kiosk.dict())
    if kiosk_op.inserted_id:
        kiosk = await GetKioskOr404(kiosk_op.inserted_id)
        return kiosk


@router_kiosk.post("/get_kiosk", response_model=dict)
async def get_all_kiosks(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = KioskPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_kiosk.get("/kiosk/{id}", response_model=KioskOnDB)
async def get_kiosk_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    kiosk = await dbase.find_one({"_id": id,"companyId": ObjectId(current_user.companyId)})
    if kiosk:
        return kiosk
    else:
        raise HTTPException(status_code=404, detail="Kiosk not found")


@router_kiosk.delete("/kiosk/{id}", dependencies=[Depends(GetKioskOr404)], response_model=dict)
async def delete_kiosk_by_id(id: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    kiosk_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if kiosk_op.deleted_count:
        return {"status": f"deleted count: {kiosk_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_kiosk.put("/kiosk/{id_}", response_model=KioskOnDB)
async def update_kiosk(id_: str, data_in: KioskBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    kiosk = IsiDefault(data_in, current_user, True)
    kiosk.updateTime = dateTimeNow()
    kiosk_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": kiosk.dict(skip_defaults=True)}
    )
    if kiosk_op.modified_count:
        return await GetKioskOr404(id_)
    else:
        raise HTTPException(status_code=304)