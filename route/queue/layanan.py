from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.queue.layanan import LayananBase, LayananOnDB, LayananPage, comboLayanan
from model.util import SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user

router_layanan = APIRouter()
dbase = MGDB.queue_layanan

async def GetLayananOr404(id: str):
    _id = ValidateObjectId(id)
    layanan = await dbase.find_one({"_id": _id})
    if layanan:
        return layanan
    else:
        raise HTTPException(status_code=404, detail="Layanan not found")

# =================================================================================


@router_layanan.post("/layanan", response_model=LayananOnDB)
async def add_layanan(data_in: LayananBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    layanan = IsiDefault(data_in, current_user)
    layanan.tags = ListToUp(layanan.tags)
    layanan.title = layanan.title.upper()
    layanan_op = await dbase.insert_one(layanan.dict())
    if layanan_op.inserted_id:
        layanan = await GetLayananOr404(layanan_op.inserted_id)
        return layanan


@router_layanan.post("/get_layanan", response_model=dict)
async def get_all_layanans(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = LayananPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_layanan.get("/layanan/{id}", response_model=LayananOnDB)
async def get_layanan_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    layanan = await dbase.find_one({"_id": id,"companyId": ObjectId(current_user.companyId)})
    if layanan:
        return layanan
    else:
        raise HTTPException(status_code=404, detail="Layanan not found")


@router_layanan.delete("/layanan/{id}", dependencies=[Depends(GetLayananOr404)], response_model=dict)
async def delete_layanan_by_id(id: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    layanan_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if layanan_op.deleted_count:
        return {"status": f"deleted count: {layanan_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_layanan.put("/layanan/{id_}", response_model=LayananOnDB)
async def update_layanan(id_: str, data_in: LayananBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    layanan = IsiDefault(data_in, current_user, True)
    layanan.updateTime = dateTimeNow()
    layanan_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": layanan.dict(skip_defaults=True)}
    )
    if layanan_op.modified_count:
        return await GetLayananOr404(id_)
    else:
        raise HTTPException(status_code=304)

@router_layanan.get("/combo_layananan_by_branch/{branchId}", response_model=List[comboLayanan])
async def combo_layananan_by_branch(branchId:str, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    datas_cursor = dbase.find({"companyId":ObjectId(current_user.companyId)})
    datas = await datas_cursor.to_list(length=100)
    print(datas)
    return datas  