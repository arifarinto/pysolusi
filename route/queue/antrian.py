# backend/tancho/antrians/routes.py

from util.util_company import GetCompanyOr404
from model.user import AkunCreate
from util.util_user import CreateAccount, GetAccountByUserComp
from model.queue.masyarakat import LogAntrian
from route.queue.masyarakat import GetMasyByIdAkunAndLay, GetMasyarakatByIdAkun, SelfCekOrAddMasyarakat
from util.util_waktu import convertStrDateToDate, dateNow, dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.queue.antrian import AntrianBase, AntrianOnDB, AntrianPage, PengantreBase
from model.util import FieldArrayRequest, SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import RandomString, ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user

router_antrian = APIRouter()
dbase = MGDB.queue_antrian

async def GetAntrianOr404(id: str):
    _id = ValidateObjectId(id)
    antrian = await dbase.find_one({"_id": _id})
    if antrian:
        return antrian
    else:
        raise HTTPException(status_code=404, detail="Antrian not found")

# =================================================================================


@router_antrian.post("/antrian", response_model=AntrianOnDB)
async def add_antrian(antrian: AntrianBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    antrian.companyId = ObjectId(current_user.companyId)
    antrian.tanggal = convertStrDateToDate(antrian.tanggal)
    antrian_op = await dbase.insert_one(antrian.dict())
    if antrian_op.inserted_id:
        antrian = await GetAntrianOr404(antrian_op.inserted_id)
        return antrian


@router_antrian.post("/get_antrian", response_model=dict)
async def get_all_antrians(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = AntrianPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_antrian.get("/monitor_get_antrian", response_model=List[AntrianOnDB])
async def monitor_get_antrian(current_user: TokenData = Security(get_current_user, scopes=["ROLE_MONITOR","*"])):
    search = {"companyId":ObjectId(current_user.companyId), "tanggal":dateNow(), "showAt.idAkun":ObjectId(current_user.accountId)}
    print(search)
    datas_cursor = dbase.find(search,{"pengantre":0}).limit(100).sort("title", 1)
    datas = await datas_cursor.to_list(length=100)
    return datas

@router_antrian.get("/kiosk_get_antrian", response_model=List[AntrianOnDB])
async def kiosk_get_antrian(current_user: TokenData = Security(get_current_user, scopes=["ROLE_KIOSK","*"])):
    search = {"companyId":ObjectId(current_user.companyId), "tanggal":dateNow(), "takeAt.idAkun":ObjectId(current_user.accountId)}
    datas_cursor = dbase.find(search,{"pengantre":0}).limit(100).sort("title", 1)
    datas = await datas_cursor.to_list(length=100)
    return datas


@router_antrian.get("/antrian/{id}", response_model=AntrianOnDB)
async def get_antrian_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    antrian = await dbase.find_one({"_id": id,"companyId": ObjectId(current_user.companyId)})
    if antrian:
        return antrian
    else:
        raise HTTPException(status_code=404, detail="Antrian not found")


@router_antrian.delete("/antrian/{id}", dependencies=[Depends(GetAntrianOr404)], response_model=dict)
async def delete_antrian_by_id(id: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    antrian_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if antrian_op.deleted_count:
        return {"status": f"deleted count: {antrian_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_antrian.put("/antrian/{id_}", response_model=AntrianOnDB)
async def update_antrian(id_: str, data_in: AntrianBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    antrian = IsiDefault(data_in, current_user, True)
    antrian.updateTime = dateTimeNow()
    antrian_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": antrian.dict(skip_defaults=True)}
    )
    if antrian_op.modified_count:
        return await GetAntrianOr404(id_)
    else:
        raise HTTPException(status_code=304)

# =================================================================================

#masyarakat ambil antrian online
@router_antrian.get("/ambil_antrian_online/{id}", response_model=PengantreBase)
async def ambil_antrian_online(id: str, current_user: TokenData = Security(get_current_user, scopes=["ROLE_CUSTOMER", "*"])):
    #cek akan dapat number antrian berapa
    cek = await GetAntrianOr404(id)
    #proteksi tanggal
    if cek['tanggal'] != dateNow():
        raise HTTPException(status_code=400, detail="Antrian tidak Valid, tanggal tidak sesuai")
    if current_user.companyId != str(cek['companyId']):
        print("bikin akun baru atau change company trus jwt baru")
        #cek, apakah punya company id
        cekUser = await GetAccountByUserComp(current_user.userId,current_user.companyId)
        if not cekUser: 
            akun = AkunCreate()
            akun.companyId = cek['companyId']
            akun.userId = ObjectId(current_user.userId)
            print("create akun di company ini, lanjut update current user nya")
            opakun = await CreateAccount(akun)
            current_user.accountId = opakun.object_id
            current_user.companyId = cek['companyId']
            cekCompany = await GetCompanyOr404(cek['companyId'])
            current_user.companyName = cekCompany['companyName']
        else:
            print("update current user dgn current user dari get cekUser")
            current_user.accountId = cekUser[0]['object_id']
            current_user.companyId = cek['companyId']
            cekCompany = await GetCompanyOr404(cek['companyId'])
            current_user.companyName = cekCompany['companyName']
    
    #cek sudah punya data di masyarakat atau belum
    await SelfCekOrAddMasyarakat(current_user)
    #cek sudah ambil berapa antrian, not same, less than 3
    cMasy = await GetMasyByIdAkunAndLay(current_user.accountId,id)
    if cMasy:
        raise HTTPException(status_code=400, detail="Anda sudah memiliki antrian di Layanan ini")
    
    jmlAntrian = len(cek['pengantre'])
    if cek['capacity'] < jmlAntrian and cek['capacity'] != 0:
        raise HTTPException(status_code=400, detail="Kuota antrian "+ cek['capacity']+" telah penuh")
    #cek kode layanan
    cLayan = await MGDB.queue_layanan.find_one({"_id":cek["layananId"]})
    peng = PengantreBase()
    peng.idAkun = ObjectId(current_user.accountId)
    peng.kode = cLayan['kode']
    peng.number = jmlAntrian + 1
    peng.isOnline = True
    
    peng.createTime = dateTimeNow()
    await dbase.update_one(
        {"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)}, 
        {"$addToSet": {"pengantre":peng.dict()},"$inc":{"queTotal":+1}}
    )
    #update akun bahwa dia sudah punya antrian aktif
    logAntrian = LogAntrian()
    logAntrian.createTime = dateTimeNow()
    logAntrian.layananId = cek["layananId"]
    logAntrian.antrianId = ObjectId(id)
    logAntrian.compayId = cLayan["companyId"]
    logAntrian.kode = peng.kode
    logAntrian.number = peng.number
    logAntrian.branchId = cek['branchId']
    logAntrian.isFinished = False
    MGDB.queue_masyarakat.update_one(
        {"idAkun": ObjectId(current_user.accountId),"companyId": ObjectId(current_user.companyId)}, 
        {"$addToSet": {"logAntrian":logAntrian.dict()}}
    )
    return peng.dict()

#check in setelah sampai dikantor dengan cara scan qr code
@router_antrian.get("/check_in_antrian_online/{kode}", response_model=dict)
async def check_in_antrian_online(kode: str, current_user: TokenData = Security(get_current_user, scopes=["ROLE_CUSTOMER", "*"])):
    arkode = kode.split('*')
    #baca kode qr di kiosk, kiosk harus aktif (id*kode4digit) 
    kiosk = await MGDB.queue_kiosk.find_one({"_id": ObjectId(arkode[0]),"kode":arkode[1],"isActive":True})
    if not kiosk:
        raise HTTPException(status_code=400, detail="Kode QR tidak Valid")
    #cocokan antrian yang aktif dengan kiosk nya
    cekLogMasy = await MGDB.queue_masyarakat.find_one({
        "idAkun": ObjectId(current_user.accountId),
        "logAntrian.branchId":kiosk['branchId'],
        "logAntrian.isFinished":False
    },{"logAntrian.$":1})
    print(cekLogMasy)
    if not cekLogMasy:
        raise HTTPException(status_code=400, detail="Anda belum mengambil antrian online di kantor ini")
    
    antrianId = cekLogMasy['logAntrian'][0]['antrianId']
    layananId = cekLogMasy['logAntrian'][0]['layananId']
    nomorAmbil = cekLogMasy['logAntrian'][0]['number']

    #cek lagi kiosk nya ter register di layanan ini nggak takeAt, idAkun
    if str(layananId) not in kiosk['listLayananId']:
        raise HTTPException(status_code=400, detail="Nomor Antrian Online Anda tidak dapat CHECK IN di sini, silahkan hubungi bagian informasi")
    
    cekAntrian = await dbase.find_one({"_id": ObjectId(antrianId)},{"queNow":1,"timeNow":1})
    if nomorAmbil < cekAntrian['queNow']:
        #ini telat
        print("telat ini bro")
        await dbase.update_one(
            {"_id": ObjectId(antrianId),
            "companyId": ObjectId(current_user.companyId),
            "pengantre.idAkun":ObjectId(current_user.accountId),
            "pengantre.number":nomorAmbil}, 
            {"$set": {
                "pengantre.$.createTime":cekAntrian['timeNow'],
                "pengantre.$.isCheckedIn":True,
                "pengantre.$.checkedInTime": dateTimeNow()
            }}
        )
    else:
    #update antrian
        await dbase.update_one(
            {"_id": ObjectId(antrianId),
            "companyId": ObjectId(current_user.companyId),
            "pengantre.idAkun":ObjectId(current_user.accountId),
            "pengantre.number":nomorAmbil}, 
            {"$set": {
                "pengantre.$.isCheckedIn":True,
                "pengantre.$.checkedInTime": dateTimeNow()
            }}
        )

    #kirim perintah ke layar agar refresh qr code
    # await MGDB.queue_kiosk.update_one({"_id": ObjectId(arkode[0])},{"$set":{"kode":RandomString(4)}})
    return {"detail":"ok"}

#masyarakat ambil antrian offline
@router_antrian.get("/ambil_antrian_offline/{id}", response_model=PengantreBase)
async def ambil_antrian_offline(id: str, current_user: TokenData = Security(get_current_user, scopes=["ROLE_KIOSK", "*"])):
    #cek akan dapat number antrian berapa
    cek = await GetAntrianOr404(id)
    #proteksi tanggal
    if cek['tanggal'] != dateNow():
        raise HTTPException(status_code=400, detail="Antrian tidak Valid")
    if current_user.companyId != str(cek['companyId']):
        raise HTTPException(status_code=400, detail="Akun tidak Valid")
    
    jmlAntrian = len(cek['pengantre'])
    if cek['capacity'] < jmlAntrian and cek['capacity'] != 0:
        raise HTTPException(status_code=400, detail="Kuota antrian "+ cek['capacity']+" telah penuh")
    #cek kode layanan
    cLayan = await MGDB.queue_layanan.find_one({"_id":cek["layananId"]})
    peng = PengantreBase()
    peng.idAkun = ObjectId(current_user.accountId)
    peng.kode = cLayan['kode']
    peng.number = jmlAntrian + 1
    peng.isOnline = False
    peng.isCalled = False
    
    peng.createTime = dateTimeNow()
    peng.checkedInTime = dateTimeNow()
    await dbase.update_one(
        {"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)}, 
        {"$addToSet": {"pengantre":peng.dict(skip_defaults=True)},"$inc":{"queTotal":+1}}
    )
    
    return peng.dict(skip_defaults=True)

#petugas panggil antrian
@router_antrian.get("/panggil_antrian/{id}/{is_ulang}", response_model=dict)
async def panggil_antrian(id: str,is_ulang: bool = False, current_user: TokenData = Security(get_current_user, scopes=["ROLE_LOKET", "*"])):
    #cari id antrian dan number yang harus dipanggil
    cek = await dbase.find_one({"_id": ObjectId(id),"loketOnDuty.idAkun":ObjectId(current_user.accountId)},
        {"pengantre":0}
    )
    if not cek:
        raise HTTPException(status_code=400, detail="Antrian tidak valid atau anda belum check in")
    
    if cek['tanggal'] != dateNow():
        raise HTTPException(status_code=400, detail="Tanggal Antrian tidak Valid")
    
    if is_ulang == True:
        number = cek['queNow']
        kode = cek['kodeNow']
    else:
        pipeline = [
                {
                    '$match': {
                        '_id': ObjectId(id)
                    }
                }, {
                    '$unwind': {
                        'path': '$pengantre'
                    }
                }, {
                    '$match': {
                        'pengantre.isCalled': False, 
                        '$or': [
                            {
                                'pengantre.isOnline': False
                            }, {
                                'pengantre.isCheckedIn': True
                            }
                        ]
                    }
                }, {
                    '$sort': {
                        'pengantre.createTime': 1, 
                        'pengantre.checkedInTime': 1
                    }
                }, {
                    '$limit': 1
                }
            ]
        
        cursor = dbase.aggregate(pipeline)
        dpang = await cursor.to_list(1)
        print(dpang)
        if not dpang:
            raise HTTPException(status_code=400, detail="Antrian Habis")

        number = dpang[0]['pengantre']['number']
        kode = dpang[0]['pengantre']['kode']
        isOnline = dpang[0]['pengantre']['isOnline']
        idMasy = dpang[0]['pengantre']['idAkun']

        #update number sebelumnya
        dbase.update_one(
            {"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId),"pengantre.number":cek['queNow']}, 
            {"$set": {"pengantre.$.finishTime":dateTimeNow()}}
        )

        dbase.update_one(
            {"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId),"pengantre.number":number}, 
            {"$set": {
                "queNow":number,
                "kodeNow":kode,
                "timeNow": dpang[0]['pengantre']['createTime'],
                "pengantre.$.isCalled":True,
                "pengantre.$.calledTime": dateTimeNow(),
                "pengantre.$.servedById":current_user.accountId,
                "pengantre.$.servedByName":current_user.userName
                }}
        )
        MGDB.queue_masyarakat.update_one(
            {"idAkun": idMasy,"logAntrian.antrianId":ObjectId(id),"logAntrian.number":number}, 
            {"$set": {"logAntrian.$.isFinished":True}}
        )
        #update juga loketOnDuty nya
        activeNumber = kode+str(number)
        dbase.update_one(
            {"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId),"loketOnDuty.idAkun":ObjectId(current_user.accountId)}, 
            {"$set": {
                "loketOnDuty.$.totalServed":True,
                "loketOnDuty.$.activeNumber": number,
                "loketOnDuty.$.activeNoAntrian":activeNumber,
                "loketOnDuty.$.lastActive":dateTimeNow()
                },
            "$inc":{"loketOnDuty.$.totalServed":+1}}
        )

    #kirim perintah ke layar monitor agar bersuara
    #kirim notif ke orang yang dipanggil
    #kirim notif ke number 5 berikutnya

    return {"status":"ok","isOnline": isOnline, "nomor":number,"kode":kode,"namaLoket":cek['title'], "kodeLoket": cek['kode'],"namaPetugas":cek['loketOnDuty'][0]['name']}

#transfer antrian
@router_antrian.get("/transfer_antrian/{id}/{number}/{kirimkeId}", response_model=dict)
async def transfer_antrian(id: str, number:str, kirimkeId: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    #cari antrian sekarang, trus ditransfer
    cek = await dbase.find_one({
        "_id": ObjectId(id),
        "companyId": ObjectId(current_user.companyId),
        "pengantre.servedById":ObjectId(current_user.accountId),
        "pengantre.number":number
        },{"pengantre.$":1})
    
    if not cek:
        raise HTTPException(status_code=400, detail="Antrian tidak valid atau anda tidak berhak")

    await dbase.update_one(
        {"_id": ObjectId(kirimkeId),"companyId": ObjectId(current_user.companyId)}, 
        {"$addToSet": {"pengantre":cek['pengantre'][0]},"$inc":{"queTotal":+1}}
    )

    return {"detail":"ok"}
