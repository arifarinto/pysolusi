from util.util_user import CreateUser
from model.user import UserCreate
from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.queue.masyarakat import MasyarakatBase, MasyarakatOnDB, MasyarakatPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from util.util import RandomString, ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user

router_masyarakat = APIRouter()
dbase = MGDB.queue_masyarakat

async def GetMasyarakatOr404(id: str):
    _id = ValidateObjectId(id)
    masyarakat = await dbase.find_one({"_id": _id})
    if masyarakat:
        return masyarakat
    else:
        raise HTTPException(status_code=404, detail="Masyarakat not found") 

async def GetMasyarakatByIdAkun(id: str):
    _id = ValidateObjectId(id)
    masyarakat = await dbase.find_one({"idAkun": _id})
    if masyarakat:
        return masyarakat
    else:
        return False

async def GetMasyByIdAkunAndLay(id: str,antrianId: str):
    _id = ValidateObjectId(id)
    masyarakat = await dbase.find_one({"idAkun": _id,"logAntrian.antrianId":ObjectId(antrianId),"logAntrian.isFinished":False})
    if masyarakat:
        return masyarakat
    else:
        return False

# =================================================================================

async def SelfCekOrAddMasyarakat(current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    cek_masyarakat = await GetMasyarakatByIdAkun(current_user.accountId)
    if cek_masyarakat:
        return cek_masyarakat
    else:
        #insert ke table masyarakat
        masyarakat = MasyarakatBase()
        masyarakat.companyId = ObjectId(current_user.companyId)
        masyarakat.companyName = current_user.companyName
        masyarakat.creatorName = current_user.userName
        masyarakat.creatorId = ObjectId(current_user.accountId)
        masyarakat.createTime = dateTimeNow()
        masyarakat.updateTime = dateTimeNow()
        masyarakat.idAkun = ObjectId(current_user.accountId)
        masyarakat.userId = ObjectId(current_user.userId)
        masyarakat.name = current_user.userName
        masyarakat_op = await dbase.insert_one(masyarakat.dict())
        if masyarakat_op.inserted_id:
            return masyarakat

@router_masyarakat.post("/masyarakat", response_model=MasyarakatOnDB)
async def add_masyarakat(data_in: MasyarakatBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    masyarakat = IsiDefault(data_in, current_user)
    masyarakat.tags = ListToUp(masyarakat.tags)
    user = UserCreate()
    user.companyId = current_user.companyId
    user.companyName = current_user.companyName
    user.name = masyarakat.name
    user.phone = masyarakat.phone
    user.email = masyarakat.email
    user.role = ["ROLE_CUSTOMER"]
    user_op = await CreateUser(user, 'default')
    masyarakat.idAkun = user_op.object_id
    masyarakat_op = await dbase.insert_one(masyarakat.dict())
    if masyarakat_op.inserted_id:
        masyarakat = await GetMasyarakatOr404(masyarakat_op.inserted_id)
        return masyarakat


@router_masyarakat.post("/get_masyarakat", response_model=dict)
async def get_all_masyarakats(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = MasyarakatPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_masyarakat.get("/masyarakat/{id}", response_model=MasyarakatOnDB)
async def get_masyarakat_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    masyarakat = await dbase.find_one({"_id": id,"companyId": ObjectId(current_user.companyId)})
    if masyarakat:
        return masyarakat
    else:
        raise HTTPException(status_code=404, detail="Masyarakat not found")


@router_masyarakat.delete("/masyarakat/{id}", dependencies=[Depends(GetMasyarakatOr404)], response_model=dict)
async def delete_masyarakat_by_id(id: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    masyarakat_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if masyarakat_op.deleted_count:
        return {"status": f"deleted count: {masyarakat_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_masyarakat.put("/masyarakat/{id_}", response_model=MasyarakatOnDB)
async def update_masyarakat(id_: str, data_in: MasyarakatBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    masyarakat = IsiDefault(data_in, current_user, True)
    masyarakat.updateTime = dateTimeNow()
    masyarakat_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": masyarakat.dict(skip_defaults=True)}
    )
    if masyarakat_op.modified_count:
        return await GetMasyarakatOr404(id_)
    else:
        raise HTTPException(status_code=304)