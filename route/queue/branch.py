from route.queue.layanan import GetLayananOr404
from util.util_waktu import dateNow, dateTimeNow
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime, timedelta
import math

from model.queue.branch import BranchBase, BranchOnDB, BranchPage, BranchJenis
from model.util import SearchRequest, FieldObjectIdRequest
from model.auth import TokenData
from model.user import UserCreate
from model.queue.antrian import AntrianBase
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp, RandomString
from util.util_user import CreateUser
from route.auth import get_current_user

router_branch = APIRouter()
dbase = MGDB.queue_branch

async def GetBranchOr404(id: str):
    _id = ValidateObjectId(id)
    branch = await dbase.find_one({"_id": _id})
    if branch:
        return branch
    else:
        raise HTTPException(status_code=404, detail="Branch not found")

async def GetBranchByIdAkun(id: str):
    _id = ValidateObjectId(id)
    branch = await dbase.find_one({"idAkun": _id})
    if branch:
        return branch
    else:
        raise HTTPException(status_code=404, detail="Branch not found")

async def GetKiosksByLayanan(companyId:str, branchId: str, jenis:str, layananId:str):
    datas_cursor = MGDB.queue_kiosk.find({
        "companyId":ObjectId(companyId), 
        "branchId":ObjectId(branchId), 
        "jenis":jenis,
        "listLayananId":[layananId]},
        {"_id":0,"idAkun":1})
    datas = await datas_cursor.to_list(length=100)
    return datas

# =================================================================================


@router_branch.post("/branch", response_model=BranchOnDB)
async def add_branch(jenis : BranchJenis ,data_in: BranchBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    branch = IsiDefault(data_in, current_user)
    
    branch.tags = ListToUp(branch.tags)
    branch.title = branch.title.upper()
    branch.jenis = jenis
    user = UserCreate()
    user.companyId = current_user.companyId
    user.companyName = current_user.companyName
    user.name = branch.title
    user.phone = branch.phone
    user.email = branch.email
    user.role = ['ROLE_EMPLOYEE','ROLE_BRANCH']
    
    user_op = await CreateUser(user, 'default')
 
    branch.idAkun = user_op.object_id
    branch_op = await dbase.insert_one(branch.dict())
    if branch_op.inserted_id:
        branch = await GetBranchOr404(branch_op.inserted_id)
        return branch
    else:
       raise HTTPException(status_code=404, detail="Proses Gagal")


@router_branch.post("/get_branch", response_model=dict)
async def get_all_branchs(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, current_user: TokenData = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria,{"jenis":0}).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = BranchPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_branch.get("/branch/{id}", response_model=BranchOnDB)
async def get_branch_by_id(id: ObjectId = Depends(ValidateObjectId), current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    branch = await dbase.find_one({"_id": id,"companyId": ObjectId(current_user.companyId)})
    print(branch)
    if branch:
        return branch
    else:
        raise HTTPException(status_code=404, detail="Branch not found")


@router_branch.delete("/branch/{id}", dependencies=[Depends(GetBranchOr404)], response_model=dict)
async def delete_branch_by_id(id: str, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    branch_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if branch_op.deleted_count:
        return {"status": f"deleted count: {branch_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_branch.put("/branch/{id_}", response_model=BranchOnDB)
async def update_branch(id_: str, data_in: BranchBase, current_user: TokenData = Security(get_current_user, scopes=["*", "*"])):
    branch = IsiDefault(data_in, current_user, True)
    branch.updateTime = dateTimeNow()
    branch_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": branch.dict(skip_defaults=True)}
    )
    if branch_op.modified_count:
        return await GetBranchOr404(id_)
    else:
        raise HTTPException(status_code=304)


@router_branch.get("/generate_skedul_to_antrian", response_model=dict)
async def generate_skedul_to_antrian():
    branch = await dbase.find_one({"isSetSkedul":True})
    if branch:
        nextDays = branch['createSkedulBefore']
        tglCreate = dateNow() + timedelta(days=nextDays)
        hari = tglCreate.strftime("%A")
        print(hari)
        skedul = await MGDB.queue_skedul.find_one({"hari":hari})
        if skedul:
            print("ada")
            antrian = AntrianBase()
            antrian.companyId = ObjectId(branch['companyId'])
            antrian.branchId = branch['_id']
            antrian.layananId = ObjectId(skedul['idLayanan'])
            lay = await GetLayananOr404(skedul['idLayanan'])
            antrian.title = lay['title']
            antrian.kode = lay['kode']
            antrian.tanggal = tglCreate
            antrian.hari = hari
            antrian.mulai = skedul['mulai']
            antrian.mulaiIstirahat = skedul['mulaiIstirahat']
            antrian.selesaiIstirahat = skedul['selesaiIstirahat']
            antrian.selesai = skedul['selesai']
            antrian.capacity = skedul['capacity']
            #get kiosk yang akan menampilkan 
            ckioskLayar = await GetKiosksByLayanan(branch['companyId'], branch['_id'], "layar",skedul['idLayanan'])
            antrian.showAt = ckioskLayar
            ckiosk = await GetKiosksByLayanan(branch['companyId'], branch['_id'], "kiosk",skedul['idLayanan'])
            antrian.takeAt = ckiosk
            await MGDB.queue_antrian.insert_one(antrian.dict())
            return {"detail":"ada jadwal"}
        else:
            return {"detail":"tidak ada jadwal"}

        # UPDATE isSetSkedul = False
        # await dbase.update_one({"_id": branch["_id"]}, {"$set": {"isSetSkedul":False}})
        
    else:
        raise HTTPException(status_code=404, detail="Branch not found")