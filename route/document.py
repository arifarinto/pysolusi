from function.user import GetUserOr404
from docxtpl.template import DocxTemplate
from jinja2.loaders import BaseLoader
from bson.objectid import ObjectId
from config.config import MGDB
from fastapi.exceptions import HTTPException
from util.util import IsiDefault, cleanNullTerms
import requests
from definitions import OPEN_FILE_ROOT, SVC_EMAIL_ROOT, TEMPLATE_DOC_PATH
from typing import List
from function.document import (
    AddDocumentSetting,
    CreateDocument,
    DeleteDocument,
    GetAllDocuments,
    GetDocumentSettingByCompanyId,
    GetDocumentSettingOr404,
    GetDocumentOr404,
    GetDocumentsByCompanyId,
    GetDocumentsOnUserId,
    UpdateDocument,
    UpdateDocumentSetting,
)
from route.auth import get_current_user
from fastapi import Security
from model.default import JwtToken, UserTipeEnum
from model.support import (
    DocumentData,
    DocumentInput,
    DocumentPage,
    DocumentSettingBase,
    DocumentSettingOnDB,
    DocumentSettingPage,
)
from fastapi.routing import APIRouter
import json
from fastapi import FastAPI, File, UploadFile
import shutil
import os
from jinja2 import Environment, PackageLoader, meta
import math

router_document = APIRouter()

# === CRUD Document Setting ===
@router_document.post("/add_doc_setting", response_model=DocumentSettingOnDB)
async def add_doc_setting(
    docSetting: DocumentSettingBase,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    docSetting = IsiDefault(docSetting, current_user)
    return await AddDocumentSetting(docSetting)


@router_document.get("/my_account/get_doc_settings")
async def get_doc_setting_by_my_companyId(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    return await get_doc_setting_by_companyId(
        current_user.companyId, size, page, sort, dir, current_user
    )


@router_document.get("/{companyId}/get_doc_settings")
async def get_doc_setting_by_companyId(
    companyId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    docSettings = await GetDocumentSettingByCompanyId(companyId)
    skip = page * size
    datas = docSettings[skip : skip + size]
    totalElements = len(docSettings)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentSettingPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_document.get(
    "/get_doc_setting/{docSettingId}", response_model=DocumentSettingOnDB
)
async def get_doc_setting(
    docSettingId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    docSetting = await GetDocumentSettingOr404(docSettingId)
    return docSetting


@router_document.post(
    "/update_doc_setting/{docSettingId}", response_model=DocumentSettingOnDB
)
async def update_doc_setting(
    docSetting: DocumentSettingBase,
    docSettingId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    data = docSetting.dict(skip_defaults=True)
    data = cleanNullTerms(data)
    return await UpdateDocumentSetting(docSettingId, data)


@router_document.post(
    "/upload_doc_template/{docSettingId}", response_model=DocumentSettingOnDB
)
async def upload_doc_template(
    docSettingId: str,
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    docSetting = await GetDocumentSettingOr404(docSettingId)
    arrName = file.filename.split(".")
    if arrName[1] == "doc" or arrName[1] == "docx":
        originName = arrName[0] + "-" + docSettingId + "." + arrName[1]
        file_object = file.file
        upload = open(os.path.join(TEMPLATE_DOC_PATH, originName), "wb+")
        shutil.copyfileobj(file_object, upload)
        upload.close()

        # ambil semua list variable yang ada di dalam template
        env = Environment()
        filename = os.path.join(TEMPLATE_DOC_PATH, originName)
        template = DocxTemplate(filename)
        parameters = []
        for p in template.paragraphs:
            parsed_content = p.text
            parsed_content = env.parse(parsed_content)
            parameters_in_document = meta.find_undeclared_variables(parsed_content)
            for p in parameters_in_document:
                if not p in parameters:
                    parameters.append(p)

        dbase = MGDB.tbl_document_setting
        docSettingId = str(docSetting["_id"])
        await dbase.update_one(
            {"_id": ObjectId(docSettingId)},
            {"$set": {"templateFilename": originName, "parameter": parameters}},
        )
        return await GetDocumentSettingOr404(docSettingId)
    else:
        raise HTTPException(
            status_code=400, detail="Format dokumen tidak sesuai, harus doc atau docx"
        )
    # return "Template file dengan nama " + originName + " telah diupload"


# === CRUD DOCUMENT ===


# @router_document.get("/coba")
# async def coba():
#     return "ok"


@router_document.get("/all_documents", response_model=DocumentPage)
async def all_documents(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    documents = await GetAllDocuments()
    skip = page * size
    datas = documents[skip : skip + size]
    totalElements = len(documents)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_document.get("/user/{userId}", response_model=DocumentPage)
async def get_documents_by_user_id(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    documents = await GetDocumentsOnUserId(userId)
    skip = page * size
    datas = documents[skip : skip + size]
    totalElements = len(documents)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_document.post("", response_model=DocumentData)
async def add_document(
    data: DocumentInput,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    """
    Endpoint untuk menambahkan data document baru. Wajib login sebagai admin.
    """
    # pass
    document = await CreateDocument(data, current_user)
    linkDocument = {
        "linkDocument": OPEN_FILE_ROOT + "/get_document/" + str(document["id"]),
        "titleDocument": document["title"],
    }
    linkDocument = json.dumps(linkDocument, indent=4)
    user = await GetUserOr404(document["userId"])
    userEmail = user["email"]
    userEmail = "ristiriantoadi@gmail.com"
    params = {
        "emailTo": userEmail,
        # "subject": "Dokumen Klien",#
        "subject": document["title"],
        "templateName": "template_email_dokumen_baru.html",
        "environment": linkDocument,
    }
    url = SVC_EMAIL_ROOT + "/kirim_email_standard"
    response = requests.post(url, headers={"Accept": "application/json"}, params=params)
    return document


@router_document.get("/{idDocument}", response_model=DocumentData)
async def get_document_by_id_document(
    idDocument: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    # checkIfUserAuthorized(current_user,userId)
    document = await GetDocumentOr404(idDocument)
    return document


@router_document.get("/company/{companyId}", response_model=DocumentPage)
async def get_documents_by_company_id(
    companyId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    # search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["", "*"]),
):
    print("get documents by company id")
    documents = await GetDocumentsByCompanyId(companyId)  #
    skip = page * size
    datas = documents[skip : skip + size]
    totalElements = len(documents)
    totalPages = math.ceil(totalElements / size)
    reply = DocumentPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_document.put("/{idDocument}/update", response_model=DocumentData)
async def update_document_general(
    idDocument: str,
    data: DocumentInput,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    return await UpdateDocument(idDocument, data.dict())


@router_document.delete("/{idDocument}")
async def delete_document(
    idDocument: str,
    current_user: JwtToken = Security(get_current_user, scopes=["client", "*"]),
):
    await DeleteDocument(idDocument)
    return "Berhasil hapus document dengan id " + idDocument
