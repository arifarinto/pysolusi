import math
from typing import List
from bson.objectid import ObjectId
from config.config import MGDB
from fastapi.exceptions import HTTPException
from fastapi.params import Security
from fastapi.routing import APIRouter
from function.user import CreateUser, UpdateDateOfBirth
from model.default import IdentityData, JwtToken, UserInput
from model.membership.kasir import KasirBase, KasirCredential, KasirPage
from model.util import ObjectIdStr, SearchRequest
from route.auth import get_current_user
from util.util import RandomNumber, cleanNullTerms
from util.util_waktu import dateNowStr


router_kasir = APIRouter()
dbase = MGDB.user_membership_kasir


async def GetKasirOr404(userId: str):
    # dbase = MGDB.user_admin
    kasir = await dbase.find_one({"userId": ObjectId(userId)})

    if "isDelete" in kasir == False:
        kasir["isDelete"] = False

    if kasir and (kasir["isDelete"] == False):
        return kasir
    else:
        raise HTTPException(status_code=404, detail="Kasir tidak ditemukan")


async def UpdateAdmin(userId: str, updateData: dict):
    await GetKasirOr404(userId)
    try:
        updateData = cleanNullTerms(updateData)
        updateData = UpdateDateOfBirth(updateData)
        await dbase.update_one({"userId": ObjectId(userId)}, {"$set": updateData})
        return await GetKasirOr404(userId)
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")


@router_kasir.post("/kasir", response_model=KasirBase)
async def add_kasir(
    dataKasir: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    user = KasirCredential(**dataKasir.dict())
    arName = user.name.split(" ")
    user.username = arName[0] + RandomNumber(4)
    user.companyId = ObjectId(current_user.companyId)
    identity = IdentityData()
    identity.dateOfBirth = dateNowStr()
    user.identity = identity
    return await CreateUser("user_membership_kasir", user, "pass", True, ["edc"])
    # pass


@router_kasir.post("/get_all_kasir")
async def get_all_kasir(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    kasirs = []
    async for kasir in dbase.find({"isDelete": False}):
        kasirs.append(kasir)
    skip = page * size
    datas = kasirs[skip : skip + size]
    totalElements = len(kasirs)
    totalPages = math.ceil(totalElements / size)
    reply = KasirPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_kasir.get("/get_kasir_by_company_id", response_model=KasirPage)
async def get_kasir_by_company_id(
    companyId: ObjectIdStr,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    kasirs = []
    async for kasir in dbase.find(
        {"isDelete": False, "companyId": ObjectId(companyId)}
    ):
        kasirs.append(kasir)
    skip = page * size
    datas = kasirs[skip : skip + size]
    totalElements = len(kasirs)
    totalPages = math.ceil(totalElements / size)
    reply = KasirPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply
    # pass


@router_kasir.get("/get_kasir_by_user_id", response_model=KasirBase)
async def get_kasir_by_user_id(
    userId: ObjectIdStr,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await GetKasirOr404(userId)


@router_kasir.put("/kasir/{userId}", response_model=KasirBase)
async def update_kasir(
    userId: str,
    kasirData: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await UpdateAdmin(userId, kasirData.dict())


@router_kasir.delete("/kasir/{userId}")
async def delete_kasir(
    userId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    await GetKasirOr404(userId)
    try:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$set": {"isDelete": True}}
        )
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")
    return "Kasir dengan userId " + userId + " telah dihapus"
