# backend/tancho/beritas/routes.py

from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.berita import BeritaOnDB, BeritaBase, BeritaPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_berita = APIRouter()
dbase = MGDB.tbl_berita


async def GetBeritaOr404(id_: str):
    _id = ValidateObjectId(id_)
    berita = await dbase.find_one({"_id": _id})
    if berita:
        return berita
    else:
        raise HTTPException(status_code=404, detail="Berita not found")


# =================================================================================


@router_berita.post("/berita", response_model=BeritaOnDB)
async def add_berita(
    data_in: BeritaBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    berita = IsiDefault(data_in, current_user)
    berita.title = berita.title.upper()
    cberita = await dbase.find_one(
        {"companyId": ObjectId(current_user.companyId), "title": berita.title}
    )
    if cberita:
        raise HTTPException(
            status_code=400, detail="Berita tidak boleh dengan judul yang sama"
        )
    berita_op = await dbase.insert_one(berita.dict())
    if berita_op.inserted_id:
        berita = await GetBeritaOr404(berita_op.inserted_id)
        return berita


@router_berita.post("/get_berita", response_model=dict)
async def get_all_beritas(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    skip = page * size
    search.defaultObjectId.append(
        FieldObjectIdRequest(field="companyId", key=ObjectId(current_user.companyId))
    )
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = BeritaPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_berita.get("/berita/{id_}", response_model=BeritaOnDB)
async def get_berita_by_id(
    id_: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    berita = await dbase.find_one(
        {"_id": id_, "companyId": ObjectId(current_user.companyId)}
    )
    if berita:
        return berita
    else:
        raise HTTPException(status_code=404, detail="Berita not found")


@router_berita.delete(
    "/berita/{id_}", dependencies=[Depends(GetBeritaOr404)], response_model=dict
)
async def delete_berita_by_id(
    id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    berita_op = await dbase.delete_one(
        {"_id": ObjectId(id_), "companyId": ObjectId(current_user.companyId)}
    )
    if berita_op.deleted_count:
        return {"status": f"deleted count: {berita_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_berita.put("/berita/{id_}", response_model=BeritaOnDB)
async def update_berita(
    id_: str,
    data_in: BeritaBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    berita = IsiDefault(data_in, current_user, True)
    berita.updateTime = dateTimeNow()
    berita_op = await dbase.update_one(
        {"_id": ObjectId(id_), "companyId": ObjectId(current_user.companyId)},
        {"$set": berita.dict(skip_defaults=True)},
    )
    if berita_op.modified_count:
        return await GetBeritaOr404(id_)
    else:
        raise HTTPException(status_code=304)
