from bson.objectid import ObjectId
from function.inquiry import GetInquiry
from function.notif import CreateNotifMembership
from model.default import InquiryBase, JwtToken, NotifMembershipData
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response,
    Security,
    BackgroundTasks,
)
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
from route.auth import get_current_user, login_for_access_token
from util.util_trx import transaksi_universal
from function.user import CheckMemberByUserId, CreateQrCode
from util.util import RandomString

router_trxuser = APIRouter()
CHANNEL = "MOBILE"


@router_trxuser.post("/cek_saldo", response_model=dict)
async def cek_saldo(
    code: str,
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    trxName = "cek_saldo"
    return trxName


@router_trxuser.post("/inquiry_data_kartu", response_model=dict)
async def inquiry_data_kartu(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    trxName = "inquiry_data_kartu"


@router_trxuser.post("/set_limit_kartu", response_model=dict)
async def set_limit_kartu(
    code: str,
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    trxName = "set_limit_kartu"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxuser.post("/cek_nomor_va", response_model=dict)
async def cek_nomor_va(
    code: str,
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    trxName = "cek_nomor_va"
    return trxName


@router_trxuser.post("/create_nomor_va", response_model=dict)
async def create_nomor_va(
    code: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):

    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "create_nomor_va"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxuser.get("/create_qr_code_cpm/{amount}", response_model=dict)
async def create_qr_code_cpm(
    amount: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await CreateQrCode(current_user.companyId, current_user.userId, amount)


@router_trxuser.post("/get_data_invoice", response_model=dict)
async def get_data_invoice(
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    )
):
    trxName = "get_data_invoice"


@router_trxuser.post("/inquiry_invoice_internal", response_model=dict)
async def inquiry_invoice_internal(
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    )
):
    trxName = "inquiry_invoice_internal"


@router_trxuser.post("/transaksi_invoice_internal", response_model=dict)
async def transaksi_invoice_internal(
    code: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):

    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    # amount = 0
    trxName = "transaksi_invoice_internal"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = current_user.userId
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxuser.post("/get_data_donasi", response_model=dict)
async def get_data_donasi(
    code: str,
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    trxName = "get_data_donasi"
    return trxName


@router_trxuser.post("/inquiry_donasi", response_model=dict)
async def inquiry_donasi(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    trxName = "inquiry_donasi"


@router_trxuser.post("/transaksi_donasi", response_model=dict)
async def transaksi_donasi(
    code: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    # ini yang debit dan kredit, userId-nya sama -- apa maksudnya?
    amount = 0
    trxName = "transaksi_donasi"
    noreff = RandomString(8)
    # debtUserId = current_user.userId
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxuser.post("/transfer_saldo_on_us", response_model=dict)
async def transfer_saldo_on_us(
    code: str,
    # userIdTujuan: str,
    # amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):

    trxName = "transfer_saldo_on_us"
    noreff = RandomString(8)
    debtUserId = current_user.userId

    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    creditUserId = userIdTujuan

    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1

    # notif = NotifMembershipData()
    # notif.title = "Transaksi Berhasil"
    # notif.message = "Transaksi transfer telah berhasil dilakukan"
    # notif.userId = ObjectId(current_user.userId)
    # notif.id = ObjectId()
    # await CreateNotifMembership(current_user.userId, notif)


@router_trxuser.post("/transfer_ke_rekening_bank_sendiri", response_model=dict)
async def transfer_ke_rekening_bank_sendiri(
    code: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):

    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    # amount = 0
    trxName = "transfer_ke_rekening_bank_sendiri"
    noreff = RandomString(8)
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    trx1 = transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxuser.post("/create_qris_cpm", response_model=dict)
async def create_qris_cpm(
    code: str,
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    trxName = "create_qris_cpm"
    return trxName


@router_trxuser.post("/inquiry_qris_mpm_statis", response_model=dict)
async def inquiry_qris_mpm_statis(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    trxName = "inquiry_qris_mpm_statis"


@router_trxuser.post("/inquiry_qris_mpm_dinamis", response_model=dict)
async def inquiry_qris_mpm_dinamis(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    trxName = "inquiry_qris_mpm_dinamis"


@router_trxuser.post("/transaksi_qris_merchant", response_model=dict)
async def transaksi_qris_merchant(
    code: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    # amount = 0
    trxName = "transaksi_qris_merchant"
    noreff = RandomString(8)
    # debtUserId = current_user.userId
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxuser.post("/pembayaran_online_fixed_payment", response_model=dict)
async def pembayaran_online_fixed_payment(
    code: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    # amount = 0
    trxName = "pembayaran_online_fixed_payment"
    noreff = RandomString(8)
    # debtUserId = current_user.userId
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxuser.post("/pembayaran_online_open_payment", response_model=dict)
async def pembayaran_online_open_payment(
    code: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    # amount = 0
    trxName = "pembayaran_online_open_payment"
    noreff = RandomString(8)
    # debtUserId = current_user.userId
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxuser.post("/payment_qr_code", response_model=dict)
# async def payment_qr_code(
#     code: str,
#     userIdTujuan: str,
#     amount: int,
#     current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
# ):
async def payment_qr_code(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # pass
    # amount = 0

    trxName = "transfer_saldo_on_us"
    noreff = RandomString(8)
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    print("user id tujuan: " + str(userIdTujuan))
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    amount = inquiry.amount
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1
