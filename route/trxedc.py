from function.inquiry import CreateInquiry, GetInquiry
from model.default import InquiryBase, JwtToken
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response,
    Security,
    BackgroundTasks,
)
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
from route.auth import get_current_user, login_for_access_token
from util.util_trx import cek_limit_trx, transaksi_universal
from function.user import (
    CheckEdcByUserId,
    CheckMemberByUserId,
    GetUserByNfcOr404,
    InquiryQrCode,
)
from util.util import RandomString

router_trxedc = APIRouter()

CHANNEL = "EDC"


@router_trxedc.post("/inquiry_userid", response_model=dict)
async def inquiry_userid(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    trxName = "inquiry_userid"
    return await CreateInquiry(current_user, userIdTujuan, amount)


@router_trxedc.post("/inquiry_data_kartu", response_model=InquiryBase)
async def inquiry_data_kartu(
    amount: int,
    nfcId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    cek = await GetUserByNfcOr404(nfcId)
    userIdTujuan = str(cek["userId"])
    # cLimit = await cek_limit_trx(userIdTujuan)
    inq = await CreateInquiry(current_user, userIdTujuan, amount)
    return inq


@router_trxedc.post("/transaksi_dengan_kartu", response_model=dict)
async def transaksi_dengan_kartu(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    trxName = "transaksi_dengan_kartu"
    noreff = RandomString(8)

    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxedc.post("/transaksi_kartu_unlimited", response_model=dict)
async def transaksi_kartu_unlimited(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "transaksi_kartu_unlimited"
    noreff = RandomString(8)
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxedc.post("/topup_saldo_dengan_kartu", response_model=dict)
async def topup_saldo_dengan_kartu(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    # question / pertanyaan:
    # 1. maksudnya topup saldo dengan kartu bagaimana?
    # 2. siapa yang menjadi current user dalam transaksi ini?

    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "topup_saldo_dengan_kartu"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxedc.get("/inquiry_qr_code_cpm/{qrcode}", response_model=dict)
async def inquiry_qr_code_cpm(
    qrcode: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    """
    Cek apakah qr code nya masih valid atau sudah expired
    """
    return await InquiryQrCode(current_user, qrcode)


@router_trxedc.post("/transaksi_qr_code_cpm", response_model=dict)
async def transaksi_qr_code_cpm(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "transaksi_qr_code_cpm"
    noreff = RandomString(8)
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxedc.post("/topup_saldo_dengan_userid", response_model=dict)
async def topup_saldo_dengan_userid(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "topup_saldo_dengan_userid"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxedc.post("/transfer_ke_rekening_bank_sendiri", response_model=dict)
async def transfer_ke_rekening_bank_sendiri(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    # question:
    # 1. userIdTujuannya siapa?
    # 2. current user nya siapa?
    # 3. di mana nomor rekening banknya didefinisikan?
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "transfer_ke_rekening_bank_sendiri"
    noreff = RandomString(8)
    debtUserId = current_user.userId
    creditUserId = userIdTujuan
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxedc.post("/inquiry_qris_cpm", response_model=dict)
async def inquiry_qris_cpm(
    qrcode: str,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    return await InquiryQrCode(current_user, qrcode)


@router_trxedc.post("/transaksi_qris_customer", response_model=dict)
async def transaksi_qris_customer(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    amount = inquiry.amount

    trxName = "transaksi_qris_customer"
    noreff = RandomString(8)
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1


@router_trxedc.post("/create_qris_mpm_dinamis", response_model=dict)
async def create_qris_mpm_dinamis(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    trxName = "create_qris_mpm_dinamis"


@router_trxedc.post("/create_qris_mpm_statis", response_model=dict)
async def create_qris_mpm_statis(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["edc", "*"]),
):
    trxName = "create_qris_mpm_statis"


@router_trxedc.post("/payment_qr_code_mpm", response_model=dict)
async def payment_qr_code_mpm(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # pass
    # amount = 0
    trxName = "transfer_saldo_on_us"
    noreff = RandomString(8)
    inquiry = await GetInquiry(code)
    inquiry = InquiryBase(**inquiry)
    userIdTujuan = inquiry.userDestination
    debtUserId = userIdTujuan
    creditUserId = current_user.userId
    # await CheckMemberByUserId(userIdTujuan, current_user.companyId)
    await CheckEdcByUserId(userIdTujuan, current_user.companyId)
    amount = inquiry.amount
    trx1 = await transaksi_universal(
        code,
        current_user.companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
    )
    return trx1
