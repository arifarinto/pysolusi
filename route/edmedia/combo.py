# backend/tancho/pengajars/routes.py

from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.edmedia.pengajar import PengajarMapelPage, comboPengajar
from model.edmedia.pelajar import PelajarMapelPage
from model.edmedia.materi import comboMateri
from model.edmedia.silabus import comboSilabus
from model.edmedia.kelas import comboKelas
from model.edmedia.ruang import comboRuang
from model.edmedia.kbm import comboTipe, comboGenerateKbm
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import *

router_combo = APIRouter()

# =================================================================================

@router_combo.get("/get_combo_pengajar", response_model=List[comboPengajar])
async def get_combo_pengajar(current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)

    pengajars_cursor = MGDB.user_edmedia_pengajar.find(criteria, {"userId" : 1, "name" : 1, "noId" : 1}).sort("name", 1)
    pengajars = await pengajars_cursor.to_list(1000)
    return pengajars


@router_combo.get("/get_combo_pengajar/{userId}", response_model=comboPengajar)
async def get_combo_pengajar_by_user_id_pengajar(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    criteria["userId"] = ObjectId(userId)

    pengajars_cursor = await MGDB.user_edmedia_pengajar.find_one(criteria, {"userId" : 1, "name" : 1, "noId" : 1})
    return pengajars_cursor


@router_combo.get("/get_combo_materi", response_model=List[comboMateri])
async def get_combo_materi(current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    if "pengajar" in current_user.roles:
        pengajar = await MGDB.user_edmedia_pengajar.find_one({
            "companyId": ObjectId(current_user.companyId),
            "userId": ObjectId(current_user.userId)})
        silabus = await MGDB.edmedia_silabus.find_one({
            "companyId": ObjectId(current_user.companyId),
            "pengajar": str(pengajar["_id"])})
        if silabus:
            criteria["idSilabus"] = silabus["_id"]
        else:
            criteria["_id"] = None
    
    materis_cursor = MGDB.edmedia_materi.find(criteria, {"topik" : 1, "subTopik" : 1, "judul" : 1, "subJudul" : 1}).sort("topik", 1)
    materis = await materis_cursor.to_list(100000)
    return materis


@router_combo.get("/get_combo_materi/{idMateri}", response_model=comboMateri)
async def get_combo_materi_by_id_materi(idMateri: str , current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    criteria["_id"] = ObjectId(idMateri)

    materis_cursor = await MGDB.edmedia_materi.find_one(criteria, {"topik" : 1, "subTopik" : 1, "judul" : 1, "subJudul" : 1})
    return materis_cursor


# @router_combo.get("/get_combo_materi_rpp/{idSilabus}", response_model=List[comboMateri])
# async def get_combo_materi_by_id_silabus(idSilabus: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
#     criteria = {}
#     criteria["companyId"] = ObjectId(current_user.companyId)
#     criteria["idSilabus"] = ObjectId(idSilabus)
    
#     materis_cursor = MGDB.edmedia_materi.find(criteria, {"topik" : 1, "subTopik" : 1, "judul" : 1, "subJudul" : 1}).sort("topik", 1)
#     materis = await materis_cursor.to_list(100000)  
#     return materis


# @router_combo.get("/get_combo_materi_pengajar/{userId}", response_model=List[comboMateri])
# async def get_combo_materi_by_user_id_pengajar(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
#     data = await MGDB.user_edmedia_pengajar.find_one({
#         "companyId": ObjectId(current_user.companyId),
#         "userId": ObjectId(userId)})
#     criteria = {}
#     criteria["companyId"] = ObjectId(current_user.companyId)
#     criteria["pengajar"] = str(data['_id'])
    
#     silabuss_cursor = MGDB.edmedia_silabus.find(criteria, {"namaMapel" : 1, "programStudi" : 1,"jurusan":1,"kelas":1,"semester":1})
#     silabuss = await silabuss_cursor.to_list(100000)
#     data_id = []
#     for id in silabuss:
#         data_id.append({'idSilabus': id['_id']})
    
#     criterias = {}
#     criterias["companyId"] = ObjectId(current_user.companyId)
#     criterias["$or"] = data_id
#     materis_cursor = MGDB.edmedia_materi.find(criterias, {"topik" : 1, "subTopik" : 1, "judul" : 1, "subJudul" : 1}).sort("topik", 1)
#     materis = await materis_cursor.to_list(100000)  
#     return materis


@router_combo.get("/get_combo_silabus", response_model=List[comboSilabus]) # combo all untuk admin dan pengajar
async def get_combo_silabus(current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    if "pengajar" in current_user.roles:
        pengajar = await MGDB.user_edmedia_pengajar.find_one({
            "companyId": ObjectId(current_user.companyId),
            "userId": ObjectId(current_user.userId)})
        if pengajar:
            criteria["pengajar"] = str(pengajar["_id"])
        else:
            criteria["_id"] = None
    project = {
        "namaMapel": 1,
        "kelas": 1,
        "tahun": 1,
        "semester": 1,
        "programStudi": 1,
        "jurusan": 1
    }
    sort = [
        ("namaMapel", 1),
        ("kelas", 1),
        ("tahun", -1),
        ("semester", 1)
    ]
    silabuss_cursor = MGDB.edmedia_silabus.find(criteria, project).sort(sort)
    silabuss = await silabuss_cursor.to_list(100000)  
    return silabuss


@router_combo.get("/get_combo_silabus/{idSilabus}", response_model=List[comboSilabus]) # combo all untuk admin dan pengajar
async def get_combo_silabus_by_id_silabus(idSilabus: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    criteria["_id"] = ObjectId(idSilabus)
    if "pengajar" in current_user.roles:
        pengajar = await MGDB.user_edmedia_pengajar.find_one({
            "companyId": ObjectId(current_user.companyId),
            "userId": ObjectId(current_user.userId)})
        if pengajar:
            criteria["pengajar"] = str(pengajar["_id"])
        else:
            criteria["_id"] = None
    project = {
        "namaMapel": 1,
        "kelas": 1,
        "tahun": 1,
        "semester": 1,
        "programStudi": 1,
        "jurusan": 1
    }
    sort = [
        ("namaMapel", 1),
        ("kelas", 1),
        ("tahun", -1),
        ("semester", 1)
    ]
    silabuss_cursor = MGDB.edmedia_silabus.find(criteria, project).sort(sort)
    silabuss = await silabuss_cursor.to_list(100000)  
    return silabuss


@router_combo.get("/get_combo_silabus/userId", response_model=List[comboSilabus]) # combo opsi silabus di timeline
async def get_combo_silabus_timeline_by_user_id(current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    criteria["rpp.isAkses"] = True
    if "pengajar" in current_user.roles:
        criteria["rpp.pengajar.userId"] = ObjectId(current_user.userId)
    elif "pelajar" in current_user.roles:
        criteria["pelajar.userId"] = ObjectId(current_user.userId)
    pipeline = [
    {
        '$unwind': {
            'path': '$rpp'
        }
    }, {
        '$match': criteria
    }, {
        '$group': {
            '_id': {
                '_id': '$idSilabus',
                'namaMapel': '$namaMapel', 
                'kelas': '$kelas', 
                'tahun': '$tahun', 
                'semester': '$semester', 
                'programStudi': '$programStudi', 
                'jurusan': '$jurusan'
            }
        }
    }, {
        '$project': {
            '_id': 0, 
            '_id': '$_id._id',
            'namaMapel': '$_id.namaMapel', 
            'kelas': '$_id.kelas', 
            'tahun': '$_id.tahun', 
            'semester': '$_id.semester', 
            'programStudi': '$_id.programStudi', 
            'jurusan': '$_id.jurusan'
        }
    }
    ]    
    timeline_cursor = MGDB.edmedia_kbm.aggregate(pipeline)
    timeline = await timeline_cursor.to_list(1000)
    return timeline


@router_combo.get("/get_combo_generate_kbm", response_model=List[comboGenerateKbm])
async def get_combo_generate_kbm(current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)

    pipeline = [
    {
        '$match': criteria
    }, {
        '$group': {
            '_id': '$companyId', 
            'tahun': {
                '$addToSet': '$tahun'
            }, 
            'semester': {
                '$addToSet': '$semester'
            }, 
            'programStudi': {
                '$addToSet': '$programStudi'
            }, 
            'jurusan': {
                '$addToSet': '$jurusan'
            }, 
            'kelas': {
                '$addToSet': '$kelas'
            }
        }
    }
    ]
    kbm_cursor = MGDB.edmedia_silabus.aggregate(pipeline)
    kbm = await kbm_cursor.to_list(1)
    return kbm


# @router_combo.get("/get_combo_silabus_pelajar/{userId}", response_model=List[comboSilabus])
# async def get_combo_silabus_by_user_id_pelajar(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
#     data = await MGDB.user_user_edmedia_pelajar.find_one({
#         "companyId": ObjectId(current_user.companyId),
#         "userId": ObjectId(userId)})
#     criteria = {}
#     criteria["companyId"] = ObjectId(current_user.companyId)
#     criteria["kelas"] = data['kelasNow']
    
#     silabuss_cursor = MGDB.edmedia_silabus.find(criteria, {"namaMapel" : 1, "programStudi" : 1,"jurusan":1,"kelas":1,"semester":1}).sort("namaMapel", 1)
#     silabuss = await silabuss_cursor.to_list(100000)  
#     return silabuss


# @router_combo.get("/get_combo_silabus_pengajar/{userId}", response_model=List[comboSilabus])
# async def get_combo_silabus_by_user_id_pengajar(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
#     roles = current_user.roles
#     data = await MGDB.user_edmedia_pengajar.find_one({
#         "companyId": ObjectId(current_user.companyId),
#         "userId": ObjectId(userId)})
#     criteria = {}
#     criteria["companyId"] = ObjectId(current_user.companyId)
#     criteria["pengajar"] = str(data['_id'])
    
#     silabuss_cursor = MGDB.edmedia_silabus.find(criteria, {"namaMapel" : 1, "programStudi" : 1,"jurusan":1,"kelas":1,"semester":1}).sort("namaMapel", 1)
#     silabuss = await silabuss_cursor.to_list(100000)  
#     return silabuss


@router_combo.get("/get_combo_kelas")
async def get_combo_kelas(current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)

    pipeline = [
    {
        '$match': criteria
    }, {
        '$group': {
            '_id': '$companyId', 
            'kelas': {
                '$addToSet': '$kelas'
            }
        }
    }
    ]

    kelass_cursor = MGDB.edmedia_kelas.aggregate(pipeline)
    kelass = await kelass_cursor.to_list(1)
    if kelass:
        kelass = kelass[0]['kelas']
        kelass.sort()
        return kelass
    else:
        return []


@router_combo.get("/get_combo_kelasDetail/{kelas}")
async def get_combo_kelas_detail(kelas: str, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)
    criteria["kelas"] = kelas

    pipeline = [
    {
        '$match': criteria
    }, {
        '$group': {
            '_id': '$kelas', 
            'kelas': {
                '$addToSet': '$kelasDetail'
            }
        }
    }, {
        '$match': {
            '_id': kelas
        }
    }
    ]

    kelass_cursor = MGDB.edmedia_kelas.aggregate(pipeline)
    kelass = await kelass_cursor.to_list(1)
    if kelass:
        kelass = kelass[0]['kelas']
        kelass.sort()
        return kelass
    else:
        return []


@router_combo.get("/get_combo_ruang", response_model=List[comboRuang])
async def get_combo_ruang(current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = {}
    criteria["companyId"] = ObjectId(current_user.companyId)

    ruangs_cursor = MGDB.edmedia_ruang.find(criteria, {"name" : 1}).sort("name", 1)
    ruangs = await ruangs_cursor.to_list(100000)
    return ruangs


# @router_combo.get("/get_combo_tipe", response_model=List[comboTipe])
# async def get_combo_tipe(current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
#     criteria = {}
#     criteria["companyId"] = ObjectId(current_user.companyId)
    
#     pipeline = [
#     {
#         '$match': criteria
#     }, {
#         '$unwind': {
#             'path': '$rpp'
#         }
#     }, {
#         '$group': {
#             '_id': '$rpp.tipe', 
#             'fieldN': {
#                 '$sum': 1
#             }
#         }
#     }, {
#         '$project': {
#             '_id': 0, 
#             'tipe': '$_id'
#         }
#     }, {
#         '$sort': {
#             'tipe': 1
#         }
#     }
#     ]
#     tipes_cursor = MGDB.edmedia_kbm.aggregate(pipeline)
#     tipes = await tipes_cursor.to_list(100000)  
#     return tipes

@router_combo.get("/get_combo_check_datetime")
async def get_combo_check_datetime():
    return {
        'datetime.now()': datetime.now(),
        'dateNow()': dateNow(),
        'dateTimeNow()': dateTimeNow(),
        'timeNow()': timeNow(),
        'dateTimeNowStr()': dateTimeNowStr(),
        'dateNowStr()': dateNowStr(),
        'timeNowStr()': timeNowStr()
    }


@router_combo.post("/get_combo_history_pelajar/{userId}", response_model=dict)
async def get_combo_history_by_user_id_pelajar(userId: str, size: int = 10, page: int = 0, sort: str = "tahun", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    data = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId)})
    criteria = CreateCriteria(search)

    pipeline = [
    {
        '$match': {
            'companyId': data['companyId'], 
            'userId': data['userId']
        }
    }, {
        '$unwind': {
            'path': '$dataKbm'
        }
    }, {
        '$group': {
            '_id': {
                'tahun': '$dataKbm.tahun', 
                'semester': '$dataKbm.semester', 
                'kelas': '$dataKbm.kelas', 
                'kelasDetail': '$dataKbm.kelasDetail'
            }, 
            'fieldN': {
                '$sum': 1
            }
        }
    }, {
        '$project': {
            '_id': 0, 
            'tahun': '$_id.tahun', 
            'semester': '$_id.semester', 
            'kelas': '$_id.kelas', 
            'kelasDetail': '$_id.kelasDetail'
        }
    }, {
        '$match': criteria
    }, {
        '$sort': {
            'tahun': 1, 
            'semester': 1, 
            'kelas': 1, 
            'kelasDetail': 1
        }
    }, {
        '$sort': {
            sort: dir
        }
    }
    ]
    tipes_cursor = MGDB.user_edmedia_pelajar.aggregate(pipeline)
    tipes = await tipes_cursor.to_list(100000)

    skip = page * size
    listdata = tipes
    datas = listdata[skip:skip+size]
    totalElements = len(listdata)
    totalPages = math.ceil(totalElements / size)
    reply = PelajarMapelPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_combo.post("/get_combo_history_pengajar/{userId}", response_model=dict)
async def get_combo_history_by_user_id_pengajar(userId: str, size: int = 10, page: int = 0, sort: str = "tahun", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    criteria = CreateCriteria(search)
    criteria = {'$and': criteria['$and'][1:]} if len(criteria['$and']) > 1 else {'$and': [{}]}

    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId), 
            'pengajar.userId': ObjectId(userId)
        }
    }, {
        '$group': {
            '_id': {
                'tahun': '$tahun', 
                'semester': '$semester', 
                'kelas': '$kelas', 
                'kelasDetail': '$kelasDetail', 
                'namaMapel': '$namaMapel',
                'idSilabus': '$idSilabus'
            }, 
            'fieldN': {
                '$sum': 1
            }
        }
    }, {
        '$project': {
            '_id': 0, 
            'tahun': '$_id.tahun', 
            'semester': '$_id.semester', 
            'kelas': '$_id.kelas', 
            'kelasDetail': '$_id.kelasDetail', 
            'namaMapel': '$_id.namaMapel',
            'idSilabus': '$_id.idSilabus'
        }
    }, {
        '$match': criteria
    }, {
        '$sort': {
            'tahun': 1, 
            'semester': 1, 
            'kelas': 1, 
            'kelasDetail': 1, 
            'namaMapel': 1
        }
    }, {
        '$sort': {
            sort: dir
        }
    }
    ]
    tipes_cursor = MGDB.edmedia_kbm.aggregate(pipeline)
    tipes = await tipes_cursor.to_list(100000)

    skip = page * size
    listdata = tipes
    datas = listdata[skip:skip+size]
    totalElements = len(listdata)
    totalPages = math.ceil(totalElements / size)
    reply = PengajarMapelPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply