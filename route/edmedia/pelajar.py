# backend/tancho/pelajars/routes.py

from bson.objectid import ObjectId
from pydantic.types import NoneStr
from pydantic.typing import NONE_TYPES, NoneType
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, UploadFile, File
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from function.company import GetCompanyOr404
from function.notif import CreateNotif
from model.edmedia.pelajar import PelajarBase, PelajarCredential, PelajarInput, PelajarOnDB, PelajarPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import AddressData, IdentityData, JwtToken, NotifData, UserDataBase, CredentialData, UserAksesBase, NotifData
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria, ValidateEmail, ValidPhoneNumber, PWDCONTEXT
from route.auth import get_current_user
from util.util_waktu import convertDateToStrDate, convertStrDateToDate, convertDateToStrPassword, dateNowStr, dateTimeNow, bulanToNumber
from util.util_excel import getString, getDate, getJenisKelamin
from route.auth import CreateUser

router_pelajar = APIRouter()
dbase = MGDB['user_edmedia_pelajar']

async def GetPelajarOr404(id_: str):
    _id = ValidateObjectId(id_)
    pelajar = await dbase.find_one({"_id": _id})
    if pelajar:
        return pelajar
    else:
        raise HTTPException(status_code=404, detail="Pelajar not found")

# =================================================================================


@router_pelajar.post("/pelajar", response_model=PelajarBase)
async def add_pelajar(dataIn: PelajarInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pelajar = PelajarCredential(**dataIn.dict())
    pelajar = IsiDefault(pelajar, current_user)
    pelajar.userId = ObjectId()

    cpelajar = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "noId": pelajar.noId})
    if cpelajar:
        raise HTTPException(status_code=400, detail="Pelajar with NIS : "+pelajar.noId+" is registered")
    
    pelajar.name = pelajar.name.upper()
    pelajar.tags = ListToUp(pelajar.tags)
    pelajar.username = pelajar.noId

    identity = IdentityData()
    if pelajar.identity.dateOfBirth:
        identity.dateOfBirth = pelajar.identity.dateOfBirth
        password = convertDateToStrPassword(pelajar.identity.dateOfBirth)
    else:
        password = pelajar.noId 

    pelajar.identity = identity
    user_op = await CreateUser("user_edmedia_pelajar", pelajar, password, True, ['pelajar'])
    return user_op

#TODO (sedang koreksi library yg salah) coding masih salah
@router_pelajar.post("/upload_pelajar")
async def upload_pelajar(file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file, converters={"No ID": str, "NIK": str})
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    userdata = []
    userakses = []
    usernotif = []

    # PROTEKSI FIELD KOSONG
    for index,row in df.iterrows():
        no = getString(row['No ID'])
        nama = getString(row['Nama'])
        tahunMasuk = getString(row['Tahun Masuk'])
        kelas = getString(row['Kelas'])
        kelasDetail = getString(row['Kelas Detail'])
        tahun = getString(row['Tahun'])
        semester = getString(row['Semester'])
        programStudi = getString(row['Program Studi'])
        jurusan = getString(row['Jurusan'])
        alamat = getString(row['Alamat'])
        tempatLahir = getString(row['Tempat Lahir'])
        tanggalLahir = getString(row['Tanggal Lahir'])
        jenisKelamin = getString(row['Jenis Kelamin'])
        if None in [no, nama, tahunMasuk, kelas, kelasDetail, tahun, semester, programStudi, jurusan, alamat, tempatLahir, tanggalLahir, jenisKelamin]:
            raise HTTPException(status_code=404, detail="No ID, Nama, Tahun Masuk, Kelas, Kelas Detail, Tahun, Semester, Program Studi, Jurusan, Alamat, Tempat Lahir, Tanggal Lahir, dan Jenis Kelamin harus diisi")

    # PROTEKSI DUPLICATE ID
    listpop = []
    pop = df[df.duplicated(["No ID"], keep=False)]
    if pop.empty:
        pass
    else:
        print("ada data duplicate")
        for index, row in pop.sort_values(by=["No ID"]).iterrows():
            rowNonvalid += 1
            listpop.append(index)
            unValid.append({
                "name": row["Nama"],
                "noId": row["No ID"],
                "thnMasuk": row["Tahun Masuk"],
                "kelasNow": row["Kelas"],
                "kelasDetailNow": row["Kelas Detail"],
                "note": "Nomor Identitas terdata lebih dari 1"
            })
        df = df.drop(listpop)

    dataIn = PelajarCredential()
    for index,row in df.iterrows():
        try:
            noId = getString(row["No ID"])
        except:
            raise HTTPException(status_code=400, detail="Kolom No ID harus ada")
        # print(row)
        data_pelajar = await dbase.find_one(
            {
                "companyId": ObjectId(current_user.companyId),
                "noId": noId,
            }
        )
        if data_pelajar:
            rowNonvalid = rowNonvalid + 1
            unValid.append({
                "name": row["Nama"],
                "noId": row["No ID"],
                "thnMasuk": row["Tahun Masuk"],
                "kelasNow": row["Kelas"],
                "kelasDetailNow": row["Kelas Detail"],
                "note": "Pelajar dengan Nomor identitas ini sudah terdaftar"
            })
            continue

        pelajar = IsiDefault(dataIn, current_user)
        pelajar.userId = ObjectId()
        pelajar.noId = getString(row['No ID'])

        dCompany = await GetCompanyOr404(str(current_user.companyId))
        #cek apakah company sudah memiliki solusi sesuai dengan tblName
        tblName = "user_edmedia_pelajar"
        arTblName = tblName.split('_')
        if arTblName[1] not in dCompany['solution']:
            raise HTTPException(status_code=400, detail="Company yang dipilih belum memiliki solusi "+arTblName[1])

        if pelajar.tipeNik: pelajar.tipeNik = getString(row['Tipe NIK']).lower()
        if pelajar.nik: pelajar.nik = getString(row['NIK'])
        pelajar.username = dCompany['initial'] + "." + pelajar.noId.upper()
        pelajar.name = getString(row['Nama'].upper())
        pelajar.thnMasuk = getString(str(row['Tahun Masuk']).upper())
        pelajar.kelasNow = getString(str(row['Kelas']).upper())
        pelajar.kelasDetailNow = getString(str(row['Kelas Detail']).upper())
        pelajar.tahunNow = getString(str(row['Tahun']).upper())
        pelajar.semesterNow = getString(str(row['Semester']).upper())
        pelajar.programStudi = getString(str(row['Program Studi']).upper())
        pelajar.jurusan = getString(str(row['Jurusan']).upper())

        address = AddressData()
        address.street = getString(row['Alamat'])
        pelajar.address = address

        identity = IdentityData()
        identity.gender = getJenisKelamin(row['Jenis Kelamin']).lower()
        identity.placeOfBirth = getString(row['Tempat Lahir'].upper())
        dateOfBirth = getString(row['Tanggal Lahir']).strip()
        if dateOfBirth:
            try:
                if len(dateOfBirth) > 4:
                    arrTglLahir = dateOfBirth.lower().split(" ")
                    if len(arrTglLahir) > 2:
                        bulan = bulanToNumber(arrTglLahir[1])
                        dateOfBirth = (
                            arrTglLahir[2] + "-" + bulan + "-" + arrTglLahir[0].zfill(2)
                        )
                        identity.dateOfBirth = datetime.strptime(dateOfBirth, "%Y-%m-%d")
                    else:
                        print(dateOfBirth)
                        identity.dateOfBirth = getDate(dateOfBirth)
                else:
                    print(dateOfBirth)
                    identity.dateOfBirth = getDate(dateOfBirth)
            except:
                raise HTTPException(
                    status_code=400,
                    detail="Tanggal lahir " + dateOfBirth + " tidak VALID",
                )
        
        pelajar.identity = identity
                
        if pelajar.identity.dateOfBirth:
            password = convertDateToStrPassword(pelajar.identity.dateOfBirth)
            # format password DDMMYY
        else:
            password = pelajar.noId

        pelajar.tags = getString(row['Tags'])
        if pelajar.tags:
            pelajar.tags = getString(row['Tags']).split(",")
            pelajar.tags = ListToUp(pelajar.tags)
        else:
            pelajar.tags = []

        #CREDENTIAL
        credential = CredentialData()
        credential.tblName = 'user_edmedia_pelajar'
        credential.role = ['pelajar']
        credential.password = PWDCONTEXT.encrypt(password)
        credential.wrongPasswordCount = 0
        credential.loginCount = 0
        credential.isFirstLogin = True
        pelajar.credential = credential

        # insert to user_edmedia_pelajar
        rowValid = rowValid + 1
        result.append(pelajar.dict())

        # insert to userdatabase
        uData = UserDataBase()
        uData.companyId = current_user.companyId
        uData.userId = pelajar.userId
        uData.noId = pelajar.noId
        uData.name = pelajar.name
        uData.email = None
        userdata.append(uData.dict())

        # insert to userAksesdata
        uAkses = UserAksesBase()
        uAkses.companyId = current_user.companyId
        uAkses.userId = pelajar.userId
        uAkses.noId = pelajar.noId
        userakses.append(uAkses.dict())

        # #insert to mysql
        # reqUser = {
        #     "companyId" : str(user.companyId),
        #     "userId" : str(user.companyId),
        #     "name" : user.name
        #     }
        # url = 'http://localhost:8000/api_sql/user/cek_all_user_data'
        # # x = requests.post(url, json = reqUser)
        # # # print(x.text)    

        # kirim email
        # if ValidateEmail(user.email):
        #     print("kirim email")
        #     email = EmailData()
        #     email.projectName = dCompany["name"]
        #     email.name = user.name
        #     email.emailTo = user.email
        #     email.token = password
        #     # SendNewAccountEmail(EmailData)

        notif = NotifData()
        notif.title = "AKTIVASI AKUN"
        notif.name = pelajar.name.upper()
        notif.role = credential.role
        notif.email = None
        notif.message = (
            "Selamat bergabung di "
            + dCompany["name"]
            + ", Username : "
            + pelajar.username
            + " atau Email atau Nohp, Password : "
            + password
        )
        notif.isSecret = True
        usernotif.append({
            "False": False,
            "userId": pelajar.userId,
            "companyId": current_user.companyId,
            "notif": notif
        })

    #aneh
    print(result)
    print(userdata)
    print(userakses)
    print(usernotif)
    dbase.insert_many(result)
    MGDB['user_data'].insert_many(userdata)
    MGDB['user_akses'].insert_many(userakses)
    for x in usernotif:
        await CreateNotif(x["False"], x["userId"], x["companyId"], x["notif"])
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


@router_pelajar.post("/get_pelajar", response_model=dict)
async def get_all_pelajars(size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = PelajarPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_pelajar.get("/pelajar/{id_}", response_model=PelajarOnDB)
async def get_pelajar_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pelajar = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if pelajar:
        return pelajar
    else:
        raise HTTPException(status_code=404, detail="Pelajar not found")


@router_pelajar.get("/pelajar/user/{userId}", response_model=PelajarOnDB)
async def get_pelajar_by_user_id(userId: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pelajar = await dbase.find_one({"userId": userId,"companyId": ObjectId(current_user.companyId)})
    if pelajar:
        return pelajar
    else:
        raise HTTPException(status_code=404, detail="Pelajar not found")


@router_pelajar.delete("/pelajar/{id_}", dependencies=[Depends(GetPelajarOr404)], response_model=dict)
async def delete_pelajar_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pelajar_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if pelajar_op.deleted_count:
        return {"status": f"deleted count: {pelajar_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_pelajar.put("/pelajar/{id_}", response_model=PelajarOnDB)
async def update_pelajar(id_: str, dataIn: PelajarCredential, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # pelajar = IsiDefault(dataIn, current_user, True)
    dataIn.updateTime = dateTimeNow()
    identity = IdentityData()
    if dataIn.identity.dateOfBirth:
        identity.dateOfBirth = convertStrDateToDate(dataIn.identity.dateOfBirth)
    dataIn.identity.dateOfBirth = identity.dateOfBirth

    if ValidateEmail(dataIn.email):
        dataIn.email = dataIn.email.lower()
        user_email = await dbase.find_one(
            {"email": dataIn.email, "companyId": ObjectId(current_user.companyId)}
        )
        if user_email:
            raise HTTPException(
                status_code=400,
                detail="Email sudah terdaftar, gunakan email yang lain atau silahkan login",
            )
    else:
        dataIn.email = None

    if ValidPhoneNumber(dataIn.phone):
        user_phone = await dbase.find_one(
            {"email": dataIn.email, "companyId": ObjectId(current_user.companyId)}
        )
        if user_phone:
            raise HTTPException(
                status_code=400,
                detail="NoHP sudah terdaftar, gunakan nohp yang lain atau silahkan login",
            )
    else:
        dataIn.phone = None

    pelajar_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": dataIn.dict(skip_defaults=True)}
    )
    if pelajar_op.modified_count or pelajar_op.matched_count:
        return await GetPelajarOr404(id_)
    else:
        raise HTTPException(status_code=304)