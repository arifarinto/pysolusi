# backend/tancho/silabuss/routes.py

from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.edmedia.silabus import SilabusBase, SilabusOnDB, SilabusPage, comboSilabus
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_silabus = APIRouter()
dbase = MGDB.edmedia_silabus

async def GetSilabusOr404(id_: str):
    _id = ValidateObjectId(id_)
    silabus = await dbase.find_one({"_id": _id})
    if silabus:
        return silabus
    else:
        raise HTTPException(status_code=404, detail="Silabus not found")

# =================================================================================

@router_silabus.post("/silabus", response_model=SilabusOnDB)
async def add_silabus(data_in: SilabusBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    silabus = IsiDefault(data_in, current_user)
    silabus.programStudi = silabus.programStudi.upper()
    csilabus = await dbase.find_one({
        "companyId": ObjectId(current_user.companyId), 
        "namaMapel": silabus.namaMapel,
        "programStudi": silabus.programStudi,
        "jurusan": silabus.jurusan,
        "kelas": silabus.kelas,
        "semester": silabus.semester,
        "tahun": silabus.tahun
        })
    if csilabus:
        raise HTTPException(status_code=400, detail="Silabus is registered")
    silabus.jurusan = silabus.jurusan.upper()
    silabus.kelas = silabus.kelas.upper()
    silabus.namaMapel = silabus.namaMapel.upper()
    silabus.tags = ListToUp(silabus.tags)
    silabus_op = await dbase.insert_one(silabus.dict())
    if silabus_op.inserted_id:
        silabus = await GetSilabusOr404(silabus_op.inserted_id)
        return silabus

@router_silabus.post("/upload_silabus")
async def upload_silabus(file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    data_in = SilabusBase()
    for index,row in df.iterrows():
        silabus = IsiDefault(data_in, current_user)
        silabus.kelas = getString(row['Kelas'])
        silabus.namaMapel = getString(row['Mata Pelajaran'])
        silabus.programStudi = getString(row['Jenjang'])
        silabus.jurusan = getString(row['Jurusan'])
        silabus.semester = getNumber(row['Semester'])
        silabus.tahun = getNumber(row['Tahun'])
        silabus.jumlahPertemuan = getNumber(row['Jumlah Pertemuan'])
        silabus.pengajar = getString(row['NIP'])
        if None in [silabus.kelas, silabus.namaMapel, silabus.programStudi, silabus.jurusan, silabus.semester, silabus.jumlahPertemuan, silabus.pengajar]:
            raise HTTPException(status_code=404, detail="Kelas, Mata Pelajaran, Jenjang, Jurusan, Semester, Tahun, Jumlah Pertemuan, dan NIP harus diisi")
        else:
            silabus.kelas = getString(str(row['Kelas']).upper())
            silabus.namaMapel = getString(row['Mata Pelajaran'].upper())
            silabus.programStudi = getString(row['Jenjang'].upper())
            silabus.jurusan = getString(row['Jurusan'].upper())
            silabus.semester = int(getNumber(row['Semester']))
            silabus.tahun = str(getString(row['Tahun']))
            silabus.jumlahPertemuan = int(getNumber(row['Jumlah Pertemuan']))
            silabus.bobotSks = getNumber(row['Bobot Sks'])
            if silabus.bobotSks:
                silabus.bobotSks = int(getNumber(row['Bobot Sks']))
            else:
                silabus.bobotSks = 0
            silabus.tags = getString(row['Tags'])
            if silabus.tags:
                silabus.tags = getString(row['Tags']).split(",")
                silabus.tags = ListToUp(silabus.tags)
            else:
                silabus.tags = []
            silabus.pengajar = getString(row['NIP'])
            if silabus.pengajar:
                silabus.pengajar = getString(row['NIP']).split(",")
                silabus.pengajar = ListToUp(silabus.pengajar)
                # idPengajar = []
                idPengajarValid = []
                idPengajarNonValid = []
                for noId in silabus.pengajar:
                    cPengajar = await MGDB.user_edmedia_pengajar.find_one({
                        "companyId": ObjectId(current_user.companyId), 
                        "noId": noId
                        })
                    if cPengajar:
                        idPengajarValid.append(str(cPengajar['_id']))
                    else:
                        idPengajarNonValid.append(noId)

            silabus.pengajar = idPengajarValid

            csilabus = await dbase.find_one({
                "companyId": ObjectId(current_user.companyId), 
                "namaMapel": silabus.namaMapel,
                "programStudi": silabus.programStudi,
                "jurusan": silabus.jurusan,
                "kelas": silabus.kelas,
                "semester": silabus.semester,
                "tahun": silabus.tahun
                })
            if idPengajarNonValid:
                rowNonvalid = rowNonvalid + 1
                if len(idPengajarNonValid) == 1:
                    unValid.append({
                        "kelas":silabus.kelas,
                        "namaMapel":silabus.namaMapel,
                        "note": f"NIP {idPengajarNonValid[0]} tidak terdaftar di Data Pengajar"
                        })
                elif len(idPengajarNonValid) > 1:
                    idNV = ",".join(idPengajarNonValid)
                    unValid.append({
                        "kelas":silabus.kelas,
                        "namaMapel":silabus.namaMapel,
                        "note": f"NIP {idNV} tidak terdaftar di Data Pengajar"
                        })                        
            elif csilabus:
                rowNonvalid = rowNonvalid + 1
                unValid.append({
                    "kelas":silabus.kelas,
                    "namaMapel":silabus.namaMapel,
                    "note":"telah terdaftar"
                    })
                pass
            else:
                rowValid = rowValid + 1
                result.append(silabus.dict())
    #aneh
    print(result)
    dbase.insert_many(result)
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


@router_silabus.post("/get_silabus", response_model=dict)
async def get_all_silabuss(size: int = 10, page: int = 0, sort: str = "kelas", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort([(sort, dir), ("namaMapel", dir), ("programStudi", dir), ("jurusan", dir), ("semester", dir)])
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = SilabusPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_silabus.get("/silabus/{id_}", response_model=SilabusOnDB)
async def get_silabus_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    silabus = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if silabus:
        return silabus
    else:
        raise HTTPException(status_code=404, detail="Silabus not found")


@router_silabus.delete("/silabus/{id_}", dependencies=[Depends(GetSilabusOr404)], response_model=dict)
async def delete_silabus_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    silabus_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if silabus_op.deleted_count:
        return {"status": f"deleted count: {silabus_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_silabus.put("/silabus/{id_}", response_model=SilabusOnDB)
async def update_silabus(id_: str, data_in: SilabusBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # silabus = IsiDefault(data_in, current_user, True)
    silabus = data_in
    silabus.updateTime = dateTimeNow()
    silabus_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": silabus.dict(skip_defaults=True)}
    )
    if silabus_op.modified_count or silabus_op.matched_count:
        return await GetSilabusOr404(id_)
    else:
        raise HTTPException(status_code=304)