# backend/tancho/pengajars/routes.py

from function.user import CreateUser
from util.util_excel import getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from function.company import GetCompanyOr404
from function.notif import CreateNotif
from model.edmedia.pengajar import PengajarBase, PengajarCredential, PengajarOnDB, PengajarPage, comboPengajar
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import AddressData, IdentityData, JwtToken, UserInput, CredentialData, UserDataBase, UserAksesBase, NotifData
from util.util import ValidateObjectId, IsiDefault, CreateCriteria, ListToUp, PWDCONTEXT
from route.auth import get_current_user
from util.util_waktu import convertDateToStrPassword, convertStrDateToDate, dateTimeNow, bulanToNumber
from util.util_excel import getString, getDate, getJenisKelamin

router_pengajar = APIRouter()
dbase = MGDB.user_edmedia_pengajar

async def GetPengajarOr404(id_: str):
    _id = ValidateObjectId(id_)
    pengajar = await dbase.find_one({"_id": _id})
    if pengajar:
        return pengajar
    else:
        raise HTTPException(status_code=404, detail="Pengajar not found")

# =================================================================================


@router_pengajar.post("/pengajar", response_model=PengajarBase)
async def add_pengajar(dataIn: UserInput, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengajar = PengajarCredential(**dataIn.dict())
    pengajar = IsiDefault(pengajar, current_user)
    pengajar.userId = ObjectId()

    cpengajar = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "noId": pengajar.noId})
    if cpengajar:
        raise HTTPException(status_code=400, detail="Pengajar with NIP : "+pengajar.noId+" is registered")
    
    pengajar.name = pengajar.name.upper()
    pengajar.tags = ListToUp(pengajar.tags)
    pengajar.username = pengajar.noId

    identity = IdentityData()
    if pengajar.identity.dateOfBirth:
        identity.dateOfBirth = pengajar.identity.dateOfBirth
        password = convertDateToStrPassword(pengajar.identity.dateOfBirth)
    else:
        password = pengajar.noId

    pengajar.identity = identity
    pengajar_op = await CreateUser("user_edmedia_pengajar", pengajar, password, True, ['pengajar'])
    return pengajar_op

#TODO (sedang koreksi library yg salah) coding masih salah
@router_pengajar.post("/upload_pengajar")
async def upload_pengajar(file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file, converters={"No ID": str, "NIK": str})
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    userdata = []
    userakses = []
    usernotif = []

    # PROTEKSI FIELD KOSONG
    for index,row in df.iterrows():
        no = getString(row['No ID'])
        nama = getString(row['Nama'])
        alamat = getString(row['Alamat'])
        tempatLahir = getString(row['Tempat Lahir'])
        tanggalLahir = getString(row['Tanggal Lahir'])
        jenisKelamin = getString(row['Jenis Kelamin'])
        if None in [no, nama, alamat, tempatLahir, tanggalLahir, jenisKelamin]:
            raise HTTPException(status_code=404, detail="No ID, Nama, Alamat, Tempat Lahir, Tanggal Lahir, dan Jenis Kelamin harus diisi")

    # PROTEKSI DUPLICATE ID
    listpop = []
    pop = df[df.duplicated(["No ID"], keep=False)]
    if pop.empty:
        pass
    else:
        print("ada data duplicate")
        for index, row in pop.sort_values(by=["No ID"]).iterrows():
            rowNonvalid += 1
            listpop.append(index)
            unValid.append({
                "name": row["Nama"],
                "noId": row["No ID"],
                "note": "Nomor Identitas terdata lebih dari 1"
            })
        df = df.drop(listpop)

    dataIn = PengajarCredential()
    for index,row in df.iterrows():
        try:
            noId = getString(row["No ID"])
        except:
            raise HTTPException(status_code=400, detail="Kolom No ID harus ada")
        # print(row)
        data_pengajar = await dbase.find_one(
            {
                "companyId": ObjectId(current_user.companyId),
                "noId": noId,
            }
        )
        if data_pengajar:
            rowNonvalid = rowNonvalid + 1
            unValid.append({
                "name": row["Nama"],
                "noId": row["No ID"],
                "note": "Pengajar dengan Nomor identitas ini sudah terdaftar"
            })
            continue

        pengajar = IsiDefault(dataIn, current_user)
        pengajar.userId = ObjectId()
        pengajar.noId = getString(row['No ID'])

        dCompany = await GetCompanyOr404(str(current_user.companyId))
        #cek apakah company sudah memiliki solusi sesuai dengan tblName
        tblName = "user_edmedia_pengajar"
        arTblName = tblName.split('_')
        if arTblName[1] not in dCompany['solution']:
            raise HTTPException(status_code=400, detail="Company yang dipilih belum memiliki solusi "+arTblName[1])

        if pengajar.tipeNik: pengajar.tipeNik = getString(row['Tipe NIK']).lower()
        if pengajar.nik: pengajar.nik = getString(row['NIK'])
        pengajar.username = dCompany['initial'] + "." + pengajar.noId.upper()
        pengajar.name = getString(row['Nama'].upper())

        address = AddressData()
        address.street = getString(row['Alamat'])
        pengajar.address = address

        identity = IdentityData()
        identity.gender = getJenisKelamin(row['Jenis Kelamin']).lower()
        identity.placeOfBirth = getString(row['Tempat Lahir'].upper())
        dateOfBirth = getString(row['Tanggal Lahir']).strip()
        if dateOfBirth:
            try:
                if len(dateOfBirth) > 4:
                    arrTglLahir = dateOfBirth.lower().split(" ")
                    if len(arrTglLahir) > 2:
                        bulan = bulanToNumber(arrTglLahir[1])
                        dateOfBirth = (
                            arrTglLahir[2] + "-" + bulan + "-" + arrTglLahir[0].zfill(2)
                        )
                        identity.dateOfBirth = datetime.strptime(dateOfBirth, "%Y-%m-%d")
                    else:
                        print(dateOfBirth)
                        identity.dateOfBirth = getDate(dateOfBirth)
                else:
                    print(dateOfBirth)
                    identity.dateOfBirth = getDate(dateOfBirth)
            except:
                raise HTTPException(
                    status_code=400,
                    detail="Tanggal lahir " + dateOfBirth + " tidak VALID",
                )
        
        pengajar.identity = identity
                
        if pengajar.identity.dateOfBirth:
            password = convertDateToStrPassword(pengajar.identity.dateOfBirth)
            # format password DDMMYY
        else:
            password = pengajar.noId

        pengajar.tags = getString(row['Tags'])
        if pengajar.tags:
            pengajar.tags = getString(row['Tags']).split(",")
            pengajar.tags = ListToUp(pengajar.tags)
        else:
            pengajar.tags = []

        #CREDENTIAL
        credential = CredentialData()
        credential.tblName = 'user_edmedia_pengajar'
        credential.role = ['pengajar']
        credential.password = PWDCONTEXT.encrypt(password)
        credential.wrongPasswordCount = 0
        credential.loginCount = 0
        credential.isFirstLogin = True
        pengajar.credential = credential

        # insert to user_edmedia_pengajar
        rowValid = rowValid + 1
        result.append(pengajar.dict())

        # insert to userdatabase
        uData = UserDataBase()
        uData.companyId = current_user.companyId
        uData.userId = pengajar.userId
        uData.noId = pengajar.noId
        uData.name = pengajar.name
        uData.email = None
        userdata.append(uData.dict())

        # insert to userAksesdata
        uAkses = UserAksesBase()
        uAkses.companyId = current_user.companyId
        uAkses.userId = pengajar.userId
        uAkses.noId = pengajar.noId
        userakses.append(uAkses.dict())

        # #insert to mysql
        # reqUser = {
        #     "companyId" : str(user.companyId),
        #     "userId" : str(user.companyId),
        #     "name" : user.name
        #     }
        # url = 'http://localhost:8000/api_sql/user/cek_all_user_data'
        # # x = requests.post(url, json = reqUser)
        # # # print(x.text)   

        # kirim email
        # if ValidateEmail(user.email):
        #     print("kirim email")
        #     email = EmailData()
        #     email.projectName = dCompany["name"]
        #     email.name = user.name
        #     email.emailTo = user.email
        #     email.token = password
        #     # SendNewAccountEmail(EmailData)

        notif = NotifData()
        notif.title = "AKTIVASI AKUN"
        notif.name = pengajar.name.upper()
        notif.role = credential.role
        notif.email = None
        notif.message = (
            "Selamat bergabung di "
            + dCompany["name"]
            + ", Username : "
            + pengajar.username
            + " atau Email atau Nohp, Password : "
            + password
        )
        notif.isSecret = True
        usernotif.append({
            "False": False,
            "userId": pengajar.userId,
            "companyId": current_user.companyId,
            "notif": notif
        })

    #aneh
    print(result)
    print(userdata)
    print(userakses)
    print(usernotif)
    dbase.insert_many(result)
    MGDB['user_data'].insert_many(userdata)
    MGDB['user_akses'].insert_many(userakses)
    for x in usernotif:
        await CreateNotif(x["False"], x["userId"], x["companyId"], x["notif"])
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


@router_pengajar.post("/get_pengajar", response_model=dict)
async def get_all_pengajars(size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = PengajarPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_pengajar.get("/pengajar/{id_}", response_model=PengajarBase)
async def get_pengajar_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengajar = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if pengajar:
        return pengajar
    else:
        raise HTTPException(status_code=404, detail="Pengajar not found")


@router_pengajar.get("/pengajar/user/{id_}", response_model=PengajarOnDB)
async def get_pengajar_by_user_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengajar = await dbase.find_one({"userId": id_,"companyId": ObjectId(current_user.companyId)})
    if pengajar:
        return pengajar
    else:
        raise HTTPException(status_code=404, detail="Pengajar not found")


@router_pengajar.delete("/pengajar/{id_}", dependencies=[Depends(GetPengajarOr404)], response_model=dict)
async def delete_pengajar_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pengajar_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if pengajar_op.deleted_count:
        return {"status": f"deleted count: {pengajar_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_pengajar.put("/pengajar/{id_}", response_model=PengajarOnDB)
async def update_pengajar(id_: str, dataIn: PengajarCredential, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # pengajar = IsiDefault(dataIn, current_user, True)
    dataIn.updateTime = dateTimeNow()
    identity = IdentityData()
    if dataIn.identity.dateOfBirth:
        identity.dateOfBirth = convertStrDateToDate(dataIn.identity.dateOfBirth)
    dataIn.identity.dateOfBirth = identity.dateOfBirth
    pengajar_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": dataIn.dict(skip_defaults=True)}
    )
    if pengajar_op.modified_count or pengajar_op.matched_count:
        return await GetPengajarOr404(id_)
    else:
        raise HTTPException(status_code=304)