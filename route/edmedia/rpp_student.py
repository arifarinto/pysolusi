# backend/tancho/kbms/routes.py 

from os import PRIO_PGRP
from bson.objectid import ObjectId
from pydantic.utils import Obj
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime, timedelta
import logging
import math
import random

from model.edmedia.kbm import GenerateKbm, KbmBase, KbmOnDB, KbmPage, RppBase, RppsBase, RppOnDB, RppPage, UserBasic, ListRppPage, PesertaPage
from model.edmedia.pelajar import PelajarKbm, PelajarExcul, PelajarNilai, PelajarKesehatan, PelajarKepribadian, PelajarBase, PelajarOnDB, PelajarDatas, PelajarHistory, PelajarMapel, MapelPage, HistoryPage
from model.edmedia.materi import CommentBase, CommentReply, SoalDijawab, TugasDijawab, SoalPage, comboMateri
from model.util import SearchRequest, FieldObjectIdRequest, FieldRequest
from model.default import JwtToken
from util.util import RandomString, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_rpp_student = APIRouter()
dbase = MGDB.edmedia_kbm

async def GetRppOr404(id_: str):
    _id = ValidateObjectId(id_)
    rpp = await dbase.find_one({"_id": _id})
    if rpp:
        return rpp
    else:
        raise HTTPException(status_code=404, detail="Rpp not found")


# =================================================================================
#student

@router_rpp_student.post("/get_rpp/{userId}", response_model=dict)
async def get_rpp_by_user_id_pelajar(userId: str, size: int = 10, page: int = 0, sort: str = "rpp.jamMulai", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pelajar","*"])):
    cPelajar = await MGDB.user_edmedia_pelajar.find_one({
        "companyId": ObjectId(current_user.companyId),
        "userId": ObjectId(userId)})
    data = cPelajar
    
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    search.defaultObjectId.append(FieldObjectIdRequest(field='pelajar.userId',key=ObjectId(userId)))
    criteria = CreateCriteria(search)
    criteria['$and'].append({'kelas': data['kelasNow']})
    criteria['$and'].append({'kelasDetail': data['kelasDetailNow']})
    print(criteria)
    
    pipeline = [
        {'$match': criteria},
        {'$unwind': {'path': '$rpp'}},
        {'$match': {'rpp.jamMulai': {'$lte': datetime.now() + timedelta(hours=2)}}},
        {'$sort': {'rpp.jamMulai': -1}}        
    ]
    
    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listrpp = cdatas
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = RppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_student.post("/get_rpp/tanggal/{userId}/{tanggal}", response_model=dict)
async def get_rpp_by_user_id_pelajar_by_tanggal(userId: str, tanggal: str, size: int = 10, page: int = 0, sort: str = "rpp.jamMulai", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pelajar","*"])):
    cPelajar = await MGDB.user_edmedia_pelajar.find_one({
        "companyId": ObjectId(current_user.companyId),
        "userId": ObjectId(userId)})
    data = cPelajar
    tgl = datetime.strptime(tanggal, '%Y-%m-%d')
    tglAwal = tgl - timedelta(days=1)
    tglAkhir = tgl + timedelta(days=1)

    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    search.defaultObjectId.append(FieldObjectIdRequest(field='pelajar.userId',key=ObjectId(userId)))
    criteria = CreateCriteria(search)
    criteria['$and'].append({'kelas': data['kelasNow']})
    criteria['$and'].append({'kelasDetail': data['kelasDetailNow']})
    print(criteria)
    
    pipeline = [
        {'$match': criteria},
        {'$unwind': {'path': '$rpp'}},
        {'$match': {'rpp.jamMulai': {
                '$gte': tgl, 
                '$lt': tglAkhir
            }}},
        {'$sort': {'rpp.jamMulai': -1}}        
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listrpp = cdatas
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = RppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_student.post("/get_rpp/silabus/{userId}/{idSilabus}", response_model=dict)
async def get_rpp_by_user_id_pelajar_by_id_silabus(userId: str, idSilabus: str, size: int = 10, page: int = 0, sort: str = "rpp.jamMulai", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pelajar","*"])):
    cPelajar = await MGDB.user_edmedia_pelajar.find_one({
        "companyId": ObjectId(current_user.companyId),
        "userId": ObjectId(userId)})
    data = cPelajar

    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    search.defaultObjectId.append(FieldObjectIdRequest(field='pelajar.userId',key=ObjectId(userId)))
    criteria = CreateCriteria(search)
    criteria['$and'].append({'kelas': data['kelasNow']})
    criteria['$and'].append({'kelasDetail': data['kelasDetailNow']})
    criteria['$and'].append({'idSilabus': ObjectId(idSilabus)})
    print(criteria)
    
    pipeline = [
        {'$match': criteria},
        {'$unwind': {'path': '$rpp'}},
        {'$sort': {'rpp.jamMulai': -1}}        
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listrpp = cdatas
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = RppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_student.get("/jadwal/pelajar/{userId}")
async def get_jadwal_by_id_pelajar(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    pelajar = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)})
    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId), 
            'programStudi': pelajar['programStudi'], 
            'jurusan': pelajar['jurusan'], 
            'kelas': pelajar['kelasNow'], 
            'kelasDetail': pelajar['kelasDetailNow'], 
            'tahun': pelajar['tahunNow'], 
            'semester': int(pelajar['semesterNow']), 
            'pelajar.userId': ObjectId(userId)
        }
    }, {
        '$unwind': {
            'path': '$rpp'
        }
    }, {
        '$addFields': {
            'jamMulai': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.jamMulai', None
                        ]
                    }, '00:00', {
                        '$dateToString': {
                            'format': '%H:%M', 
                            'date': '$rpp.jamMulai'
                        }
                    }
                ]
            }, 
            'jamSelesai': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.jamSelesai', None
                        ]
                    }, '00:00', {
                        '$dateToString': {
                            'format': '%H:%M', 
                            'date': '$rpp.jamSelesai'
                        }
                    }
                ]
            }, 
            'hari': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.jamSelesai', None
                        ]
                    }, '0', {
                        '$toString': {
                            '$dayOfWeek': '$rpp.jamMulai'
                        }
                    }
                ]
            }, 
            'namaPengajar': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.pengajar', None
                        ]
                    }, '-', '$rpp.pengajar.nama'
                ]
            }, 
            'namaRuang': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.namaRuang', None
                        ]
                    }, '-', '$rpp.namaRuang'
                ]
            }
        }
    }, {
        '$project': {
            '_id': 0, 
            'hari': '$hari', 
            'jamMulai': '$jamMulai', 
            'jamSelesai': '$jamSelesai', 
            'mapel': '$namaMapel', 
            'namaPengajar': '$namaPengajar', 
            'ruang': '$namaRuang'
        }
    }, {
        '$group': {
            '_id': '$hari', 
            'detail': {
                '$addToSet': {
                    'jamMulai': '$jamMulai', 
                    'jamSelesai': '$jamSelesai', 
                    'mapel': '$mapel', 
                    'namaPengajar': '$namaPengajar', 
                    'ruang': '$ruang'
                }
            }
        }
    }, {
        '$sort': {
            '_id': 1
        }
    }
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    jadwal = []
    for x in cdatas:
        hari = {'0': 'NONE', '1': 'MINGGU', '2': 'SENIN', '3': 'SELASA', '4': 'RABU', '5': 'KAMIS', '6': 'JUMAT', '7': 'SABTU'}
        if x['_id']:
            x['_id'] = hari[x['_id']]
            # x['jadwal'].sort()
            # jadwal.append(x)
            cdatas = x['detail']
            cdatas.sort(key=lambda x: x.get('jamMulai'), reverse=False)
            jadwal.append(x)
    print(cdatas)
    return jadwal


# =================================================================================
#latihan soal

@router_rpp_student.post("/latihan/{userId}/{idKbm}/{pertemuanKe}", response_model=List[SoalDijawab])
async def add_latihan_soal_by_user_id_pelajar(userId: str, idKbm: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    pelajar = IsiDefault(PelajarBase(), current_user)
    pelajar.updateTime = dateTimeNow()
    # DATA RPP
    data = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    datas = data['rpp'][0]

    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']

    # DATA SISWA
    csiswa = await MGDB.edmedia_kbm.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)})

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_datetime = now.strftime("%Y-%m-%d %H:%M:%S")

    # mulai = str(datas['tgl'].date())+" "+datas['jamMulai']
    # selesai = str(datas['tgl'].date())+" "+datas['jamSelesai']
    mulai = str(datas['jamMulai'])
    selesai = str(datas['jamSelesai'])

    if current_datetime < mulai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif current_datetime > selesai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    else:
        jmlmateri = len(datas['idMateri'])
        # id_materi = [str(i['_id']) for i in datas['idMateri']]
        jmlsoal = datas['jumlahSoal']
        jmlsoalpermateri = []
        if jmlsoal == None:
            raise HTTPException(status_code=400, detail="Tidak ada soal di pertemuan ini")
        elif jmlsoal == 0:
            raise HTTPException(status_code=400, detail="Tidak ada soal di pertemuan ini")
        else:
            # GENERATE SOAL
            datalatihan = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
            if datalatihan == {}: # jika tidak ada silabus
                print(datalatihan)       
                while len(jmlsoalpermateri) < jmlmateri:
                    if jmlmateri - len(jmlsoalpermateri) == 1:
                        soal = jmlsoal - sum(jmlsoalpermateri)
                        jmlsoalpermateri.append(soal)
                    else:
                        soal = math.floor(jmlsoal/jmlmateri)
                        jmlsoalpermateri.append(soal)
                
                soals = []
                # materisoal = dict(zip(id_materi, jmlsoalpermateri))
                materisoal = dict(zip(datas['idMateri'], jmlsoalpermateri))
                for x,y in materisoal.items():
                    csoal = await MGDB.edmedia_materi.find_one({'_id': ObjectId(x)}, {'soal': 1, '_id': 0})
                    listsoal = csoal['soal']

                    # ADD EXCEPTION UNTUK SOAL YG SUDAH DIGENERATE ---> BELUM
                    # ceksoal = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
                    # listid = [ ceksoal['dataKbm'][0]['soal'][ceksoal['dataKbm'][0]['soal'].index(x)]['id'] for x in ceksoal['dataKbm'][0]['soal'] ]
                    # print(listid)

                    rndm = random.sample(range(0, len(listsoal)), y)
                    for i in range(y):
                        soals.append(listsoal[rndm[i]])
                        
                random.shuffle(soals)
                for x in soals:
                    x['jawaban'] = None
                    x['isBenar'] = False
                    x['noSoal'] = soals.index(x)+1
                
                pelajar.dataKbm = []
                datasoal = PelajarKbm()
                datasoal.idSilabus = silabus
                datasoal.pertemuanKe = pertemuanKe
                datasoal.waktu = dateTimeNow()
                datasoal.tahun = csiswa['tahun']
                datasoal.semester = str(csiswa['semester'])
                datasoal.kelas = csiswa['kelas']
                datasoal.kelasDetail = csiswa['kelasDetail']
                datasoal.isHadir = True
                datasoal.tugas = []
                datasoal.nilaiTugas = 0
                datasoal.tugasDone = False
                datasoal.soal = soals
                datasoal.nilaiSoal = 0
                datasoal.soalDone = False
                pelajar.dataKbm.append(datasoal.dict(skip_defaults=True))
            
                pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
                    {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
                    {"$push": {"dataKbm": pelajar.dict(skip_defaults=True)['dataKbm'][0]}}
                )

                csoal = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
                if not csoal == {}:
                    for x in csoal['dataKbm'][0]['soal']:
                        x['kunci'] = None
                        x['isBenar'] = False
                    return csoal['dataKbm'][0]['soal']

            else: # jika ada silabus
                if not datalatihan['dataKbm'][0]['soal']:
                    print("ada silabus, tidak ada soal")
                    while len(jmlsoalpermateri) < jmlmateri:
                        if jmlmateri - len(jmlsoalpermateri) == 1:
                            soal = jmlsoal - sum(jmlsoalpermateri)
                            jmlsoalpermateri.append(soal)
                        else:
                            soal = math.floor(jmlsoal/jmlmateri)
                            jmlsoalpermateri.append(soal)
                    
                    soals = []
                    # materisoal = dict(zip(id_materi, jmlsoalpermateri))
                    materisoal = dict(zip(datas['idMateri'], jmlsoalpermateri))
                    for x,y in materisoal.items():
                        csoal = await MGDB.edmedia_materi.find_one({'_id': ObjectId(x)}, {'soal': 1, '_id': 0})
                        listsoal = csoal['soal']

                        # ADD EXCEPTION UNTUK SOAL YG SUDAH DIGENERATE ---> BELUM
                        # ceksoal = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
                        # listid = [ ceksoal['dataKbm'][0]['soal'][ceksoal['dataKbm'][0]['soal'].index(x)]['id'] for x in ceksoal['dataKbm'][0]['soal'] ]
                        # print(listid)

                        rndm = random.sample(range(0, len(listsoal)), y)
                        for i in range(y):
                            soals.append(listsoal[rndm[i]])
                            
                    random.shuffle(soals)
                    for x in soals:
                        x['jawaban'] = None
                        x['isBenar'] = False
                        x['noSoal'] = soals.index(x)+1
                    
                    pelajar.dataKbm = []
                    datasoal = PelajarKbm()
                    datasoal.idSilabus = silabus
                    datasoal.pertemuanKe = pertemuanKe
                    datasoal.waktu = dateTimeNow()
                    datasoal.tahun = csiswa['tahun']
                    datasoal.semester = str(csiswa['semester'])
                    datasoal.kelas = csiswa['kelas']
                    datasoal.kelasDetail = csiswa['kelasDetail']
                    datasoal.isHadir = True
                    datasoal.tugas = []
                    datasoal.nilaiTugas = 0
                    datasoal.tugasDone = False
                    datasoal.soal = soals
                    datasoal.nilaiSoal = 0
                    datasoal.soalDone = False
                    pelajar.dataKbm.append(datasoal.dict(skip_defaults=True))
                
                    # pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
                    #     {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
                    #     {"$push": {"dataKbm": pelajar.dict(skip_defaults=True)['dataKbm'][0]}}
                    # )

                    pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
                        {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
                        {"$set": {
                            "dataKbm.$[pertemuan].soal" : soals}},
                        upsert=False,
                        array_filters=[
                            {"pertemuan.pertemuanKe": pertemuanKe, "pertemuan.idSilabus": silabus}
                        ]
                    )

                    csoal = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
                    if not csoal == {}:
                        for x in csoal['dataKbm'][0]['soal']:
                            x['kunci'] = None
                            x['isBenar'] = False
                        return csoal['dataKbm'][0]['soal']
                    
                else:
                    print("ada silabus, ada soal")
                    csoal = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
                    if not csoal == {}:
                        for x in csoal['dataKbm'][0]['soal']:
                            x['kunci'] = None
                            x['isBenar'] = False
                        return csoal['dataKbm'][0]['soal']


@router_rpp_student.get("/get_latihan/{userId}/{idKbm}/{pertemuanKe}", response_model=List[SoalDijawab])
async def get_all_latihan_soal_by_user_id_pelajar(userId: str, idKbm: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    # DATA RPP
    data = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    datas = data['rpp'][0]

    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_datetime = now.strftime("%Y-%m-%d %H:%M:%S")

    # mulai = str(datas['tgl'].date())+" "+datas['jamMulai']
    # selesai = str(datas['tgl'].date())+" "+datas['jamSelesai']
    mulai = str(datas['jamMulai'])
    selesai = str(datas['jamSelesai'])

    if current_datetime < mulai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif current_datetime > selesai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    else:
        soal = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
        if not soal == {}:
            for x in soal['dataKbm'][0]['soal']:
                x['kunci'] = None
                x['isBenar'] = False
            return soal['dataKbm'][0]['soal']
        else:
            raise HTTPException(status_code=404, detail="latihanSoal not found")


@router_rpp_student.post("/get_latihan/{userId}/{idKbm}/{pertemuanKe}", response_model=dict)
async def get_one_latihan_soal_by_user_id_pelajar(userId: str, idKbm: str, pertemuanKe: int, size: int = 1, page: int = 0, sort: str = "noSoal", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pelajar","*"])):
    # DATA RPP
    data = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    datas = data['rpp'][0]

    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_datetime = now.strftime("%Y-%m-%d %H:%M:%S")

    # mulai = str(datas['tgl'].date())+" "+datas['jamMulai']
    # selesai = str(datas['tgl'].date())+" "+datas['jamSelesai']
    mulai = str(datas['jamMulai'])
    selesai = str(datas['jamSelesai'])

    if current_datetime < mulai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif current_datetime > selesai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    else:
        skip = page * size
        search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
        search.defaultObjectId.append(FieldObjectIdRequest(field='userId',key=ObjectId(userId)))
        criteria = CreateCriteria(search)
        datas_cursor = MGDB.user_edmedia_pelajar.find(criteria, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}}).sort(sort, dir)
        cdatas = await datas_cursor.to_list(length=size)
        listsoal = cdatas[0]['dataKbm'][0]['soal']

        datas = listsoal[skip:skip+1]
        totalElements = await dbase.count_documents(criteria)
        totalPages = math.ceil(totalElements / size)
        reply = SoalPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
        return reply


@router_rpp_student.post("/start_latihan/{userId}/{idKbm}/{pertemuanKe}/{noSoal}", response_model=SoalDijawab)
async def kerjakan_latihan_soal_by_user_id_pelajar_by_pertemuan_per_nomor(userId: str, idKbm: str, pertemuanKe: int, noSoal: int, data_in: SoalDijawab, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    pelajar = IsiDefault(PelajarBase(), current_user)
    pelajar.updateTime = dateTimeNow()
    hasil = data_in
    
    # DATA RPP
    data = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    datas = data['rpp'][0]

    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']

    # DATA PELAJAR
    dpelajar = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
    hasilTes = dpelajar['dataKbm'][0]
    
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_datetime = now.strftime("%Y-%m-%d %H:%M:%S")

    # mulai = str(datas['tgl'].date())+" "+datas['jamMulai']
    # selesai = str(datas['tgl'].date())+" "+datas['jamSelesai']
    mulai = str(datas['jamMulai'])
    selesai = str(datas['jamSelesai'])

    if current_datetime < mulai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif current_datetime > selesai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif hasilTes['soalDone'] == True:
        raise HTTPException(status_code=400, detail="Pengerjaan Soal telah diakhiri")
    else:
        kunci = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
        kuncijwb = kunci['dataKbm'][0]['soal'][noSoal-1]['kunci']
        
        if hasil.jawaban == kuncijwb:
            isBenar = True
        else:
            isBenar = False
        
        pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
            {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
            {"$set": {
                "dataKbm.$[pertemuan].soal.$[soal].jawaban" : hasil.jawaban,
                "dataKbm.$[pertemuan].soal.$[soal].isBenar" : isBenar}},
            upsert=False,
            array_filters=[
                {"pertemuan.pertemuanKe": pertemuanKe, "pertemuan.idSilabus": silabus},
                {"soal.noSoal": noSoal}
            ]
        )

        if pelajar_op.modified_count or pelajar_op.matched_count:
            return hasil.dict()
        else:
            raise HTTPException(status_code=304)


@router_rpp_student.post("/start_latihan/{userId}/{idKbm}/{pertemuanKe}")
async def kerjakan_all_latihan_soal_by_user_id_pelajar(userId: str, idKbm: str, pertemuanKe: int, data_in: List[SoalDijawab], current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    pelajar = IsiDefault(PelajarBase(), current_user)
    pelajar.updateTime = dateTimeNow()
    hasil = data_in
    
    # DATA RPP
    data = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    datas = data['rpp'][0]

    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']

    # DATA PELAJAR
    dpelajar = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
    hasilTes = dpelajar['dataKbm'][0]
    
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_datetime = now.strftime("%Y-%m-%d %H:%M:%S")

    # mulai = str(datas['tgl'].date())+" "+datas['jamMulai']
    # selesai = str(datas['tgl'].date())+" "+datas['jamSelesai']
    mulai = str(datas['jamMulai'])
    selesai = str(datas['jamSelesai'])

    if current_datetime < mulai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif current_datetime > selesai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif hasilTes['soalDone'] == True:
        raise HTTPException(status_code=400, detail="Pengerjaan Soal telah diakhiri")
    else:
        kunci = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
        kuncijwb = kunci['dataKbm'][0]['soal']
        print(kuncijwb)
        
        jmlSoal = 0
        soalBenar = 0
        for jawaban in hasil:
            print(jawaban)
            jmlSoal += 1
            if jawaban.jawaban == kunci['dataKbm'][0]['soal'][hasil.index(jawaban)]['kunci']:
                isBenar = True
                soalBenar += 1
            else:
                isBenar = False

            pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
                {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
                {"$set": {
                    "dataKbm.$[pertemuan].soal.$[soal].jawaban" : jawaban.jawaban,
                    "dataKbm.$[pertemuan].soal.$[soal].isBenar" : isBenar}},
                upsert=False,
                array_filters=[
                    {"pertemuan.pertemuanKe": pertemuanKe, "pertemuan.idSilabus": silabus},
                    {"soal.noSoal": hasil.index(jawaban)+1}
                ]
            )        
            
        nilaiSoal = round(soalBenar / jmlSoal * 100, 2)

        pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
            {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
            {"$set": {
                "dataKbm.$[pertemuan].nilaiSoal" : nilaiSoal,
                "dataKbm.$[pertemuan].soalDone" : True}},
            upsert=False,
            array_filters=[
                {"pertemuan.pertemuanKe": pertemuanKe, "pertemuan.idSilabus": silabus}
            ]
        )

        if pelajar_op.modified_count or pelajar_op.matched_count:
            return {
                'Jumlah Soal': jmlSoal,
                'Jawaban Benar': soalBenar,
                'Nilai': nilaiSoal
            }        
        else:
            raise HTTPException(status_code=304)


@router_rpp_student.post("/stop_latihan/{userId}/{idKbm}/{pertemuanKe}")
async def submit_latihan_soal_by_user_id_pelajar(userId: str, idKbm: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    pelajar = IsiDefault(PelajarBase(), current_user)
    pelajar.updateTime = dateTimeNow()
    # hasil = data_in
    
    # DATA RPP
    data = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    datas = data['rpp'][0]

    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']

    # DATA PELAJAR
    dpelajar = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
    hasilTes = dpelajar['dataKbm'][0]
    
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_datetime = now.strftime("%Y-%m-%d %H:%M:%S")

    # mulai = str(datas['tgl'].date())+" "+datas['jamMulai']
    # selesai = str(datas['tgl'].date())+" "+datas['jamSelesai']
    mulai = str(datas['jamMulai'])
    selesai = str(datas['jamSelesai'])

    if current_datetime < mulai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif current_datetime > selesai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif hasilTes['soalDone'] == True:
        raise HTTPException(status_code=400, detail="Pengerjaan Soal telah diakhiri")
    else:
        kunci = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
        kuncijwb = kunci['dataKbm'][0]['soal']
        print(kuncijwb)
        
        jmlSoal = len(kuncijwb)
        soalBenar = 0
        for jawaban in kuncijwb:
            if jawaban['isBenar'] == True:
                soalBenar += 1
        
        nilaiSoal = round(soalBenar / jmlSoal * 100, 2)

        pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
            {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
            {"$set": {
                "dataKbm.$[pertemuan].nilaiSoal" : nilaiSoal,
                "dataKbm.$[pertemuan].soalDone" : True}},
            upsert=False,
            array_filters=[
                {"pertemuan.pertemuanKe": pertemuanKe, "pertemuan.idSilabus": silabus}
            ]
        )        
            
        if pelajar_op.modified_count or pelajar_op.matched_count:
            return {
                'Jumlah Soal': jmlSoal,
                'Jawaban Benar': soalBenar,
                'Nilai': nilaiSoal
            }        
        else:
            raise HTTPException(status_code=304)


#upload tugas
@router_rpp_student.get("/get_tugas/{userId}/{idKbm}/{pertemuanKe}", response_model=List[TugasDijawab])
async def get_tugas_by_user_id_pelajar(userId: str, idKbm: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    pelajar = IsiDefault(PelajarBase(), current_user)
    pelajar.updateTime = dateTimeNow()
    # DATA RPP
    if pertemuanKe-1 == 0:
        pertemuan = 1
    else:
        pertemuan = pertemuanKe-1
    data0 = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuan}}})
    datas0 = data0['rpp'][0]

    data = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    datas = data['rpp'][0]

    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']

    # DATA SISWA
    csiswa = await MGDB.edmedia_kbm.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)})

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_datetime = now.strftime("%Y-%m-%d %H:%M:%S")
    
    mulai = str(datas0['jamMulai']) #str(datas['jamMulai'])
    selesai = str(datas['jamSelesai'])
    
    if current_datetime < mulai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif current_datetime > selesai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    else:
        datatugas = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
        print(datatugas)
        if datatugas == {}: # jika tidak ada silabus
            print("tidak ada silabus")
            jmlmateri = len(datas['idMateri'])
            # id_materi = [str(i['_id']) for i in datas['idMateri']]
            listtugas = []
            # for id in id_materi:
            for id in datas['idMateri']:
                ctugas = await MGDB.edmedia_materi.find_one({'_id': ObjectId(id)}, {'tugas': 1, '_id': 0})
                print(ctugas)
                tugas = ctugas['tugas'][0]
                tugas['jawaban'] = None #data_in.jawaban
                tugas['imageJawaban'] = None #data_in.imageJawaban
                tugas['nilai'] = 0 #None
                listtugas.append(tugas)

            if not listtugas:
                raise HTTPException(status_code=400, detail="Tidak ada tugas di pertemuan ini")
            else:
                pelajar.dataKbm = []
                datatugas = PelajarKbm()
                datatugas.idSilabus = silabus
                datatugas.pertemuanKe = pertemuanKe
                datatugas.waktu = dateTimeNow()
                datatugas.tahun = csiswa['tahun']
                datatugas.semester = str(csiswa['semester'])
                datatugas.kelas = csiswa['kelas']
                datatugas.kelasDetail = csiswa['kelasDetail']
                datatugas.isHadir = True
                datatugas.tugas = listtugas
                datatugas.nilaiTugas = 0
                datatugas.tugasDone = False
                datatugas.soal = []
                datatugas.nilaiSoal = 0
                datatugas.soalDone = False
                pelajar.dataKbm.append(datatugas.dict(skip_defaults=True))

                pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
                    {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
                    {"$push": {"dataKbm": pelajar.dict(skip_defaults=True)['dataKbm'][0]}}
                )

                cdata = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
                if not cdata == {}:
                    return cdata['dataKbm'][0]['tugas']
    
        else: # jika ada silabus
            if not datatugas['dataKbm'][0]['tugas']:
                print("ada silabus, tidak ada tugas")
                jmlmateri = len(datas['idMateri'])
                # id_materi = [str(i['_id']) for i in datas['idMateri']]
                listtugas = []
                # for id in id_materi:
                for id in datas['idMateri']:
                    ctugas = await MGDB.edmedia_materi.find_one({'_id': ObjectId(id)}, {'tugas': 1, '_id': 0})
                    print(ctugas)
                    tugas = ctugas['tugas'][0]
                    tugas['jawaban'] = None #data_in.jawaban
                    tugas['imageJawaban'] = None #data_in.imageJawaban
                    tugas['nilai'] = 0 #None
                    listtugas.append(tugas)

                if not listtugas:
                    raise HTTPException(status_code=400, detail="Tidak ada tugas di pertemuan ini")
                else:
                    pelajar.dataKbm = []
                    datatugas = PelajarKbm()
                    datatugas.idSilabus = silabus
                    datatugas.pertemuanKe = pertemuanKe
                    datatugas.waktu = dateTimeNow()
                    datatugas.tahun = csiswa['tahun']
                    datatugas.semester = str(csiswa['semester'])
                    datatugas.kelas = csiswa['kelas']
                    datatugas.kelasDetail = csiswa['kelasDetail']
                    datatugas.isHadir = True
                    datatugas.tugas = listtugas
                    datatugas.nilaiTugas = 0
                    datatugas.tugasDone = False
                    datatugas.soal = []
                    datatugas.nilaiSoal = 0
                    datatugas.soalDone = False
                    pelajar.dataKbm.append(datatugas.dict(skip_defaults=True))

                    pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
                        {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
                        {"$set": {
                            "dataKbm.$[pertemuan].tugas" : listtugas}},
                        upsert=False,
                        array_filters=[
                            {"pertemuan.pertemuanKe": pertemuanKe, "pertemuan.idSilabus": silabus}
                        ]
                    )

                    cdata = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
                    if not cdata == {}:
                        return cdata['dataKbm'][0]['tugas']

            else:
                print("ada silabus, ada tugas")
                cdata = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
                if not cdata == {}:
                    return cdata['dataKbm'][0]['tugas']                


@router_rpp_student.post("/kerjakan_tugas/{userId}/{idKbm}/{pertemuanKe}/{idDetailTugas}")
async def kerjakan_tugas_by_user_id_pelajar_by_id_tugas(userId: str, idKbm: str, pertemuanKe: int, idDetailTugas: str, data_in: TugasDijawab, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    pelajar = IsiDefault(PelajarBase(), current_user)
    pelajar.updateTime = dateTimeNow()
    # DATA RPP
    data = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    datas = data['rpp'][0]

    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus'] 

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_datetime = now.strftime("%Y-%m-%d %H:%M:%S")
    
    mulai = str(datas['jamMulai'])
    selesai = str(datas['jamSelesai'])

    if current_datetime < mulai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    elif current_datetime > selesai:
        raise HTTPException(status_code=400, detail="Tidak bisa akses di luar waktu terjadwal")
    else:               
        pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
            {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
            {"$set": {
                "dataKbm.$[pertemuan].tugas.$[tugas].jawaban" : data_in.jawaban,
                "dataKbm.$[pertemuan].tugas.$[tugas].imageJawaban" : data_in.imageJawaban,
                "dataKbm.$[pertemuan].tugasDone" : True}},
            upsert=False,
            array_filters=[
                {"pertemuan.pertemuanKe": pertemuanKe, "pertemuan.idSilabus": silabus},
                {"tugas.id": ObjectId(idDetailTugas)}
            ]
        )

        if pelajar_op.modified_count or pelajar_op.matched_count:
            return {"status": "ok"}
        else:
            raise HTTPException(status_code=304)


@router_rpp_student.get("/get_nilai_student/{userId}/{idKbm}/{pertemuanKe}", response_model=PelajarDatas)
async def get_nilai_by_user_id_pelajar_kbm_by_pertemuan(userId: str, idKbm: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']
    print(silabus, type(silabus)) 

    pipeline = [
    {'$unwind': {'path': '$dataKbm'}},
    {'$match': {
            'companyId': ObjectId(current_user.companyId), 
            'userId': ObjectId(userId), 
            'dataKbm.idSilabus': ObjectId(silabus), 
            'dataKbm.pertemuanKe': pertemuanKe}},
    {'$project': {
            '_id': 1, 
            'userId': 1, 
            'name': 1, 
            'noId': 1, 
            'kelasDetailNow': 1, 
            'dataKbm': 1}
    }
    ]

    datas_cursor = MGDB.user_edmedia_pelajar.aggregate(pipeline)
    pelajar = await datas_cursor.to_list(length=1000)
    print(pelajar)

    if pelajar:
        return pelajar[0]
    else:
        raise HTTPException(status_code=404, detail="Data Nilai tidak ada")

@router_rpp_student.post("/get_mapel_student/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}", response_model=dict)
async def get_mapel_student_by_user_id_pelajar(userId: str, tahun: str, semester: str, kelas: str, kelasDetail: str, size: int = 10, page: int = 0, sort: str = "namaMapel", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    criteria = CreateCriteria(search)
    criteria['$and'].append({'dataKbm.tahun': tahun})
    criteria['$and'].append({'dataKbm.semester': semester})
    criteria['$and'].append({'dataKbm.kelas': kelas})
    criteria['$and'].append({'dataKbm.kelasDetail': kelasDetail})

    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId), 
            'userId': ObjectId(userId)
        }
    }, {
        '$unwind': {
            'path': '$dataKbm'
        }
    }, {
        '$match': criteria
    }, {
        '$lookup': {
            'from': 'edmedia_silabus', 
            'localField': 'dataKbm.idSilabus', 
            'foreignField': '_id', 
            'as': 'silabus'
        }
    }, {
        '$unwind': {
            'path': '$silabus'
        }
    }, {
        '$addFields': {
            'programStudi': '$silabus.programStudi', 
            'jurusan': '$silabus.jurusan', 
            'namaMapel': '$silabus.namaMapel'
        }
    }, {
        '$group': {
            '_id': {
                'idSilabus': '$dataKbm.idSilabus', 
                'tahun': '$dataKbm.tahun', 
                'semester': '$dataKbm.semester', 
                'kelas': '$dataKbm.kelas', 
                'kelasDetail': '$dataKbm.kelasDetail', 
                'programStudi': '$programStudi', 
                'jurusan': '$jurusan', 
                'namaMapel': '$namaMapel'
            }, 
            'fieldN': {
                '$sum': 1
            }
        }
    }, {
        '$project': {
            '_id': 0, 
            'idSilabus': '$_id.idSilabus', 
            'tahun': '$_id.tahun', 
            'semester': '$_id.semester', 
            'kelas': '$_id.kelas', 
            'kelasDetail': '$_id.kelasDetail', 
            'programStudi': '$_id.programStudi', 
            'jurusan': '$_id.jurusan', 
            'namaMapel': '$_id.namaMapel'
        }
    }, {
        '$sort': {
            sort: dir
        }
    }
    ]

    datas_cursor = MGDB.user_edmedia_pelajar.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=100)
    listdata = cdatas
    skip = page * size
    datas = listdata[skip:skip+size]
    totalElements = len(listdata)
    totalPages = math.ceil(totalElements / size)
    reply = MapelPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_student.post("/get_detail_student/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}/{idSilabus}", response_model=dict)
async def get_detail_student_by_user_id_pelajar(userId: str, tahun: str, semester: str, kelas: str, kelasDetail: str, idSilabus: str, size: int = 10, page: int = 0, sort: str = "dataKbm.pertemuanKe", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    criteria = CreateCriteria(search)
    criteria['$and'].append({'dataKbm.tahun': tahun})
    criteria['$and'].append({'dataKbm.semester': semester})
    criteria['$and'].append({'dataKbm.kelas': kelas})
    criteria['$and'].append({'dataKbm.kelasDetail': kelasDetail})
    criteria['$and'].append({'dataKbm.idSilabus': ObjectId(idSilabus)})

    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId), 
            'userId': ObjectId(userId)
        }
    }, {
        '$unwind': {
            'path': '$dataKbm'
        }
    }, {
        '$match': criteria
    }, {
        '$lookup': {
            'from': 'edmedia_silabus', 
            'localField': 'dataKbm.idSilabus', 
            'foreignField': '_id', 
            'as': 'silabus'
        }
    }, {
        '$unwind': {
            'path': '$silabus'
        }
    }, {
        '$addFields': {
            'programStudi': '$silabus.programStudi', 
            'jurusan': '$silabus.jurusan', 
            'namaMapel': '$silabus.namaMapel'
        }
    }, {
        '$sort': {
            sort: dir
        }
    }
    ]

    datas_cursor = MGDB.user_edmedia_pelajar.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=100)
    skip = page * size
    listdata = cdatas
    datas = listdata[skip:skip+size]
    totalElements = len(listdata)
    totalPages = math.ceil(totalElements / size)
    reply = HistoryPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_student.post("/get_pertemuan_student/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}/{idSilabus}/{pertemuanKe}", response_model=dict)
async def get_pertemuan_student_by_user_id_pelajar(userId: str, tahun: str, semester: str, kelas: str, kelasDetail: str, idSilabus: str, pertemuanKe: int, size: int = 10, page: int = 0, sort: str = "dataKbm.pertemuanKe", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    criteria = CreateCriteria(search)
    criteria['$and'].append({'dataKbm.tahun': tahun})
    criteria['$and'].append({'dataKbm.semester': semester})
    criteria['$and'].append({'dataKbm.kelas': kelas})
    criteria['$and'].append({'dataKbm.kelasDetail': kelasDetail})
    criteria['$and'].append({'dataKbm.idSilabus': ObjectId(idSilabus)})
    criteria['$and'].append({'dataKbm.pertemuanKe': pertemuanKe})

    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId), 
            'userId': ObjectId(userId)
        }
    }, {
        '$unwind': {
            'path': '$dataKbm'
        }
    }, {
        '$match': criteria
    }, {
        '$lookup': {
            'from': 'edmedia_silabus', 
            'localField': 'dataKbm.idSilabus', 
            'foreignField': '_id', 
            'as': 'silabus'
        }
    }, {
        '$unwind': {
            'path': '$silabus'
        }
    }, {
        '$addFields': {
            'programStudi': '$silabus.programStudi', 
            'jurusan': '$silabus.jurusan', 
            'namaMapel': '$silabus.namaMapel'
        }
    }, {
        '$sort': {
            sort: dir
        }
    }
    ]

    datas_cursor = MGDB.user_edmedia_pelajar.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=100)
    skip = page * size
    listdata = cdatas
    datas = listdata[skip:skip+size]
    totalElements = len(listdata)
    totalPages = math.ceil(totalElements / size)
    reply = HistoryPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_student.post("/get_activity_student/{userId}", response_model=dict)
async def get_activity_student_by_user_id_pelajar(userId: str, size: int = 10, page: int = 0, sort: str = "dataKbm.waktu", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pelajar", "*"])):
    criteria = CreateCriteria(search)

    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId), 
            'userId': ObjectId(userId)
        }
    }, {
        '$unwind': {
            'path': '$dataKbm'
        }
    }, {
        '$match': criteria
    }, {
        '$lookup': {
            'from': 'edmedia_silabus', 
            'localField': 'dataKbm.idSilabus', 
            'foreignField': '_id', 
            'as': 'silabus'
        }
    }, {
        '$unwind': {
            'path': '$silabus'
        }
    }, {
        '$addFields': {
            'programStudi': '$silabus.programStudi', 
            'jurusan': '$silabus.jurusan', 
            'namaMapel': '$silabus.namaMapel'
        }
    }, {
        '$sort': {
            sort: dir
        }
    }
    ]

    datas_cursor = MGDB.user_edmedia_pelajar.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=100)
    skip = page * size
    listdata = cdatas
    datas = listdata[skip:skip+size]
    totalElements = len(listdata)
    totalPages = math.ceil(totalElements / size)
    reply = HistoryPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply