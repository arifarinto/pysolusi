# backend/tancho/kelass/routes.py

from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.edmedia.kelas import KelasBase, KelasOnDB, KelasPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_kelas = APIRouter()
dbase = MGDB.edmedia_kelas

async def GetKelasOr404(id_: str):
    _id = ValidateObjectId(id_)
    kelas = await dbase.find_one({"_id": _id})
    if kelas:
        return kelas
    else:
        raise HTTPException(status_code=404, detail="Kelas not found")

# =================================================================================

@router_kelas.post("/kelas", response_model=KelasOnDB)
async def add_kelas(data_in: KelasBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    kelas = IsiDefault(data_in, current_user)
    kelas.kelasDetail = kelas.kelasDetail.upper()
    ckelas = await dbase.find_one({
        "companyId": ObjectId(current_user.companyId), 
        "tahun":kelas.tahun, 
        "semester":kelas.semester, 
        "kelas":kelas.kelas, 
        "kelasDetail": kelas.kelasDetail})
    if ckelas:
        raise HTTPException(status_code=400, detail="Kelas is registered")
    kelas_op = await dbase.insert_one(kelas.dict())
    if kelas_op.inserted_id:
        kelas = await GetKelasOr404(kelas_op.inserted_id)
        return kelas

@router_kelas.post("/upload_kelas")
async def upload_kelas(file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file, converters={'Tahun': str, 'Semester': str, 'Kelas': str})
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    data_in = KelasBase()
    for index,row in df.iterrows():
        kelas = IsiDefault(data_in, current_user)
        kelas.waliKelas = getString(row['NIP Wali Kelas'])
        kelas.tahun = getString(row['Tahun'])
        kelas.semester = getString(row['Semester'])
        kelas.kelas = getString(row['Kelas'])
        kelas.kelasDetail = getString(row['Kelas Detail'])
        kelas.kapasitas = getNumber(row['Kapasitas'])
        if None in [kelas.waliKelas, kelas.tahun, kelas.semester, kelas.kelas, kelas.kelasDetail, kelas.kapasitas]:
            raise HTTPException(status_code=404, detail="Tahun, Semester, Kelas, Kelas Detail, Kapasitas, dan NIP Wali Kelas harus diisi")
        else:
            kelas.tahun = getString(row['Tahun'].upper())
            kelas.semester = getString(row['Semester'].upper())
            kelas.kelas = getString(row['Kelas'].upper())
            kelas.kelasDetail = getString(row['Kelas Detail'].upper())
            kelas.kapasitas = getNumber(row['Kapasitas'])
            kelas.note = getString(row['Note'])
            kelas.aktif = True

            nip = getString(row['NIP Wali Kelas'])
            cPengajar = await MGDB.user_edmedia_pengajar.find_one({
                "companyId": ObjectId(current_user.companyId), 
                "noId": nip
                })
            if cPengajar:
                kelas.waliKelas = cPengajar['userId']

                ckelas = await dbase.find_one({
                    "companyId": ObjectId(current_user.companyId), 
                    "tahun":kelas.tahun, 
                    "semester":kelas.semester, 
                    "kelas":kelas.kelas, 
                    "kelasDetail": kelas.kelasDetail
                })
                if ckelas:
                    rowNonvalid += 1
                    unValid.append({
                        "tahun":kelas.tahun,
                        "semester":kelas.semester,
                        "kelas":kelas.kelas, 
                        "kelasDetail": kelas.kelasDetail,
                        "note":"telah terdaftar"
                        })
                else:
                    rowValid += 1
                    result.append(kelas.dict())

            else:
                rowNonvalid += 1
                unValid.append({
                    "tahun":kelas.tahun,
                    "semester":kelas.semester,
                    "kelas":kelas.kelas, 
                    "kelasDetail": kelas.kelasDetail,
                    "note":f"NIP Wali Kelas {nip} tidak terdaftar"
                    })

    #aneh
    print(result)
    dbase.insert_many(result)
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


@router_kelas.post("/get_kelas", response_model=dict)
async def get_all_kelass(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = KelasPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_kelas.get("/kelas/{id_}", response_model=KelasOnDB)
async def get_kelas_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    kelas = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if kelas:
        return kelas
    else:
        raise HTTPException(status_code=404, detail="Kelas not found")


@router_kelas.delete("/kelas/{id_}", dependencies=[Depends(GetKelasOr404)], response_model=dict)
async def delete_kelas_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    kelas_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if kelas_op.deleted_count:
        return {"status": f"deleted count: {kelas_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_kelas.put("/kelas/{id_}", response_model=KelasOnDB)
async def update_kelas(id_: str, data_in: KelasBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    kelas = IsiDefault(data_in, current_user, True)
    kelas.updateTime = dateTimeNow()
    kelas_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": kelas.dict(skip_defaults=True)}
    )
    if kelas_op.modified_count:
        return await GetKelasOr404(id_)
    else:
        raise HTTPException(status_code=304)