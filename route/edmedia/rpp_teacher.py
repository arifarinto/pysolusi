# backend/tancho/kbms/routes.py 

from bson.objectid import ObjectId
from pydantic.utils import Obj
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime, timedelta
import logging
import math
import random

from model.edmedia.silabus import comboKbmTeacher
from model.edmedia.kelas import comboKelas
from model.edmedia.kbm import GenerateKbm, KbmBase, KbmOnDB, KbmPage, RppBase, RppsBase, RppOnDB, RppPage, UserBasic, UserAbsesni, ListRppPage, PesertaPage
from model.edmedia.pelajar import PelajarKbm, PelajarExcul, PelajarNilai, PelajarKesehatan, PelajarKepribadian, PelajarBase, PelajarOnDB, PelajarDatas, PelajarHistory, PelajarMapel, MapelPage, HistoryPage
from model.edmedia.materi import CommentBase, CommentReply, SoalDijawab, TugasDijawab, SoalPage, comboMateri
from model.util import SearchRequest, FieldObjectIdRequest, FieldRequest
from model.default import JwtToken
from util.util import RandomString, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_rpp_teacher = APIRouter()
dbase = MGDB.edmedia_kbm

async def GetRppOr404(id_: str):
    _id = ValidateObjectId(id_)
    rpp = await dbase.find_one({"_id": _id})
    if rpp:
        return rpp
    else:
        raise HTTPException(status_code=404, detail="Rpp not found")


# # =================================================================================
# #teacher

@router_rpp_teacher.post("/get_rpp_pengajar/{userId}", response_model=dict)
async def get_rpp_by_user_id_pengajar(userId: str, size: int = 10, page: int = 0, sort: str = "rpp.jamMulai", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pengajar","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    criteria['$and'].append({'rpp.pengajar.userId': ObjectId(userId)})
    print(criteria)
    
    pipeline = [
        {'$match': criteria},
        {'$unwind': {'path': '$rpp'}},
        {'$match': {'rpp.jamMulai': {'$lte': datetime.now() + timedelta(hours=2)}}},
        {'$sort': {'rpp.jamMulai': -1}}        
    ]
    
    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listrpp = cdatas
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = RppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply

@router_rpp_teacher.post("/get_next_rpp_pengajar/{userId}", response_model=dict)
async def get_next_rpp_by_user_id_pengajar(userId: str, size: int = 10, page: int = 0, sort: str = "rpp.jamMulai", dir : int = -1, limit: int = 4, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pengajar","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    criteria['$and'].append({'rpp.pengajar.userId': ObjectId(userId)})
    print(criteria)
    
    pipeline = [
        {'$match': criteria},
        {'$unwind': {'path': '$rpp'}},
        {'$match': {'rpp.jamMulai': {'$gte': datetime.now()}}},
        {'$sort': {'rpp.jamMulai': 1}},
        {'$limit': limit}
    ]
    
    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listrpp = cdatas
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = RppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_teacher.post("/get_rpp_pengajar/tanggal/{userId}/{tanggal}", response_model=dict)
async def get_rpp_by_user_id_pengajar_by_tanggal(userId: str, tanggal: str, size: int = 10, page: int = 0, sort: str = "rpp.jamMulai", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pengajar","*"])):
    # cPengajar = await MGDB.user_edmedia_pengajar.find_one({
    #     "companyId": ObjectId(current_user.companyId),
    #     "userId": ObjectId(userId)})
    # data = cPengajar
    tgl = datetime.strptime(tanggal, '%Y-%m-%d')
    tglAwal = tgl - timedelta(days=1)
    tglAkhir = tgl + timedelta(days=1)

    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    search.defaultObjectId.append(FieldObjectIdRequest(field='pengajar.userId',key=ObjectId(userId)))
    criteria = CreateCriteria(search)
    # criteria['$and'].append({'kelas': data['kelasNow']})
    # criteria['$and'].append({'kelasDetail': data['kelasDetailNow']})
    
    pipeline = [
        {'$match': criteria},
        {'$unwind': {'path': '$rpp'}},
        {'$match': {'rpp.jamMulai': {
                '$gte': tgl, 
                '$lt': tglAkhir
            }}},
        {'$sort': {'rpp.jamMulai': -1}}        
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listrpp = cdatas
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = RppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_teacher.get("/jadwal/pengajar/{userId}")
async def get_jadwal_by_user_id_pengajar(userId: str, current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    # cari data maksiman tahun dan semester di silabus pengajar
    pengajar = await MGDB.user_edmedia_pengajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)})
    pip_silabus = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId), 
            'pengajar.userId': pengajar['userId']
        }
    }, {
        '$group': {
            '_id': {
                'tahun': '$tahun', 
                'semester': '$semester'
            }, 
            'fieldN': {
                '$sum': 1
            }
        }
    }, {
        '$project': {
            '_id': 0, 
            'tahun': '$_id.tahun', 
            'semester': '$_id.semester'
        }
    }, {
        '$sort': {
            'tahun': -1, 
            'semester': -1
        }
    }, {
        '$limit': 1
    }
    ]
    pengajar_cursor = dbase.aggregate(pip_silabus)
    cpengajar = await pengajar_cursor.to_list(length=1)
    tahun = cpengajar[0]['tahun']
    semester = cpengajar[0]['semester']
    
    # bikin jadwal
    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId), 
            'pengajar.userId': ObjectId(userId),
            'tahun': tahun,
            'semester': semester
        }
    }, {
        '$unwind': {
            'path': '$rpp'
        }
    }, {
        '$addFields': {
            'jamMulai': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.jamMulai', None
                        ]
                    }, '00:00', {
                        '$dateToString': {
                            'format': '%H:%M', 
                            'date': '$rpp.jamMulai'
                        }
                    }
                ]
            }, 
            'jamSelesai': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.jamSelesai', None
                        ]
                    }, '00:00', {
                        '$dateToString': {
                            'format': '%H:%M', 
                            'date': '$rpp.jamSelesai'
                        }
                    }
                ]
            }, 
            'hari': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.jamSelesai', None
                        ]
                    }, '0', {
                        '$toString': {
                            '$dayOfWeek': '$rpp.jamMulai'
                        }
                    }
                ]
            }, 
            'namaRuang': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.namaRuang', None
                        ]
                    }, '-', '$rpp.namaRuang'
                ]
            }
        }
    }, {
        '$project': {
            '_id': 0, 
            'hari': '$hari', 
            'jamMulai': '$jamMulai', 
            'jamSelesai': '$jamSelesai', 
            'kelas': '$kelasDetail', 
            'mapel': '$namaMapel', 
            'ruang': '$namaRuang'
        }
    }, {
        '$group': {
            '_id': '$hari', 
            'detail': {
                '$addToSet': {
                    'jamMulai': '$jamMulai', 
                    'jamSelesai': '$jamSelesai', 
                    'kelas': '$kelas', 
                    'mapel': '$mapel', 
                    'ruang': '$ruang'
                }
            }
        }
    }, {
        '$sort': {
            '_id': 1
        }
    }
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    print(cdatas)
    jadwal = []
    for x in cdatas:
        hari = {'0': 'NONE', '1': 'MINGGU', '2': 'SENIN', '3': 'SELASA', '4': 'RABU', '5': 'KAMIS', '6': 'JUMAT', '7': 'SABTU'}
        if x['_id']:
            x['_id'] = hari[x['_id']]
            # x['jadwal'].sort()
            cdatas = x['detail']
            cdatas.sort(key=lambda x: x.get('jamMulai'), reverse=False)            
            jadwal.append(x)
    print(cdatas)
    return jadwal


@router_rpp_teacher.post("/get_peserta/{id_}", response_model=dict)
async def get_peserta_by_id_kbm(id_: str, size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pengajar","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    search.defaultObjectId.append(FieldObjectIdRequest(field='_id',key=ObjectId(id_)))
    criteria = CreateCriteria(search)
    print(criteria)

    if dir == 1: sorting = False
    if dir == -1: sorting = True
    datas_cursor = await dbase.find_one(criteria, {'_id': 0, 'pelajar': 1})
    cdatas = datas_cursor['pelajar']
    cdatas.sort(key=lambda x: x.get(sort), reverse=sorting)
    print(cdatas)

    listrpp = cdatas
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = PesertaPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_teacher.post("/all_peserta_absen/{id_}/{pertemuanKe}")
async def add_all_peserta_absen_by_id_kbm_by_pertemuan(id_: str, pertemuanKe: int, data_in: List[UserAbsesni], current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    rpp = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    if rpp:
        for absen in data_in:
            pertemuan = pertemuanKe-1
            rpp_op = await dbase.update_one(
                {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
                {"$set": {f"rpp.{pertemuan}.pesertaAbsen.$[id].isAbsen": absen.isAbsen}},
                upsert=False,
                array_filters=[
                    {"id.userId": ObjectId(absen.userId)}
                ]
            )
    if rpp_op.modified_count or rpp_op.matched_count:
        # return await GetRppOr404(id_)
        return {"status": "ok"}
    else:
        raise HTTPException(status_code=304)


@router_rpp_teacher.post("/peserta_absen/{id_}/{pertemuanKe}/{userIdPelajar}")
async def add_peserta_absen_by_id_kbm_by_pertemuan(id_: str, pertemuanKe: int, userIdPelajar: str, data_in: UserAbsesni, current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    pelajar = await MGDB.user_edmedia_pelajar.find_one({"companyId": ObjectId(current_user.companyId), "userId": ObjectId(userIdPelajar)})
    data_in.userId = pelajar['userId']
    data_in.name = pelajar['name']
    data_in.nomorIdentitas = pelajar['noId']
    
    rpp = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    if rpp:
        pertemuan = pertemuanKe-1
        rpp_op = await dbase.update_one(
            {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
            {"$set": {f"rpp.{pertemuan}.pesertaAbsen.$[id].isAbsen": data_in.isAbsen}},
            upsert=False,
            array_filters=[
                {"id.userId": ObjectId(userIdPelajar)}
            ]
        )
    if rpp_op.modified_count or rpp_op.matched_count:
        # return await GetRppOr404(id_)
        return {"status": "ok"}
    else:
        raise HTTPException(status_code=304)


@router_rpp_teacher.get("/peserta_absen/{id_}/{pertemuanKe}", response_model=List[UserAbsesni])
async def get_peserta_absen_by_id_kbm_by_pertemuan(id_: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    rpp = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    if rpp['rpp'][0]['pesertaAbsen']:
        print('ada pesertaAbsen')
        return rpp['rpp'][0]['pesertaAbsen']
    else:
        print('tidak ada pesertaAbsen')
        datas_cursor = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'pelajar': 1})
        cdatas = datas_cursor['pelajar']
        cdatas.sort(key=lambda x: x.get('name'))
        peserta = []
        for x in cdatas:
            x['isHadir'] = False
            x['isAbsen'] = False
            peserta.append(x)

        rpp_op = await dbase.update_one(
            {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
            {"$set": {f"rpp.{pertemuanKe-1}.pesertaAbsen": peserta}}
        )

        rpp = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
        print(rpp['rpp'][0]['pesertaAbsen'])
        return rpp['rpp'][0]['pesertaAbsen']


@router_rpp_teacher.get("/get_kelas_teacher/{idPengajar}", response_model=List[comboKbmTeacher])
async def get_kelas_by_user_id_teacher(idPengajar: str, current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    crpp = dbase.find({"pengajar.userId": ObjectId(idPengajar),"companyId": ObjectId(current_user.companyId)})
    rpp = await crpp.to_list(length=1000)
    if rpp:
        return rpp
    else:
        raise HTTPException(status_code=404, detail="Rpp not found")


@router_rpp_teacher.get("/get_nilai/{kelasDetail}/{idKbm}/{pertemuanKe}", response_model=List[PelajarDatas])
async def get_nilai_by_kbm_by_pertemuan(kelasDetail: str, idKbm: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']
    print(silabus, type(silabus))

    pipeline = [
        {'$unwind': {'path': '$dataKbm'}},
        {'$match': {
            'companyId': ObjectId(current_user.companyId), 
            'kelasDetailNow': kelasDetail, 
            'dataKbm.idSilabus': silabus, 
            'dataKbm.pertemuanKe': pertemuanKe}},
        {'$project': {
            '_id': 1, 
            'userId': 1, 
            'name': 1, 
            'noId': 1, 
            'kelasDetailNow': 1, 
            'dataKbm': 1}
        }
    ]

    datas_cursor = MGDB.user_edmedia_pelajar.aggregate(pipeline)
    pelajar = await datas_cursor.to_list(length=1000)
    print(pelajar)

    if pelajar:
        return pelajar
    else:
        raise HTTPException(status_code=404, detail="Data Nilai tidak ada")


@router_rpp_teacher.get("/koreksi_get/{userId}/{idKbm}/{pertemuanKe}", response_model=List[TugasDijawab])
async def get_koreksi_by_user_id_pelajar(userId: str, idKbm: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']

    datatugas = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
    tugas = datatugas['dataKbm'][0]['tugas'] 
    if tugas:
        return tugas
    else:
        raise HTTPException(status_code=404, detail="Data Tugas tidak ada")


@router_rpp_teacher.post("/koreksi_tugas/{userId}/{idKbm}/{pertemuanKe}/{idDetailTugas}")
async def koreksi_tugas_by_user_id_pelajar_by_id_tugas(userId: str, idKbm: str, pertemuanKe: int, idDetailTugas: str, data_in: TugasDijawab, current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    pelajar = IsiDefault(PelajarBase(), current_user)
    pelajar.updateTime = dateTimeNow()

    # DATA KBM
    kbm = await dbase.find_one({"_id": ObjectId(idKbm),"companyId": ObjectId(current_user.companyId)}, {'idSilabus': 1})
    silabus = kbm['idSilabus']

    pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
        {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": {
            "dataKbm.$[pertemuan].tugas.$[tugas].nilai" : data_in.nilai}},
        upsert=False,
        array_filters=[
            {"pertemuan.pertemuanKe": pertemuanKe, "pertemuan.idSilabus": silabus},
            {"tugas.id": ObjectId(idDetailTugas)}
        ]
    )

    data = await MGDB.user_edmedia_pelajar.find_one({"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, {'_id': 0, 'dataKbm': {'$elemMatch': {'idSilabus': silabus, 'pertemuanKe': pertemuanKe}}})
    cdata = data['dataKbm'][0]
    if data:
        jmlTugas = len(cdata['tugas'])
        poin = 0
        for i in cdata['tugas']:
            poin += i['point']
        konvert = 100 / poin

        nilaiTugas = 0
        for i in cdata['tugas']:
            nilaiTugas += (i['point'] * konvert / 100) * i['nilai']
    else:
        raise HTTPException(status_code=404, detail="Data Tugas tidak ada")

    pelajar_op = await MGDB.user_edmedia_pelajar.update_one(
        {"userId": ObjectId(userId),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": {
            "dataKbm.$[pertemuan].nilaiTugas" : nilaiTugas}},
        upsert=False,
        array_filters=[
            {"pertemuan.pertemuanKe": pertemuanKe, "pertemuan.idSilabus": silabus}
        ]
    )

    if pelajar_op.modified_count or pelajar_op.matched_count:
        return {"status": "ok"}
    else:
        raise HTTPException(status_code=304)


@router_rpp_teacher.post("/get_detail_teacher/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}/{idSilabus}", response_model=dict)
async def get_detail_kbm_by_user_id_teacher(userId: str, tahun: str, semester: str, kelas: str, kelasDetail: str, idSilabus: str, size: int = 10, page: int = 0, sort: str = "rpp.pertemuanKe", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    criteria = CreateCriteria(search)
    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId), 
            'pengajar.userId': ObjectId(userId), 
            'idSilabus': ObjectId(idSilabus)
        }
    }, {
        '$unwind': {
            'path': '$rpp'
        }
    }, {
        '$match': {
            'rpp.pengajar.userId': ObjectId(userId), 
            'tahun': tahun, 
            'semester': int(semester), 
            'kelas': kelas, 
            'kelasDetail': kelasDetail
        }
    }, {
        '$match': criteria
    }, {
        '$sort': {
            sort: dir
        }
    }
    ]

    datas_cursor = MGDB.edmedia_kbm.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=100)
    skip = page * size
    listdata = cdatas
    datas = listdata[skip:skip+size]
    totalElements = len(listdata)
    totalPages = math.ceil(totalElements / size)
    reply = RppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_teacher.post("/get_detail_teacher/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}/{idSilabus}/{pertemuanKe}", response_model=dict)
async def get_detail_student_by_user_id_teacher(userId: str, tahun: str, semester: str, kelas: str, kelasDetail: str, idSilabus: str, pertemuanKe: int, size: int = 10, page: int = 0, sort: str = "dataKbm.pertemuanKe", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pengajar", "*"])):
    criteria = CreateCriteria(search)
    criteria['$and'].append({'dataKbm.tahun': tahun})
    criteria['$and'].append({'dataKbm.semester': semester})
    criteria['$and'].append({'dataKbm.kelas': kelas})
    criteria['$and'].append({'dataKbm.kelasDetail': kelasDetail})
    criteria['$and'].append({'dataKbm.idSilabus': ObjectId(idSilabus)})
    criteria['$and'].append({'dataKbm.pertemuanKe': pertemuanKe})
    print(criteria)
    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(current_user.companyId)
        }
    }, {
        '$unwind': {
            'path': '$dataKbm'
        }
    }, {
        '$match': criteria
    }, {
        '$lookup': {
            'from': 'edmedia_silabus', 
            'localField': 'dataKbm.idSilabus', 
            'foreignField': '_id', 
            'as': 'silabus'
        }
    }, {
        '$unwind': {
            'path': '$silabus'
        }
    }, {
        '$addFields': {
            'programStudi': '$silabus.programStudi', 
            'jurusan': '$silabus.jurusan', 
            'namaMapel': '$silabus.namaMapel'
        }
    }, {
        '$sort': {
            sort: dir
        }
    }
    ]

    datas_cursor = MGDB.user_edmedia_pelajar.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=100)
    skip = page * size
    listdata = cdatas
    datas = listdata[skip:skip+size]
    totalElements = len(listdata)
    totalPages = math.ceil(totalElements / size)
    reply = HistoryPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp_teacher.post("/get_activity_pengajar/{userId}", response_model=dict)
async def get_activity_by_user_id_pengajar(userId: str, size: int = 10, page: int = 0, sort: str = "rpp.jamMulai", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["pengajar","*"])):
    criteria = CreateCriteria(search)
    print(criteria)
    
    pipeline = [
        {'$unwind': {'path': '$rpp'}},
        {'$match': {
            'companyId': ObjectId(current_user.companyId), 
            'rpp.isAkses': False, 
            'rpp.isDone': True, 
            'rpp.pengajar.userId': ObjectId(userId), 
            'rpp.jamMulai': {'$lte': datetime.now()}
        }},
        {'$match': criteria},
        {'$sort': {sort: dir}}
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listrpp = cdatas
    skip = page * size
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = RppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply