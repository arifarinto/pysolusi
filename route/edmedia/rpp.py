# backend/tancho/kbms/routes.py 

import typing
from bson.objectid import ObjectId
from pydantic.utils import Obj
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, Response, File, UploadFile
from typing import List, Optional
from datetime import datetime, timedelta
import logging
import pandas as pd
from io import BytesIO
import math
import random

from model.edmedia.kbm import GenerateKbm, KbmBase, KbmOnDB, KbmPage, RppBase, RppsBase, RppOnDB, RppPage, TipeUjian, UserBasic, ListRppPage, PesertaPage
from model.edmedia.pelajar import PelajarKbm, PelajarExcul, PelajarNilai, PelajarKesehatan, PelajarKepribadian, PelajarBase, PelajarOnDB
from model.edmedia.materi import CommentBase, CommentReply, SoalDijawab, TugasDijawab, SoalPage, comboMateri
from model.util import SearchRequest, FieldObjectIdRequest, FieldRequest
from model.default import JwtToken
from util.util import RandomString, ValidateObjectId, IsiDefault, CreateCriteria, ListToUp
from route.auth import get_current_user
from util.util_waktu import dateTimeNow
from util.util_excel import getDate, getNumber, getString


router_rpp = APIRouter()
dbase = MGDB.edmedia_kbm

async def GetRppOr404(id_: str):
    _id = ValidateObjectId(id_)
    rpp = await dbase.find_one({"_id": _id})
    if rpp:
        return rpp
    else:
        raise HTTPException(status_code=404, detail="Rpp not found")

# =================================================================================


@router_rpp.post("/get_rpp", response_model=dict)
async def get_all_rpps(size: int = 10, page: int = 0, sort: str = "tahun", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort([("kelasDetail", dir), ("namaMapel", dir), (sort, dir), ("semester", dir)])
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = KbmPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply

# @router_rpp.get("/rpp/{id_}", response_model=RppBase)
# async def get_rpp_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin", "*"])):
#     rpp = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
#     if rpp:
#         return rpp
#     else:
#         raise HTTPException(status_code=404, detail="Rpp not found")


@router_rpp.delete("/rpp/{id_}", dependencies=[Depends(GetRppOr404)], response_model=dict)
async def delete_rpp_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin", "*"])):
    rpp_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if rpp_op.deleted_count:
        return {"status": f"deleted count: {rpp_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_rpp.post("/rpp/all/{id_}", response_model=dict)
async def get_rpp_by_id_kbm(id_: str, size: int = 10, page: int = 0, sort: str = "pertemuanKe", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    search.defaultObjectId.append(FieldObjectIdRequest(field='_id',key=ObjectId(id_)))
    criteria = CreateCriteria(search)
    print(criteria)

    datas_cursor = dbase.find(criteria, {'_id': 0, 'rpp': 1})
    cdatas = await datas_cursor.to_list(length=size)
    listrpp = cdatas[0]['rpp']
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = ListRppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp.get("/rpp/{id_}/{pertemuanKe}", response_model=RppBase)
async def get_rpp_by_id_kbm_by_pertemuan(id_: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin", "*"])):
    rpp = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    if rpp:
        return rpp['rpp'][0]
    else:
        raise HTTPException(status_code=404, detail="Rpp not found")


@router_rpp.get("/get_rpp/{id_}/{pertemuanKe}", response_model=RppsBase)
async def get_rpp_lengkap_by_id_kbm_by_pertemuan(id_: str, pertemuanKe: int, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin","*"])):
    pipeline = [
        {'$match': {
            'companyId': ObjectId(current_user.companyId),
            '_id': ObjectId(id_)
            }},
        {'$unwind': {'path': '$rpp'}},
        {'$match': {'rpp.pertemuanKe': pertemuanKe}}
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    return cdatas[0]


@router_rpp.put("/rpp/{id_}/{pertemuanKe}", response_model=RppBase)
async def update_rpp_by_id_kbm_by_pertemuan(pertemuanKe: int, id_: str, data_in: RppBase, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pengajar", "*"])):
    # rpp = IsiDefault(data_in, current_user, True)
    # rpp.updateTime = dateTimeNow()
    rpp = data_in
    if rpp.idRuang:
        ruang = await MGDB.edmedia_ruang.find_one({"_id": ObjectId(rpp.idRuang),"companyId": ObjectId(current_user.companyId)})
        if ruang:
            rpp.idRuang = ruang["_id"]
            rpp.namaRuang = ruang["name"]
    # if rpp.idMateri:
    #     listIdMateri = []
    #     for id in rpp.idMateri:
    #         data = id.dict()
    #         dict = {'_id' if k == 'id' else k:v for k,v in data.items()}
    #         dict['_id'] = ObjectId(dict['_id'])
    #         listIdMateri.append(dict)
    # print(listIdMateri)
    # rpp.idMateri = listIdMateri

    
# =========== UNDER CONSTRUCTION =========== START (JIKA TIPE JAM = TIME)
    
    # if rpp.tgl:
    #     rpp.tgl = datetime.strptime(str(rpp.tgl), "%Y-%m-%d")
    
    # if rpp.jamMulai: 
    #     if rpp.tgl:
    #         rpp.jamMulai = datetime.strptime(str(rpp.tgl.date())+" "+str(rpp.jamMulai.time()), "%Y-%m-%d %H:%M:%S")
    #     else:
    #         cdata = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    #         if cdata:
    #             data = cdata['rpp'][0]
    #             tanggal = data['tgl'].date()
    #             rpp.jamMulai = datetime.strptime(str(tanggal)+" "+str(rpp.jamMulai.time()), "%Y-%m-%d %H:%M:%S")
    #         else:
    #             raise HTTPException(status_code=400, detail="Update tanggal terlebih dahulu")
    # if rpp.jamSelesai: 
    #     if rpp.tgl:
    #         rpp.jamSelesai = datetime.strptime(str(rpp.tgl.date())+" "+str(rpp.jamSelesai.time()), "%Y-%m-%d %H:%M:%S")
    #     else:
    #         cdata = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    #         if cdata:
    #             data = cdata['rpp'][0]
    #             tanggal = data['tgl'].date()
    #             rpp.jamSelesai = datetime.strptime(str(tanggal)+" "+str(rpp.jamSelesai.time()), "%Y-%m-%d %H:%M:%S")
    #         else:
    #             raise HTTPException(status_code=400, detail="Update tanggal terlebih dahulu")
    # print(rpp.tgl, type(rpp.tgl))
    # print(rpp.jamMulai, type(rpp.jamMulai))
    # print(rpp.jamSelesai, type(rpp.jamSelesai))

# =========== UNDER CONSTRUCTION =========== END

    updt = {}
    for x,y in rpp.dict(skip_defaults=True).items():
        updt[f"rpp.$[pertemuan].{x}"] = y
    print(updt)    
    rpp_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": updt},
        upsert=False,
        array_filters=[
            {"pertemuan.pertemuanKe": pertemuanKe}
        ]
    )

    if rpp_op.modified_count or rpp_op.matched_count:
        # return await GetRppOr404(id_)
        rpps = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
        if rpps:
            return rpps['rpp'][0]
    else:
        raise HTTPException(status_code=304)


@router_rpp.post("/rpp/{id_}")
async def update_rpp_by_excel_by_id_kbm(id_: str, file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pengajar", "*"])):
    df = pd.read_excel(file.file, converters={'Jumlah Soal': str, 'Bobot': str})
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0

    # PROTEKSI DUPLICATE ID
    listpop = []
    pop = df[df.duplicated(["Pertemuan"], keep=False)]
    if pop.empty:
        pass
    else:
        print("ada data duplicate")
        for index, row in pop.sort_values(by=["Pertemuan"]).iterrows():
            rowNonvalid += 1
            listpop.append(index)
            unValid.append({
                "pertemuanKe": row["Pertemuan"],
                "tanggal": row["Tanggal"],
                "jamMulai": row["Jam Mulai"],
                "jamSelesai": row["Jam Selesai"],
                "codeMateri": row["Kode Materi"],
                "note": "Pertemuan terdata lebih dari 1"
            })
        df = df.drop(listpop)

    rpp = RppBase()
    for index,row in df.iterrows():
        pertemuanKe = int(getNumber(row['Pertemuan']))
        tanggal = getString(row['Tanggal'])
        jamMulai = getString(row['Jam Mulai'])
        jamSelesai = getString(row['Jam Selesai'])
        nip = getString(row['NIP Pengajar'])
        codeMateri = getString(row['Kode Materi'])
        rpp.isAkses = getString(row['isAkses'])
        rpp.isUjian = getString(row['isUjian'])
        rpp.tipe = getString(row['Tipe'])
        kodeRuang = getString(row['Kode Ruang'])
        rpp.note = getString(row['Note'])
        cek = [pertemuanKe, tanggal, jamMulai, jamSelesai, nip, codeMateri, rpp.isAkses, rpp.isUjian, rpp.tipe]
        for i in cek:
            print(i, i is None)
        if None in [pertemuanKe, tanggal, jamMulai, jamSelesai, nip, codeMateri, rpp.isAkses, rpp.isUjian, rpp.tipe]:
            raise HTTPException(status_code=404, detail="Pertemuan, Tanggal, Jam Mulai, Jam Selesai, NIP Pengajar, Kode Materi, isAkses, isUjian, dan Tipe harus diisi")
        else:
            error = 0
            listError = []
            if codeMateri:
                codeMateri = getString(row['Kode Materi']).split(",")
                codeMateri = ListToUp(codeMateri)
                listIdMateri = []
                for i in codeMateri:
                    code = await MGDB.edmedia_materi.find_one({"codeMateri": i})
                    if code:
                        listIdMateri.append(str(code['_id']))
                    else:
                        error += 1
                        listError.append(f'Kode Materi {i} tidak valid')
            rpp.idMateri = listIdMateri

            if rpp.isAkses.upper() == "YA" or rpp.isAkses.upper() == "YES":
                rpp.isAkses = True
            else:
                rpp.isAkses = False

            if rpp.isUjian.upper() == "YA" or rpp.isUjian.upper() == "YES":
                rpp.isUjian = True
            else:
                rpp.isUjian = False


            if rpp.tipe.lower() not in ['harian', 'uas', 'uts']:
                error += 1
                listError.append('Tipe tidak valid')
            else:
                rpp.tipe = getString(row['Tipe']).lower()
            
            rpp.jumlahSoal = getString(row['Jumlah Soal'])
            rpp.bobot = getString(row['Bobot'])
            rpp.jumlahSoal = int(rpp.jumlahSoal)
            rpp.bobot = int(rpp.bobot)
            
            tgl = getDate(tanggal)
            if not tgl:
                error += 1
                listError.append('Tanggal tidak valid, ikuti format YYYY-MM-DD, contoh: 2020-12-01')
            else:
                tgl = str(tgl.date())
                jamMulai = tgl + ' ' + jamMulai
                jamSelesai = tgl + ' ' + jamSelesai
                rpp.jamMulai = datetime.strptime(jamMulai, '%Y-%m-%d %H:%M:%S')
                rpp.jamSelesai = datetime.strptime(jamSelesai, '%Y-%m-%d %H:%M:%S')

            if kodeRuang:
                code = await MGDB.edmedia_ruang.find_one({"kode": kodeRuang})
                if code:
                    rpp.idRuang = code['_id']
                    rpp.namaRuang = code['name']
            
            nip = await MGDB.user_edmedia_pengajar.find_one({"noId": nip})
            if nip:
                rpp.pengajar = {
                'userId': nip['userId'],
                'name': nip['name'],
                'nomorIdentitas': nip['noId']
                }
            else:
                error += 1
                listError.append('NIP Pengajar tidak terdaftar')   

            if error > 0:
                rowNonvalid += 1
                unValid.append({
                    "pertemuanKe": row["Pertemuan"],
                    "tanggal": row["Tanggal"],
                    "jamMulai": row["Jam Mulai"],
                    "jamSelesai": row["Jam Selesai"],
                    "codeMateri": row["Kode Materi"],
                    "note": listError
                })
            else:
                rowValid += 1
                result.append({
                    'pertemuanKe': pertemuanKe-1,
                    'update': rpp.dict(skip_defaults=True)
                    })

    updt = {}
    for data in result:
        for x,y in data['update'].items():
            updt[f"rpp.{data['pertemuanKe']}.{x}"] = y

    rpp_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": updt}
    )

    #aneh
    print(updt)
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


# =================================================================================
#komen

@router_rpp.post("/comment/{id_}/{pertemuanKe}")
async def add_comment_by_id_kbm_by_pertemuan(id_: str, pertemuanKe: int, data_in: CommentBase, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pelajar,pengajar", "*"])):
    pelajar = await MGDB.user_edmedia_pelajar.find_one({"companyId": ObjectId(current_user.companyId), "userId": ObjectId(current_user.userId)})
    if pelajar:
        commenter = pelajar['name'] # komen pelajar
        print("ada pelajar")
    else:
        pengajar = await MGDB.user_edmedia_pengajar.find_one({"companyId": ObjectId(current_user.companyId), "userId": ObjectId(current_user.userId)})
        if pengajar:
            commenter = pengajar['name'] # komen pengajar
            print("ada pengajar")
        else:
            commenter = "Anonymous"
            # raise HTTPException(status_code=400, detail="Anda tidak terdaftar sebagai Pelajar maupun Pengajar")

    data_in.id = ObjectId()
    data_in.createTime = dateTimeNow()
    data_in.userId = ObjectId(current_user.userId)
    data_in.name = commenter
    rpp = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    print(rpp)
    if rpp:
        pertemuan = pertemuanKe-1
        rpp_op = await dbase.update_one(
            {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
            {"$push": {f"rpp.{pertemuan}.komen": data_in.dict(skip_defaults=True)}}
        )
    if rpp_op.modified_count:
        # return await GetRppOr404(id_)
        return {"status": "ok"}
    else:
        raise HTTPException(status_code=304)


@router_rpp.post("/reply/{id_}/{pertemuanKe}/{idKomen}")
async def add_reply_by_id_kbm_by_pertemuan(id_: str, pertemuanKe: int, idKomen: str, data_in: CommentReply, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pelajar,pengajar", "*"])):
    pelajar = await MGDB.user_edmedia_pelajar.find_one({"companyId": ObjectId(current_user.companyId), "userId": ObjectId(current_user.userId)})
    if pelajar:
        commenter = pelajar['name'] # komen pelajar
        print("ada pelajar")
    else:
        pengajar = await MGDB.user_edmedia_pengajar.find_one({"companyId": ObjectId(current_user.companyId), "userId": ObjectId(current_user.userId)})
        if pengajar:
            commenter = pengajar['name'] # komen pengajar
            print("ada pengajar")
        else:
            commenter = "Anonymous"
            # raise HTTPException(status_code=400, detail="Anda tidak terdaftar sebagai Pelajar maupun Pengajar")

    data_in.id = ObjectId()
    data_in.createTime = dateTimeNow()
    data_in.userId = ObjectId(current_user.userId)
    data_in.name = commenter
    rpp = await dbase.find_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, {'rpp': {'$elemMatch': {'pertemuanKe': pertemuanKe}}})
    if rpp:
        pertemuan = pertemuanKe-1
        rpp_op = await dbase.update_one(
            {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
            {"$push": {f"rpp.{pertemuan}.komen.$[id].reply": data_in.dict(skip_defaults=True)}},
            upsert=False,
            array_filters=[
                {"id.id": ObjectId(idKomen)}
            ]
        )
    if rpp_op.modified_count:
        # return await GetRppOr404(id_)
        return {"status": "ok"}
    else:
        raise HTTPException(status_code=304)


@router_rpp.get("/comment/{id_}/{pertemuanKe}/{idKomen}", response_model=CommentBase)
async def get_comment_by_id_kbm_by_pertemuan(id_: str, pertemuanKe: int, idKomen: str, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pelajar,pengajar", "*"])):
    pipeline = [
        {'$match': {
            'companyId': ObjectId(current_user.companyId),
            '_id': ObjectId(id_)
            }},
        {'$unwind': {'path': '$rpp'}},
        {'$unwind': {'path': '$rpp.komen'}},
        {'$match': {'rpp.pertemuanKe': pertemuanKe, 'rpp.komen.id': ObjectId(idKomen)}}
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    return cdatas[0]['rpp']['komen']


@router_rpp.post("/get_activity", response_model=dict)
async def get_activity(size: int = 10, page: int = 0, sort: str = "rpp.jamMulai", dir : int = -1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin","*"])):
    criteria = CreateCriteria(search)
    print(criteria)
    
    pipeline = [
        {'$unwind': {'path': '$rpp'}},
        {'$match': {
            'companyId': ObjectId(current_user.companyId)
        }},
        {'$match': criteria},
        {'$sort': {sort: dir}}
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listrpp = cdatas
    skip = page * size
    datas = listrpp[skip:skip+size]
    totalElements = len(listrpp)
    totalPages = math.ceil(totalElements / size)
    reply = RppPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_rpp.post("/absensi/{companyId}/data_absensi.xls")
async def download_absensi(companyId: str, dateStart: Optional[str] = None, dateEnd: Optional[str] = None, search : SearchRequest = None):
    'format date: yyyy-mm-dd'
    criteria = CreateCriteria(search)
    tglAwal = dateStart
    tglAkhir = datetime.strptime(dateEnd, '%Y-%m-%d') + timedelta(days=1)
    tglAkhir = datetime.strftime(tglAkhir, '%Y-%m-%d')
    dateStart = {"rpp.jamMulai": {"$gte": datetime.fromisoformat(tglAwal)}}
    dateEnd = {"rpp.jamMulai": {"$lte": datetime.fromisoformat(tglAkhir)}}
    criteria['$and'].append(dateStart)
    criteria['$and'].append(dateEnd)

    pipeline = [
    {
        '$unwind': {
            'path': '$rpp'
        }
    }, {
        '$match': {
            'companyId': ObjectId(companyId)
        }
    }, {
        '$match': criteria
    }, {
        '$unwind': {
            'path': '$rpp.pesertaAbsen'
        }
    }, {
        '$project': {
            'namaMapel': 1, 
            'kelas': 1, 
            'kelasDetail': 1, 
            'tahun': 1, 
            'semester': 1, 
            'pertemuanKe': '$rpp.pertemuanKe', 
            'tipe': '$rpp.tipe', 
            'nama': '$rpp.pesertaAbsen.name', 
            'nis': '$rpp.pesertaAbsen.nomorIdentitas', 
            'isAbsen': '$rpp.pesertaAbsen.isAbsen'
        }
    }
    ]

    cursor_absensi = dbase.aggregate(pipeline)
    absensi = await cursor_absensi.to_list(100000)

    if absensi:
        df = pd.DataFrame(list(absensi))
        df = df.rename_axis('No').reset_index()
        df['No'] = df['No'] + 1
        with BytesIO() as b:
            # Use the StringIO object as the filehandle.
            writer = pd.ExcelWriter(b, engine='xlsxwriter')
            df.to_excel(writer, sheet_name='data_absensi', index=False)
            writer.save()
            return Response(content=b.getvalue(), media_type='application/vnd.ms-excel')
    else:
        raise HTTPException(status_code=400, detail="Data Not Found")


@router_rpp.post("/hasil_pengerjaan/{companyId}/report.xls")
async def download_hasil_pengerjaan(companyId: str, kelasDetail: Optional[str] = None, idSilabus: Optional[str] = None, pertemuanKe: Optional[int] = None, search : SearchRequest = None):
    criteria = CreateCriteria(search)
    if kelasDetail:
        criteria['$and'].append({"dataKbm.kelasDetail": kelasDetail})
    
    if idSilabus:
        criteria['$and'].append({"dataKbm.idSilabus": ObjectId(idSilabus)})
    
    if pertemuanKe:
        criteria['$and'].append({"dataKbm.pertemuanKe": pertemuanKe})

    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(companyId)
        }
    }, {
        '$unwind': {
            'path': '$dataKbm'
        }
    }, {
        '$match': criteria
    }, {
        '$lookup': {
            'from': 'edmedia_silabus', 
            'localField': 'dataKbm.idSilabus', 
            'foreignField': '_id', 
            'as': 'silabus'
        }
    }, {
        '$unwind': {
            'path': '$silabus'
        }
    }, {
        '$addFields': {
            'namaMapel': '$silabus.namaMapel'
        }
    }, {
        '$project': {
            '_id': 0, 
            'namaMapel': '$namaMapel', 
            'tahun': '$dataKbm.tahun', 
            'semester': '$dataKbm.semester', 
            'pertemuanKe': '$dataKbm.pertemuanKe', 
            'kelasDetail': '$dataKbm.kelasDetail', 
            'nama': '$nama', 
            'nis': '$noId', 
            'soalDone': '$dataKbm.soalDone', 
            'tugasDone': '$dataKbm.tugasDone', 
            'nilaiSoal': '$dataKbm.nilaiSoal',
            'nilaiTugas': '$dataKbm.nilaiTugas'
        }
    }, {
        '$sort': {
            'namaMapel': 1, 
            'tahun': 1, 
            'semester': 1, 
            'pertemuanKe': 1, 
            'kelasDetail': 1, 
            'nama': 1
        }
    }
    ]

    datas_cursor = MGDB.user_edmedia_pelajar.aggregate(pipeline)
    datas = await datas_cursor.to_list(100000)

    if datas:
        df = pd.DataFrame(list(datas))
        df = df.rename_axis('No').reset_index()
        df['No'] = df['No'] + 1
        with BytesIO() as b:
            # Use the StringIO object as the filehandle.
            writer = pd.ExcelWriter(b, engine='xlsxwriter')
            df.to_excel(writer, sheet_name='hasil_pengerjaan', index=False)
            writer.save()
            return Response(content=b.getvalue(), media_type='application/vnd.ms-excel')
    else:
        raise HTTPException(status_code=400, detail="Data Not Found")


@router_rpp.post("/absensi/mapel/{companyId}/{idSilabus}")
async def absensi_per_mapel(companyId: str, idSilabus: str, search : SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pengajar", "*"])):
    criteria = CreateCriteria(search)
    print(criteria)
    
    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(companyId),
            'idSilabus': ObjectId(idSilabus)
        }
    }, {
        '$unwind': {
            'path': '$rpp'
        }
    }, {
        '$unwind': {
            'path': '$rpp.pesertaAbsen'
        }
    }, {
        '$project': {
            '_id': 1, 
            'kelas': 1, 
            'kelasDetail': 1, 
            'namaMapel': 1, 
            'idSilabus': 1, 
            'tahun': 1, 
            'semester': 1, 
            'rpp.pesertaAbsen': 1, 
            'pertemuan': '$rpp.pertemuanKe', 
            'tgl': {
                '$dateToString': {
                    'format': '%Y-%m-%d', 
                    'date': '$rpp.jamMulai'
                }
            }, 
            'nama': '$rpp.pesertaAbsen.nama', 
            'hari': {
                '$isoDayOfWeek': '$rpp.jamMulai'
            }
        }
    }, {
        '$match': criteria
    }, {
        '$sort': {
            'nama': 1, 
            'pertemuan': 1
        }
    }
    ]
    cursor = dbase.aggregate(pipeline)
    data_absen = await cursor.to_list(100000)
    return data_absen


@router_rpp.get("/get_tanggal_awal_minggu/{tanggal}")
async def get_tanggal_awal_minggu(tanggal: str):
    tgl = datetime.strptime(tanggal, "%Y-%m-%d")
    tglAwal = tgl - timedelta(days=tgl.weekday() % 7)
    return tglAwal.strftime("%Y-%m-%d")


@router_rpp.get("/absensi/tanggal/{companyId}/{tanggal}", response_model=list)
async def absensi_per_tanggal(companyId: str, tanggal: str, kelasDetail: Optional[str] = None, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pengajar", "*"])):
    tgl = datetime.strptime(tanggal, "%Y-%m-%d")
    tglAwal = tgl - timedelta(days=tgl.weekday() % 7)
    tglAkhir = tglAwal + timedelta(6)
    tglAwal = str(tglAwal.date())
    tglAkhir = str(tglAkhir.date())
    match = {'tgl': {
                '$gte': tglAwal, 
                '$lte': tglAkhir
        }
    }
    if kelasDetail:
        match['kelasDetail'] = kelasDetail
    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(companyId)
        }
    }, {
        '$unwind': {
            'path': '$rpp'
        }
    }, {
        '$unwind': {
            'path': '$rpp.pesertaAbsen'
        }
    }, {
        '$addFields': {
            'count': 1
        }
    }, {
        '$project': {
            '_id': 0, 
            'kelas': 1, 
            'kelasDetail': 1, 
            'rpp.pesertaAbsen': 1, 
            'rpp.jamMulai': 1, 
            'tgl': {
                '$dateToString': {
                    'format': '%Y-%m-%d', 
                    'date': '$rpp.jamMulai'
                }
            }, 
            'hari': {
                '$isoDayOfWeek': '$rpp.jamMulai'
            }, 
            'count': 1, 
            'absen': {
                '$cond': [
                    {
                        '$eq': [
                            '$rpp.pesertaAbsen.isAbsen', True
                        ]
                    }, 1, 0
                ]
            }
        }
    }, {
        '$group': {
            '_id': {
                'tgl': '$tgl', 
                'dateTime': '$rpp.jamMulai', 
                'nama': '$rpp.pesertaAbsen.nama', 
                'nis': '$rpp.pesertaAbsen.nomorIdentitas', 
                'kelas': '$kelas', 
                'kelasDetail': '$kelasDetail'
            }, 
            'tot': {
                '$sum': '$count'
            }, 
            'absensi': {
                '$sum': '$absen'
            }
        }
    }, {
        '$project': {
            '_id': 0, 
            'tgl': '$_id.tgl', 
            'nama': '$_id.nama', 
            'nis': '$_id.nis', 
            'kelas': '$_id.kelas', 
            'kelasDetail': '$_id.kelasDetail', 
            'tot': 1, 
            'absensi': 1
        }
    }, {
        '$match': match
    }, {
        '$sort': {
            'tgl': 1, 
            'nama': 1
        }
    }
    ]
    cursor = dbase.aggregate(pipeline)
    data_absen = await cursor.to_list(100000)
    return data_absen


@router_rpp.post("/nilai/mapel/{companyId}/{idSilabus}")
async def nilai_per_mapel(companyId: str, idSilabus: str, search : SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pengajar", "*"])):
    criteria = CreateCriteria(search)
    criteria['dataKbm.idSilabus'] = ObjectId(idSilabus)
    print(criteria)
    
    pipeline = [
    {
        '$match': {
            'companyId': ObjectId(companyId)
        }
    }, {
        '$unwind': {
            'path': '$dataKbm'
        }
    }, {
        '$project': {
            'userId': 1, 
            'name': 1, 
            'noId': 1, 
            'kelasNow': 1, 
            'kelasDetailNow': 1, 
            'dataKbm': 1, 
            'pertemuan': '$dataKbm.pertemuanKe',
            'tgl': {
                '$dateToString': {
                    'format': '%Y-%m-%d', 
                    'date': '$dataKbm.waktu'
                }
            }
        }
    }, {
        '$match': criteria
    }, {
        '$sort': {
            'nama': 1, 
            'pertemuan': 1
        }
    }
    ]
    cursor = dbase.aggregate(pipeline)
    data_absen = await cursor.to_list(100000)
    return data_absen