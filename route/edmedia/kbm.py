# backend/tancho/kbms/routes.py

from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import logging
import math

from model.edmedia.kbm import GenerateKbm, KbmBase, KbmOnDB, KbmPage, RppBase
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import RandomString, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_kbm = APIRouter()
dbase = MGDB.edmedia_kbm

async def GetKbmOr404(id_: str):
    _id = ValidateObjectId(id_)
    kbm = await dbase.find_one({"_id": _id})
    if kbm:
        return kbm
    else:
        raise HTTPException(status_code=404, detail="Kbm not found")

# =================================================================================

@router_kbm.post("/manual_generate_kbm", response_model=dict)
async def manual_generate_kbm(data_in: GenerateKbm, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    #nabrakin data kelas dan data silabus
    kabm = KbmBase()
    kbm = IsiDefault(kabm,current_user,True)
    kbm.tahun = data_in.tahun
    kbm.kelas = data_in.kelas
    kbm.kelasDetail = data_in.kelasDetail
    kbm.semester = data_in.semester
    kbm.programStudi = data_in.programStudi
    kbm.jurusan = data_in.jurusan
    kbm.kapasitas = data_in.kapasitas
    kbm.pelajar = []
    cPelajar = MGDB.user_edmedia_pelajar.find({
        "companyId": ObjectId(current_user.companyId), "programStudi": kbm.programStudi, "jurusan": kbm.jurusan, "kelasNow": kbm.kelas, "kelasDetailNow": kbm.kelasDetail, "tahunNow": kbm.tahun, "semesterNow": str(kbm.semester)})
    pelajar = await cPelajar.to_list(length=1000)
    for id in pelajar:
        kbm.pelajar.append({'userId': id['userId'], 'name': id['name'], 'nomorIdentitas': id['noId']})

    cSilabus = MGDB.edmedia_silabus.find({
        "companyId": ObjectId(current_user.companyId), 
        "programStudi": kbm.programStudi,
        "jurusan": kbm.jurusan,
        "kelas": kbm.kelas,
        "semester": kbm.semester,
        "tahun": kbm.tahun
        })
    datas = await cSilabus.to_list(length=100)
    for x in datas:
        kbm.namaMapel = x['namaMapel']
        ckbm = await dbase.find_one({
            "companyId": ObjectId(current_user.companyId),
            "tahun": kbm.tahun,
            "semester": kbm.semester,
            "kelas": kbm.kelas,
            "kelasDetail": kbm.kelasDetail,
            "namaMapel": kbm.namaMapel,
            "programStudi": kbm.programStudi,
            "jurusan": kbm.jurusan
            })
        if ckbm:
            print("sudah ada")
        else:
            print("belum ada")
            kbm.joinCode = RandomString()
            kbm.idSilabus = x['_id']

            kbm.isExcul = x['isExcul']
            kbm.jumlahPertemuan = x['jumlahPertemuan']
            kbm.pengajar = []
            if x['pengajar']:
                for id in x['pengajar']:
                    cPengajar = await MGDB.user_edmedia_pengajar.find_one({"companyId": ObjectId(current_user.companyId), "_id": ObjectId(id)})
                    kbm.pengajar.append({'userId': cPengajar['userId'], 'name': cPengajar['name'], 'nomorIdentitas': cPengajar['noId']})
            else:
                kbm.pengajar = []
            kbm.rpp = []
            for y in range(0, kbm.jumlahPertemuan):
                rpp = RppBase()
                rpp.pertemuanKe = y+1
                kbm.rpp.append(rpp)

            kbm.tags = x['tags']
            kbm.isAktif = True
            dbase.insert_one(kbm.dict())
    
    return {"status":"ok"}

@router_kbm.get("/auto_generate_kbm/{tahun}/{semester}", response_model=dict)
async def auto_generate_kbm(tahun: str, semester:str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    cKelas = MGDB.edmedia_kelas.find({
        "companyId": ObjectId(current_user.companyId),
        "tahun": tahun,
        "semester":semester
    })
    datas = await cKelas.to_list(length=1000)
    gKbm = GenerateKbm()
    for x in datas:
        gKbm.tahun = tahun
        gKbm.semester = semester
        gKbm.kelas = x['kelas']
        gKbm.kelasDetail = x['kelasDetail']
        gKbm.kapasitas = x['kapasitas']
        await manual_generate_kbm(gKbm,current_user)

    return {"status":"ok"}

@router_kbm.post("/get_kbm", response_model=dict)
async def get_all_kbms(size: int = 10, page: int = 0, sort: str = "tahun", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort([("kelasDetail", dir), ("namaMapel", dir), (sort, dir), ("semester", dir)])
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = KbmPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_kbm.get("/kbm/{id_}", response_model=KbmOnDB)
async def get_kbm_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    kbm = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if kbm:
        return kbm
    else:
        raise HTTPException(status_code=404, detail="Kbm not found")


@router_kbm.delete("/kbm/{id_}", dependencies=[Depends(GetKbmOr404)], response_model=dict)
async def delete_kbm_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    kbm_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if kbm_op.deleted_count:
        return {"status": f"deleted count: {kbm_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_kbm.put("/kbm/{id_}", response_model=KbmOnDB)
async def update_kbm(id_: str, data_in: KbmBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # kbm = IsiDefault(data_in, current_user, True)
    kbm = data_in
    kbm.updateTime = dateTimeNow()
    if kbm.jumlahPelajar > kbm.kapasitas:
        raise HTTPException(status_code=400, detail="Jumlah pelajar melebihi Kapasitas")
    else:
        kbm_op = await dbase.update_one(
            {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
            {"$set": kbm.dict(skip_defaults=True)}
        )
    if kbm_op.modified_count or kbm_op.matched_count:
        return await GetKbmOr404(id_)
    else:
        raise HTTPException(status_code=304)