# backend/tancho/materis/routes.py

from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.edmedia.materi import MateriBase, MateriBasic, MateriOnDB, MateriPage, SoalKunci, TugasBase, TugasDijawab, comboMateri, detailMateriPage, detailSoalPage, detailTugasPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ConvertToMgdb, ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_materi = APIRouter()
dbase = MGDB.edmedia_materi

async def GetMateriOr404(id_: str):
    _id = ValidateObjectId(id_)
    materi = await dbase.find_one({"_id": _id})
    if materi:
        return materi
    else:
        raise HTTPException(status_code=404, detail="Materi not found")

# =================================================================================

@router_materi.post("/materi/{idSilabus}", response_model=MateriOnDB)
async def add_materi(data_in: MateriBase, idSilabus: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    materi = IsiDefault(data_in, current_user)
    materi.idSilabus = ObjectId(idSilabus)
    materi.topik = materi.topik
    materi.subTopik = materi.subTopik.upper()
    materi.judul = materi.judul.upper()
    materi.subJudul = materi.subJudul.capitalize()
    materi.tags = ListToUp(materi.tags)
    cmateri = await dbase.find_one({
        "companyId": ObjectId(current_user.companyId), 
        "idSilabus": ObjectId(idSilabus),
        "topik":materi.topik,
        "subTopik":materi.subTopik
    })
    print(cmateri)
    if cmateri:
        raise HTTPException(status_code=400, detail="Materi is registered")
    print(materi)
    namaMapel = await MGDB.edmedia_silabus.find_one({"_id": ObjectId(idSilabus)})
    mapel = namaMapel['namaMapel'].split()
    if len(mapel) == 1:
        mapel_word = mapel[0][:2] + mapel[0][-2:]
    elif len(mapel) > 1:
        mapel_word = ''
        for i in mapel:
            mapel_word += i[:1] + i[-1:]
    materi.codeMateri = mapel_word + '.' + materi.subTopik
    materi_op = await dbase.insert_one(materi.dict())
    if materi_op.inserted_id:
        materi = await GetMateriOr404(materi_op.inserted_id)
        return materi

@router_materi.post("/upload_materi/{idSilabus}")
async def upload_materi(idSilabus: str, file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file, converters={'Sub BAB': str})
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    data_in = MateriBase()
    for index,row in df.iterrows():
        materi = IsiDefault(data_in, current_user)
        materi.idSilabus = ObjectId(idSilabus)
        materi.topik = getString(row['BAB'])
        materi.subTopik = getString(row['Sub BAB'])
        materi.judul = getString(row['Judul'])
        materi.subJudul = getString(row['Sub Judul'])
        if None in [materi.topik, materi.subTopik, materi.judul, materi.subJudul]:
            raise HTTPException(status_code=404, detail="Bab, Sub Bab, Judul, dan Sub Judul harus diisi")
        else:
            materi.topik = getNumber(row['BAB'])
            materi.subTopik = getString(row['Sub BAB'])
            if materi.subTopik:
                materi.subTopik = getString(row['Sub BAB'].upper())
            materi.judul = getString(row['Judul'])
            if materi.judul:
                materi.judul = getString(row['Judul'].upper())
            materi.subJudul = getString(row['Sub Judul'])
            if materi.subJudul:
                materi.subJudul = getString(row['Sub Judul'].capitalize())
            materi.tags = getString(row['Tags'])
            if materi.tags:
                materi.tags = getString(row['Tags']).split(",")
                materi.tags = ListToUp(materi.tags)
            else:
                materi.tags = []
            print(materi)

            cmateri = await dbase.find_one({
                "companyId": ObjectId(current_user.companyId), 
                "idSilabus": ObjectId(idSilabus),
                "topik":materi.topik,
                "subTopik":materi.subTopik
            })

            namaMapel = await MGDB.edmedia_silabus.find_one({"_id": ObjectId(idSilabus)})
            mapel = namaMapel['namaMapel'].split()
            if len(mapel) == 1:
                mapel_word = mapel[0][:2] + mapel[0][-2:]
            elif len(mapel) > 1:
                mapel_word = ''
                for i in mapel:
                    mapel_word += i[:1] + i[-1:]
            materi.codeMateri = mapel_word + '.' + materi.subTopik

            if cmateri:
                rowNonvalid = rowNonvalid + 1
                unValid.append({
                    "topik":materi.topik,
                    "subTopik":materi.subTopik,
                    "note":"telah terdaftar"
                    })
                pass
            else:
                rowValid = rowValid + 1
                result.append(materi.dict())
    #aneh
    print(result)
    dbase.insert_many(result)
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}

@router_materi.post("/get_materi", response_model=dict)
async def get_all_materis(size: int = 10, page: int = 0, sort: str = "topik", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort([(sort, dir), ("subTopik", dir), ("judul", dir), ("subJudul", dir)])
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = MateriPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_materi.get("/materi/{id_}", response_model=MateriOnDB)
async def get_materi_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    materi = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if materi:
        return materi
    else:
        raise HTTPException(status_code=404, detail="Materi not found")


@router_materi.delete("/materi/{id_}", dependencies=[Depends(GetMateriOr404)], response_model=dict)
async def delete_materi_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    materi_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if materi_op.deleted_count:
        return {"status": f"deleted count: {materi_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_materi.put("/materi/{id_}", response_model=MateriOnDB)
async def update_materi(id_: str, data_in: MateriBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # materi = IsiDefault(data_in, current_user, True)
    materi = data_in
    materi.updateTime = dateTimeNow()
    # materi.materi = None
    # materi.tugas = None
    # materi.soal = None
    if materi.idSilabus:
        materi.idSilabus = ObjectId(materi.idSilabus)
    materi_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": materi.dict(skip_defaults=True)}
    )
    if materi_op.modified_count or materi_op.matched_count:
        return await GetMateriOr404(id_)
    else:
        raise HTTPException(status_code=304)

# =================================================================================

@router_materi.post("/add_detail_materi/{idMateri}/{topik}/{subTopik}", response_model=MateriBasic)
async def add_detail_materi(idMateri: str, topik:int, subTopik:str, dmteri: MateriBasic, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    dmteri.id = ObjectId()
    dmteri.judul = dmteri.judul.capitalize()
    if dmteri.materi == "-":
        dmteri.materi = None
    if dmteri.link == ["-"]:
        dmteri.link = [] 
    cmateri = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId), 
        "materi.judul": dmteri.judul})
    if cmateri:
        raise HTTPException(status_code=400, detail="DetailMateri is registered")
    print( {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        {"$addToSet": {"materi":dmteri.dict()}})
    materi_op = await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        {"$addToSet": {"materi":dmteri.dict()}}
    )
    if materi_op.modified_count or materi_op.matched_count:
        return dmteri
    else:
        raise HTTPException(status_code=400, detail="Add Data Failed")

@router_materi.delete("/delete_detail_materi/{idMateri}/{topik}/{subTopik}/{idDetailMateri}")
async def delete_detail_materi(idMateri: str, topik:int, subTopik:str, idDetailMateri: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):

    cmateri = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId), 
        "materi.id": ObjectId(idDetailMateri)})
    if not cmateri:
        raise HTTPException(status_code=404, detail="Data not found")
    
    await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        {"$pull": {"materi":{"id":ObjectId(idDetailMateri)}}}
    )
    return {"status": "Delete OK"}

@router_materi.put("/update_detail_materi/{idMateri}/{topik}/{subTopik}/{idDetailMateri}", response_model=MateriBasic)
async def update_detail_materi(idMateri: str, topik:int, subTopik:str, idDetailMateri: str,  dmteri: MateriBasic, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # dmteri.pop('id', None)
    # dmteri.pop('comment', None)
    if dmteri.judul is not None: dmteri.judul = dmteri.judul.capitalize()
    cmateri = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId), 
        "materi.id": ObjectId(idDetailMateri)})
    if not cmateri:
        raise HTTPException(status_code=404, detail="Data not found")
    
    data = ConvertToMgdb("materi",dmteri.dict(skip_defaults=True))
    print(data)
    materi_op = await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik,"materi.id":ObjectId(idDetailMateri)}, 
        {"$set": data}
    )
    if materi_op.modified_count or materi_op.matched_count:
        return dmteri
    else:
        raise HTTPException(status_code=400, detail="Update failed")

@router_materi.post("/upload_detail_materi/{idMateri}/{topik}/{subTopik}")
async def upload_detail_materi(idMateri: str, topik:int, subTopik:str, file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    dmteri = MateriBasic()
    for index,row in df.iterrows():
        dmteri.id = ObjectId()
        dmteri.urutan = getNumber(row['Urutan'])
        dmteri.judul = getString(row['Judul'])
        dmteri.materi = getString(row['Materi'])
        dmteri.target = getString(row['Target'])
        dmteri.summary = getString(row['Ringkasan'])
        dmteri.link = getString(row['Link Referensi'])
        if None in [dmteri.urutan, dmteri.judul, dmteri.target, dmteri.summary]:
            raise HTTPException(status_code=404, detail="Urutan, Judul, Target, dan Ringkasan harus diisi. Materi dan Link Referensi bisa diisi salah satu")
        elif dmteri.materi == None and dmteri.link == None:
            raise HTTPException(status_code=404, detail="Urutan, Judul, Target, dan Ringkasan harus diisi. Materi dan Link Referensi bisa diisi salah satu")
        else:
            dmteri.urutan = int(getNumber(row['Urutan']))
            dmteri.judul = getString(row['Judul'].upper())
            dmteri.materi = getString(row['Materi'])
            if dmteri.materi == "-":
                dmteri.materi = None
            dmteri.target = getString(row['Target'])
            if dmteri.target:
                dmteri.target = getString(row['Target']).split(",")
                dmteri.target = ListToUp(dmteri.target)
            dmteri.summary = getString(row['Ringkasan'])
            if dmteri.summary:
                dmteri.summary = getString(row['Ringkasan']).split(",")
                dmteri.summary = ListToUp(dmteri.summary)
            dmteri.link = getString(row['Link Referensi'])
            if dmteri.link:
                if dmteri.link == ["-"]:
                    dmteri.link = []
                else: 
                    dmteri.link = getString(row['Link Referensi']).split(",")
                    dmteri.link = ListToUp(dmteri.link)

            cmateri = await dbase.find_one({
                "_id": ObjectId(idMateri), 
                "companyId": ObjectId(current_user.companyId), 
                "materi.judul": dmteri.judul})
            if cmateri:
                rowNonvalid = rowNonvalid + 1
                unValid.append({
                    "judul":dmteri.judul,
                    "note":"telah terdaftar"
                    })
                pass
            else:
                rowValid = rowValid + 1
                result.append(dmteri.dict())
    #aneh
    print(result)
    materi_op = await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        # {"$addToSet": {"materi":result}}
        {"$addToSet": {"materi": {"$each": result}}}
    )
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


@router_materi.post("/get_detail/{idMateri}", response_model=dict)
async def get_detail_materi_by_id_materi(idMateri: str, size: int = 10, page: int = 0, sort: str = "urutan", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    search.defaultObjectId.append(FieldObjectIdRequest(field='_id',key=ObjectId(idMateri)))    
    criteria = CreateCriteria(search)
    criteria['$and'].pop(0)
    print(criteria)
    pipeline = [
        {'$match': {
            'companyId': ObjectId(current_user.companyId), 
            '_id': ObjectId(idMateri)
        }},
        {'$project': {'tugas': 0, 'soal': 0, 'tags': 0}},
        {'$unwind': {'path': '$materi'}},
        {'$match': criteria}, 
        {'$project': {
            'id': '$materi.id', 
            'urutan': '$materi.urutan', 
            'judul': '$materi.judul', 
            'target': '$materi.target', 
            'materi': '$materi.materi', 
            'summary': '$materi.summary', 
            'link': '$materi.link', 
            'imageUrl': '$materi.imageUrl', 
            'comment': '$materi.comment'}},
        {'$sort': {sort: dir}}        
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listmateri = cdatas
    datas = listmateri[skip:skip+size]
    totalElements = len(listmateri)
    totalPages = math.ceil(totalElements / size)
    reply = detailMateriPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_materi.post("/get_detail", response_model=dict)
async def get_all_detail_materi(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria, {'tugas': 0, 'soal': 0, 'tags': 0}).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = MateriPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_materi.get("/get_detail/{idDetailMateri}", response_model=MateriBasic)
async def get_detail_materi_by_id_detail_materi(idDetailMateri: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    data = dbase.find({'materi': {'$elemMatch': {'id': ObjectId(idDetailMateri)}}}, {'materi': {'$elemMatch': {'id': ObjectId(idDetailMateri)}}})
    detail = await data.to_list(100)
    if detail:
        return detail[0]['materi'][0]
    else:
        raise HTTPException(status_code=404, detail="detailMateri not found")

# =================================================================================

@router_materi.post("/add_detail_tugas/{idMateri}/{topik}/{subTopik}", response_model=TugasBase)
async def add_detail_tugas(idMateri: str, topik:int, subTopik:str, dmteri: TugasBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    dmteri.id = ObjectId()
    ctugas = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId), 
        "tugas.tugas": dmteri.tugas})
    if ctugas:
        raise HTTPException(status_code=400, detail="Tugas is registered")
    
    await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        {"$addToSet": {"tugas":dmteri.dict()}}
    )
    return dmteri

@router_materi.delete("/delete_detail_tugas/{idMateri}/{topik}/{subTopik}/{idDetailTugas}")
async def delete_detail_tugas(idMateri: str, topik:int, subTopik:str, idDetailTugas: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):

    ctugas = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId), 
        "tugas.id": ObjectId(idDetailTugas)})
    if not ctugas:
        raise HTTPException(status_code=404, detail="Data not found")
    
    await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        {"$pull": {"tugas":{"id":ObjectId(idDetailTugas)}}}
    )
    return {"status": "Delete OK"}

@router_materi.put("/update_detail_tugas/{idMateri}/{topik}/{subTopik}/{idDetailTugas}", response_model=TugasBase)
async def update_detail_tugas(idMateri: str, topik:int, subTopik:str, idDetailTugas: str,  dmteri: TugasBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # dmteri.pop('id', None)
    ctugas = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId), 
        "tugas.id": ObjectId(idDetailTugas)})
    if not ctugas:
        raise HTTPException(status_code=404, detail="Data not found")
    
    data = ConvertToMgdb("tugas",dmteri.dict(skip_defaults=True))
    print(data)
    tugas_op = await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik,"tugas.id":ObjectId(idDetailTugas)}, 
        {"$set": data}
    )
    if tugas_op.modified_count or tugas_op.matched_count:
        return dmteri
    else:
        raise HTTPException(status_code=400, detail="Update failed")
    
@router_materi.post("/upload_tugas/{idMateri}/{topik}/{subTopik}")
async def upload_tugas(idMateri: str, topik:int, subTopik:str, file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file, converters={'Tugas': str})
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    tugas = TugasBase()
    for index,row in df.iterrows():
        tugas.id = ObjectId()
        tugas.tugas = getString(row['Tugas'])
        tugas.point = getNumber(row['Point'])
        if None in [tugas.tugas, tugas.point]:
            raise HTTPException(status_code=404, detail="Tugas dan Point harus diisi")
        else:
            tugas.tugas = getString(row['Tugas'].upper())
            tugas.point = getNumber(row['Point'])

            cmateri = await dbase.find_one({
                "_id": ObjectId(idMateri), 
                "companyId": ObjectId(current_user.companyId), 
                "tugas.tugas": tugas.tugas})
            if cmateri:
                rowNonvalid = rowNonvalid + 1
                unValid.append({
                    "tugas":tugas.tugas,
                    "point":tugas.point,
                    "note":"telah terdaftar"
                    })
                pass
            else:
                rowValid = rowValid + 1
                result.append(tugas.dict())
    #aneh
    print(result)
    
    materi_op = await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        {"$addToSet": {"tugas": {"$each": result}}}
    )
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


@router_materi.post("/get_tugas/{idMateri}", response_model=dict)
async def get_detail_tugas_by_id_materi(idMateri: str, size: int = 10, page: int = 0, sort: str = "no", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    search.defaultObjectId.append(FieldObjectIdRequest(field='_id',key=ObjectId(idMateri)))    
    criteria = CreateCriteria(search)

    pipeline = [
        {'$match': criteria},
        {'$project': {'materi': 0, 'soal': 0, 'tags': 0}},
        {'$unwind': {'path': '$tugas'}},
        {'$sort': {sort: dir}}        
    ]

    pipeline = [
        {'$match': criteria},
        {'$project': {'materi': 0, 'soal': 0, 'tags': 0}},
        {'$unwind': {'path': '$tugas'}},
        {'$project': {
            'id': '$tugas.id', 
            'no': '$tugas.no', 
            'tugas': '$tugas.tugas', 
            'imageTugas': '$tugas.imageTugas', 
            'point': '$tugas.point'}},
        {'$sort': {sort: dir}}        
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listmateri = cdatas
    datas = listmateri[skip:skip+size]
    totalElements = len(listmateri)
    totalPages = math.ceil(totalElements / size)
    reply = detailTugasPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_materi.post("/get_tugas", response_model=dict)
async def get_all_detail_tugas(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria, {'materi': 0, 'soal': 0, 'tags': 0}).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = MateriPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_materi.get("/get_tugas/{idDetailTugas}", response_model=TugasDijawab)
async def get_detail_tugas_by_id_detail_tugas(idDetailTugas: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    data = dbase.find({'tugas': {'$elemMatch': {'id': ObjectId(idDetailTugas)}}}, {'tugas': {'$elemMatch': {'id': ObjectId(idDetailTugas)}}})
    detail = await data.to_list(100)
    if detail:
        return detail[0]['tugas'][0]
    else:
        raise HTTPException(status_code=404, detail="Tugas not found")

# =================================================================================

@router_materi.post("/add_detail_soal/{idMateri}/{topik}/{subTopik}", response_model=SoalKunci)
async def add_detail_soal(idMateri: str, topik:int, subTopik:str, dmteri: SoalKunci, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    dmteri.id = ObjectId()
    detail = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId)})
    if detail:
        dmteri.idMateri = ObjectId(idMateri)
        dmteri.idSilabus = detail["idSilabus"]
    csoal = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId), 
        "soal.pertanyaan": dmteri.pertanyaan})
    if csoal:
        raise HTTPException(status_code=400, detail="Soal is registered")
    
    await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        {"$addToSet": {"soal":dmteri.dict()}}
    )
    return dmteri

@router_materi.delete("/delete_detail_soal/{idMateri}/{topik}/{subTopik}/{idDetailSoal}")
async def delete_detail_soal(idMateri: str, topik:int, subTopik:str, idDetailSoal: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):

    csoal = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId), 
        "soal.id": ObjectId(idDetailSoal)})
    if not csoal:
        raise HTTPException(status_code=404, detail="Data not found")
    
    await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        {"$pull": {"soal":{"id":ObjectId(idDetailSoal)}}}
    )
    return {"status": "Delete OK"}

@router_materi.put("/update_detail_soal/{idMateri}/{topik}/{subTopik}/{idDetailSoal}", response_model=SoalKunci)
async def update_detail_soal(idMateri: str, topik:int, subTopik:str, idDetailSoal: str,  dmteri: SoalKunci, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # dmteri.pop('id', None)
    csoal = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId), 
        "soal.id": ObjectId(idDetailSoal)})
    if not csoal:
        raise HTTPException(status_code=404, detail="Data not found")
    
    data = ConvertToMgdb("soal",dmteri.dict(skip_defaults=True))
    print(data)
    soal_op = await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik,"soal.id":ObjectId(idDetailSoal)}, 
        {"$set": data}
    )
    if soal_op.modified_count or soal_op.matched_count:
        return dmteri
    else:
        raise HTTPException(status_code=400, detail="Update failed")

@router_materi.post("/upload_soal/{idMateri}/{topik}/{subTopik}")
async def upload_soal(idMateri: str, topik:int, subTopik:str, file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0

    detail = await dbase.find_one({
        "_id": ObjectId(idMateri), 
        "companyId": ObjectId(current_user.companyId)})
    if detail:
        cIdMateri = ObjectId(idMateri)
        cIdSilabus = detail["idSilabus"]

    soal = SoalKunci()
    for index,row in df.iterrows():
        soal.id = ObjectId()
        soal.pertanyaan = getString(row['Pertanyaan'])
        soal.opsiA = getString(row['opsiA'])
        soal.opsiB = getString(row['opsiB'])
        soal.opsiC = getString(row['opsiC'])
        soal.opsiD = getString(row['opsiD'])
        soal.opsiE = getString(row['opsiE'])
        soal.kunci = getString(row['Kunci'])
        if None in [soal.id, soal.pertanyaan, soal.opsiA, soal.opsiB, soal.opsiC, soal.opsiD, soal.opsiE, soal.kunci]:
            raise HTTPException(status_code=404, detail="Pertanyaan, Opsi Jawaban, dan Kunci harus diisi")
        else:
            soal.pertanyaan = getString(row['Pertanyaan'])
            soal.opsiA = getString(row['opsiA'])
            soal.opsiB = getString(row['opsiB'])
            soal.opsiC = getString(row['opsiC'])
            soal.opsiD = getString(row['opsiD'])
            soal.opsiE = getString(row['opsiE'])
            soal.kunci = getString(row['Kunci'])
            soal.idMateri = cIdMateri
            soal.idSilabus = cIdSilabus
            if soal.kunci not in ['opsiA', 'opsiB', 'opsiC', 'opsiD', 'opsiE']:
                rowNonvalid = rowNonvalid + 1
                unValid.append({
                    "soal":soal.pertanyaan,
                    "note":"Kunci tidak valid"
                    })
            else:
                cmateri = await dbase.find_one({
                    "_id": ObjectId(idMateri), 
                    "companyId": ObjectId(current_user.companyId), 
                    "soal.pertanyaan": soal.pertanyaan})
                if cmateri:
                    rowNonvalid = rowNonvalid + 1
                    unValid.append({
                        "soal":soal.pertanyaan,
                        "note":"telah terdaftar"
                        })
                    pass
                else:
                    rowValid = rowValid + 1
                    result.append(soal.dict())
    #aneh
    print(result)
    materi_op = await dbase.update_one(
        {"_id": ObjectId(idMateri),"companyId": ObjectId(current_user.companyId),"topik":topik,"subTopik":subTopik}, 
        {"$addToSet": {"soal": {"$each": result}}}
    )
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


@router_materi.post("/get_soal/{idMateri}", response_model=dict)
async def get_detail_soal_by_id_materi(idMateri: str, size: int = 10, page: int = 0, sort: str = "no", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    search.defaultObjectId.append(FieldObjectIdRequest(field='_id',key=ObjectId(idMateri)))    
    criteria = CreateCriteria(search)

    pipeline = [
        {'$match': criteria},
        {'$project': {'materi': 0, 'tugas': 0, 'tags': 0}},
        {'$unwind': {'path': '$soal'}},
        {'$project': {
            'id': '$soal.id', 
            'no': '$soal.no', 
            'pertanyaan': '$soal.pertanyaan', 
            'imageUrl': '$soal.imageUrl', 
            'opsiA': '$soal.opsiA', 
            'imageA': '$soal.imageA', 
            'opsiB': '$soal.opsiB', 
            'imageB': '$soal.imageB', 
            'opsiC': '$soal.opsiC', 
            'imageC': '$soal.imageC', 
            'opsiD': '$soal.opsiD', 
            'imageD': '$soal.imageD', 
            'opsiE': '$soal.opsiE', 
            'imageE': '$soal.imageE', 
            'kunci': '$soal.kunci', 
            'isChoice': '$soal.isChoice'}},
        {'$sort': {sort: dir}}        
    ]

    datas_cursor = dbase.aggregate(pipeline)
    cdatas = await datas_cursor.to_list(length=1000)
    listmateri = cdatas
    datas = listmateri[skip:skip+size]
    totalElements = len(listmateri)
    totalPages = math.ceil(totalElements / size)
    reply = detailSoalPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_materi.post("/get_soal", response_model=dict)
async def get_all_detail_soal(size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria, {'materi': 0, 'tugas': 0, 'tags': 0}).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = MateriPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_materi.get("/get_soal/{idDetailSoal}", response_model=SoalKunci)
async def get_detail_soal_by_id_detail_soal(idDetailSoal: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    data = dbase.find({'soal': {'$elemMatch': {'id': ObjectId(idDetailSoal)}}}, {'soal': {'$elemMatch': {'id': ObjectId(idDetailSoal)}}})
    detail = await data.to_list(100)
    if detail:
        return detail[0]['soal'][0]
    else:
        raise HTTPException(status_code=404, detail="Soal not found") 