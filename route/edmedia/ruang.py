# backend/tancho/ruangs/routes.py

from util.util_excel import getNumber, getString
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import logging
import math
import pandas as pd

from model.edmedia.ruang import RuangBase, RuangOnDB, RuangPage
from model.util import SearchRequest, FieldObjectIdRequest
from model.default import JwtToken
from util.util import ListToUp, ValidateObjectId, IsiDefault, CreateCriteria
from route.auth import get_current_user
from util.util_waktu import dateTimeNow

router_ruang = APIRouter()
dbase = MGDB.edmedia_ruang

async def GetRuangOr404(id_: str):
    _id = ValidateObjectId(id_)
    ruang = await dbase.find_one({"_id": _id})
    if ruang:
        return ruang
    else:
        raise HTTPException(status_code=404, detail="Ruang not found")

# =================================================================================

@router_ruang.post("/ruang", response_model=RuangOnDB)
async def add_ruang(data_in: RuangBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    ruang = IsiDefault(data_in, current_user)
    ruang.name = ruang.name.upper()
    cruang = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "name": ruang.name})
    if cruang:
        raise HTTPException(status_code=400, detail="Ruang with Name : "+ruang.name+" is registered")
    ruang.kode = ruang.kode.upper()
    ruang.tags = ListToUp(ruang.tags)
    ruang_op = await dbase.insert_one(ruang.dict())
    if ruang_op.inserted_id:
        ruang = await GetRuangOr404(ruang_op.inserted_id)
        return ruang

@router_ruang.post("/upload_ruang")
async def upload_ruang(file: UploadFile = File(...), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    df = pd.read_excel(file.file)
    print(df)
    result = []
    unValid = []
    rowValid = 0
    rowNonvalid = 0
    data_in = RuangBase()
    for index,row in df.iterrows():
        ruang = IsiDefault(data_in, current_user)
        ruang.kode = getString(str(row['Kode']))
        ruang.name = getString(row['Nama'])
        ruang.gedung = getString(row['Gedung'])
        ruang.kapasitas = getNumber(row['Kapasitas'])
        if None in [ruang.kode, ruang.name, ruang.gedung, ruang.kapasitas]:
            raise HTTPException(status_code=404, detail="Kode, Nama, Gedung, dan Kapasitas harus diisi")
        else:
            ruang.kode = getString(str(row['Kode']).upper())
            ruang.name = getString(row['Nama'].upper())
            ruang.gedung = getString(row['Gedung'].upper())
            ruang.kapasitas = getNumber(row['Kapasitas'])
            ruang.tags = getString(row['Tags'])
            if ruang.tags:
                ruang.tags = getString(row['Tags']).split(",")
                ruang.tags = ListToUp(ruang.tags)
            else:
                ruang.tags = []
            cruang = await dbase.find_one({"companyId": ObjectId(current_user.companyId), "name": ruang.name})
            if cruang:
                rowNonvalid = rowNonvalid + 1
                unValid.append({
                    "name":ruang.name,
                    "note":"telah terdaftar"
                    })
                pass
            else:
                rowValid = rowValid + 1
                result.append(ruang.dict())
    #aneh
    print(result)
    dbase.insert_many(result)
    return {"rowValid" : rowValid, "rowInValid" : rowNonvalid, "inValid" : unValid}


@router_ruang.post("/get_ruang", response_model=dict)
async def get_all_ruangs(size: int = 10, page: int = 0, sort: str = "name", dir : int = 1, search: SearchRequest = None, current_user: JwtToken = Security(get_current_user, scopes=["*","*"])):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(current_user.companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    print(datas)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = RuangPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_ruang.get("/ruang/{id_}", response_model=RuangOnDB)
async def get_ruang_by_id(id_: ObjectId = Depends(ValidateObjectId), current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    ruang = await dbase.find_one({"_id": id_,"companyId": ObjectId(current_user.companyId)})
    if ruang:
        return ruang
    else:
        raise HTTPException(status_code=404, detail="Ruang not found")


@router_ruang.delete("/ruang/{id_}", dependencies=[Depends(GetRuangOr404)], response_model=dict)
async def delete_ruang_by_id(id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    ruang_op = await dbase.delete_one({"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)})
    if ruang_op.deleted_count:
        return {"status": f"deleted count: {ruang_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_ruang.put("/ruang/{id_}", response_model=RuangOnDB)
async def update_ruang(id_: str, data_in: RuangBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    # ruang = IsiDefault(data_in, current_user, True)
    ruang = data_in
    ruang.updateTime = dateTimeNow()
    ruang_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": ruang.dict(skip_defaults=True)}
    )
    if ruang_op.modified_count or ruang_op.matched_count:
        return await GetRuangOr404(id_)
    else:
        raise HTTPException(status_code=304)