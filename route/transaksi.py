import math
from fastapi.routing import APIRouter
from model.default import JwtToken
from fastapi import Security
from model.util import SearchRequest
from route.auth import get_current_user
from sqlmodel import Session, select
from sqlalchemy import text, and_, asc, desc
from sql.model import UserBase, engine
from sql.model import TransactionBase
from sql.report import mutation_by_userid, trx_by_userid
from sql.user import ceks_all_balance

router_transaksi = APIRouter()


@router_transaksi.post("/users")
async def get_all_users(
    size: int = 10,
    page: int = 0,
    sort: str = "name",
    sortType: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    """
    SortType = 1 -> ascending
    SortType = 0 -> descending
    """
    skip = page * size
    criteria = []
    for ii in search.search:
        if len(ii.field) > 2:
            criteria.append(text(ii.field + " like '%" + ii.key + "%'"))
    direction = asc if sortType == 1 else desc
    with Session(engine) as session:
        statement = (
            select(UserBase)
            .where(and_(*criteria))
            .limit(size)
            .offset(skip)
            .order_by(direction(text(sort)))
        )
        results = session.exec(statement)
        users = results.all()
        datas = users
        totalElements = len(session.exec(select(UserBase)).all())
        totalPages = math.ceil(totalElements / size)
        return {
            "status": 200,
            "content": datas,
            "size": size,
            "page": page,
            "totalElements": totalElements,
            "totalPages": totalPages,
            "sort": sort,
            "sortDirection": sortType,
        }


@router_transaksi.post("/user_by_id")
async def get_user_by_id(
    userId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    with Session(engine) as session:
        statement = select(UserBase).where(UserBase.userId == userId)
        return session.exec(statement).all()[0]


@router_transaksi.get("/trx_by_userid/{userUid}", response_model=dict)
async def get_transactions_by_user_id(
    userUid: str,
    size: int = 10,
    page: int = 0,
    sort: str = "createTime",
    sortType: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):

    return await trx_by_userid(userUid, size, page, sort, sortType)


@router_transaksi.get("/mutation_by_userid/{userUid}", response_model=dict)
async def get_mutations_by_userid(
    userUid: str,
    size: int = 10,
    page: int = 0,
    sort: str = "createTime",
    sortType: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    return await mutation_by_userid(userUid, size, page, sort, sortType)


@router_transaksi.get("/ceks_all_balance/{userId}")
async def get_saldo_by_user_id(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "createTime",
    sortType: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    skip = page * size
    balances = ceks_all_balance(userId)
    datas = balances[skip : skip + size]
    totalElements = len(balances)
    totalPages = math.ceil(totalElements / size)
    return {
        "status": 200,
        "content": datas,
        "size": size,
        "page": page,
        "totalElements": totalElements,
        "totalPages": totalPages,
        "sort": sort,
        "sortDirection": sortType,
    }
