from function.inquiry import CreateInquiry
from model.default import JwtToken
from util.util_trx import transaksi_universal
from function.user import CheckMemberByUserId, GetAdminCompany, GetUserIdByNomorVA
from fastapi import APIRouter, Security
from route.auth import get_current_user

router_trxsystem = APIRouter()

CHANNEL = "H2H"


@router_trxsystem.post("/inquiry_userid", response_model=dict)
async def inquiry_userid(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["kasir", "*"]),
):
    trxName = "inquiry_userid"
    return await CreateInquiry(current_user, userIdTujuan, amount)


@router_trxsystem.get(
    "/topup_va_bank_by_gateway/{companyId}/{bank}/{userId}/{amount}/{noreff}"
)
async def topup_va_bank_by_gateway(
    bank: str, userId: str, amount: int, noreff: str, companyId: str
):
    trxName = "topup_va_bank_by_gateway"
    code = "direct-nocode"
    # TODO cek di table_topupva_duplicate untuk pengecekan duplicate transaksi

    # pertanyaan
    # 1. Nggak ngerti maksud TODO-nya. table_topupva_duplicate itu apa? kenapa nggak pakai inquiry aja kayak yang lain?

    # DEBT
    cekAdmin = await GetAdminCompany(companyId)
    debtUserId = str(cekAdmin["userId"])
    # CREDIT
    creditUserId = userId
    await CheckMemberByUserId(creditUserId, companyId)
    trx1 = await transaksi_universal(
        code,
        companyId,
        debtUserId,
        creditUserId,
        trxName,
        amount,
        CHANNEL,
        noreff,
        bank,
    )
    # TODO insert ke table_topupva_duplicate untuk pengecekan duplicate transaksi
    # TODO if berhasil, potong biaya top up va dan dicek apakah perlu auto payment invoice
    return trx1


@router_trxsystem.get("/auto_delete_log_topup")
async def auto_delete_log_topup():
    trxName = "auto_delete_log_topup_yang_udah_180_menit"

    return {"status": "ok"}
