
from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from pydantic.networks import import_email_validator
from config import config
from route.auth import router_auth

from route.web.faq import router_faq
from route.web.home import router_home
from route.web.about import router_about
from route.web.academic import router_academic
from route.web.curiculum import router_curiculum
from route.web.achievement import router_achievement
from route.web.agenda import router_agenda
from route.web.alumni import router_alumni
from route.web.excul import router_excul
from route.web.facility import router_facility
from route.web.gallery import router_gallery
from route.web.news import router_news
from route.web.program import router_program
from route.web.slider import router_slider
from route.web.teacher import router_teacher
from route.web.testimony import router_testimony
from route.web.weekly import router_weekly
from route.web.tutorial import router_tutorial
from route.web.crud_list_detail import router_listdetail

app = FastAPI(openapi_url="/api_web/openapi.json",docs_url="/api_web/swgr")
app.mount("/api_wisata/static", StaticFiles(directory="static"), name="static")
if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-web',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}


app.include_router(router_auth,prefix="/api_default/auth",tags=["auth"],responses={404: {"description": "Not found"}})

app.include_router(router_faq,prefix="/api_web/web_faq",tags=["faq"],responses={404: {"description": "Not found"}})
app.include_router(router_home,prefix="/api_web/web_home",tags=["home"],responses={404: {"description": "Not found"}})
app.include_router(router_about,prefix="/api_web/web_about",tags=["about"],responses={404: {"description": "Not found"}})
app.include_router(router_achievement,prefix="/api_web/web_achieve",tags=["achievement"],responses={404: {"description": "Not found"}})
app.include_router(router_alumni,prefix="/api_web/web_alumni",tags=["alumni"],responses={404: {"description": "Not found"}})
app.include_router(router_academic,prefix="/api_web/web_academic",tags=["academic"],responses={404: {"description": "Not found"}})
app.include_router(router_curiculum,prefix="/api_web/web_curiculum",tags=["curiculum"],responses={404: {"description": "Not found"}})
app.include_router(router_excul,prefix="/api_web/web_excul",tags=["excul"],responses={404: {"description": "Not found"}})
app.include_router(router_agenda,prefix="/api_web/web_agenda",tags=["agenda"],responses={404: {"description": "Not found"}})
app.include_router(router_facility,prefix="/api_web/web_facility",tags=["facility"],responses={404: {"description": "Not found"}})
app.include_router(router_gallery,prefix="/api_web/web_gallery",tags=["gallery"],responses={404: {"description": "Not found"}})
app.include_router(router_news,prefix="/api_web/web_news",tags=["news"],responses={404: {"description": "Not found"}})
app.include_router(router_program,prefix="/api_web/web_program",tags=["program"],responses={404: {"description": "Not found"}})
app.include_router(router_slider,prefix="/api_web/web_slider",tags=["slider"],responses={404: {"description": "Not found"}})
app.include_router(router_teacher,prefix="/api_web/web_teacher",tags=["teacher"],responses={404: {"description": "Not found"}})
app.include_router(router_testimony,prefix="/api_web/web_testimony",tags=["testimony"],responses={404: {"description": "Not found"}})
app.include_router(router_weekly,prefix="/api_web/web_weekly",tags=["weekly"],responses={404: {"description": "Not found"}})
app.include_router(router_tutorial,prefix="/api_web/web_tutorial",tags=["tutorial"],responses={404: {"description": "Not found"}})
app.include_router(router_listdetail,prefix="/api_web/web_listdetail",tags=["list detail"],responses={404: {"description": "Not found"}})


@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()