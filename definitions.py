# Di sini, definisikan semua konstan / variable yang dipakai di code.
# contoh: path ke project root dll
import os

PROJECT_ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
CRM_KATALIS_ROOT = "http://localhost:8000/api_crmkatalis"
SVC_EMAIL_ROOT = "http://localhost:8001/svc_email"
OPEN_FILE_ROOT = "http://localhost:8002/open_file/file"
TEMPLATE_DOC_PATH = PROJECT_ROOT_PATH + "/template-doc"
UPLOAD_PATH = PROJECT_ROOT_PATH + "/upload-file"
