# service untuk kirim notif firebase, wa dan sms
# service untuk kirim email
import logging
from pathlib import Path
from typing import Optional
from util.util_waktu import dateTimeNowStr
from config.config import CONF, REDQUE, ENVIRONMENT, PROJECT_NAME

from fastapi import FastAPI, BackgroundTasks
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
import json 
import requests

FBASEKEY = CONF.get("firebase", dict())["authorization"]

app = FastAPI(openapi_url="/svc_notif/openapi.json", docs_url="/svc_notif/swgr")
app.mount("/svc_notif/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-service-notif',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}

def sendToFirebase(msgFirebase):
    url = "https://fcm.googleapis.com/fcm/send"
    headers = {'Content-type': 'application/json','Authorization': FBASEKEY}
    res = requests.post(url, json=msgFirebase, headers=headers)
    print(res.status_code)
    return res.status_code

def sendToWa(msgWa):
    url = "http://sundev.duckdns.org/wa_receiver"
    headers = {'Content-type': 'application/json','Authorization': 'xxx123'}
    res = requests.post(url, json=msgWa, headers=headers)
    print(res.status_code)
    return res.status_code

@app.post("/svc_notif/kirim_broadcast_firebase", response_model=dict)
async def kirim_broadcast_firebase(background_tasks: BackgroundTasks, topics: str, title: str, message: str):
    waktuKirim = dateTimeNowStr()
    msgFirebase = {
        "to": "/topics/"+ topics,
        "data": {
            "type": "ticket",
            "content": {
                "firebase": "string",
                "writerName": "CS",
                "message": message,
                "readStatus": False,
                "isDelete": False,
                "dateTime": waktuKirim
                }
            },
        "notification": {
            "title": title,
            "body": message,
            "sound": True
            }
    }
    background_tasks.add_task(sendToFirebase, msgFirebase)
    return {"ok":"ok"}

@app.post("/svc_notif/kirim_japri_firebase", response_model=dict)
async def kirim_japri_firebase(background_tasks: BackgroundTasks, firebaseId: str, title: str, message: str):
    waktuKirim = dateTimeNowStr()
    msgFirebase = {
                "to": firebaseId,
                "data": {
                    "type": "chat",
                    "content": {
                        "firebase": "string",
                        "writerName": "CS",
                        "message": message,
                        "readStatus": False,
                        "isDelete": False,
                        "dateTime": waktuKirim
                        }
                    },
                "notification": {
                    "title": title,
                    "body": message,
                    "sound": True
                    }
            }
    background_tasks.add_task(sendToFirebase, msgFirebase)
    return {"ok":"ok"}

@app.post("/svc_notif/kirim_whatsapp", response_model=dict)
async def kirim_whatsapp(background_tasks: BackgroundTasks, numberSource: str, numberDestination: str, message: str):
    msgWa = {"numberSource": numberSource, "numberDestination": numberDestination, "message": message}
    background_tasks.add_task(sendToWa, msgWa)
    return {"ok":"ok"}


@app.get("/svc_mqtt/send_notif_error/{problem}")
async def send_notif_error(problem:str):
    print("pesan error diterima, kemudian lanjutkan kirim ke telegram bot"+problem)