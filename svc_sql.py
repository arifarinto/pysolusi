from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from sql.model import create_db_and_tables
from sql.coa import router_coa
from sql.user import router_user
from sql.transaction import router_transaction
from sql.report import router_report

app = FastAPI(openapi_url="/svc_sql/openapi.json", docs_url="/svc_sql/swgr")
app.mount("/svc_sql/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == "production":
    # setup Elastic APM Agent
    apm = make_apm_client(
        {
            "SERVICE_NAME": "pysolusi-service-sql",
            "SERVER_URL": "http://apm-server.logging:8200",
            "ELASTIC_APM_TRANSACTION_IGNORE_URLS": ["/health"],
            "METRICS_SETS": "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
        }
    )
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*", "*"],
    allow_credentials=True,
    allow_methods=["*", "*"],
    allow_headers=["*", "*"],
)


@app.get("/health")
async def health():
    return {"status": "ok"}


@app.on_event("startup")
def on_startup():
    print("start")
    create_db_and_tables()


app.include_router(
    router_coa,
    prefix="/svc_sql/coa",
    tags=["coa"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_user,
    prefix="/svc_sql/user",
    tags=["user"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_transaction,
    prefix="/svc_sql/trx",
    tags=["transaction"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_report,
    prefix="/svc_sql/report",
    tags=["report"],
    responses={404: {"description": "Not found"}},
)
