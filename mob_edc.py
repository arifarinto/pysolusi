from datetime import date
from bson.objectid import ObjectId

from fastapi.exceptions import HTTPException
from function.inquiry import CreateInquiry, GetInquiry
from model.membership.edc import EdcBase
from model.support import InvoiceData
from route.trxedc import inquiry_data_kartu, transaksi_dengan_kartu
from route.trxuser import payment_qr_code, transfer_saldo_on_us
from util.util import RandomString, ValidateObjectId, cleanNullTerms
from function.user import (
    CheckMemberByUserId,
    CreateOpenVa,
    CreateQrCode,
    InquiryQrCode,
    UpdateDateOfBirth,
)
import json
from route.info import get_all_infos
from model.util import FieldBoolRequest, SearchRequest
from route.notif import get_my_notifs
from model.default import InquiryBase, JwtToken, UserBasic, UserDataBase, UserInput
from route.auth import (
    get_current_user,
    login_for_access_token,
    user_change_password,
    user_create_password_first_login,
)
from route.user import get_my_profile, get_my_datas
from fastapi import FastAPI, Response, Depends, Security
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.openapi.docs import get_swagger_ui_html
from config import config
from config.config import MGDB, URL_SQL
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
import requests
from typing import List, Optional
from route.auth import router_auth
from model.membership.member import MemberBase

from function.invoice import (
    GetInvoiceOr404,
    GetInvoicesOnUserId,
    GetInvoicesPaidOnUserId,
    GetInvoicesUnpaidOnUserId,
)

from sqlmodel import Session, select, or_
from sql.model import TransactionBase, engine

CHANNEL = "EDC"

app = FastAPI(openapi_url="/mob_edc/openapi.json", docs_url="/mob_edc/swgr")
app.mount("/mob_edc/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == "production":
    # setup Elastic APM Agent
    apm = make_apm_client(
        {
            "SERVICE_NAME": "pysolusi-mobile",
            "SERVER_URL": "http://apm-server.logging:8200",
            "ELASTIC_APM_TRANSACTION_IGNORE_URLS": ["/health"],
            "METRICS_SETS": "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
        }
    )
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*", "*"],
    allow_credentials=True,
    allow_methods=["*", "*"],
    allow_headers=["*", "*"],
)


async def GetEdcOr404(userId: str):
    userId = ValidateObjectId(userId)
    dbase = MGDB["user_membership_edc"]
    member = await dbase.find_one({"userId": userId, "isDelete": False})
    if member:
        return member
    else:
        raise HTTPException(status_code=404, detail="EDC tidak ditemukan")


@app.get("/health")
async def health():
    return {"status": "ok"}


app.include_router(
    router_auth,
    prefix="/api_default/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)


@app.post("/mob_edc/login", response_model=dict, tags=["login"])
async def mobile_login(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await login_for_access_token(response, form_data)


@app.post("/mob_edc/first_login", response_model=dict, tags=["login"])
async def first_login(
    newPassword: str,
    email: str,
    phone: str,
    isWa: bool,
    dateOfBirth: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await user_create_password_first_login(
        newPassword, email, phone, isWa, dateOfBirth, current_user
    )


@app.post("/mob_edc/change_password", response_model=dict, tags=["login"])
async def change_password(
    oldPassword: str,
    newPassword: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await user_change_password(oldPassword, newPassword, current_user)


# ============================================


@app.get("/mob_edc/get_home_dasboard", response_model=dict, tags=["umum"])
async def get_home_dasboard(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return {"response": "menampilkan data untuk grafik dashboard edc"}


@app.post("/mob_edc/get_beritas", response_model=dict, tags=["info"])
async def get_berita(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_all_infos(size, page, sort, dir, search, current_user)


@app.get("/mob_edc/get_profil", response_model=MemberBase, tags=["umum"])
async def get_profil(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_my_profile(current_user)


@app.put("/mob_edc/update_profil", response_model=EdcBase, tags=["umum"])
async def update_profil(
    profilData: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # cek apakah member ada
    await GetEdcOr404(current_user.userId)

    profilData = profilData.dict()
    profilData = cleanNullTerms(profilData)
    profilData = UpdateDateOfBirth(profilData)

    dbase = MGDB["user_membership_edc"]
    op = await dbase.update_one(
        {"userId": ObjectId(current_user.userId)}, {"$set": profilData}
    )
    if op.modified_count:
        member = await GetEdcOr404(current_user.userId)
        return member
    else:
        raise HTTPException(status_code=304, detail="Gagal update data")


# get user_data yang isi nya berbagai data
@app.get("/mob_edc/get_data", response_model=List[UserDataBase], tags=["umum"])
async def get_user_data(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_my_datas(current_user)


@app.post("/mob_edc/get_notifs", response_model=dict, tags=["umum"])
async def get_notifs(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # return await get_all_infos(size, page, sort, dir, search, current_user)
    return await get_my_notifs(size, page, sort, dir, search, current_user)


# ============================================


@app.post("/mob_edc/add_norek_transfer_bank", response_model=dict, tags=["withdraw"])
async def add_norek_transfer_bank(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await add_norek_transfer_bank()


@app.post("/mob_edc/update_norek_transfer_bank", response_model=dict, tags=["withdraw"])
async def update_norek_transfer_bank(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await update_norek_transfer_bank()


@app.post("/mob_edc/delete_norek_transfer_bank", response_model=dict, tags=["withdraw"])
async def delete_norek_transfer_bank(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await delete_norek_transfer_bank()


@app.post(
    "/mob_edc/inquiry_balance_withdraw", response_model=InquiryBase, tags=["withdraw"]
)
async def inquiry_balance_withdraw(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await inquiry_balance_withdraw(response, form_data)


@app.post("/mob_edc/payment_balance_withdraw", response_model=dict, tags=["withdraw"])
async def payment_balance_withdraw(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await payment_balance_withdraw(response, form_data)


# ============================================


@app.get("/mob_edc/get_saldo_utama", tags=["info transaksi"])
async def get_saldo(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    url = URL_SQL + "/svc_sql/user/cek_main_balance/" + current_user.userId
    print(url)
    res = requests.get(url)
    print(res.json())
    return res.json()


@app.get("/mob_edc/get_semua_saldo", tags=["info transaksi"])
async def get_semua_saldo(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    url = URL_SQL + "/svc_sql/user/ceks_all_balance/" + current_user.userId
    print(url)
    res = requests.get(url)
    print(res.json())
    return res.json()


# @app.get("/mob_edc/get_semua_transaksi", tags=["info transaksi"])
# async def get_semua_transaksi(
#     limit: int = -1,
#     current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
# ):
#     # pass
#     url = (
#         URL_SQL
#         + "/svc_sql/user/ceks_all_transaksi/"
#         + current_user.userId
#         + "?limit="
#         + str(limit)
#     )
#     res = requests.get(url)
#     print(res.json())
#     return res.json()


# @app.get("/mob_edc/get_semua_mutasi", tags=["info transaksi"])
# async def get_semua_mutasi(
#     limit: int = -1,
#     current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
# ):
#     """
#     Get all mutasi yang dimiliki oleh current user. Limit adalah variabel yang mendefinisikan jumlah data mutasi yang akan dikembalikan oleh endpoint.

#     Apabila limit = 5, maka data yang akan dikembalikan adalah lima buah data
#     Apabila limit = 10, maka data yang akan dikembalikan adalah sepuluh buah data
#     Apabila limit < 0, sama artinya dengan mengembalikan semua data mutasi
#     """
#     url = (
#         URL_SQL
#         + "/svc_sql/user/ceks_all_mutasi/"
#         + current_user.userId
#         + "?limit="
#         + str(limit)
#     )
#     res = requests.get(url)
#     print(res.json())
#     return res.json()


@app.post("/mob_edc/get_data_transaksi", response_model=dict, tags=["info transaksi"])
async def get_data_transaksi(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    url = URL_SQL + "/svc_sql/report/trx_by_userid/" + current_user.userId
    res = requests.get(url)
    print(res.json())
    return res.json()


@app.post("/mob_edc/get_mutasi_saldo", response_model=dict, tags=["info transaksi"])
async def get_mutasi_saldo(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    url = URL_SQL + "/svc_sql/report/mutation_by_userid/" + current_user.userId
    res = requests.get(url)
    print(res.json())
    return res.json()


@app.post(
    "/mob_edc/get_all_transaksi_transfer_internal",
    response_model=dict,
    tags=["info transaksi"],
)
async def get_all_transaksi_transfer_internal(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    with Session(engine) as session:
        statement = select(TransactionBase).where(
            TransactionBase.debtUserId == current_user.userId,
            TransactionBase.debtCoa == "21-200",
        )
        results = session.exec(statement)
        return results.all()


# ============================================


@app.post(
    "/mob_edc/get_create_va_bank/{bank}", response_model=dict, tags=["create va top up"]
)
async def get_create_va_bank(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await get_create_va_bank(response, form_data)


@app.post(
    "/mob_edc/get_create_channel_topup/{channel}/{amount}",
    response_model=dict,
    tags=["create va top up"],
)
async def get_create_channel_topup(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await get_create_channel_topup(response, form_data)


@app.post(
    "/mob_edc/get_info_topup/{bank}", response_model=dict, tags=["create va top up"]
)
async def get_info_topup(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await get_info_topup(response, form_data)


# ============================================


@app.post("/mob_edc/get_all_invoice", response_model=dict, tags=["invoice"])
async def get_all_invoice(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    # return await get_all_invoice(response, form_data)
    return await GetInvoicesOnUserId(current_user.userId)


@app.post("/mob_edc/get_invoice_unpaid", response_model=dict, tags=["invoice"])
# async def get_invoice_unpaid(
#     response: Response, form_data: OAuth2PasswordRequestForm = Depends()
# ):
async def get_invoice_unpaid(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    # return await get_invoice_unpaid(response, form_data)
    return await GetInvoicesUnpaidOnUserId(current_user.userId)


@app.post("/mob_edc/get_invoice_paid", response_model=dict, tags=["invoice"])
# async def get_invoice_paid(
#     response: Response, form_data: OAuth2PasswordRequestForm = Depends()
# ):
#     return await get_invoice_paid(response, form_data)
async def get_invoice_paid(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await GetInvoicesPaidOnUserId(current_user.userId)


@app.post("/mob_edc/inquiry_invoice", response_model=InquiryBase, tags=["invoice"])
async def inquiry_invoice(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await inquiry_invoice(response, form_data)


@app.post("/mob_edc/payment_invoice", response_model=dict, tags=["invoice"])
async def payment_invoice(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await payment_invoice(response, form_data)


# ============================================


@app.post("/mob_edc/get_donasi", response_model=dict, tags=["donasi"])
async def get_donasi(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await get_donasi(response, form_data)


@app.post("/mob_edc/inquiry_donasi", response_model=InquiryBase, tags=["donasi"])
async def inquiry_donasi(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await inquiry_donasi(response, form_data)


@app.post("/mob_edc/payment_donasi", response_model=dict, tags=["donasi"])
async def payment_donasi(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await payment_donasi(response, form_data)


# ============================================


@app.post(
    "/mob_edc/inquiry_data_kartu",
    response_model=InquiryBase,
    tags=["aktivitas transaksi"],
)
async def inq_data_kartu(
    amount: int,
    nfcId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await inquiry_data_kartu(amount, nfcId, current_user)


@app.post(
    "/mob_edc/transaksi_dengan_kartu",
    response_model=dict,
    tags=["aktivitas transaksi"],
)
async def trx_dengan_kartu(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await transaksi_dengan_kartu(code, current_user)


@app.post(
    "/mob_edc/inquiry_transfer_on_us",
    response_model=InquiryBase,
    tags=["aktivitas transaksi"],
)
async def inquiry_transfer_on_us(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await CreateInquiry(current_user, userIdTujuan, amount)


@app.post("/mob_edc/payment_transfer_on_us", tags=["aktivitas transaksi"])
async def payment_transfer_on_us(
    code: str,
    # userIdTujuan: str,
    # amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):

    return await transfer_saldo_on_us(code, current_user)


# ============================================


@app.post(
    "/mob_edc/create_mpm_qrcode", response_model=dict, tags=["pembayaran qr code"]
)
async def create_mpm_qrcode(
    amount: int, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    # return await create_mpm_qrcode(response, form_data)
    # question / pertanyaan /  note:
    # untuk sekarang, fungsi create qrcode mpm atau cpm sama2 pakai CreateQrCode, kodenya sama, entitasnya sama, bedanya cuma usernya
    # apa qr code mpm beda atribut / beda behavior dengan qr code cpm? atau pembedanya cuma yang qr code mpm di-generate merchant, qr cpde cpm di-generate customer?
    return await CreateQrCode(current_user.companyId, current_user.userId, amount)


@app.post(
    "/mob_edc/inquiry_cpm_qrcode",
    response_model=InquiryBase,
    tags=["pembayaran qr code"],
)
async def inquiry_cpm_qrcode(
    qrcode: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await InquiryQrCode(current_user, qrcode)


@app.post(
    "/mob_edc/payment_cpm_qrcode", response_model=dict, tags=["pembayaran qr code"]
)
# async def payment_cpm_qrcode(
#     code: str, userIdTujuan: str, amount: int, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
# ):
# return await payment_qr_code(code, userIdTujuan, amount, current_user)
async def payment_cpm_qrcode(
    code: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await payment_qr_code(code, current_user)


@app.on_event("startup")
async def app_startup():
    config.load_config()


@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()
