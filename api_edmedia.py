from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from route.auth import router_auth

from route.edmedia.kbm import router_kbm
from route.edmedia.pelajar import router_pelajar
from route.edmedia.pengajar import router_pengajar
from route.edmedia.materi import router_materi
from route.edmedia.ruang import router_ruang
from route.edmedia.kelas import router_kelas
from route.edmedia.silabus import router_silabus
from route.edmedia.combo import router_combo
from route.edmedia.rpp import router_rpp
from route.edmedia.rpp_student import router_rpp_student
from route.edmedia.rpp_teacher import router_rpp_teacher

app = FastAPI(openapi_url="/api_edmedia/openapi.json", docs_url="/api_edmedia/swgr")
app.mount("/api_edmedia/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-edmedia',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}

app.include_router(router_auth,prefix="/api_default/auth",tags=["auth"],responses={404: {"description": "Not found"}})

app.include_router(router_kbm,prefix="/api_edmedia/kbm",tags=["kbm"],responses={404: {"description": "Not found"}})
app.include_router(router_pelajar,prefix="/api_edmedia/pelajar",tags=["pelajar"],responses={404: {"description": "Not found"}})
app.include_router(router_pengajar,prefix="/api_edmedia/pengajar",tags=["pengajar"],responses={404: {"description": "Not found"}})
app.include_router(router_ruang,prefix="/api_edmedia/ruang",tags=["ruang"],responses={404: {"description": "Not found"}})
app.include_router(router_kelas,prefix="/api_edmedia/kelas",tags=["kelas"],responses={404: {"description": "Not found"}})
app.include_router(router_materi,prefix="/api_edmedia/materi",tags=["materi"],responses={404: {"description": "Not found"}})
app.include_router(router_silabus,prefix="/api_edmedia/silabus",tags=["silabus"],responses={404: {"description": "Not found"}})
app.include_router(router_combo,prefix="/api_edmedia/combo",tags=["combo"],responses={404: {"description": "Not found"}})
app.include_router(router_rpp,prefix="/api_edmedia/rpp",tags=["rpp"],responses={404: {"description": "Not found"}})
app.include_router(router_rpp_student,prefix="/api_edmedia/student_rpp",tags=["rpp-student"],responses={404: {"description": "Not found"}})
app.include_router(router_rpp_teacher,prefix="/api_edmedia/teacher_rpp",tags=["rpp-teacher"],responses={404: {"description": "Not found"}})

@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()