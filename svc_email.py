# service untuk kirim email
import logging
from pathlib import Path
from typing import Optional
from config.config import CONF, REDQUE, ENVIRONMENT, PROJECT_NAME
import emails
from emails.template import JinjaTemplate

from fastapi import FastAPI, BackgroundTasks
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
import json

SMTP_TLS = Optional
SMTP_PORT = CONF.get("email", dict())["smptPort"]
HOST_TOKEN = CONF.get("email", dict())["hostLinkToken"]
SMTP_HOST = CONF.get("email", dict())["host"]
SMTP_USER = CONF.get("email", dict())["user"]
SMTP_PASSWORD = CONF.get("email", dict())["password"]
EMAILS_FROM_EMAIL = CONF.get("email", dict())["from"]
EMAILS_FROM_NAME = CONF.get("fastapi", dict())["projectName"]

EMAIL_RESET_TOKEN_EXPIRE_HOURS = CONF.get("email", dict())["hoursValid"]
EMAIL_TEMPLATES_DIR = CONF.get("email", dict())["templateDir"]

app = FastAPI(openapi_url="/svc_email/openapi.json", docs_url="/svc_email/swgr")
app.mount("/svc_email/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == "production":
    # setup Elastic APM Agent
    apm = make_apm_client(
        {
            "SERVICE_NAME": "pysolusi-service-email",
            "SERVER_URL": "http://apm-server.logging:8200",
            "ELASTIC_APM_TRANSACTION_IGNORE_URLS": ["/health"],
            "METRICS_SETS": "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
        }
    )
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*", "*"],
    allow_credentials=True,
    allow_methods=["*", "*"],
    allow_headers=["*", "*"],
)


@app.get("/health")
async def health():
    return {"status": "ok"}


def sendEmail(emailTo: str, subject: str, templateName: str, environment=dict):
    print("sendemail")
    with open(Path(EMAIL_TEMPLATES_DIR) / templateName) as f:
        templateStr = f.read()
    message = emails.Message(
        subject=JinjaTemplate(subject),
        html=JinjaTemplate(templateStr),
        mail_from=(EMAILS_FROM_NAME, EMAILS_FROM_EMAIL),
    )
    smtp_options = {"host": SMTP_HOST, "port": SMTP_PORT}
    smtp_options["tls"] = SMTP_TLS
    smtp_options["user"] = SMTP_USER
    smtp_options["password"] = SMTP_PASSWORD
    # print("this is inside send email")
    if ENVIRONMENT != "xdevelopment":
        response = message.send(to=emailTo, render=environment, smtp=smtp_options)
        logging.info(f"send email result: {response}")


# {"project_name": "coba coba","email": "arifarinto@gmail.com","password": "1234","valid_hours": 1,"link": "http://sundev.duckdns.org"}
@app.post("/svc_email/kirim_email_standard", response_model=dict)
async def kirim_email_standard(
    emailTo: str,
    subject: str,
    templateName: str,
    environment: str,
    background_tasks: BackgroundTasks,
):
    jEnv = json.loads(environment)
    print("kirim_email_standard")
    # sendEmail(emailTo, subject, templateName, jEnv)
    background_tasks.add_task(sendEmail, emailTo, subject, templateName, jEnv)
    # sendEmail(emailTo, subject, templateName, jEnv)
    return {"ok": "ok"}
