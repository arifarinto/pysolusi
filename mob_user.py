from datetime import date
from bson.objectid import ObjectId

from fastapi.exceptions import HTTPException
from function.inquiry import CreateInquiry
from model.support import InvoiceData
from route.donasi import GetDonasiOr404
from route.trxedc import payment_qr_code_mpm
from route.trxsystem import topup_va_bank_by_gateway
from route.trxuser import payment_qr_code, transfer_saldo_on_us
from sql.user import cek_main_balance
from util.util import RandomString, ValidateObjectId, cleanNullTerms
from function.user import (
    CheckMemberByUserId,
    CreateCardQrCode,
    CreateOpenVa,
    CreateQrCode,
    GetUserOr404,
    InquiryQrCode,
)
import json
from route.info import get_all_infos
from model.util import FieldBoolRequest, ObjectIdStr, SearchRequest
from route.notif import get_my_notifs
from model.default import (
    InquiryBase,
    InquiryOnDB,
    JwtToken,
    UserBasic,
    UserCard,
    UserDataBase,
    UserInput,
    VaNumberData,
)
from route.auth import (
    get_current_user,
    login_for_access_token,
    user_change_password,
    user_create_password_first_login,
)
from route.user import get_my_profile, get_my_datas
from fastapi import FastAPI, Response, Depends, Security
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.openapi.docs import get_swagger_ui_html
from config import config
from config.config import MGDB, URL_SQL
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
import requests
import math
from typing import List, Optional
from route.auth import router_auth
from model.membership.member import MemberBase

from function.invoice import (
    GetInvoiceOr404,
    GetInvoicesOnUserId,
    GetInvoicesPaidOnUserId,
    GetInvoicesUnpaidOnUserId,
)

from sqlmodel import Session, select, or_

from sql.model import TransactionBase, engine

CHANNEL = "MOBILE"

app = FastAPI(openapi_url="/mob_user/openapi.json", docs_url="/mob_user/swgr")
app.mount("/mob_user/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == "production":
    # setup Elastic APM Agent
    apm = make_apm_client(
        {
            "SERVICE_NAME": "pysolusi-mobile",
            "SERVER_URL": "http://apm-server.logging:8200",
            "ELASTIC_APM_TRANSACTION_IGNORE_URLS": ["/health"],
            "METRICS_SETS": "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
        }
    )
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*", "*"],
    allow_credentials=True,
    allow_methods=["*", "*"],
    allow_headers=["*", "*"],
)


@app.get("/health")
async def health():
    return {"status": "ok"}


app.include_router(
    router_auth,
    prefix="/api_default/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)


@app.post("/mob_user/login", response_model=dict, tags=["login"])
async def mobile_login(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await login_for_access_token(response, form_data)


@app.post("/mob_user/first_login", response_model=dict, tags=["login"])
async def first_login(
    newPassword: str,
    email: str,
    phone: str,
    isWa: bool,
    dateOfBirth: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await user_create_password_first_login(
        newPassword, email, phone, isWa, dateOfBirth, current_user
    )


@app.post("/mob_user/change_password", response_model=dict, tags=["login"])
async def change_password(
    oldPassword: str,
    newPassword: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await user_change_password(oldPassword, newPassword, current_user)


# ============================================


async def GetMembershipOr404(userId: str):
    userId = ValidateObjectId(userId)
    dbase = MGDB["user_membership_member"]
    member = await dbase.find_one({"userId": userId})
    if member:
        return member
    else:
        raise HTTPException(status_code=404, detail="Member tidak ditemukan")


# ============================================


@app.post("/mob_user/get_beritas", response_model=dict, tags=["info"])
async def get_berita(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_all_infos(size, page, sort, dir, search, current_user)


@app.post("/mob_user/get_notifs", response_model=dict, tags=["info"])
async def get_notifs(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # return await get_all_infos(size, page, sort, dir, search, current_user)
    return await get_my_notifs(size, page, sort, dir, search, current_user)


@app.get("/mob_user/get_headline_news", response_model=dict, tags=["info"])
async def get_headline_news(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    search = SearchRequest()
    search.defaultBool.append(FieldBoolRequest(field="isHeadLine", key=True))
    return await get_all_infos(10, 0, "updateTime", -1, search, current_user)


# ============================================


@app.get("/mob_user/get_profil", response_model=MemberBase, tags=["umum"])
async def get_profil(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_my_profile(current_user)


@app.put("/mob_user/update_profil", response_model=MemberBase, tags=["umum"])
async def update_profil(
    profilData: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # cek apakah member ada
    await GetMembershipOr404(current_user.userId)

    profilData = profilData.dict()
    profilData = cleanNullTerms(profilData)
    print("profildata: " + str(profilData))
    dbase = MGDB["user_membership_member"]
    op = await dbase.update_one(
        {"userId": ObjectId(current_user.userId)}, {"$set": profilData}
    )
    if op.modified_count:
        member = await GetMembershipOr404(current_user.userId)
        print("Member: " + str(member))
        return member
    else:
        raise HTTPException(status_code=304, detail="Gagal update data")


# get user_data yang isi nya berbagai data
@app.get("/mob_user/get_data", response_model=List[UserDataBase], tags=["umum"])
async def get_user_data(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_my_datas(current_user)


@app.get("/mob_user/get_kartu", response_model=UserCard, tags=["umum"])
async def get_kartu(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):

    # Ambil dari user data, nfcId, get by nfc id
    # di sini, create card qr code, sekalian create nfc id juga
    await CreateCardQrCode(current_user.companyId, current_user.userId)

    data = await get_my_datas(current_user)
    data = data[0]
    card = UserCard(**data)
    # user = await GetUserOr404(current_user.userId)
    user = await GetMembershipOr404(current_user.userId)
    card = card.dict()
    print("card: " + str(card))
    print("user: " + str(user))

    # note: di model userdata, nggak ada field identity dan address, tapi di membership ada

    card["identity"] = user["identity"]
    card["address"] = user["address"]

    return card


@app.post("/mob_user/update_limit_kartu", response_model=dict, tags=["umum"])
async def update_limit_kartu(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await update_limit_kartu()


@app.post("/mob_user/get_notifs", response_model=dict, tags=["umum"])
async def get_notif(
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_my_notifs(size, page, sort, dir, search, current_user)


# ============================================


@app.post(
    "/mob_user/create_va_bank/{bank}",
    response_model=VaNumberData,
    tags=["create va top up"],
)
async def create_va_bank(
    bank: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    cUser = await get_my_profile(current_user)
    return await CreateOpenVa(
        current_user.companyId, bank, current_user.userId, cUser["name"]
    )


@app.get(
    "/mob_user/get_va_all",
    response_model=List[VaNumberData],
    tags=["create va top up"],
)
async def get_all_va(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    pipeline = [
        {"$match": {"userId": ObjectId(current_user.userId)}},
        {"$unwind": {"path": "$vaNumber"}},
        {"$replaceRoot": {"newRoot": "$vaNumber"}},
    ]
    dbase = MGDB["user_data"]
    results = await dbase.aggregate(pipeline).to_list(1000)
    return results


@app.get(
    "/mob_user/get_va_by_id",
    response_model=List[VaNumberData],
    tags=["create va top up"],
)
async def get_va_by_id(
    idVa: ObjectIdStr,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # pass
    pipeline = [
        {"$match": {"userId": ObjectId(current_user.userId)}},
        {"$unwind": {"path": "$vaNumber"}},
        {"$replaceRoot": {"newRoot": "$vaNumber"}},
        {"$match": {"id": ObjectId(idVa)}},
    ]
    dbase = MGDB["user_data"]
    results = await dbase.aggregate(pipeline).to_list(1000)
    return results


@app.post(
    "/mob_user/get_create_channel_topup/{channel}/{amount}",
    response_model=dict,
    tags=["create va top up"],
)
async def get_create_channel_topup(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await get_create_channel_topup(response, form_data)


@app.post("/mob_user/get_info_topup", response_model=dict, tags=["create va top up"])
async def get_info_topup(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await get_info_topup(response, form_data)


# ============================================


@app.post("/mob_user/add_norek_transfer_bank", response_model=dict, tags=["withdraw"])
async def add_norek_transfer_bank(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await add_norek_transfer_bank()


@app.post(
    "/mob_user/update_norek_transfer_bank", response_model=dict, tags=["withdraw"]
)
async def update_norek_transfer_bank(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await update_norek_transfer_bank()


@app.post(
    "/mob_user/delete_norek_transfer_bank", response_model=dict, tags=["withdraw"]
)
async def delete_norek_transfer_bank(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await delete_norek_transfer_bank()


@app.post(
    "/mob_user/inquiry_balance_withdraw", response_model=InquiryBase, tags=["withdraw"]
)
async def inquiry_balance_withdraw(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await inquiry_balance_withdraw(response, form_data)


@app.post("/mob_user/payment_balance_withdraw", response_model=dict, tags=["withdraw"])
async def payment_balance_withdraw(
    response: Response, form_data: OAuth2PasswordRequestForm = Depends()
):
    return await payment_balance_withdraw(response, form_data)


# ============================================


@app.get("/mob_user/get_saldo_utama", tags=["info transaksi"])
async def get_saldo(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    # url = URL_SQL + "/svc_sql/user/cek_main_balance/" + current_user.userId
    # res = requests.get(url)
    # print(res.json())
    # return res.json()

    return cek_main_balance(current_user.userId)


@app.get("/mob_user/get_semua_saldo", tags=["info transaksi"])
async def get_semua_saldo(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    url = URL_SQL + "/svc_sql/user/ceks_all_balance/" + current_user.userId
    res = requests.get(url)
    print(res.json())
    return res.json()


# @app.get("/mob_user/get_semua_transaksi", tags=["info transaksi"])
# async def get_semua_transaksi(
#     limit: int = -1,
#     current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
# ):
#     """
#     Kalau limit < 0, get semua data
#     kalau limit = 5, get 5 data
#     kalau limit = 10, get 10 data
#     dst

#     Ini ngembaliin data yang persis sama dengan endpoint /mob_user/get_data_transaksi, bedanya cuma endpoint yang ini punya parameter limit untuk manage jumlah data yang harus dikembalikan
#     """
#     # pass
#     url = (
#         URL_SQL
#         + "/svc_sql/user/ceks_all_transaksi/"
#         + current_user.userId
#         + "?limit="
#         + str(limit)
#     )
#     res = requests.get(url)
#     print(res.json())
#     return res.json()


# @app.get("/mob_user/get_semua_mutasi", tags=["info transaksi"])
# async def get_semua_mutasi(
#     limit: int = -1,
#     current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
# ):
#     """
#     Get all mutasi yang dimiliki oleh current user. Limit adalah variabel yang mendefinisikan jumlah data mutasi yang akan dikembalikan oleh endpoint.

#     Apabila limit = 5, maka data yang akan dikembalikan adalah lima buah data
#     Apabila limit = 10, maka data yang akan dikembalikan adalah sepuluh buah data
#     Apabila limit < 0, sama artinya dengan mengembalikan semua data mutasi
#     """
#     url = (
#         URL_SQL
#         + "/svc_sql/user/ceks_all_mutasi/"
#         + current_user.userId
#         + "?limit="
#         + str(limit)
#     )
#     res = requests.get(url)
#     print(res.json())
#     return res.json()


@app.post("/mob_user/get_data_transaksi", response_model=dict, tags=["info transaksi"])
async def get_data_transaksi(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    url = URL_SQL + "/svc_sql/report/trx_by_userid/" + current_user.userId
    res = requests.get(url)
    print(res.json())
    return res.json()


@app.post("/mob_user/get_mutasi_saldo", response_model=dict, tags=["info transaksi"])
async def get_mutasi_saldo(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    url = URL_SQL + "/svc_sql/report/mutation_by_userid/" + current_user.userId
    res = requests.get(url)
    print(res.json())
    return res.json()


@app.post(
    "/mob_user/get_all_transaksi_transfer_internal",
    response_model=dict,
    tags=["info transaksi"],
)
async def get_all_transaksi_transfer_internal(
    size: int = 10,
    page: int = 0,
    sort: str = "createTime",
    sortType: int = 1,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    with Session(engine) as session:
        statement = select(TransactionBase).where(
            TransactionBase.debtUserId == current_user.userId,
            TransactionBase.debtCoa == "21-200",
        )
        results = session.exec(statement)
        transactions = results.all()
        totalElements = len(transactions)
        totalPages = math.ceil(totalElements / size)
        return {
            "status": 200,
            "content": transactions,
            "size": size,
            "page": page,
            "totalElements": totalElements,
            "totalPages": totalPages,
            "sort": sort,
            "sortDirection": sortType,
        }


# ============================================


@app.post(
    "/mob_user/inquiry_transfer_on_us",
    response_model=InquiryBase,
    tags=["aktivitas transaksi"],
)
async def inquiry_transfer_on_us(
    userIdTujuan: str,
    amount: int,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await CreateInquiry(current_user, userIdTujuan, amount)


@app.post(
    "/mob_user/payment_transfer_on_us",
    tags=["aktivitas transaksi"],
)
async def payment_transfer_on_us(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await transfer_saldo_on_us(code, current_user)


# ============================================


@app.post("/mob_user/get_donasi", response_model=dict, tags=["donasi"])
async def get_donasi(
    idDonasi: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await GetDonasiOr404(idDonasi)


@app.post("/mob_user/inquiry_donasi", response_model=InquiryBase, tags=["donasi"])
async def inquiry_donasi(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    pass
    # PERTANYAAN
    # ini yang inquiry donasi gimana? Biasanya, endpoint inquiry membutuhkan userIdTujuan. UserIdTujuan di inquiry donasi ini apa?

    # return await inquiry_donasi()
    # return await CreateInquiry(current_user, userIdTujuan, amount)


@app.post("/mob_user/payment_donasi", response_model=dict, tags=["donasi"])
async def payment_donasi(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await payment_donasi()


# ============================================


@app.get(
    "/mob_user/create_cpm_qrcode/{amount}",
    response_model=dict,
    tags=["pembayaran qr code"],
)
async def create_cpm_qrcode(
    amount: int, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    """
    Buat QR Code untuk transaksi -- cuma raw data, bukan qr code secara visualnya. Punya waktu berlaku hanya satu menit.
    """
    return await CreateQrCode(current_user.companyId, current_user.userId, amount)


@app.get(
    "/mob_user/inquiry_mpm_qrcode/{qrcode}",
    response_model=InquiryBase,
    tags=["pembayaran qr code"],
)
async def inquiry_mpm_qrcode(
    qrcode: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    """
    Get data qr code berdasarkan kode.
    """
    return await InquiryQrCode(current_user, qrcode)


@app.post(
    "/mob_user/payment_mpm_qrcode", response_model=dict, tags=["pembayaran qr code"]
)
async def payment_mpm_qrcode(
    code: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    # return await payment_mpm_qrcode(current_user.companyId, qrcode)
    return await payment_qr_code_mpm(code, current_user)


# ============================================


@app.post(
    "/mob_user/get_invoice_unpaid", response_model=List[InvoiceData], tags=["invoice"]
)
async def get_invoice_unpaid(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await GetInvoicesUnpaidOnUserId(current_user.userId)


@app.post(
    "/mob_user/get_invoice_paid", response_model=List[InvoiceData], tags=["invoice"]
)
async def get_invoice_paid(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await GetInvoicesPaidOnUserId(current_user.userId)


@app.post(
    "/mob_user/get_invoice_all", response_model=List[InvoiceData], tags=["invoice"]
)
async def get_invoice_all(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await GetInvoicesOnUserId(current_user.userId)


@app.get(
    "/mob_user/get_invoice/{idInvoice}", response_model=InvoiceData, tags=["invoice"]
)
async def get_invoice_by_id(
    idInvoice: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await GetInvoiceOr404(idInvoice)


@app.post("/mob_user/inquiry_invoice", response_model=InquiryBase, tags=["invoice"])
async def inquiry_invoice(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    # Pertanyaan / question:
    # 1. step2nya apa aja?
    # 2. apa pas waktu bayar pakai saldo, terus muncul invoice lagi? -- dari chat di wa:
    # "Intinya mau bayar Invoice pakai saldo, partner bisa nambah invoice by api h2h, trus kita kirim Callback kalo invoice uda dibayar"
    # 3. apa itu api h2h?
    # 4. Inquiry invoice ini apa sama dengan GetInvoiceById, atau yang dimaksud inquiry di sini adalah inquiry transaksi?
    return await inquiry_invoice()


# update_cara_bayar


@app.post("/mob_user/payment_invoice_saldo", response_model=dict, tags=["invoice"])
async def payment_invoice_saldo(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await payment_invoice_saldo()


@app.post("/mob_user/payment_invoice_va", response_model=dict, tags=["invoice"])
async def payment_invoice_va(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await payment_invoice_va()


@app.post("/mob_user/payment_invoice_merchant", response_model=dict, tags=["invoice"])
async def payment_invoice_merchant(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await payment_invoice_merchant()


@app.post("/mob_user/payment_invoice_qris", response_model=dict, tags=["invoice"])
async def payment_invoice_qris(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await payment_invoice_qris()


# ============================================


@app.post(
    "/mob_user/melakukan_absen_selfi", response_model=dict, tags=["absen - akses"]
)
async def melakukan_absen_selfi(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await melakukan_absen_selfi()


@app.post("/mob_user/get_data_absen", response_model=dict, tags=["absen - akses"])
async def get_data_absen(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_data_absen()


@app.post("/mob_user/buka_pintu", response_model=dict, tags=["absen - akses"])
async def buka_pintu(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await buka_pintu()


@app.post("/mob_user/tutup_pintu", response_model=dict, tags=["absen - akses"])
async def tutup_pintu(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await tutup_pintu()


# ============================================


@app.post("/mob_user/cek_data_wahana", response_model=dict, tags=["wisata"])
async def cek_data_wahana(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await cek_data_wahana()


@app.post("/mob_user/cek_detail_wahana", response_model=dict, tags=["wisata"])
async def cek_detail_wahana(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await cek_detail_wahana()


@app.post("/mob_user/inquiry_beli_tiket", response_model=InquiryBase, tags=["wisata"])
async def inquiry_beli_tiket(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await inquiry_beli_tiket()


@app.post("/mob_user/payment_beli_tiket", response_model=dict, tags=["wisata"])
async def payment_beli_tiket(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await payment_beli_tiket()


@app.post("/mob_user/topup_va", response_model=dict, tags=["topup"])
async def topup_va(
    bank: str,
    userId: str,
    amount: int,
    noreff: str,
    companyId: str
    # current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    # return await payment_beli_tiket()
    return await topup_va_bank_by_gateway(bank, userId, amount, noreff, companyId)


@app.on_event("startup")
async def app_startup():
    config.load_config()


@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()
