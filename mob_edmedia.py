from datetime import date
from model.support import InvoiceData
from util.util import cleanNullTerms
from route.trxuser import transfer_saldo_on_us
from function.user import CreateOpenVa, CreateQrCode, InquiryQrCode
import json
from route.info import get_all_infos
from model.util import FieldBoolRequest, SearchRequest
from route.notif import get_my_notifs
from model.default import JwtToken, UserBasic, UserDataBase
from route.auth import (
    get_current_user,
    login_for_access_token,
    user_change_password,
    user_create_password_first_login,
)
from route.user import get_my_profile, get_my_datas
from fastapi import FastAPI, Response, Depends, Security
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.openapi.docs import get_swagger_ui_html
from config import config
from config.config import URL_SQL
from fastapi.security import (
    OAuth2PasswordBearer,
    OAuth2PasswordRequestForm,
    SecurityScopes,
)
import requests
from typing import List, Optional
from route.auth import router_auth
from route.edmedia.pelajar import *
from route.edmedia.pengajar import *
from route.edmedia.rpp_student import *
from route.edmedia.rpp_teacher import *
from route.edmedia.combo import *
from route.edmedia.rpp import *

from model.edmedia.kbm import UserAbsesni
from model.edmedia.silabus import comboKbmTeacher
from model.edmedia.pelajar import PelajarDatas
from model.edmedia.materi import SoalDijawab, TugasDijawab

from model.membership.member import MemberBase

from function.invoice import (
    GetInvoiceOr404,
    GetInvoicesOnUserId,
    GetInvoicesPaidOnUserId,
    GetInvoicesUnpaidOnUserId,
)

from sqlmodel import Session, select, or_

from sql.model import TransactionBase, engine

app = FastAPI(openapi_url="/mob_user/openapi.json", docs_url="/mob_user/swgr")
app.mount("/mob_user/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == "production":
    # setup Elastic APM Agent
    apm = make_apm_client(
        {
            "SERVICE_NAME": "pysolusi-mobile",
            "SERVER_URL": "http://apm-server.logging:8200",
            "ELASTIC_APM_TRANSACTION_IGNORE_URLS": ["/health"],
            "METRICS_SETS": "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
        }
    )
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*", "*"],
    allow_credentials=True,
    allow_methods=["*", "*"],
    allow_headers=["*", "*"],
)


@app.get("/health")
async def health():
    return {"status": "ok"}


app.include_router(
    router_auth,
    prefix="/api_default/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)

# ============================================


@app.post(
    "/mob_edmedia/student/profile/pelajar",
    response_model=PelajarBase,
    tags=["edmedia-student"],
)
async def mob_add_pelajar(
    dataIn: PelajarInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await add_pelajar(dataIn, current_user)


@app.post("/mob_edmedia/student/profile/upload_pelajar", tags=["edmedia-student"])
async def mob_upload_pelajar(
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await upload_pelajar(file, current_user)


@app.post(
    "/mob_edmedia/student/profile/get_pelajar",
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_get_all_pelajars(
    size: int = 10,
    page: int = 0,
    sort: str = "name",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_all_pelajars(size, page, sort, dir, search, current_user)


@app.get(
    "/mob_edmedia/student/profile/pelajar/{id_}",
    response_model=PelajarOnDB,
    tags=["edmedia-student"],
)
async def mob_get_pelajar_by_id(
    id_: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_pelajar_by_id(id_, current_user)


@app.get(
    "/mob_edmedia/student/profile/pelajar/user/{userId}",
    response_model=PelajarOnDB,
    tags=["edmedia-student"],
)
async def mob_get_pelajar_by_user_id(
    userId: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_pelajar_by_user_id(userId, current_user)


@app.delete(
    "/mob_edmedia/student/profile/pelajar/{id_}",
    dependencies=[Depends(GetPelajarOr404)],
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_delete_pelajar_by_id(
    id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await delete_pelajar_by_id(id_, current_user)


@app.put(
    "/mob_edmedia/student/profile/pelajar/{id_}",
    response_model=PelajarOnDB,
    tags=["edmedia-student"],
)
async def mob_update_pelajar(
    id_: str,
    dataIn: PelajarBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await update_pelajar(id_, dataIn, current_user)


@app.post(
    "/mob_edmedia/student/get_rpp/{userId}",
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_get_rpp_by_user_id_pelajar(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "rpp.jamMulai",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_rpp_by_user_id_pelajar(
        userId, size, page, sort, dir, search, current_user
    )


@app.post(
    "/mob_edmedia/student/get_rpp/tanggal/{userId}/{tanggal}",
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_get_rpp_by_user_id_pelajar_by_tanggal(
    userId: str,
    tanggal: str,
    size: int = 10,
    page: int = 0,
    sort: str = "rpp.jamMulai",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_rpp_by_user_id_pelajar_by_tanggal(
        userId, tanggal, size, page, sort, dir, search, current_user
    )


@app.post(
    "/mob_edmedia/student/get_rpp/silabus/{userId}/{idSilabus}",
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_get_rpp_by_user_id_pelajar_by_id_silabus(
    userId: str,
    idSilabus: str,
    size: int = 10,
    page: int = 0,
    sort: str = "rpp.jamMulai",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_rpp_by_user_id_pelajar_by_id_silabus(
        userId, idSilabus, size, page, sort, dir, search, current_user
    )


@app.get("/mob_edmedia/student/jadwal/pelajar/{userId}", tags=["edmedia-student"])
async def mob_get_jadwal_by_id_pelajar(
    userId: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_jadwal_by_id_pelajar(userId, current_user)


@app.post(
    "/mob_edmedia/student/latihan/{userId}/{idKbm}/{pertemuanKe}",
    response_model=List[SoalDijawab],
    tags=["edmedia-student"],
)
async def mob_add_latihan_soal_by_user_id_pelajar(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await add_latihan_soal_by_user_id_pelajar(
        userId, idKbm, pertemuanKe, current_user
    )


@app.get(
    "/mob_edmedia/student/get_latihan/{userId}/{idKbm}/{pertemuanKe}",
    response_model=List[SoalDijawab],
    tags=["edmedia-student"],
)
async def mob_get_all_latihan_soal_by_user_id_pelajar(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_all_latihan_soal_by_user_id_pelajar(
        userId, idKbm, pertemuanKe, current_user
    )


@app.post(
    "/mob_edmedia/student/get_latihan/{userId}/{idKbm}/{pertemuanKe}",
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_get_one_latihan_soal_by_user_id_pelajar(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    size: int = 1,
    page: int = 0,
    sort: str = "noSoal",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_one_latihan_soal_by_user_id_pelajar(
        userId, idKbm, pertemuanKe, size, page, sort, dir, search, current_user
    )


@app.post(
    "/mob_edmedia/student/start_latihan/{userId}/{idKbm}/{pertemuanKe}/{noSoal}",
    response_model=SoalDijawab,
    tags=["edmedia-student"],
)
async def mob_kerjakan_latihan_soal_by_user_id_pelajar_by_pertemuan_per_nomor(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    noSoal: int,
    data_in: SoalDijawab,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await kerjakan_latihan_soal_by_user_id_pelajar_by_pertemuan_per_nomor(
        userId, idKbm, pertemuanKe, noSoal, data_in, current_user
    )


@app.post(
    "/mob_edmedia/student/start_latihan/{userId}/{idKbm}/{pertemuanKe}",
    tags=["edmedia-student"],
)
async def mob_kerjakan_all_latihan_soal_by_user_id_pelajar(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    data_in: List[SoalDijawab],
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await kerjakan_all_latihan_soal_by_user_id_pelajar(
        userId, idKbm, pertemuanKe, data_in, current_user
    )


@app.post(
    "/mob_edmedia/student/stop_latihan/{userId}/{idKbm}/{pertemuanKe}",
    tags=["edmedia-student"],
)
async def mob_submit_latihan_soal_by_user_id_pelajar(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await submit_latihan_soal_by_user_id_pelajar(
        userId, idKbm, pertemuanKe, current_user
    )


@app.get(
    "/mob_edmedia/student/get_tugas/{userId}/{idKbm}/{pertemuanKe}",
    response_model=List[TugasDijawab],
    tags=["edmedia-student"],
)
async def mob_get_tugas_by_user_id_pelajar(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_tugas_by_user_id_pelajar(userId, idKbm, pertemuanKe, current_user)


@app.post(
    "/mob_edmedia/student/kerjakan_tugas/{userId}/{idKbm}/{pertemuanKe}/{idDetailTugas}",
    tags=["edmedia-student"],
)
async def mob_kerjakan_tugas_by_user_id_pelajar_by_id_tugas(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    idDetailTugas: str,
    data_in: TugasDijawab,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await kerjakan_tugas_by_user_id_pelajar_by_id_tugas(
        userId, idKbm, pertemuanKe, idDetailTugas, data_in, current_user
    )


@app.get(
    "/mob_edmedia/student/get_nilai_student/{userId}/{idKbm}/{pertemuanKe}",
    response_model=PelajarDatas,
    tags=["edmedia-student"],
)
async def mob_get_nilai_by_user_id_pelajar_kbm_by_pertemuan(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_nilai_by_user_id_pelajar_kbm_by_pertemuan(
        userId, idKbm, pertemuanKe, current_user
    )


@app.post(
    "/mob_edmedia/student/get_mapel_student/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}",
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_get_mapel_student_by_user_id_pelajar(
    userId: str,
    tahun: str,
    semester: str,
    kelas: str,
    kelasDetail: str,
    size: int = 10,
    page: int = 0,
    sort: str = "namaMapel",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_mapel_student_by_user_id_pelajar(
        userId,
        tahun,
        semester,
        kelas,
        kelasDetail,
        size,
        page,
        sort,
        dir,
        search,
        current_user,
    )


@app.post(
    "/mob_edmedia/student/get_detail_student/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}/{idSilabus}",
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_get_detail_student_by_user_id_pelajar(
    userId: str,
    tahun: str,
    semester: str,
    kelas: str,
    kelasDetail: str,
    idSilabus: str,
    size: int = 10,
    page: int = 0,
    sort: str = "dataKbm.pertemuanKe",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_detail_student_by_user_id_pelajar(
        userId,
        tahun,
        semester,
        kelas,
        kelasDetail,
        idSilabus,
        size,
        page,
        sort,
        dir,
        search,
        current_user,
    )


@app.post(
    "/mob_edmedia/student/get_pertemuan_student/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}/{idSilabus}/{pertemuanKe}",
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_get_pertemuan_student_by_user_id_pelajar(
    userId: str,
    tahun: str,
    semester: str,
    kelas: str,
    kelasDetail: str,
    idSilabus: str,
    pertemuanKe: int,
    size: int = 10,
    page: int = 0,
    sort: str = "dataKbm.pertemuanKe",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_pertemuan_student_by_user_id_pelajar(
        userId,
        tahun,
        semester,
        kelas,
        kelasDetail,
        idSilabus,
        pertemuanKe,
        size,
        page,
        sort,
        dir,
        search,
        current_user,
    )


@app.post(
    "/mob_edmedia/student/get_activity_student/{userId}",
    response_model=dict,
    tags=["edmedia-student"],
)
async def mob_get_activity_student_by_user_id_pelajar(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "dataKbm.waktu",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pelajar", "*"]),
):
    return await get_activity_student_by_user_id_pelajar(
        userId, size, page, sort, dir, search, current_user
    )


# ============================================


@app.post(
    "/mob_edmedia/teacher/profile/pengajar",
    response_model=PengajarBase,
    tags=["edmedia-teacher"],
)
async def mob_add_pengajar(
    dataIn: UserInput,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await add_pengajar(dataIn, current_user)


@app.post("/mob_edmedia/teacher/profile/upload_pengajar", tags=["edmedia-teacher"])
async def mob_upload_pengajar(
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await upload_pengajar(file, current_user)


@app.post(
    "/mob_edmedia/teacher/profile/get_pengajar",
    response_model=dict,
    tags=["edmedia-teacher"],
)
async def mob_get_all_pengajars(
    size: int = 10,
    page: int = 0,
    sort: str = "name",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_all_pengajars(size, page, sort, dir, search, current_user)


@app.get(
    "/mob_edmedia/teacher/profile/pengajar/{id_}",
    response_model=PengajarBase,
    tags=["edmedia-teacher"],
)
async def mob_get_pengajar_by_id(
    id_: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_pengajar_by_id(id_, current_user)


@app.get(
    "/mob_edmedia/teacher/profile/pengajar/user/{id_}",
    response_model=PengajarOnDB,
    tags=["edmedia-teacher"],
)
async def mob_get_pengajar_by_user_id(
    id_: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_pengajar_by_user_id(id_, current_user)


@app.delete(
    "/mob_edmedia/teacher/profile/pengajar/{id_}",
    dependencies=[Depends(GetPengajarOr404)],
    response_model=dict,
    tags=["edmedia-teacher"],
)
async def mob_delete_pengajar_by_id(
    id_: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await delete_pengajar_by_id(id_, current_user)


@app.put(
    "/mob_edmedia/teacher/profile/pengajar/{id_}",
    response_model=PengajarOnDB,
    tags=["edmedia-teacher"],
)
async def mob_update_pengajar(
    id_: str,
    dataIn: PengajarBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await update_pengajar(id_, dataIn, current_user)


@app.post(
    "/mob_edmedia/teacher/get_rpp_pengajar/{userId}",
    response_model=dict,
    tags=["edmedia-teacher"],
)
async def mob_get_rpp_by_user_id_pengajar(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "rpp.jamMulai",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_rpp_by_user_id_pengajar(
        userId, size, page, sort, dir, search, current_user
    )


@app.post(
    "/mob_edmedia/teacher/get_next_rpp_pengajar/{userId}",
    response_model=dict,
    tags=["edmedia-teacher"],
)
async def mob_get_next_rpp_by_user_id_pengajar(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "rpp.jamMulai",
    dir: int = -1,
    limit: int = 4,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_next_rpp_by_user_id_pengajar(
        userId, size, page, sort, dir, limit, search, current_user
    )


@app.post(
    "/mob_edmedia/teacher/get_rpp_pengajar/tanggal/{userId}/{tanggal}",
    response_model=dict,
    tags=["edmedia-teacher"],
)
async def mob_get_rpp_by_user_id_pengajar_by_tanggal(
    userId: str,
    tanggal: str,
    size: int = 10,
    page: int = 0,
    sort: str = "rpp.jamMulai",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_rpp_by_user_id_pengajar_by_tanggal(
        userId, tanggal, size, page, sort, dir, search, current_user
    )


@app.get("/mob_edmedia/teacher/jadwal/pengajar/{userId}", tags=["edmedia-teacher"])
async def mob_get_jadwal_by_user_id_pengajar(
    userId: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_jadwal_by_user_id_pengajar(userId, current_user)


@app.post(
    "/mob_edmedia/teacher/get_peserta/{id_}",
    response_model=dict,
    tags=["edmedia-teacher"],
)
async def mob_get_peserta_by_id_kbm(
    id_: str,
    size: int = 10,
    page: int = 0,
    sort: str = "name",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_peserta_by_id_kbm(id_, size, page, sort, dir, search, current_user)


@app.post(
    "/mob_edmedia/teacher/all_peserta_absen/{id_}/{pertemuanKe}",
    tags=["edmedia-teacher"],
)
async def mob_add_all_peserta_absen_by_id_kbm_by_pertemuan(
    id_: str,
    pertemuanKe: int,
    data_in: List[UserAbsesni],
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await add_all_peserta_absen_by_id_kbm_by_pertemuan(
        id_, pertemuanKe, data_in, current_user
    )


@app.post(
    "/mob_edmedia/teacher/peserta_absen/{id_}/{pertemuanKe}/{userIdPelajar}",
    tags=["edmedia-teacher"],
)
async def mob_add_peserta_absen_by_id_kbm_by_pertemuan(
    id_: str,
    pertemuanKe: int,
    userIdPelajar: str,
    data_in: UserAbsesni,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await add_peserta_absen_by_id_kbm_by_pertemuan(
        id_, pertemuanKe, userIdPelajar, data_in, current_user
    )


@app.get(
    "/mob_edmedia/teacher/peserta_absen/{id_}/{pertemuanKe}",
    response_model=List[UserAbsesni],
    tags=["edmedia-teacher"],
)
async def mob_get_peserta_absen_by_id_kbm_by_pertemuan(
    id_: str,
    pertemuanKe: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_peserta_absen_by_id_kbm_by_pertemuan(
        id_, pertemuanKe, current_user
    )


@app.get(
    "/mob_edmedia/teacher/get_kelas_teacher/{idPengajar}",
    response_model=List[comboKbmTeacher],
    tags=["edmedia-teacher"],
)
async def mob_get_kelas_by_user_id_teacher(
    idPengajar: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_kelas_by_user_id_teacher(idPengajar, current_user)


@app.get(
    "/mob_edmedia/teacher/get_nilai/{kelasDetail}/{idKbm}/{pertemuanKe}",
    response_model=List[PelajarDatas],
    tags=["edmedia-teacher"],
)
async def mob_get_nilai_by_kbm_by_pertemuan(
    kelasDetail: str,
    idKbm: str,
    pertemuanKe: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_nilai_by_kbm_by_pertemuan(
        kelasDetail, idKbm, pertemuanKe, current_user
    )


@app.get(
    "/mob_edmedia/teacher/koreksi_get/{userId}/{idKbm}/{pertemuanKe}",
    response_model=List[TugasDijawab],
    tags=["edmedia-teacher"],
)
async def mob_get_koreksi_by_user_id_pelajar(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_koreksi_by_user_id_pelajar(
        userId, idKbm, pertemuanKe, current_user
    )


@app.post(
    "/mob_edmedia/teacher/koreksi_tugas/{userId}/{idKbm}/{pertemuanKe}/{idDetailTugas}",
    tags=["edmedia-teacher"],
)
async def mob_koreksi_tugas_by_user_id_pelajar_by_id_tugas(
    userId: str,
    idKbm: str,
    pertemuanKe: int,
    idDetailTugas: str,
    data_in: TugasDijawab,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await koreksi_tugas_by_user_id_pelajar_by_id_tugas(
        userId, idKbm, pertemuanKe, idDetailTugas, data_in, current_user
    )


@app.post(
    "/mob_edmedia/teacher/get_detail_teacher/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}/{idSilabus}",
    response_model=dict,
    tags=["edmedia-teacher"],
)
async def mob_get_detail_student_by_user_id_teacher(
    userId: str,
    tahun: str,
    semester: str,
    kelas: str,
    kelasDetail: str,
    idSilabus: str,
    size: int = 10,
    page: int = 0,
    sort: str = "rpp.pertemuanKe",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_detail_student_by_user_id_teacher(
        userId,
        tahun,
        semester,
        kelas,
        kelasDetail,
        idSilabus,
        size,
        page,
        sort,
        dir,
        search,
        current_user,
    )


@app.post(
    "/mob_edmedia/teacher/get_detail_teacher/{userId}/{tahun}/{semester}/{kelas}/{kelasDetail}/{idSilabus}/{pertemuanKe}",
    response_model=dict,
    tags=["edmedia-teacher"],
)
async def mob_get_detail_student_by_user_id_teacher(
    userId: str,
    tahun: str,
    semester: str,
    kelas: str,
    kelasDetail: str,
    idSilabus: str,
    pertemuanKe: int,
    size: int = 10,
    page: int = 0,
    sort: str = "dataKbm.pertemuanKe",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_detail_student_by_user_id_teacher(
        userId,
        tahun,
        semester,
        kelas,
        kelasDetail,
        idSilabus,
        pertemuanKe,
        size,
        page,
        sort,
        dir,
        search,
        current_user,
    )


@app.post(
    "/mob_edmedia/teacher/get_activity_pengajar/{userId}",
    response_model=dict,
    tags=["edmedia-teacher"],
)
async def mob_get_activity_by_user_id_pengajar(
    userId: str,
    size: int = 10,
    page: int = 0,
    sort: str = "rpp.jamMulai",
    dir: int = -1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(
        get_current_user, scopes=["pengajar", "*"]),
):
    return await get_activity_by_user_id_pengajar(
        userId, size, page, sort, dir, search, current_user
    )


# ============================================


@app.get(
    "/mob_edmedia/combo/get_combo_pengajar",
    response_model=List[comboPengajar],
    tags=["edmedia-combo"],
)
async def mob_get_combo_pengajar(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_combo_pengajar(current_user)


@app.get(
    "/mob_edmedia/combo/get_combo_pengajar/{userId}",
    response_model=comboPengajar,
    tags=["edmedia-combo"],
)
async def mob_get_combo_pengajar_by_user_id_pengajar(
    userId: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_combo_pengajar_by_user_id_pengajar(userId, current_user)


@app.get(
    "/mob_edmedia/combo/get_combo_materi",
    response_model=List[comboMateri],
    tags=["edmedia-combo"],
)
async def mob_get_combo_materi(
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin,pengajar", "*"]
    )
):
    return await get_combo_materi(current_user)


@app.get(
    "/mob_edmedia/combo/get_combo_materi/{idMateri}",
    response_model=comboMateri,
    tags=["edmedia-combo"],
)
async def mob_get_combo_materi_by_id_materi(
    idMateri: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    return await get_combo_materi_by_id_materi(idMateri, current_user)


@app.get(
    "/mob_edmedia/combo/get_combo_silabus",
    response_model=List[comboSilabus],
    tags=["edmedia-combo"],
)  # combo all untuk admin dan pengajar
async def mob_get_combo_silabus(
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin,pengajar", "*"]
    )
):
    return await get_combo_silabus(current_user)


@app.get(
    "/mob_edmedia/combo/get_combo_silabus/userId",
    response_model=List[comboSilabus],
    tags=["edmedia-combo"],
)  # combo opsi silabus di timeline
async def mob_get_combo_silabus_timeline_by_user_id(
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin, admin, pengajar, pelajar", "*"]
    )
):
    return await get_combo_silabus_timeline_by_user_id(current_user)


@app.get(
    "/mob_edmedia/combo/get_combo_generate_kbm",
    response_model=List[comboGenerateKbm],
    tags=["edmedia-combo"],
)
async def mob_get_combo_generate_kbm(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_combo_generate_kbm(current_user)


@app.get("/mob_edmedia/combo/get_combo_kelas", tags=["edmedia-combo"])
async def mob_get_combo_kelas(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_combo_kelas(current_user)


@app.get("/mob_edmedia/combo/get_combo_kelasDetail/{kelas}", tags=["edmedia-combo"])
async def mob_get_combo_kelas_detail(
    kelas: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_combo_kelas_detail(kelas, current_user)


@app.get(
    "/mob_edmedia/combo/get_combo_ruang",
    response_model=List[comboRuang],
    tags=["edmedia-combo"],
)
async def mob_get_combo_ruang(
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    return await get_combo_ruang(current_user)


@app.post(
    "/mob_edmedia/comment/{id_}/{pertemuanKe}",
    tags=["edmedia-rpp"]
)
async def mob_add_comment_by_id_kbm_by_pertemuan(
    id_: str,
    pertemuanKe: int,
    data_in: CommentBase,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pelajar,pengajar", "*"])
):
    return await add_comment_by_id_kbm_by_pertemuan(id_, pertemuanKe, data_in, current_user)


@app.post(
    "/mob_edmedia/reply/{id_}/{pertemuanKe}/{idKomen}",
    tags=["edmedia-rpp"]
)
async def mob_add_reply_by_id_kbm_by_pertemuan(
    id_: str,
    pertemuanKe: int,
    idKomen: str,
    data_in: CommentReply,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pelajar,pengajar", "*"])
):
    return await add_reply_by_id_kbm_by_pertemuan(id_, pertemuanKe, idKomen, data_in, current_user)


@app.get(
    "/mob_edmedia/comment/{id_}/{pertemuanKe}/{idKomen}",
    tags=["edmedia-rpp"],
    response_model=CommentBase
)
async def mob_get_comment_by_id_kbm_by_pertemuan(
    id_: str,
    pertemuanKe: int,
    idKomen: str,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pelajar,pengajar", "*"])
):
    return await get_comment_by_id_kbm_by_pertemuan(id_, pertemuanKe, idKomen, current_user)


@app.post(
    "/mob_edmedia/absensi/mapel/{companyId}/{idSilabus}",
    tags=["edmedia-rpp"],
)
async def mob_absensi_per_mapel(
    companyId: str,
    idSilabus: str,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pengajar", "*"])
):
    return await absensi_per_mapel(companyId, idSilabus, search, current_user)


@app.get(
    "/mob_edmedia/absensi/tanggal/{companyId}/{tanggal}",
    tags=["edmedia-rpp"],
    response_model=list
)
async def mob_absensi_per_tanggal(
    companyId: str,
    tanggal: str,
    kelasDetail: Optional[str] = None,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pengajar", "*"])
):
    return await absensi_per_tanggal(companyId, tanggal, kelasDetail, current_user)


@app.post(
    "/mob_edmedia/nilai/mapel/{companyId}/{idSilabus}",
    tags=["edmedia-rpp"],
)
async def mob__nilai_per_mapel(
    companyId: str,
    idSilabus: str,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin,admin,pengajar", "*"])
):
    return await nilai_per_mapel(companyId, idSilabus, search, current_user)


@app.on_event("startup")
async def app_startup():
    config.load_config()


@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()
