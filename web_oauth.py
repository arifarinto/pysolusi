from typing import Optional
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from util.util import RandomString, ValidateObjectId
from config.config import MGDB
from config import config
from model.default import RegisterBase
from fastapi import FastAPI, Depends, HTTPException
from starlette.config import Config
from starlette.requests import Request
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import HTMLResponse, RedirectResponse
from authlib.integrations.starlette_client import OAuth
from datetime import timedelta

from function.user import CheckEmailOnly, CheckEmailWithCompany, CreateCredentialTemporary
from util.util_waktu import dateTimeNow

import os
import requests_oauthlib
from requests_oauthlib.compliance_fixes import facebook_compliance_fix

BASEURL = ''

app = FastAPI(openapi_url="/web_oauth/openapi.json",docs_url="/web_oauth/swgr")
if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-goauth',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)
    
app.add_middleware(SessionMiddleware, secret_key='ifpeoiu83iueoi9028')

if config.ENVIRONMENT == 'production':
    BASEURL = 'http://sundev.duckdns.org'
elif config.ENVIRONMENT == 'staging':
    BASEURL = 'http://sundev.duckdns.org'
elif config.ENVIRONMENT == 'development':
    BASEURL = 'http://sundev.duckdns.org'


@app.get("/health")
async def health():
    return {"status": "ok"}

# GOOGLE ==================================================================================

oauth = OAuth(Config('google.env'))

CONF_URL = 'https://accounts.google.com/.well-known/openid-configuration'
oauth.register(
    name='google',
    server_metadata_url=CONF_URL,
    client_kwargs={
        'scope': 'openid email profile'
    }
)

@app.get('/web_oauth/google_cek')
async def login(companyId:str, returnUrl:str, userTipe:str, request: Request):
    # redirect_uri = request.url_for('auth')
    redirect_uri = BASEURL+'/web_oauth/auth'
    request.session['companyId'] = companyId
    request.session['returnUrl'] = returnUrl
    request.session['userTipe'] = userTipe
    return await oauth.google.authorize_redirect(request, redirect_uri)
    

@app.route('/web_oauth/auth')
async def auth(request: Request):
    token = await oauth.google.authorize_access_token(request)
    user = await oauth.google.parse_id_token(request, token)
    request.session['user'] = dict(user)
    email = user['email']
    returnUrl = request.session.get('returnUrl')
    userTipe = request.session.get('userTipe')
    #jika sudah terdaftar return login
    cekData = await CheckEmailOnly(userTipe,email)
    if cekData != False:
        temporary = RandomString(30)
        if len(cekData) == 1:
            companyId = str(cekData[0]["companyId"])
            CreateCredentialTemporary(userTipe,companyId,email,temporary)
            #GANTI ke url front end
            return RedirectResponse(url=returnUrl+'/oauth_login?email='+email+'&companyId='+companyId+'&temporary='+temporary+'&userTipe='+userTipe)
        else:
            companyId = str(cekData[0]["companyId"])
            # milih company dulu, baru lanjut create credential temporary. INSERT ke tbl_temporary_login
            return RedirectResponse(url=returnUrl+'/oauth_login?email='+email+'&companyId='+companyId+'&temporary='+temporary+'&userTipe='+userTipe)
        
        
    #jika belum terdaftar return register
    else: 
        # {"regId":regId,"tempPwd":tempPwd,"email":cekOtp['email']}
        data = RegisterBase()
        data.email = email
        companyId = request.session.get('companyId')
        data.companyId = companyId
        data.createTime = dateTimeNow()
        data.expiredTime = dateTimeNow() + timedelta(minutes=5)
        data.otpStatus = True
        data.password = RandomString(10)
        data.tblName = userTipe
        reg_op = await MGDB.tbl_register_verify.insert_one(data.dict())
        reg_id = str(reg_op.inserted_id)
        #GANTI ke url front end
        return RedirectResponse(url=returnUrl+'/oauth_register?email='+email+'&regId='+reg_id+'&tempPwd='+data.password+'&userTipe='+userTipe)
        

@app.get('/web_oauth/logout') 
async def logout(request: Request):
    request.session.pop('user', None)
    return RedirectResponse(url='/web_oauth/')


# FACEBOOK ==================================================================================

FB_CLIENT_ID = "458532722179768"
FB_CLIENT_SECRET = "e43ffd23d040dca9531bce449c089b7a"
FB_AUTHORIZATION_BASE = "https://www.facebook.com/dialog/oauth"
FB_TOKEN = "https://graph.facebook.com/oauth/access_token"

FB_SCOPE = ["email"]

os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"


@app.get("/web_oauth/fbhome")
async def home(request: Request):
    user = request.session.get('user')
    if user is not None:
        email = user['email']
        html = (
            f'<pre>Email: {email}</pre><br>'
            '<a href="/web_oauth/fblogout">logout</a>'
        )
        return HTMLResponse(html)
    else:
        return HTMLResponse('<a href="/web_oauth/fb_cek?companyId=602d192fb1d0832ec201c118&returnUrl='+BASEURL+'/web_oauth">Login with Facebook</a>')

@app.get('/web_oauth/fblogout') 
async def logout(request: Request):
    request.session.pop('user', None)
    return RedirectResponse(url='/web_oauth/fbhome')

@app.get("/web_oauth/fb_cek")
async def login(companyId:str, returnUrl:str, userTipe:str, request: Request):
    if ValidateObjectId(companyId):
        request.session['companyId'] = companyId
        request.session['returnUrl'] = returnUrl
        request.session['userTipe'] = userTipe
        facebook = requests_oauthlib.OAuth2Session(
            FB_CLIENT_ID, redirect_uri= BASEURL + "/web_oauth/fb_callback", scope=FB_SCOPE
        )
        authorization_url, _ = facebook.authorization_url(FB_AUTHORIZATION_BASE)
        return RedirectResponse(authorization_url)
    else:
        raise HTTPException(status_code=400)


@app.get("/web_oauth/fb_callback")
async def callback(request: Request):
    facebook = requests_oauthlib.OAuth2Session(
        FB_CLIENT_ID, scope=FB_SCOPE, redirect_uri= BASEURL + "/web_oauth/fb_callback"
    )
    print("start")
    facebook = facebook_compliance_fix(facebook)
    urlCallback = str(request.url)
    print(urlCallback)
    print("mulai tanya ke facebook")
    facebook.fetch_token(
        FB_TOKEN,
        client_secret=FB_CLIENT_SECRET,
        authorization_response=urlCallback
    )
    facebook_user_data = facebook.get(
        "https://graph.facebook.com/me?fields=id,name,email,picture{url}"
    ).json()
    print("sukses dapat data")
    print(facebook_user_data)
    request.session['user'] = dict(facebook_user_data)
    email = facebook_user_data['email']
    companyId = request.session.get('companyId')
    returnUrl = request.session.get('returnUrl')
    userTipe = request.session.get('userTipe')
    #jika sudah terdaftar return login
    if await CheckEmailWithCompany(email,companyId) == True:
        temporary = RandomString(30)
        CreateCredentialTemporary(email,temporary)
        #GANTI ke url front end
        return RedirectResponse(url=returnUrl+'/oauth_login?email='+email+'&companyId='+companyId+'&temporary='+temporary+'&userTipe='+userTipe)
    #jika belum terdaftar return register
    else: 
        # {"regId":regId,"tempPwd":tempPwd,"email":cekOtp['email']}
        data = RegisterBase()
        data.email = email
        data.companyId = companyId
        data.createTime = dateTimeNow()
        data.expiredTime = dateTimeNow() + timedelta(minutes=5)
        data.otpStatus = True
        data.password = RandomString(10)
        reg_op = await MGDB.tbl_register_verify.insert_one(data.dict())
        reg_id = str(reg_op.inserted_id)
        #GANTI ke url front end
        return RedirectResponse(url=returnUrl+'/oauth_register?email='+email+'&regId='+reg_id+'&tempPwd='+data.password+'&userTipe='+userTipe)
        
    # 	https://api.katalis.info/main_base/register_self/create_account_and_password?regId=613ff38647f65d531dfe2c9b&tempPwd=ipprlhtgrk&name=arif&pwd=123123
    
@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()