from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.openapi.docs import get_swagger_ui_html
from config import config
from route.auth import router_auth
from route.crm_katalis.partner import router_partner
from route.crm_katalis.client import router_client
from route.crm_katalis.pipeline import router_pipeline

# from route.membership.member import router_member
from route.crm_katalis.member import router_member
from route.crm_katalis.lead import router_lead
from route.crm_katalis.followup import router_followup

tags_metadata = [
    {
        "name": "auth",
        "description": "Endpoint yang berkaitan dengan autentikasi. Termasuk di dalamnya reset data, login, logout, dan cek token.",
    },
    {
        "name": "partner",
        "description": "Semua endpoint yang berkaitan dengan manajemen partner, manajemen lead, dan manajemen follow up lead. Semua endpoints yang ada 'my_account'-nya digunakan oleh partner untuk mendapatkan datanya sendiri, dan wajib login sebagai partner. Endpoints yang tidak ada 'my_account'-nya berarti wajib login sebagai admin",
    },
    {
        "name": "lead",
        "description": "Semua endpoint yang berkaitan dengan manajemen lead partner.",
    },
    {
        "name": "followup",
        "description": "Semua endpoint yang berkaitan dengan manajemen followup lead.",
    },
    {
        "name": "client",
        "description": "Semua endpoint yang berkaitan dengan manajemen client dan manajemen member client. Semua endpoints yang ada 'my_account'-nya digunakan oleh client untuk mendapatkan datanya sendiri, dan wajib login sebagai client. Endpoints yang tidak ada 'my_account'-nya berarti wajib login sebagai admin",
    },
    {
        "name": "pipeline",
        "description": "Semua endpoint yang berkaitan dengan manajemen pipeline implementasi Katalis.",
    },
    {
        "name": "member",
        "description": "Semua endpoint yang berkaitan dengan manajemen user member.",
    },
]

description = """
API CRM Katalis.
"""

app = FastAPI(
    openapi_url="/api_crmkatalis/openapi.json",
    docs_url="/api_crmkatalis/swgr",
    openapi_tags=tags_metadata,
    description=description,
)
app.mount("/api_crmkatalis/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == "production":
    # setup Elastic APM Agent
    apm = make_apm_client(
        {
            "SERVICE_NAME": "pysolusi-crmkatalis",
            "SERVER_URL": "http://apm-server.logging:8200",
            "ELASTIC_APM_TRANSACTION_IGNORE_URLS": ["/health"],
            "METRICS_SETS": "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
        }
    )
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*", "*"],
    allow_credentials=True,
    allow_methods=["*", "*"],
    allow_headers=["*", "*"],
)


@app.get("/health")
async def health():
    return {"status": "ok"}


app.include_router(
    router_auth,
    prefix="/api_default/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_partner,
    prefix="/api_crmkatalis/partner",
    tags=["partner"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_lead,
    prefix="/api_crmkatalis/lead",
    tags=["lead"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_followup,
    prefix="/api_crmkatalis/followup",
    tags=["followup"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    router_client,
    prefix="/api_crmkatalis/client",
    tags=["client"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_member,
    prefix="/api_crmkatalis/member",
    tags=["member"],
    responses={404: {"description": "Not found"}},
)
app.include_router(
    router_pipeline,
    prefix="/api_crmkatalis/pipeline",
    tags=["pipeline"],
    responses={404: {"description": "Not found"}},
)


@app.on_event("startup")
async def app_startup():
    config.load_config()


@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()
