## harga, jadwal, parameter data
# config:
# - harga
# - jadwal
# - parameterData

from datetime import datetime
from model.util import ObjectIdStr
from typing import List
from model.support import ParameterData
from util.util_waktu import dateNowStr, dateTimeNow
from pydantic.main import BaseModel


class ConfigPPDBInput(BaseModel):
    harga: int = 0
    jadwal: datetime = dateTimeNow()


# class parameterInput()


class ConfigPPDB(ConfigPPDBInput):
    companyId: ObjectIdStr = None
    parameters: List[ParameterData] = []

    class Config:
        schema_extra = {
            "example": {
                "harga": 100000,
                "parameters": [
                    {
                        "key": "nama_ibu",
                        "tipe": "string",
                        "choice": [],
                        "isMultipleChoice": False,
                        "value": None,
                    },
                    {
                        "key": "pekerjaan_orangtua",
                        "tipe": "string",
                        "choice": [],
                        "isMultipleChoice": False,
                        "value": None,
                    },
                ],
            }
        }
