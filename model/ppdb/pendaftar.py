from datetime import datetime
from enum import Enum
from util.util_waktu import dateNowStr
from util.util import RandomNumber
from model.default import (
    AddressData,
    IdentityData,
    UserBasic,
    UserCredential,
    UserInput,
)
from model.support import DocumentData, InvoiceData, ParameterData
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from faker import Faker

fake = Faker()


class StatusPendaftaran(str, Enum):
    PROSES = "PROSES"
    TERIMA = "TERIMA"
    TOLAK = "TOLAK"
    TUNGGU = "TUNGGU"


class ImageActionType(str, Enum):
    imagePasphoto = "imagePasphoto"
    imageKtp = "imageKtp"
    imageIjazah = "imageIjazah"
    imageKk = "imageKk"
    imageRaport = "imageRaport"


class ImagePpdb(BaseModel):
    imagePasphoto: str = None
    imageIjazah: str = None
    imageKtp: str = None
    imageKk: str = None
    imageRaport: str = None
    imageAktaLahir: str = None


class PendaftarInput(UserInput):
    nomor: str = None
    npwp: str = None
    year: int = None
    name: str = None
    email: str = None
    jenjang: str = None
    jenjangDetail: str = None
    keterangan: int = None
    identity: IdentityData = {}
    address: AddressData = {}
    kelengkapanData: List[ParameterData] = []

    class Config:
        schema_extra = {
            "example": {
                "name": fake.name(),
                "email": fake.first_name() + "@coba.com",
                "jenjang": "SMP",
                "address": {"street": fake.address(), "note": "note"},
                "identity": {
                    "dateOfBirth": dateNowStr(),
                    "placeOfBirth": "Jakarta",
                    "gender": "male",
                },
            }
        }


class PipelineStatus(str, Enum):
    none = "none"
    todo = "todo"
    doing = "doing"
    done = "done"


class TemplateProsesBase(BaseModel):
    id: ObjectIdStr = None
    updateTime: datetime = None
    nomor: int = 0  # 1,2,3,4
    name: str = None  # presentasi, pks, integrasi va dll
    planTime: datetime = None
    note: str = None
    status: PipelineStatus = "none"
    isPassed: bool = False
    form: List[ParameterData] = []
    isValidate: bool = False


class PendaftarBase(UserBasic, PendaftarInput):
    userId: ObjectIdStr = None
    nomor: str = None
    npwp: str = None
    year: int = None
    name: str = None
    jenjang: str = None  # smp, sma
    jenjangDetail: str = None  # opsional
    keterangan: int = None
    identity: IdentityData = {}
    address: AddressData = {}
    image: ImagePpdb = {}
    kelengkapanData: List[ParameterData] = []
    lastInvoiceId: ObjectIdStr = None
    isVerified: bool = False
    status: StatusPendaftaran = "PROSES"
    invoicesData: List[InvoiceData] = []
    documents: List[DocumentData] = []
    pipelines: List[TemplateProsesBase] = []


class PendaftarCredential(PendaftarBase, UserCredential):
    pass


class PendaftarOnDB(PendaftarBase):
    id: ObjectIdStr = Field(alias="_id")


class PendaftarPage(DefaultPage):
    content: List[PendaftarOnDB] = []
