from enum import Enum
from pydantic import Field
from typing import List
from datetime import datetime

from pydantic.main import BaseModel

from .util import DefaultPage, ObjectIdStr, DefaultData

class ActionType(str, Enum):
    imageAvatar = "imageAvatar"
    imageKtp = "imageKtp"
    imageSelfieKtp = "imageSelfieKtp"
    imageNpwp = "imageNpwp"
    imageOther = "imageOther"

class ImageType(str, Enum):
    std = "std"
    thumb = "thumb"
    pas = "pas"
    ico = "ico"

class MediaBase(DefaultData):
    createTime: datetime = None
    userId: ObjectIdStr = None
    name: str = None
    fileName: str = None
    fileType: str = None
    used: int = 0
    temp: bool = True
    originName: str = None

class MediaBaseOnDB(MediaBase):
    id : ObjectIdStr = Field(alias="_id")

class MediaPage (DefaultPage):
    content: List[MediaBaseOnDB] = []

class UploadBase64(BaseModel):
    idMemberTemp: str = None
    filename: str = None
    base64text: str = None