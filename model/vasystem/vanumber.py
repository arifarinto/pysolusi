from enum import Enum
from pydantic import Field
from typing import List
from datetime import datetime

from pydantic.main import BaseModel
from model.util import DefaultData, ObjectIdStr, DefaultPage

class TransactionHistory(BaseModel):
    id: ObjectIdStr = None
    createTime: datetime = None
    updateTime: datetime = None
    prefix: str = None
    poolingAccountNumber: str = None
    billingId: str = None
    name: str = None
    amount: int = 0
    journalNumber: str = None
    narration: str = None
    sourceAccountNumber: str = None
    terminalMerchantId: str = None
    callbackStatus: bool = False
    lastCallback: datetime = None

class VaNumberBase(DefaultData):
    vaNumber: str = None
    transactions: List[TransactionHistory] = []
    totalCredit: int = 0
    lastTransaction: datetime = None
    isActive: bool = True

class VaNumberOnDB(VaNumberBase):
    id : ObjectIdStr = Field(alias="_id")