from enum import Enum
from pydantic import Field
from typing import List
from datetime import datetime

from pydantic.main import BaseModel
from model.util import DefaultData, ObjectIdStr, DefaultPage

class BillingType (str, Enum):
    fixPayment = "Fix Payment"
    partialPayment = "Partial Payment"
    minimumPayment = "Minimum Payment"
    openPayment = "Open Payment"
    openMinimumPayment = "Open Minimum Payment"
    openMaximumPayment = "Open Maximum Payment"

class VaNumber(BaseModel):
    id: ObjectIdStr = None
    createTime: datetime = None
    updateTime: datetime = None
    vaNumber: str = None
    billingId: str = None
    name: str = None
    email: str = None
    phone: str = None
    descripton: str = None
    billingType: BillingType = "Fix Payment"
    amount: int = 0
    expired: datetime = None

class InstitutionBase(DefaultData):
    prefix: str = None
    title: str = None
    poolingAccountNumber: str = None
    vaNumbers: List[VaNumber] = []
    isActive: bool = True
    callbackUrl: str = None
    credential: str = None
    ipclient: str = None

class InstitutionOnDB(InstitutionBase):
    id : ObjectIdStr = Field(alias="_id")

class InstitutionPage (DefaultPage):
    content: List[InstitutionOnDB] = []

class ArrayVaNumbers (BaseModel):
    vaNumbers: VaNumber = {}

class VaNumberPage (DefaultPage):
    content: List[ArrayVaNumbers] = []