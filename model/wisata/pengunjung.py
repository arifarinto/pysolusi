#isi log aktivitasnya sekalian
from enum import Enum
from datetime import date, datetime
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput
from faker import Faker

fake = Faker()

class TiketGatewayData(BaseModel):
    id: ObjectIdStr = None
    namaWahana: str = None
    isUsed: bool = False
    usedTime: datetime = None
    jumlahTiket: int = 1

class TiketEnum(str, Enum):
    lokal_dewasa = "lokal_dewasa"
    lokal_anak = "lokal_anak"
    manca_dewasa = "manca_dewasa"
    manca_anak = "manca_anak"

class TiketInput(BaseModel):
    # id: ObjectIdStr = None
    idDestinasi: ObjectIdStr = None
    idWahana: ObjectIdStr = None
    idJadwal: ObjectIdStr = None
    jenisTiket: TiketEnum = None
    jumlah: int = 0
    visitDate: datetime = None
    isPaid: bool = False
    
class TiketData(TiketInput):
    id: ObjectIdStr = None
    key: str = None
    namaDestinasi: str = None
    namaWahana: str = None
    gateway: List[TiketGatewayData] = []
    orderTime: datetime = None
    totalHarga: int = 0

class PengunjungBase(DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None
    tikets: List[TiketData] = []

class PengunjungCredential(PengunjungBase):
    credential: CredentialData = {}

class PengunjungOnDB(PengunjungBase):
    id : ObjectIdStr = Field(alias="_id")

class PengunjungPage (DefaultPage):
    content: List[PengunjungOnDB] = []

class comboPengunjung(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None


