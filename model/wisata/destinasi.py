#isi log aktivitasnya sekalian
from datetime import date, datetime
from util.util_waktu import dateTimeNow
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput
from util.util import RandomNumber,RandomString
from faker import Faker

fake = Faker()

class WahanaJadwalData(BaseModel):
    idJadwal: ObjectIdStr = None
    tanggal: datetime = None
    isOpen: bool = True
    capacity: int = 0
    pengunjungOrder: int = 0
    pengunjungBayar: int = 0
    pengunjungDatang: int = 0
    price: int = 0

class WahanaData(BaseModel):
    id: ObjectIdStr = None
    namaWahana: str = None
    note: str = None
    images: List[str] = []
    jadwal: List[WahanaJadwalData] = []

    class Config:
        schema_extra = {
            "example": {
                "id": "string",
                "namaWahana": "string",
                "note": "string",
                "images": [],
                "jadwal": [{
                    "tanggal": dateTimeNow(),
                    "isOpen": True,
                    "capacity": 0,
                    "pengunjungOrder": 0,
                    "pengunjungBayar": 0,
                    "pengunjungDatang": 0,
                    "price" : 0,
                }]
            }
        }

class DestinasiBase(DefaultData):
    name: str = None
    address: AddressData = None
    note: str = None
    images: List[str] = []
    wahana: List[WahanaData] = []

class DestinasiOnDB(DestinasiBase):
    id : ObjectIdStr = Field(alias="_id")

class DestinasiPage (DefaultPage):
    content: List[DestinasiOnDB] = []