#isi log aktivitasnya sekalian
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput

class GatewayBase(DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None
    idDestinasi: ObjectIdStr = None
    idWahana: ObjectIdStr = None

class GatewayCredential(GatewayBase):
    credential: CredentialData = {}

class GatewayOnDB(GatewayBase):
    id : ObjectIdStr = Field(alias="_id")

class GatewayPage (DefaultPage):
    content: List[GatewayOnDB] = []

class comboGateway(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None