#isi log aktivitasnya sekalian
from model.wisata.pengunjung import TiketData
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput

class MiniatmBase(DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None
    tikets: List[TiketData] = []

class MiniatmCredential(MiniatmBase):
    credential: CredentialData = {}

class MiniatmOnDB(MiniatmBase):
    id : ObjectIdStr = Field(alias="_id")

class MiniatmPage (DefaultPage):
    content: List[MiniatmOnDB] = []

class comboMiniatm(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None