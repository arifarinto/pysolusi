from bson.objectid import ObjectId
from enum import Enum
from pydantic import BaseModel, Field
from typing import List
from datetime import datetime, date


class ObjectIdStr(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if type(v) == str:
            print(v)
            v = ObjectId(v)
        if not isinstance(v, ObjectId):
            raise ValueError("Not a valid ObjectId")
        return str(v)


class DefaultData(BaseModel):
    createTime: datetime = datetime.now()
    updateTime: datetime = datetime.now()
    companyId: ObjectIdStr = None  # bank
    creatorUserId: ObjectIdStr = None
    isDelete: bool = False


class DefaultPage(BaseModel):
    status: int = 200
    size: int = 0
    page: int = 0
    totalElements: int = 0
    totalPages: int = 0
    sort: int = 0
    sortDirection: str = None


class FieldRequest(BaseModel):
    field: str = None
    key: str = None


class FieldNumberRequest(BaseModel):
    field: str = None
    key: int = None


class FieldBoolRequest(BaseModel):
    field: str = None
    key: bool = False


class FieldObjectIdRequest(BaseModel):
    field: str = None
    key: ObjectIdStr = None


class FieldArrayRequest(BaseModel):
    field: str = None
    key: List[str] = []


class SearchRequest(BaseModel):
    search: List[FieldRequest] = []
    fixSearch: List[FieldRequest] = []
    numbSearch: List[FieldNumberRequest] = []
    tagSearch: List[FieldArrayRequest] = []
    tagInSearch: List[FieldArrayRequest] = []
    defaultBool: List[FieldBoolRequest] = []
    defaultObjectId: List[FieldObjectIdRequest] = []


class EmailData(BaseModel):
    name: str = None
    projectName: str = None
    companyName: str = None
    emailTo: str = None
    token: str = None
