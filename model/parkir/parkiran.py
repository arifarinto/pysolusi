from datetime import date, datetime
from enum import Enum
from util.util_waktu import dateTimeNow
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput
from util.util import RandomNumber,RandomString
from faker import Faker


class TipeEnum(str, Enum):
    motor = "motor"
    mobil = "mobil"
    truk = "truk"
    other = "other"


class HargaData(BaseModel):
    tipe: TipeEnum = None
    harga: int = 0


class ParkiranBase(DefaultData):
    name: str = None
    kode: str = None
    detail: str = None   # penjelasan letak parkiran
    daftarHarga : List[HargaData] = []
    kapasitas: int = 0
    note: str = None
    aktif: bool = True
    tags: List[str] = None
    images: List[str] = []


class ParkiranOnDB(ParkiranBase):
    id: ObjectIdStr = Field(alias="_id")


class ParkiranPage(DefaultPage):
    content: List[ParkiranOnDB] = []


class comboParkiran(BaseModel):
    id: ObjectIdStr = Field(alias="_id")
    name: str = None