from enum import Enum
from datetime import date, datetime, time
from fastapi.datastructures import Default
from pydantic import BaseModel, Field
from typing import List
from model.parkir.parkiran import TipeEnum
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput
from faker import Faker

from util.util_waktu import dateTimeNow

fake = Faker()


class KarcisGatewayData(BaseModel):
    id: ObjectIdStr = None
    namaPemarkir: str = None
    isUsed: bool = False
    usedTime: datetime = None


class KarcisInput(BaseModel):
    id: ObjectIdStr = None
    idParkiran: ObjectIdStr = None
    waktuKeluar: datetime = None
    isPaid: bool = False


class KarcisData(KarcisInput):
    namaParkiran: str = None
    gateway: List[KarcisGatewayData] = []
    tipeKendaraan: TipeEnum = None
    waktuMasuk: datetime = None
    totalBayar: int = 0


class PemarkirTempData(BaseModel):
    userTempId: ObjectIdStr = None
    name: str = None
    note: str = None
    karcis: KarcisData = None

    class Config:
        schema_extra = {
            "example": {
                "userTempId": "string",
                "name": "string",
                "note": "string",
                "karcis": {}
            }
        }


class UserParkirBase(DefaultData):
    tanggal: date = None
    tahun: str = None
    users: List[PemarkirTempData] = []


class UserParkirOnDB(UserParkirBase):
    id: ObjectIdStr = Field(alias="_id")


class UserParkirPage(DefaultPage):
    content: List[UserParkirOnDB] = []
