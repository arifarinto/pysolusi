from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput

class GatewayParkirBase(DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None
    idParkiran: ObjectIdStr = None

class GatewayParkirCredential(GatewayParkirBase):
    credential: CredentialData = {}

class GatewayParkirOnDB(GatewayParkirBase):
    id : ObjectIdStr = Field(alias="_id")

class GatewayParkirPage (DefaultPage):
    content: List[GatewayParkirOnDB] = []

class comboGatewayParkir(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None