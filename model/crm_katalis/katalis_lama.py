from enum import Enum
from typing import List
from pydantic import BaseModel, Field
from datetime import date, datetime
from ..util import DefaultPage, ObjectIdStr, DefaultData


class AkunDefaultBase(BaseModel):
    createTime: datetime = datetime.now()
    updateTime: datetime = datetime.now()
    companyId: ObjectIdStr = None
    companyName: str = None
    creatorId: ObjectIdStr = None  # idAkun
    levelTipe: List[str] = []  # PUSAT,WBA,SEMARANG,accountNumber
    creatorName: str = None
    isDeleted: bool = False


class MemberTempBase(AkunDefaultBase):
    idAkun: ObjectIdStr = None
    number: str = None
    number2: str = None
    number3: str = None
    name: str = None
    tags: List[str] = []
    placeOfBirth: str = None
    dateOfBirth: date = None
    address: str = None
    kota: str = None
    provinsi: str = None
    kodepos: str = None
    gender: str = None
    photoUrl: str = None
    kelas: str = None
    kelasDetail: str = None
    note: str = None
    photoStatus: bool = False
    isPrint: bool = True
    idKatalis: str = None
    isUpload: bool = False
    statusMigrasiKatalis: bool = False
    isMapping: bool = False
    nfcId: str = None
    isApprove: bool = False
    isVerified: bool = False


class MemberTempOnDB(MemberTempBase):
    id: ObjectIdStr = Field(alias="_id")


class MemberTempPage(DefaultPage):
    content: List[MemberTempOnDB] = []


class SolutionBase(str, Enum):
    queue = "QUEUE"  # antrian online
    attendance = "ATTENDANCE"  # absensi
    access = "ACCESS"  # akses pintu
    hrd = "HRD"  # hrd
    document = "DOCUMENT"  # document management
    asset = "ASSET"  # asset management
    service = "SERVICES"
    store = "STORE"  # pos, market, place
    library = "LIBRARY"  # perpustakaan
    website = "WEBSITE"  # website landing page
    transaction = "TRANSACTION"  # transaksi dengan kartu
    card_generator = "CARD_GENERATOR"  # generate kartu
    accounting = "ACCOUNTING"  # accounting
    invoice = "INVOICE"  # invoice management
    crm_ppdb = "CRM_PPDB"
    crm_pdam = "CRM_PDAM"
    crm_katalis = "CRM_KATALIS"
    crm_faskes = "CRM_FASKES"
    # membership = "MEMBERSHIP"


class JenisPelanggan(str, Enum):
    school = "SCHOOL"
    university = "UNIVERSITY"
    corporation = "CORPORATION"
    housing = "HOUSING"
    apartement = "APARTMENT"
    hotel = "HOTEL"
    channel = "CHANNEL"
    other = "OTHER"
    office = "OFFICE"
    hospital = "HOSPITAL"
    boarding_school = "BOARDING_SCHOOL"
    tourism = "TOURISM"
    bank = "BANK"
    clinic = "CLINIC"


class CompanyBase(AkunDefaultBase):
    katalisCompCode: str = None
    katalisCompanyId: str = None
    companyCode: str = None
    tags: List[
        str
    ] = []  # untuk kategori, tags yang ada apa aja : sd, smp, sma dll << lebih free
    tipe: dict = (
        {}
    )  # tipe yang ada apa aja : kantor pusat, wilayah, cabang, sales, lebih ke hierarki struktur
    solution: List[SolutionBase] = []
    lastAccountNo: int = 0
    picName: str = None
    picEmail: str = None
    tipeSourceFund: str = None  # none, internal, katalis, hasanahku
    isLocal: bool = False
    jenisPelanggan: JenisPelanggan = None
    initials: str = None
    logoUrl: str = None
    picPhone: str = None
    province: str = None
    solutions: List[str] = None
    address: str = None
    city: str = None
    regDate: datetime = None


class CompanyOnDB(CompanyBase):
    id: ObjectIdStr = Field(alias="_id")


class CompanyPage(DefaultPage):
    content: List[CompanyOnDB] = []
