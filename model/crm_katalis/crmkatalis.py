from model.default import CredentialData, UserBasic, UserInput
from model.support import DocumentBase, InvoiceBase, PipelineData
from pydantic import BaseModel, Field
from typing import List

from pydantic.utils import Obj
from ..util import DefaultPage, ObjectIdStr, DefaultData
from datetime import date, datetime
from model.default import UserBasic

# UserData -- data user umum (nama, email, no telepon)
# UserBasic -- data user yang berkaitan dengan id2 + data default yang dibuat otomatis oleh backend
# UserCredential -- data user yang LENGKAP (data umum + data default + data id + data credensial)
# Schema Admin, Partner, dan Client inherit / berbasis tiga schema di atas

# ClientData, PartnerData, AdminData, adalah schema yang isinya data profil user (nama, username, alamat, dll)
# -- tanpa data2 id dan kredensial

# ClientBasic,PartnerBasic,AdminBasic, adalah schema yang isinya data user + data default(createTime,updateTime,companyId dll) data2 id -- tanpa informasi credential
# ini bisa jadi schema yang di-return kalau front end minta data user (karena nggak mengikutsertakan kredensial)

# ClientCredential, PartnerCredential, AdminCredential -- schema lengkap (ini SEMUA data user yang disimpan di database)


class UserCredential(BaseModel):
    credential: CredentialData = None


# === Skema Client ====
class MemberInput(UserInput):
    pass


#     # number1: str = None
#     # number2: str = None
#     # number3: str = None
#     # name: str = None
#     # tags: List[str] = None
#     # placeOfBirth: str = None
#     # dateOfBirth: datetime = None
#     # address: str = None
#     # gender: str = None
#     # kelas: str = None
#     # kelasDetail: str = None
#     # photoUrl: str = None
#     # photoStatus: bool = None
#     # isVerified: bool = None
#     # isApprove: bool = None
#     # isPrint: bool = None
#     # isUploadToKatalis: bool = None
#     # idKatalis: str = None
#     # isMapping: bool = None
#     # nfcId: str = None


# class MemberBasic(MemberInput, UserBasic):
#     id: ObjectIdStr = None


# class MemberCredential(MemberBasic, UserCredential):
#     pass


class ClientInput(UserInput):
    pass


class ClientBasic(UserBasic, ClientInput):
    pass


class ClientCredential(ClientBasic, UserCredential):
    # members: List[MemberBasic] = []
    tags: List[str] = []
    pipelineName: str = None
    pipelines: List[PipelineData] = []
    invoiceSettingName: str = None


class ClientPage(DefaultPage):
    content: List[ClientBasic] = []


# === Skema Client ====

# === Skema Admin ===
class AdminInput(UserInput):
    pass


class AdminBasic(UserBasic, AdminInput):
    pass


class AdminCredential(AdminBasic, UserCredential):
    pass


# === Skema Admin ===

# === Skema Partner ===
class FollowUpInput(BaseModel):
    action: str
    response: str


class FollowUpBasic(FollowUpInput, DefaultData):
    id: ObjectIdStr = None
    pass


class FollowUpPage(DefaultPage):
    content: List[FollowUpBasic] = []


class FollowUpComplete(FollowUpBasic):
    pass


class LeadsInput(BaseModel):
    name: str = None
    tags: List[str] = None
    picName: str = None
    phone: str = None
    email: str = None
    address: str = None
    note: str = None
    size: int = None
    isActivated: bool = None
    convertedToClient: bool = False


class LeadsBasic(LeadsInput, DefaultData):
    id: ObjectIdStr = None


class LeadPage(DefaultPage):
    content: List[LeadsBasic] = []


class LeadsComplete(LeadsBasic):
    followUp: List[FollowUpBasic] = []


class PartnerInput(UserInput):
    pass


class PartnerBasic(UserBasic, PartnerInput):
    pass


class PartnerCredential(PartnerBasic, UserCredential):
    leads: List[LeadsBasic] = []


# === Skema Partner ===


class MemberKartu(BaseModel):
    id: ObjectIdStr
    name: str = None


class CetakKartuBase(DefaultData):
    jumlah: int = 0
    total: int = 0
    members: List[MemberKartu] = []
    idInvoice: ObjectIdStr = None


class CetakKartuOnDB(CetakKartuBase):
    id: ObjectIdStr = Field(alias="_id")
