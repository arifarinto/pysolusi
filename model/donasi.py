from datetime import date, datetime
from enum import Enum
from util.util_waktu import dateTimeNow
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput
from util.util import RandomNumber,RandomString
from faker import Faker

class DonaturData(BaseModel):
    id: ObjectIdStr = None
    namaDonatur: str = None
    EnableName: bool = True
    nominal: int = 0
    tgl: datetime = None

class DistribusiData(BaseModel):
    id: ObjectIdStr = None
    namaKegiatan: str = None
    waktu: datetime = None
    nominal: int = 0
    images: List[str] =[]
    keterangan: str = None

class MutasiEnum(str, Enum):
    dana_masuk = "dana_masuk"
    dana_keluar = "dana_keluar"

class MutasiDanaData(BaseModel):
    id: ObjectIdStr = None  #jika dana_keluar id adalah id distribusinya, jika dana_masuk id adalah id donatur
    userId: ObjectIdStr = None
    nama: str = None
    EnableName: bool = True
    tipeMutasi: MutasiEnum = None
    nominal: int = 0
    tgl: datetime = None
    keterangan: str = None

class DonasiBase(DefaultData):
    namaDonasi: str = None
    tglMulai: datetime = None
    tglSelesai: datetime = None
    kebutuhan: int = 0
    danaTerkumpul: int = 0
    danaSaldo : int = 0
    coaCode: str = None
    images: List[str] = []
    keterangan: str = None
    isEnable: bool = True
    donatur: List[DonaturData] = []
    distribusi: List[DistribusiData] = []
    mutasiDana: List[MutasiDanaData] = []

class DonasiOnDB(DonasiBase):
    id: ObjectIdStr = Field(alias="_id")

class DonasiPage(DefaultPage):
    content: List[DonasiOnDB] = []