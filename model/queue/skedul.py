from enum import Enum
from pydantic import Field
from typing import List
from datetime import datetime
from model.util import DefaultData, ObjectIdStr, DefaultPage

class NamaHari (str, Enum):
    Sunday = "Sunday"
    Monday = "Monday"
    Tuesday = "Tuesday"
    Wednesday = "Wednesday"
    Thursday = "Thursday"
    Friday = "Friday"
    Saturday = "Saturday"

class SkedulBase(DefaultData):
    title: str = None #nama jadwal
    idLayanan: ObjectIdStr = None
    note: str = None
    tags: List[str] = None
    hari : NamaHari = None
    mulai : str = None
    mulaiIstirahat: str = None
    selesaiIstirahat: str = None
    selesai: str = None
    capacity: int = 0

class SkedulOnDB(SkedulBase):
    id : ObjectIdStr = Field(alias="_id")

class SkedulPage (DefaultPage):
    content: List[SkedulOnDB] = []