from enum import Enum
from pydantic import Field
from typing import List
from datetime import date

from pydantic.main import BaseModel
from model.util import DefaultData, ObjectIdStr, DefaultPage

class AddrBase(BaseModel):
    address: str = None
    province: str = None
    city: str = None
    district: str = None
    village: str = None
    posCode: str = None

class LocBase(BaseModel):
    type: str = "Point"
    coordinates: List[float] = []

class BranchJenis (str, Enum):
    bank = "BANK"
    rumahSakit = "RUMAH SAKIT"
    klinik = "KLINIK"
    puskesmas = "PUSKESMAS"
    koperasi = "KOPERASI"
    pemerintah = "PEMERINTAH"
    wisata = "WISATA"
    sekolah = "SEKOLAH"
    event = "EVENT"
    kantorPos = "KANTOR POS"
    pdam = "PDAM"
    pln = "PLN"

class BranchBase(DefaultData):
    idAkun: ObjectIdStr = None
    jenis: BranchJenis = None #Bank, Rumah Sakit, Klinik, Puskesmas, 
    title: str = None #BRI Salatiga
    note: str = None
    email: str = None
    phone: str = None
    address : AddrBase = {}
    tags: List[str] = None
    location: LocBase = {}
    createSkedulBefore: int = 0 #days
    holiday: List[date] = []
    isSetSkedul: bool = False

class BranchOnDB(BranchBase):
    id : ObjectIdStr = Field(alias="_id")

class BranchPage (DefaultPage):
    content: List[BranchOnDB] = []