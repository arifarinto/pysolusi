#DATA BASE LOKET LOKET NYA
from enum import Enum
from pydantic import Field
from typing import List
from datetime import datetime
from model.util import DefaultData, ObjectIdStr, DefaultPage

class LoketBase(DefaultData):
    branchId: ObjectIdStr = None
    layananId: ObjectIdStr = None
    idAkun: ObjectIdStr = None
    name: str = None
    email: str = None
    phone: str = None
    note: str = None
    tags: List[str] = None

class LoketOnDB(LoketBase):
    id : ObjectIdStr = Field(alias="_id")

class LoketPage (DefaultPage):
    content: List[LoketOnDB] = []