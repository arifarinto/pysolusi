from enum import Enum
from pydantic import Field
from typing import List
from datetime import datetime
from model.util import DefaultData, ObjectIdStr, DefaultPage

class AudioType (str, Enum):
    boy = "boy"
    girl = "girl"

class AudioBase(DefaultData):
    title: str = None
    voiceFile: str = None
    tipe: AudioType = "boy"
    note: str = None
    tags: List[str] = None

class AudioOnDB(AudioBase):
    id : ObjectIdStr = Field(alias="_id")

class AudioPage (DefaultPage):
    content: List[AudioOnDB] = []