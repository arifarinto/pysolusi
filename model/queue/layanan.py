from enum import Enum
from pydantic import Field
from typing import List
from datetime import datetime

from pydantic.main import BaseModel
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.support import ParameterData

#info di save di info secara umum, dengan title layanan sebagai tags

class LayananBase(DefaultData):
    branchId: ObjectIdStr = None
    title: str = None #KASIR, CS, DOKTER KULIT, APOTEK
    kode: str = None
    note: str = None
    tags: List[str] = None
    form: List[ParameterData] = []
    biaya: int = 0

class LayananOnDB(LayananBase):
    id : ObjectIdStr = Field(alias="_id")

class LayananPage (DefaultPage):
    content: List[LayananOnDB] = []

class comboLayanan(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    companyId :ObjectIdStr = None
    branchId : str = None
    companyName : str = None
    title : str = None
    kode : str = None 