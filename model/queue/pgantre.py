from enum import Enum
from pydantic import Field
from typing import List
from datetime import datetime

from pydantic.main import BaseModel
from model.util import DefaultData, ObjectIdStr, DefaultPage

class LogAntrian(BaseModel):
    createTime: datetime = None
    compayId: ObjectIdStr = None
    layananId: ObjectIdStr = None
    antrianId: ObjectIdStr = None
    branchId: ObjectIdStr = None
    kode: str = None
    number: int = 0
    isFinished: bool = False

class PgantreUmumBase(DefaultData):
    userId: ObjectIdStr = None
    idAkun: ObjectIdStr = None
    name: str = None
    phone : str = None
    email : str = None
    note: str = None
    tags: List[str] = None
    logAntrian: List[LogAntrian] = []

class PgantreUmumOnDB(PgantreUmumBase):
    id : ObjectIdStr = Field(alias="_id")

class PgantreUmumPage (DefaultPage):
    content: List[PgantreUmumOnDB] = []