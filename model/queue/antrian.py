from enum import Enum
from model.queue.skedul import NamaHari
from pydantic import Field
from typing import List
from datetime import datetime, date, time

from pydantic.main import BaseModel
from model.util import ObjectIdStr, DefaultPage
from model.support import ParameterData

class AntrianStatus (str, Enum):
    wait = "wait"
    process = "process"
    sent = "sent"
    read = "read"

#DATA BASE ORANG YANG AKAN MENGANTRE
class PengantreBase(BaseModel):
    idAkun: ObjectIdStr = None
    kode: str = None
    number: int = 0
    createTime: datetime = None
    predictionCalled: datetime  = None
    calledTime:datetime = None
    isCalled: bool = False
    isCheckedIn: bool = False
    checkedInTime: datetime = None
    servedById: ObjectIdStr = None
    servedByName: str = None
    finishTime: datetime = None
    form: List[ParameterData] = []
    qrCode: str = None
    takeFrom: str = None
    isOnline: bool = False
    bintang: int = 0 #1,2,3,4,5
    komen: str = None
    
#DATA BASE LOKET YANG BERTUGAS
class LoketOnDutyBase(BaseModel):
    idAkun: ObjectIdStr = None
    name: str = None
    activeStatus: bool = True #bisa false kalo sedang istirahat atau tutup
    totalServed: int = 0
    startTime: time = None
    finishTime: time = None
    activeNumber: int = 0 #nomor yang lagi dilayani
    activeNoAntrian: str = 0 #nama nomornya A1
    lastActive: datetime = None

class DeviceBase(BaseModel):
    idAkun: ObjectIdStr = None

class AntrianBase(BaseModel):
    companyId: ObjectIdStr = None
    branchId: ObjectIdStr = None
    layananId: ObjectIdStr = None
    title: str = None
    kode: str = None
    biaya: int = 0
    tanggal: date = None
    hari : NamaHari = None
    mulai : str = None
    mulaiIstirahat: str = None
    selesaiIstirahat: str = None
    selesai: str = None
    pengantre: List[PengantreBase] = []
    capacity: int = 0
    queTotal: int = 0
    queNow: int = 0
    kodeNow: str = None
    timeNow: datetime = None
    loketOnDuty: List[LoketOnDutyBase] = []
    showAt: List[DeviceBase] = [] #monitor yang muncul
    takeAt: List[DeviceBase] = [] #kiosk utk ambil antrian
    isOpen: bool = True

class AntrianOnDB(AntrianBase):
    id : ObjectIdStr = Field(alias="_id")

class AntrianPage (DefaultPage):
    content: List[AntrianOnDB] = []