from enum import Enum
from pydantic import Field
from typing import List
from datetime import datetime
from model.util import DefaultData, ObjectIdStr, DefaultPage

class JenisKiosk (str, Enum):
    layar = "layar"
    kiosk = "kiosk"

class KioskBase(DefaultData):
    idAkun: ObjectIdStr = None
    branchId: ObjectIdStr = None
    listLayananId: List[ObjectIdStr] = []
    kode: str = None #temporary code utk generate qr code
    name: str = None
    jenis: JenisKiosk = "layar"
    lastActive: datetime = None
    isActive: bool = False
    note: str = None
    tags: List[str] = None
    info: str = None
    imageUrl: str = None
    runText: str = None
    isAudio: bool = False

class KioskOnDB(KioskBase):
    id : ObjectIdStr = Field(alias="_id")

class KioskPage (DefaultPage):
    content: List[KioskOnDB] = []