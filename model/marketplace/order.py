from enum import Enum
from pydantic import BaseModel
from typing import List
from pydantic import Field
from datetime import datetime
from model.product import ProductOrderBaseOut
from model.util import AkunDefaultBase, ObjectIdStr, DefaultPage
from model.invoice import InvoiceBase

class ProductOrder(BaseModel):
    id: ObjectIdStr = None
    createTime: datetime = None
    updateTime: datetime = None
    jumlahOrder: int = 1
    productId: ObjectIdStr = None
    product: ProductOrderBaseOut = None

class RoleAccess(str, Enum):
    customer = "CUSTOMER"
    deliveryBoy = "DELIVER_BOY"
    stokist = "STOKIST"
    admin = "ADMIN"

class DeliverAddr(BaseModel):
    id: ObjectIdStr = None
    description: str = None
    address: str = None
    provinsi: str = None
    kotaKab: str = None
    kecamatan: str = None
    kelurahan: str = None
    latitude: str = None
    longitude: str = None
    isDefault: bool = False
    createTime: datetime = None
    updateTime: datetime = None

class OrderStatus(BaseModel):
    id: ObjectIdStr = None
    status: str = None #"start", "cancel" "set alamat", "pembayaran", "diterima", "packing", "siap kirim", "dalam perjalanan", "sudah diterima"
    handleBy: str = None #id Karyawan / Delivery Boy yang handel
    createTime: datetime = None
    updateTime: datetime = None

class PaymentType(str, Enum):
    cash = "cash"
    transfer_va = "transfer_va"
    kartu_kredit = "kartu_kredit"
    emoney = "emoney"
    katalis = "katalis"
    internal = "internal"

class MerchantPaymentType(str, Enum):
    cod = "cod"
    va_bni = "va_bni"
    kk_visa = "kk_visa"
    kk_mastercard = "kk_mastercard"
    dana = "dana"
    ovo = "ovo"
    gopay = "gopay"
    linkaja = "linkaja"


class PaymentData(BaseModel):
    id: ObjectIdStr = None
    createTime: datetime = None
    updateTime: datetime = None
    paymentType: PaymentType = None #CASH, transfer va, kartu kredit, emoney, katalis, internal
    merchant: MerchantPaymentType = None #COD, va bca, va bri, visa, mastercard, dana, gopay, sopan pay, katalis
    amount: int = 0 #total belanja
    ongkir: int = 0 #
    totalAmount: int = 0
    diskon: int = 0
    finalAmount: int = 0
    paid: int = 0
    paymentRef: str = None
    paymentTime: datetime = None
    maxPaymentTime: datetime = None
    paymetStatus: bool = False

class OrderHistory(BaseModel):
    id: ObjectIdStr = None
    createTime: datetime = None
    updateTime: datetime = None
    productOrder: List[ProductOrder] = []
    totalItem: int = 0
    totalPrice: int = 0
    lastOrderStatus: str = None
    orderStatus: List[OrderStatus] = []
    deliverAddr: DeliverAddr = {}
    invoiceData: InvoiceBase = {}
    paymentData: PaymentData = {}
    userWhoOrder: ObjectIdStr = None #id yang order
    userWhoDeliver: ObjectIdStr = None #id yang anter

class PembeliBase(AkunDefaultBase):
    idAkun: ObjectIdStr = None #idAkun
    nfcId: str = None
    name: str = None
    bio: str = None
    userRating: int = 0
    isVerified: bool = False
    orderLimit: int = 100000
    orderTotal: int = 0
    orderSuccess: int = 0
    lastAddr: DeliverAddr = None
    favorite: List[str] = []
    cartData: List[ProductOrder] = [] 
    orderHistory: List[OrderHistory] = []
    deliverAddr: List[DeliverAddr] = []
    deliverBoyHistory: List[OrderHistory] = []

class PembeliOnDB(PembeliBase):
    id : ObjectIdStr = Field(alias="_id")

class PembeliPage (DefaultPage):
    content: List[PembeliOnDB] = []
