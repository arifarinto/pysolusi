from enum import Enum
from pydantic import BaseModel, Field
from typing import List, Optional
from datetime import datetime
from model.util import AkunDefaultBase, ObjectIdStr, DefaultPage
    
class ProductReview(BaseModel):
    id: ObjectIdStr = None
    review: str = None
    rate: int = 0
    userId: ObjectIdStr = None
    createTime: datetime = None
    updateTime: datetime = None
    
class ProductExtra(BaseModel):
    id: ObjectIdStr = None
    name: str = None
    description: str = None
    price: int = 0
    createTime: datetime = None
    updateTime: datetime = None
    images: str = None

class ProductNutrition(BaseModel):
    id: ObjectIdStr = None
    name: str = None
    description: str = None
    quantity: int = 0
    createTime: datetime = None
    updateTime: datetime = None

class VolType(str, Enum):
    mg = "miligram"
    gr = "gram"
    kg = "kilogram"
    ml = "cc"
    lt = "liter"
    ik = "ikat"
    bj = "biji"
    pcs = "pcs"

class StockType(str, Enum):
    daily = "daily"
    alltime = "alltime"

class ProductOrderBase(AkunDefaultBase):
    name: str = None
    tags: List[str] = []
    stokistId: ObjectIdStr = None #id user supplier
    price: int = 0
    stock: int = 500
    stockType: StockType = "daily"
    discountPrice: int = 0
    volume: int = 0
    volType: VolType = "gram"
    barcode: str = None
    images: str = None

class ProductBase(ProductOrderBase):
    description: str = None
    ingredients: str = None
    isHeadline: bool = False
    createTime: datetime = None
    updateTime: datetime = None

class ProductComplete(ProductBase):
    productReviews: List[ProductReview] = []
    extras: List[ProductExtra] = []
    nutrition: List[ProductNutrition] = []
    favoritCount: int = 0
    salesCount: int = 0
    isOnline: bool = False

class ProductOnDB(ProductComplete):
    id : ObjectIdStr = Field(alias="_id")

class ProductPage (DefaultPage):
    content: List[ProductOnDB] = []

class ProductOrderBaseOut(ProductOrderBase):
    id: ObjectIdStr = None
    