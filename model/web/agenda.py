from pydantic import Field
from typing import List
from datetime import date, datetime
from model.web.list import ListDetail
from model.util import DefaultData, ObjectIdStr, DefaultPage
from enum import Enum

class MonthType(str, Enum):
    last = "last"
    now = "now"
    next = "next"

class AgendaBase (DefaultData):
    title: str = None
    subtitle: str = None
    enable: bool = True
    tags: List[str] = []
    description: str = None
    note: str = None
    viewCount: int = 0
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    timePlan: datetime = None
    listDetail: List[ListDetail] = []

class AgendaOnDB (AgendaBase):
    id: ObjectIdStr = Field(alias="_id")

class AgendaPage (DefaultPage):
    content: List[AgendaOnDB] = []