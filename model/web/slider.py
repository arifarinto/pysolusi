from pydantic import Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage

class SliderBase (DefaultData):
    title: str = None
    subtitle: str = None
    enable: bool = True
    tags: List[str] = []
    description: str = None
    note: str = None
    viewCount: int = 0
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya

class SliderOnDB (SliderBase):
    id: ObjectIdStr = Field(alias="_id")

class SliderPage (DefaultPage):
    content: List[SliderOnDB] = []