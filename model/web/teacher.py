from enum import Enum
from pydantic import Field
from typing import List
from model.web.list import ListDetail
from model.util import DefaultData, ObjectIdStr, DefaultPage

class TeacherBase (DefaultData):
    title: str = None
    subtitle: str = None
    tipe: str = None
    enable: bool = True
    tags: List[str] = []
    description: str = None
    note: str = None
    viewCount: int = 0
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    listDetail: List[ListDetail] = []

class TeacherOnDB (TeacherBase):
    id: ObjectIdStr = Field(alias="_id")

class TeacherPage (DefaultPage):
    content: List[TeacherOnDB] = []