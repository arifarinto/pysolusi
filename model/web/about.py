from enum import Enum
from pydantic import Field
from typing import List
from model.web.list import ListDetail
from model.util import DefaultData, ObjectIdStr, DefaultPage

class AboutBase (DefaultData):
    title: str = None
    tipe: str = None
    subtitle: str = None
    enable: bool = True
    tags: List[str] = []
    description: str = None
    note: str = None
    viewCount: int = 0
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    listDetail: List[ListDetail] = []

class AboutOnDB (AboutBase):
    id: ObjectIdStr = Field(alias="_id")

class AboutPage (DefaultPage):
    content: List[AboutOnDB] = []