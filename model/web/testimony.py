from enum import Enum
from pydantic import Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage

class TestimonyBase (DefaultData):
    title: str = None
    subtitle: str = None
    enable: bool = True
    tags: List[str] = []
    description: str = None
    note: str = None
    viewCount: int = 0
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya

class TestimonyOnDB (TestimonyBase):
    id: ObjectIdStr = Field(alias="_id")

class TestimonyPage (DefaultPage):
    content: List[TestimonyOnDB] = []