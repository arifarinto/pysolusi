from datetime import datetime
from enum import Enum
from pydantic import BaseModel
from model.util import DefaultData, ObjectIdStr

class menuEnum(str, Enum):
    about = "about"
    academic = "academic"
    achievement = "achievement"
    agenda = "agenda"
    alumni = "alumni"
    curiculum = "curiculum"
    excul = "excul"
    facility = "facility"
    gallery = "gallery"
    news = "news"
    program = "program"
    teacher = "teacher"
    weekly = "weekly"
    tutorial = "tutorial"

class ListDetail(BaseModel):
    listId: str = None
    createTime: datetime = None
    title: str = None
    subTitle: str = None
    text1: str = None
    text2: str = None
    text3: str = None
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    url1: str = None
    url2: str = None