from datetime import time
from enum import Enum
from pydantic import Field
from typing import List
from model.web.list import ListDetail
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.akses.shift import NamaHari

class ExculBase (DefaultData):
    title: str = None
    day: NamaHari = None
    jam: time = None
    subtitle: str = None
    enable: bool = True
    tags: List[str] = []
    description: str = None
    note: str = None
    viewCount: int = 0
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    listDetail: List[ListDetail] = []

class ExculOnDB (ExculBase):
    id: ObjectIdStr = Field(alias="_id")

class ExculPage (DefaultPage):
    content: List[ExculOnDB] = []