from enum import Enum
from pydantic import Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage

class FaqBase(DefaultData):
    urutan: int = 0
    category: str  = None
    question: str = None
    answer: str = None
    image: str = None

class FaqOnDB(FaqBase):
    id : ObjectIdStr = Field(alias="_id")

class FaqPage (DefaultPage):
    content: List[FaqOnDB] = []