# isi log aktivitasnya sekalian
from enum import Enum
from model.support import AutomaticLoginEnum, LanguageEnum
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage

# from model.default import AddressData, IdentityData, CredentialData, UserInput
from model.default import CredentialData, UserInput


class MemberBase(DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None
    lang: LanguageEnum = "id"
    automaticLogin: AutomaticLoginEnum = "not_active"


class MemberCredential(MemberBase):
    credential: CredentialData = {}


class MemberOnDB(MemberBase):
    id: ObjectIdStr = Field(alias="_id")


class MemberPage(DefaultPage):
    content: List[MemberOnDB] = []


class comboMember(BaseModel):
    id: ObjectIdStr = Field(alias="_id")
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None
