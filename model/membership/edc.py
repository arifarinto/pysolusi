#isi log aktivitasnya sekalian
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput

class EdcBase(DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None

class EdcCredential(EdcBase):
    credential: CredentialData = {}

class EdcOnDB(EdcBase):
    id : ObjectIdStr = Field(alias="_id")

class EdcPage (DefaultPage):
    content: List[EdcOnDB] = []

class comboEdc(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None