# isi log aktivitasnya sekalian
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput


class KasirBase(DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None


class KasirCredential(KasirBase):
    credential: CredentialData = {}


class KasirOnDB(KasirBase):
    id: ObjectIdStr = Field(alias="_id")


class KasirPage(DefaultPage):
    content: List[KasirOnDB] = []


class comboKasir(BaseModel):
    id: ObjectIdStr = Field(alias="_id")
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None
