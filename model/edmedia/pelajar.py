#isi log aktivitasnya sekalian
from datetime import date, datetime
from util.util_waktu import dateNowStr
from util.util import RandomNumber
from model.edmedia.materi import SoalDijawab, TugasDijawab
from pydantic import BaseModel, Field
from typing import List

from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import CredentialData, AddressData, IdentityData, UserInput
from faker import Faker

fake = Faker()
class PelajarKbm(BaseModel):
    idSilabus: ObjectIdStr = None
    pertemuanKe: int = 0
    waktu: datetime = 0
    tahun: str = None
    semester: str = None
    kelas: str = None
    kelasDetail: str = None
    isHadir: bool = False
    tugas: List[TugasDijawab] = []
    nilaiTugas: int = 0
    tugasDone: bool = False
    soal: List[SoalDijawab] = []
    nilaiSoal: int = 0
    soalDone: bool = False

class PelajarExcul(BaseModel):
    idSilabus: ObjectIdStr = None
    pertemuanKe: int = 0
    waktu: datetime = None
    isHadir: bool = False
    note: str = None

class PelajarNilai(BaseModel):
    waktu: datetime = None
    kelas: str = None
    kelasDetail: str = None
    tahunAjaran: int = 0

class PelajarKesehatan(BaseModel):
    waktu: datetime = None
    note: str = None
    suhu: int = None
    berat: int = None
    tinggi: int = None
    tensiBawah: int = None
    tensiAtas: int = None
    keluhanSakit: str = None

class PelajarKepribadian(BaseModel):
    waktu: datetime = None
    note: str = None

class PelajarInput(UserInput):
    thnMasuk: str = None
    programStudi: str = None
    jurusan: str = None
    kelasNow: str = None
    kelasDetailNow: str = None
    tahunNow: str = None
    semesterNow: str = None
    
    class Config:
        schema_extra = {
            "example":{
                "tipeNik": "ktp",
                "nik": RandomNumber(6),
                "noId": RandomNumber(3),
                "name": fake.name(),
                "email": fake.first_name()+"@coba.com",
                "phone": "0813"+RandomNumber(6),
                "isWa": False,
                "tags": [],
                "address": {
                    "street":fake.address(),
                    "note": "note"
                },
                "identity": {
                    "dateOfBirth": dateNowStr(),
                    "placeOfBirth": "Jakarta",
                    "gender": "male"
                },
                "thnMasuk": "2021",
                "programStudi": "SMP",
                "jurusan": "SMP",
                "kelasNow": "1",
                "kelasDetailNow": "1A",
                "tahunNow": "2021",
                "semesterNow": "1"
            }
        }

class PelajarBase(DefaultData, PelajarInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    dataKbm: List[PelajarKbm] = [] #
    dataExcul: List[PelajarExcul] = [] #list aktivitas excul yang diisi oleh pembina
    nilai: List[str] = [] #rekap nilai dari semua kegiatan dalam tingkatan kelas contoh 7A
    dataSehat: List[PelajarKesehatan] =[]
    dataPribadi: List[PelajarKepribadian] = []

class PelajarCredential(PelajarBase):
    credential: CredentialData = {}

class PelajarOnDB(PelajarBase):
    id : ObjectIdStr = Field(alias="_id")

class PelajarPage (DefaultPage):
    content: List[PelajarOnDB] = []

class PelajarDatas(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    userId : ObjectIdStr = None
    name: str = None
    noId: str = None
    kelasDetailNow: str = None
    dataKbm: PelajarKbm = []

class PelajarHistory(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    userId : ObjectIdStr = None
    nama: str = None
    nis: str = None
    thnMasuk: str = None
    kelasNow: str = None
    kelasDetailNow: str = None
    tags: List[str] = []
    programStudi: str = None
    jurusan: str = None
    namaMapel: str = None
    dataKbm: PelajarKbm = []

class PelajarMapel(BaseModel):
    idSilabus: ObjectIdStr = None
    tahun: str = None
    semester: str = None
    kelas: str = None
    kelasDetail: str = None
    programStudi: str = None
    jurusan: str = None
    namaMapel: str = None

class MapelPage(DefaultPage):
    content: List[PelajarMapel] = []

class HistoryPage(DefaultPage):
    content: List[PelajarHistory] = []

class comboMapelPelajar(BaseModel):
    tahun: str = None
    semester: str = None
    kelas: str = None
    kelasDetail: str = None

class PelajarMapelPage(DefaultPage):
    content: List[comboMapelPelajar] = []