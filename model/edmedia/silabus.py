from model.util import ObjectIdStr, DefaultData, DefaultPage
from pydantic import BaseModel, Field
from typing import List

class SilabusBase(DefaultData):
    programStudi: str = None #khusus kampus, bisa juga SD, SMP
    jurusan: str = None #smk elektro, sipil
    kelas: str = None #bisa di isi semester
    semester: int = None
    tahun: str = None
    isExcul: bool = False
    namaMapel: str = None
    praSyarat: str = None
    bobotSks: int = 0
    jumlahPertemuan: int = 0
    pengajar: List[str] = []
    tags: List[str] = []

class SilabusOnDB(SilabusBase):
    id : ObjectIdStr = Field(alias="_id")

class SilabusPage (DefaultPage):
    content: List[SilabusOnDB] = []

class comboSilabus(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    namaMapel : str = None
    kelas: str = None
    tahun: str = None
    semester: int = None
    programStudi: str = None
    jurusan: str = None

class comboKbmTeacher(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    idSilabus : ObjectIdStr = None
    namaMapel: str = None
    kelas: str = None
    kelasDetail: str = None
    tahun: str = None
    semester: str = None