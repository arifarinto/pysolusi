from pydantic import BaseModel, Field
from typing import List
from .materi import MateriBase, CommentBase, comboMateri
from datetime import datetime, date, time
from model.util import DefaultData, ObjectIdStr, DefaultPage
from enum import Enum

class UserBasic(BaseModel):
    userId: ObjectIdStr = None
    name: str = None
    nomorIdentitas: str = None #A

class UserAbsesni(BaseModel):
    userId: ObjectIdStr = None
    name: str = None
    nomorIdentitas: str = None #A
    isHadir: bool = False
    isAbsen: bool = False

class TipeUjian (str, Enum):
    harian = "harian"
    uts = "uts"
    uas = "uas"

class RppBase(BaseModel):
    idMateri: List = [] # List[ObjectIdStr] = []
    isAkses: bool = False
    isUjian: bool = False
    durasi: int = None
    tipe: TipeUjian = None
    jumlahSoal: int = None
    bobot: int = None #persentase utk raport
    pertemuanKe: int = 0
    # tgl: date = None
    isDone: bool = False
    jamMulai: datetime = None # time = None
    jamSelesai: datetime = None # time = None
    idRuang: ObjectIdStr = None
    namaRuang: str = None
    note: str = None
    pengajar: UserBasic = None
    pesertaHadir: List[UserBasic] = [] 
    pesertaAbsen: List[UserBasic] = []
    komen: List[CommentBase] = [] # List[str] = []

class JadwalBase(BaseModel):
    hari: str = None
    jamMulai: time = None
    jamSelesai: time = None
    idRuang: ObjectIdStr = None
    namaRuang: str = None

class GenerateKbm(BaseModel):
    tahun: str = None
    programStudi: str = None
    jurusan: str = None
    kelas: str = None
    kelasDetail: str = None
    kapasitas: int = 0
    semester: int = 0

class KbmBase(DefaultData):
    kelas: str = None
    kelasDetail: str = None
    joinCode: str = None
    isOpen: bool = False
    idSilabus: ObjectIdStr = None
    programStudi: str = None
    jurusan: str = None
    namaMapel: str = None
    isExcul: bool = False
    tahun: str = None
    semester: int = None
    jadwal: List[JadwalBase] = []
    pengajar: List[UserBasic] = []
    kapasitas: int = 0
    jumlahPelajar: int = 0
    pelajar: List[UserBasic] = []
    jumlahPertemuan: int = 0
    rpp: List[RppBase] = [] #pertemuan ke 1 s/d selesai
    tags: List[str] = []
    isAktif: bool = True

class KbmOnDB(KbmBase):
    id : ObjectIdStr = Field(alias="_id")

class KbmPage (DefaultPage):
    content: List[KbmOnDB] = []

class RppsBase(DefaultData):
    kelas: str = None
    kelasDetail: str = None
    joinCode: str = None
    isOpen: bool = False
    idSilabus: ObjectIdStr = None
    namaMapel: str = None
    isExcul: bool = False
    tahun: str = None
    semester: int = None
    jadwal: List[JadwalBase] = []
    pengajar: List[UserBasic] = []
    kapasitas: int = 0
    jumlahPelajar: int = 0
    pelajar: List[UserBasic] = []
    jumlahPertemuan: int = 0
    rpp: RppBase = [] #pertemuan ke 1 s/d selesai
    tags: List[str] = []
    isAktif: bool = True

class RppOnDB(RppsBase):
    id : ObjectIdStr = Field(alias="_id")

class RppPage(DefaultPage):
    content: List[RppOnDB] = []

class ListRppPage(DefaultPage):
    content: List[RppBase] = []

class PesertaPage(DefaultPage):
    content: List[UserBasic] = []

class comboTipe(BaseModel):
    tipe : str = None

class comboGenerateKbm(BaseModel):
    tahun: List[str] = []
    semester: List[int] = []
    programStudi: List[str] = []
    jurusan: List[str] = []
    kelas: List[str] = []