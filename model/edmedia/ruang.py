from pydantic import BaseModel, Field
from typing import List
from datetime import datetime, date, time
from model.util import DefaultData, ObjectIdStr, DefaultPage

class RuangBase(DefaultData):
    name: str = None
    kode: str = None
    gedung: str = None
    tags: List[str] = None
    kapasitas: int = 0
    note: str = None
    aktif: bool = True

class RuangOnDB(RuangBase):
    id : ObjectIdStr = Field(alias="_id")

class RuangPage (DefaultPage):
    content: List[RuangOnDB] = []

class comboRuang(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    name : str = None