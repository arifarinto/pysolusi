from datetime import datetime
from pydantic.typing import NONE_TYPES
from model.util import ObjectIdStr, DefaultData, DefaultPage
from pydantic import BaseModel, Field
from typing import List

class TugasBase(BaseModel):
    id: ObjectIdStr = None
    tugas: str = None
    imageTugas: str = None
    point: int = 0

class TugasDijawab(TugasBase):
    jawaban: str = None
    imageJawaban: str = None
    nilai: int = 0


class SoalBase(BaseModel):
    id: ObjectIdStr = None
    idMateri: ObjectIdStr = None
    idSilabus: ObjectIdStr = None
    pertanyaan: str = None
    imageUrl: str = None
    opsiA: str = None
    imageA: str = None
    opsiB: str = None
    imageB: str = None
    opsiC: str = None
    imageC: str = None
    opsiD: str = None
    imageD: str = None
    opsiE: str = None
    imageE: str = None

class SoalKunci(SoalBase):
    kunci: str = None
    isChoice: bool = True

class SoalDijawab(SoalKunci):
    jawaban: str = None
    isBenar: bool = False
    noSoal: int = 0

class CommentReply(BaseModel):
    id: ObjectIdStr = None
    createTime: datetime = None
    userId: ObjectIdStr = None
    name: str = None
    comment: str = None

class CommentBase(BaseModel):
    id: ObjectIdStr = None
    createTime: datetime = None
    userId: ObjectIdStr = None
    name: str = None
    comment: str = None
    reply: List[CommentReply] = []

class MateriBasic(BaseModel):
    id: ObjectIdStr = None
    urutan: int = 0
    judul: str = None
    target: List[str] = []
    materi: str = None
    summary: List[str] = []
    link: List[str] = []
    imageUrl: str = None
    comment: List[CommentBase] = []

class MateriBase(DefaultData):
    idSilabus: ObjectIdStr = None
    codeMateri: str = None
    topik: int = 0 # 1,2,3,4
    judul: str = None
    subTopik: str = None #A
    subJudul: str = None
    materi: List[MateriBasic] = []
    tugas: List[TugasBase] = []
    soal: List[SoalKunci] = []
    tags: List[str] = []

class MateriOnDB(MateriBase):
    id : ObjectIdStr = Field(alias="_id")

class MateriPage (DefaultPage):
    content: List[MateriOnDB] = []

class SoalPage (DefaultPage):
    content: List[SoalDijawab] = []

class comboMateri(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    topik : int = None
    subTopik: str = None
    judul : str = None
    subJudul: str = None    

class detailMateriOnDB(MateriBasic):
    id : ObjectIdStr = None # ObjectIdStr = Field(alias="_id")

class detailMateriPage(DefaultPage):
    content: List[detailMateriOnDB] = []

class detailTugasOnDB(TugasBase):
    id : ObjectIdStr = None # ObjectIdStr = Field(alias="_id")

class detailTugasPage(DefaultPage):
    content: List[detailTugasOnDB] = []

class detailSoalOnDB(SoalKunci):
    id : ObjectIdStr = None # ObjectIdStr = Field(alias="_id")

class detailSoalPage(DefaultPage):
    content: List[detailSoalOnDB] = []