from pydantic import BaseModel, Field
from typing import List
from datetime import datetime, date, time
from model.util import DefaultData, ObjectIdStr, DefaultPage

class KelasBase(DefaultData):
    tahun: str = None
    semester: str = None
    kelas: str = None
    kelasDetail: str = None
    waliKelas: ObjectIdStr = None
    kapasitas: int = 0
    note: str = None
    aktif: bool = True

class KelasOnDB(KelasBase):
    id : ObjectIdStr = Field(alias="_id")

class KelasPage(DefaultPage):
    content: List[KelasOnDB] = []

class comboKelas(BaseModel):
    kelas: str = None