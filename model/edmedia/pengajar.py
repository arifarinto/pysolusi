#isi log aktivitasnya sekalian
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput

class PengajarBase(DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None

class PengajarCredential(PengajarBase):
    credential: CredentialData = {}

class PengajarOnDB(PengajarBase):
    id : ObjectIdStr = Field(alias="_id")

class PengajarPage(DefaultPage):
    content: List[PengajarOnDB] = []

class comboPengajar(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    userId: ObjectIdStr = None
    name: str = None
    noId: str = None

class comboMapelPengajar(BaseModel):
    idSilabus: ObjectIdStr = None
    tahun: str = None
    semester: str = None
    kelas: str = None
    kelasDetail: str = None
    namaMapel: str = None

class PengajarMapelPage(DefaultPage):
    content: List[comboMapelPengajar] = []