from enum import Enum
from pydantic import Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage

class NamaHari (str, Enum):
    Sunday = "Sunday"
    Monday = "Monday"
    Tuesday = "Tuesday"
    Wednesday = "Wednesday"
    Thursday = "Thursday"
    Friday = "Friday"
    Saturday = "Saturday"

class ShiftBase (DefaultData):
    namaShift: str = None #konek dengan namaShift nya pengguna
    hari : NamaHari = None
    sblmMasuk : str = None
    jamMasuk: str = None
    stlhMasuk : str = None
    sblmPulang : str = None
    jamPulang: str = None
    stlhPulang: str = None
    jamPulangLembur: str = None

class ShiftOnDB (ShiftBase):
    id : ObjectIdStr = Field(alias="_id")

class ShiftPage (DefaultPage):
    content: List[ShiftOnDB] = []