from typing import List
from datetime import datetime
from pydantic import Field
from pydantic.main import BaseModel
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AksesBase, AddressData, IdentityData, CredentialData, UserInput
from enum import Enum

class CutiBase(DefaultData):
    namaCuti: str = None
    tanggalcuti: datetime = None
    tanggalmasuk: datetime = None
    keterangan: str = None
    isPermitted: bool = False

class CutiOnDB(CutiBase):
    id: ObjectIdStr = Field(alias="_id")

class CutiPage(DefaultPage):
    content: List[CutiOnDB] = []


class PenggunaBase (DefaultData, UserInput):
    userId: ObjectIdStr = None
    username: str = None
    profileImage: str = None
    note: str = None
    tags: List[str] = []

class PenggunaCredential(PenggunaBase):
    credential: CredentialData = {}

class PenggunaOnDB (PenggunaBase):
    id: ObjectIdStr = Field(alias="_id")

class PenggunaPage (DefaultPage):
    content: List[PenggunaOnDB] = []
