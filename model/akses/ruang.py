from typing import List
from datetime import datetime
from pydantic import Field
from pydantic.main import BaseModel
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AksesBase, CredentialData
from enum import Enum

class RuangBase (DefaultData):
    namaRuang : str = None
    kode: str = None
    keterangan : str = None
    kapasitas : int = 0
    tags : List[str] = []

class RuangOnDB (RuangBase):
    id : ObjectIdStr = Field(alias="_id")

class RuangPage (DefaultPage):
    content: List[RuangOnDB] = []

class comboRuang(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    name : str = None