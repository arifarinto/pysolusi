# device arduino dan akses pintu
from typing import List
from datetime import datetime
from pydantic import Field
from pydantic.main import BaseModel
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AksesBase
from enum import Enum

class TipeDevice(str, Enum):
    masuk = "masuk"
    keluar = "keluar"
    semua = "semua"

class DeviceBase (DefaultData):
    userId : ObjectIdStr = None #parent Id di postgre katalis / mysql internal
    namaDevice : str = None
    keterangan : str = None
    tipe: TipeDevice = "semua"
    logDevice : List[AksesBase] = []
    tags : List[str] = [] #konek dengan pengguna sesuai hak akses buka pintu nya, jika kosong semuanya boleh akses
    lastOnline : datetime = None

class DeviceOnDB (DeviceBase):
    id : ObjectIdStr = Field(alias="_id")

class DevicePage (DefaultPage):
    content: List[DeviceOnDB] = []

class comboDevice(BaseModel):
    id : ObjectIdStr = Field(alias="_id")
    nameDevice : str = None
    tipe: TipeDevice = None