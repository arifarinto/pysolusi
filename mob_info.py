from route.auth import login_for_access_token
from fastapi import FastAPI, Response, Depends
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm, SecurityScopes

from route.auth import router_auth

app = FastAPI(openapi_url="/mob_info/openapi.json", docs_url="/mob_info/swgr")
app.mount("/mob_info/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-mobile',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}

app.include_router(router_auth,prefix="/api_default/auth",tags=["auth"],responses={404: {"description": "Not found"}})

@app.post("/mob_info/login", response_model=dict, tags=["login"])
async def mobile_login(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await login_for_access_token(response, form_data)

@app.post("/mob_info/first_login", response_model=dict, tags=["login"])
async def first_login(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await login_for_access_token(response, form_data)

@app.post("/mob_info/change_password", response_model=dict, tags=["login"])
async def change_password(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await login_for_access_token(response, form_data)

# ============================================

@app.post("/mob_info/get_home", response_model=dict, tags=["umum"])
async def get_home(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await get_home(response, form_data)

@app.post("/mob_info/cek_kartu_nfc_dan_absen", response_model=dict, tags=["umum"])
async def cek_kartu_nfc_dan_absen(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await cek_kartu_nfc_dan_absen(response, form_data)

@app.post("/mob_info/ganti_pin_kartu", response_model=dict, tags=["umum"])
async def ganti_pin_kartu(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await ganti_pin_kartu(response, form_data)

@app.post("/mob_info/inquiry_mau_nabung", response_model=dict, tags=["umum"])
async def inquiry_mau_nabung(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await inquiry_mau_nabung(response, form_data)

@app.post("/mob_info/payment_mau_nabung", response_model=dict, tags=["umum"])
async def payment_mau_nabung(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await payment_mau_nabung(response, form_data)

@app.post("/mob_info/inquiry_donasi", response_model=dict, tags=["umum"])
async def inquiry_donasi(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await inquiry_donasi(response, form_data)

@app.post("/mob_info/payment_donasi", response_model=dict, tags=["umum"])
async def payment_donasi(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await payment_donasi(response, form_data)

# ============================================

@app.post("/mob_info/cek_data_wahana", response_model=dict, tags=["wisata"])
async def cek_data_wahana(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await cek_data_wahana(response, form_data)

@app.post("/mob_info/cek_detail_wahana", response_model=dict, tags=["wisata"])
async def cek_detail_wahana(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await cek_detail_wahana(response, form_data)

@app.post("/mob_info/inquiry_beli_tiket", response_model=dict, tags=["wisata"])
async def inquiry_beli_tiket(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await inquiry_beli_tiket(response, form_data)

@app.post("/mob_info/payment_beli_tiket", response_model=dict, tags=["wisata"])
async def payment_beli_tiket(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await payment_beli_tiket(response, form_data)

# ============================================

@app.post("/mob_info/inquiry_bayar_miniatm", response_model=dict, tags=["bayar kiosk"])
async def inquiry_bayar_miniatm(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await inquiry_bayar_miniatm(response, form_data)

@app.post("/mob_info/payment_bayar_miniatm", response_model=dict, tags=["bayar kiosk"])
async def payment_bayar_miniatm(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await payment_bayar_miniatm(response, form_data)

@app.post("/mob_info/inquiry_bayar_qris", response_model=dict, tags=["bayar kiosk"])
async def inquiry_bayar_qris(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await inquiry_bayar_qris(response, form_data)

@app.post("/mob_info/payment_bayar_qris", response_model=dict, tags=["bayar kiosk"])
async def payment_bayar_qris(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await payment_bayar_qris(response, form_data)

@app.post("/mob_info/update_status_online", response_model=dict, tags=["bayar kiosk"])
async def update_status_online(response: Response, form_data: OAuth2PasswordRequestForm = Depends()):
    return await update_status_online(response, form_data)

@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()