from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from route.auth import router_auth

from route.membership.member import router_member
from route.membership.edc import router_edc
from route.membership.kasir import router_kasir

app = FastAPI(openapi_url="/api_membership/openapi.json", docs_url="/api_membership/swgr")
app.mount("/api_membership/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-membership',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}

app.include_router(router_auth,prefix="/api_default/auth",tags=["auth"],responses={404: {"description": "Not found"}})

app.include_router(router_member,prefix="/api_membership/member",tags=["member"],responses={404: {"description": "Not found"}})
app.include_router(router_edc,prefix="/api_membership/edc",tags=["edc"],responses={404: {"description": "Not found"}})
app.include_router(router_kasir,prefix="/api_membership/kasir",tags=["kasir"],responses={404: {"description": "Not found"}})

@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()