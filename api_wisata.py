from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from config import config
from route.auth import router_auth

from route.wisata.destinasi import router_destinasi
from route.wisata.pengunjung import router_pengunjung
from route.wisata.wahana import router_wahana
from route.wisata.tiket import router_tiket
from route.wisata.gateway import router_gateway
from route.wisata.kasir import router_kasir
from route.wisata.miniatm import router_miniatm
from route.wisata.combo import router_combo_wisata

app = FastAPI(openapi_url="/api_wisata/openapi.json", docs_url="/api_wisata/swgr")
app.mount("/api_wisata/static", StaticFiles(directory="static"), name="static")

if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'pysolusi-wisata',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}

app.include_router(router_auth,prefix="/api_default/auth",tags=["auth"],responses={404: {"description": "Not found"}})

app.include_router(router_destinasi,prefix="/api_wisata/destinasi",tags=["destinasi"],responses={404: {"description": "Not found"}})
app.include_router(router_wahana,prefix="/api_wisata/wahana",tags=["wahana"],responses={404: {"description": "Not found"}})
app.include_router(router_pengunjung,prefix="/api_wisata/pengunjung",tags=["pengunjung"],responses={404: {"description": "Not found"}})
app.include_router(router_tiket,prefix="/api_wisata/tiket",tags=["tiket"],responses={404: {"description": "Not found"}})
app.include_router(router_kasir,prefix="/api_wisata/kasir",tags=["kasir"],responses={404: {"description": "Not found"}})
app.include_router(router_miniatm,prefix="/api_wisata/miniatm",tags=["miniatm"],responses={404: {"description": "Not found"}})
app.include_router(router_gateway,prefix="/api_wisata/gateway",tags=["gateway"],responses={404: {"description": "Not found"}})
app.include_router(router_combo_wisata,prefix="/api_wisata/combo_wisata",tags=["combo wisata"],responses={404: {"description": "Not found"}})

@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()